include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa Drell-Yan mumu gamma in Upsilon->DiMuon mass region"
evgenConfig.keywords = ["SM","Drell-Yan", "2muon", "photon", "LO" ]
evgenConfig.contact  = [ "william.dale.heidorn@cern.ch" ]
evgenConfig.nEventsPerJob = 10000

genSeq.Sherpa_i.RunCard = """
(processes){
  Process 93 93 -> 22 13 -13
  End process;
}(processes)

(selector){
  PTNLO 22 20   E_CMS
  PT 13 3   E_CMS
  PT -13 3   E_CMS
  PseudoRapidityNLO 22 -2.7 2.7
  PseudoRapidityNLO 13 -2.8 2.8
  PseudoRapidityNLO -13 -2.8 2.8
  PT2NLO 13 -13 25   E_CMS
  Mass            13 -13 7. 13.
  Mass            13 22 1. E_CMS
  Mass            -13 22 1. E_CMS
}(selector)
"""

genSeq.Sherpa_i.NCores = 24
genSeq.Sherpa_i.CleanupGeneratedFiles = 1

