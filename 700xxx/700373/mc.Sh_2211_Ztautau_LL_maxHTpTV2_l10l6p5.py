evgenConfig.description = "Sherpa Z/gamma* -> tau tau + 0,1,2j@NLO + 3,4,5j@LO with di-leptonic tau decays and dilepton filter taking input from existing unfiltered input file."
evgenConfig.keywords = ["SM", "Z", "2tau", "2lepton", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "matthew.gignac@cern.ch", "chris.g@cern.ch" ]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 4

if runArgs.trfSubstepName == 'generate' :
   print "ERROR: These JO require an input file.  Please use the --afterburn option"
 
if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Sherpa"]
 
   ## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below
   if hasattr(testSeq, "FixHepMC"):
      fixSeq.FixHepMC.LoopsByBarcode = False
 
   ## Disable TestHepMC for the time being, cf.  
   ## https://its.cern.ch/jira/browse/ATLMCPROD-1862
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   # at least two e or mu pT>7 GeV, |eta|<2.6
   include("GeneratorFilters/MultiLeptonFilter.py")
   MultiLeptonFilter = filtSeq.MultiLeptonFilter
   MultiLeptonFilter.Ptcut = 6500.
   MultiLeptonFilter.Etacut = 2.8
   MultiLeptonFilter.NLeptons = 2

   # at least one e or mu pT>13 GeV, |eta|<2.6
   include("GeneratorFilters/LeptonFilter.py")
   LeptonFilter = filtSeq.LeptonFilter
   LeptonFilter.Ptcut = 10000.
   LeptonFilter.Etacut = 2.6

   postSeq.CountHepMC.CorrectRunNumber = True

