evgenConfig.description = "Sherpa H+c, gc->Hc, H->gamgam"
evgenConfig.keywords    = [ "SM", "Higgs", "diphoton" ]
evgenConfig.contact     = [ 'anna.ivina@cern.ch' ]
evgenConfig.inputconfcheck = "gc_Hc_Htodiphotons"
evgenConfig.nEventsPerJob = 10000

include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

# Necessary for HEFT
genSeq.Sherpa_i.Parameters += ["EW_SCHEME=3"]

genSeq.Sherpa_i.RunCard="""
(run){

	% Required for top loop diagrams
    MODEL HEFT;

	%scales, tags for scale variations
	FSF:=1.; RSF:=1.; QSF:=1.;
	SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

	MASSIVE[4] 1;
	MASS[4] 1.42;
	MASS[25] 125.;
	YUKAWA[4] 1.27;
	

    % Turn on Higgs decays
    HARD_DECAYS 1;
    STABLE[25] 0;



    HDH_BR_WEIGHTS=0;

    % Turn on 100% H->yy
    WIDTH[25] 0.00407;
    HDH_WIDTH[25,22,22] 0.00407;
    HDH_STATUS[25,22,22] 2;

    %ME generator settings
    ME_SIGNAL_GENERATOR Comix Amegic OpenLoops;





}(run)
(processes){

  # c g -> h c
  Process 4 93 -> 4 25;
  Order (*,0);
  End process;

  # g c -> h c
  Process 93 4 -> 4 25;
  Order (*,0);
  End process;

  # cbar g  -> h cbar
  Process -4 93 -> -4 25;
  Order (*,0);
  End process;

  # g cbar -> h cbar
  Process 93 -4 -> -4 25;
  Order (*,0);
  End process;


}(processes)

(selector){
   NJetFinder 1 10 0 0.4 -1 4.7 10
}(selector)

"""


