# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 22.11.2021,  13:41
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.31507496E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.42163340E+03  # scale for input parameters
    1    3.77262807E+01  # M_1
    2    3.19041629E+02  # M_2
    3    4.22755225E+03  # M_3
   11    2.46394202E+03  # A_t
   12   -1.44583705E+03  # A_b
   13   -1.50097419E+03  # A_tau
   23   -1.76379109E+03  # mu
   25    1.25020376E+01  # tan(beta)
   26    2.79064963E+03  # m_A, pole mass
   31    5.37282944E+02  # M_L11
   32    5.37282944E+02  # M_L22
   33    1.86814331E+03  # M_L33
   34    2.90335357E+02  # M_E11
   35    2.90335357E+02  # M_E22
   36    5.16256095E+02  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.85957824E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.81561788E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.13188102E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.42163340E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.42163340E+03  # (SUSY scale)
  1  1     8.41115062E-06   # Y_u(Q)^DRbar
  2  2     4.27286451E-03   # Y_c(Q)^DRbar
  3  3     1.01613455E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.42163340E+03  # (SUSY scale)
  1  1     2.11308252E-04   # Y_d(Q)^DRbar
  2  2     4.01485679E-03   # Y_s(Q)^DRbar
  3  3     2.09551572E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.42163340E+03  # (SUSY scale)
  1  1     3.68748352E-05   # Y_e(Q)^DRbar
  2  2     7.62454641E-03   # Y_mu(Q)^DRbar
  3  3     1.28232328E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.42163340E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     2.46394198E+03   # A_t(Q)^DRbar
Block Ad Q=  4.42163340E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.44583681E+03   # A_b(Q)^DRbar
Block Ae Q=  4.42163340E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.50097417E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  4.42163340E+03  # soft SUSY breaking masses at Q
   1    3.77262807E+01  # M_1
   2    3.19041629E+02  # M_2
   3    4.22755225E+03  # M_3
  21    4.58304071E+06  # M^2_(H,d)
  22   -2.68313856E+06  # M^2_(H,u)
  31    5.37282944E+02  # M_(L,11)
  32    5.37282944E+02  # M_(L,22)
  33    1.86814331E+03  # M_(L,33)
  34    2.90335357E+02  # M_(E,11)
  35    2.90335357E+02  # M_(E,22)
  36    5.16256095E+02  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.85957824E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.81561788E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.13188102E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22437818E+02  # h0
        35     2.79057566E+03  # H0
        36     2.79064963E+03  # A0
        37     2.79592342E+03  # H+
   1000001     1.01078379E+04  # ~d_L
   2000001     1.00830409E+04  # ~d_R
   1000002     1.01074796E+04  # ~u_L
   2000002     1.00851267E+04  # ~u_R
   1000003     1.01078382E+04  # ~s_L
   2000003     1.00830409E+04  # ~s_R
   1000004     1.01074798E+04  # ~c_L
   2000004     1.00851270E+04  # ~c_R
   1000005     2.34846475E+03  # ~b_1
   2000005     4.00385028E+03  # ~b_2
   1000006     4.00408229E+03  # ~t_1
   2000006     4.88272730E+03  # ~t_2
   1000011     5.31387734E+02  # ~e_L-
   2000011     3.35577047E+02  # ~e_R-
   1000012     5.25221609E+02  # ~nu_eL
   1000013     5.31411983E+02  # ~mu_L-
   2000013     3.35513155E+02  # ~mu_R-
   1000014     5.25214165E+02  # ~nu_muL
   1000015     5.39494964E+02  # ~tau_1-
   2000015     1.86848239E+03  # ~tau_2-
   1000016     1.86628825E+03  # ~nu_tauL
   1000021     4.71990824E+03  # ~g
   1000022     3.75355502E+01  # ~chi_10
   1000023     3.47753434E+02  # ~chi_20
   1000025     1.78858450E+03  # ~chi_30
   1000035     1.78860314E+03  # ~chi_40
   1000024     3.47938413E+02  # ~chi_1+
   1000037     1.78969662E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -7.61139300E-02   # alpha
Block Hmix Q=  4.42163340E+03  # Higgs mixing parameters
   1   -1.76379109E+03  # mu
   2    1.25020376E+01  # tan[beta](Q)
   3    2.42859601E+02  # v(Q)
   4    7.78772536E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.99192509E-01   # Re[R_st(1,1)]
   1  2     4.01787209E-02   # Re[R_st(1,2)]
   2  1    -4.01787209E-02   # Re[R_st(2,1)]
   2  2    -9.99192509E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -4.67927193E-03   # Re[R_sb(1,1)]
   1  2     9.99989052E-01   # Re[R_sb(1,2)]
   2  1    -9.99989052E-01   # Re[R_sb(2,1)]
   2  2    -4.67927193E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.14934655E-02   # Re[R_sta(1,1)]
   1  2     9.99933948E-01   # Re[R_sta(1,2)]
   2  1    -9.99933948E-01   # Re[R_sta(2,1)]
   2  2    -1.14934655E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.99696330E-01   # Re[N(1,1)]
   1  2    -9.10545726E-04   # Re[N(1,2)]
   1  3     2.45829076E-02   # Re[N(1,3)]
   1  4    -1.44890636E-03   # Re[N(1,4)]
   2  1    -1.90751735E-04   # Re[N(2,1)]
   2  2    -9.98971988E-01   # Re[N(2,2)]
   2  3    -4.50539880E-02   # Re[N(2,3)]
   2  4    -5.00690642E-03   # Re[N(2,4)]
   3  1    -1.63689155E-02   # Re[N(3,1)]
   3  2     3.53893673E-02   # Re[N(3,2)]
   3  3    -7.06029376E-01   # Re[N(3,3)]
   3  4    -7.07108317E-01   # Re[N(3,4)]
   4  1     1.84192830E-02   # Re[N(4,1)]
   4  2    -2.83148533E-02   # Re[N(4,2)]
   4  3     7.06320281E-01   # Re[N(4,3)]
   4  4    -7.07086035E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.97975381E-01   # Re[U(1,1)]
   1  2    -6.36014099E-02   # Re[U(1,2)]
   2  1     6.36014099E-02   # Re[U(2,1)]
   2  2    -9.97975381E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.99974723E-01   # Re[V(1,1)]
   1  2     7.11006394E-03   # Re[V(1,2)]
   2  1     7.11006394E-03   # Re[V(2,1)]
   2  2     9.99974723E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     1.64322579E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000011     2.94627220E+00   # ~e^-_L
#    BR                NDA      ID1      ID2
     2.24933568E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.59009550E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     5.16056864E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     1.64289705E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
DECAY   1000013     2.94828505E+00   # ~mu^-_L
#    BR                NDA      ID1      ID2
     2.24790563E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.58880170E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     5.15798721E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     5.30540764E-04    2     2000013        25   # BR(~mu^-_L -> ~mu^-_R h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     2.68430045E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.99489469E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.70669904E-04    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     3.39861063E-04    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     2.57036842E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     9.15203728E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.97550153E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     5.93653148E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     8.70221988E-03    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     8.38692823E-03    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     2.83374316E+00   # ~nu_e
#    BR                NDA      ID1      ID2
     2.29526601E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.56730551E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     5.13742848E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
DECAY   1000014     2.83360748E+00   # ~nu_mu
#    BR                NDA      ID1      ID2
     2.29534272E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.56727844E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     5.13737884E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
DECAY   1000016     2.57287931E+01   # ~nu_tau
#    BR                NDA      ID1      ID2
     9.06789357E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.96815575E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.94992952E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     1.52659136E-04    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
     1.73526552E-02    2     1000015        24   # BR(~nu_tau -> ~tau^-_1 W^+)
DECAY   2000001     4.90939150E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.14590867E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.88534347E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.22379190E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.24220492E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     7.11630790E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.42051623E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     5.42629404E-04    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.83835075E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.90945199E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.14589495E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.88522165E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.22385702E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.24218467E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     7.11623470E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.00851353E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     1.42050136E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     5.48163791E-04    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.83826922E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.02200436E+00   # ~b_1
#    BR                NDA      ID1      ID2
     6.48585971E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.99470359E-03    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     8.90998228E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     8.91894158E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     5.90118768E-03    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.64228899E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.07568966E+02   # ~b_2
#    BR                NDA      ID1      ID2
     5.14909005E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.61057242E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.05495506E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.05074642E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.20120446E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.89566519E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.24836969E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.80131826E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     5.08043625E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.43023224E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.55672425E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.22361046E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.28853772E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     7.11722985E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.42620443E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     7.83799148E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.08050497E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.43017228E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.55659498E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.22367515E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.28851390E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     7.11715575E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.42618985E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     7.83790984E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.07983354E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.35141637E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.59424254E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.44715783E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.42720361E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     3.20825953E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.15952384E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     2.48677359E-03    2     1000005        24   # BR(~t_1 -> ~b_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     2.86853850E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.71962783E+02   # ~t_2
#    BR                NDA      ID1      ID2
     6.30931713E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     3.20276395E-04    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.16964662E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.17682161E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     6.44465918E-04    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.36208835E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.58097470E-02    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     1.28280481E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.64412519E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     8.71259426E-05   # chi^+_1
#    BR                NDA      ID1      ID2
     2.81256247E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.77849923E-04    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
     3.58332940E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     3.58306551E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.87925775E-03    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.25507669E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     2.11098188E-03    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     2.11094462E-03    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     1.90516514E-02    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     9.43460089E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.82331834E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.82124818E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.91350592E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.34278085E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.22623179E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     9.67759322E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     8.45268379E-05   # chi^0_2
#    BR                NDA      ID1      ID2
     2.08806854E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     5.27052482E-02    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
     1.97708571E-04    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.79386413E-01    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     1.79345958E-01    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     1.11785699E-03    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     3.78371154E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     1.25540558E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     1.85036809E-04    2     1000011       -11   # BR(chi^0_3 -> ~e^-_L e^+)
     1.85036809E-04    2    -1000011        11   # BR(chi^0_3 -> ~e^+_L e^-)
     1.27516471E-04    2     2000013       -13   # BR(chi^0_3 -> ~mu^-_R mu^+)
     1.27516471E-04    2    -2000013        13   # BR(chi^0_3 -> ~mu^+_R mu^-)
     2.19190364E-04    2     1000013       -13   # BR(chi^0_3 -> ~mu^-_L mu^+)
     2.19190364E-04    2    -1000013        13   # BR(chi^0_3 -> ~mu^+_L mu^-)
     9.63066487E-03    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     9.63066487E-03    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     5.09937638E-04    2     1000012       -12   # BR(chi^0_3 -> ~nu_e nu_bar_e)
     5.09937638E-04    2    -1000012        12   # BR(chi^0_3 -> ~nu^*_e nu_e)
     5.09940366E-04    2     1000014       -14   # BR(chi^0_3 -> ~nu_mu nu_bar_mu)
     5.09940366E-04    2    -1000014        14   # BR(chi^0_3 -> ~nu^*_mu nu_mu)
     2.83256408E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.83256408E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     5.23321809E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.10739031E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     3.90723806E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.80614870E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.17061056E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     6.32590166E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     9.81910398E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     9.81910398E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.42906461E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.32929205E-04    2     2000013       -13   # BR(chi^0_4 -> ~mu^-_R mu^+)
     1.32929205E-04    2    -2000013        13   # BR(chi^0_4 -> ~mu^+_R mu^-)
     1.08176137E-04    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     1.08176137E-04    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     8.49237243E-03    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     8.49237243E-03    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     3.34817386E-04    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     3.34817386E-04    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     3.34819177E-04    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     3.34819177E-04    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     2.49573619E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.49573619E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.63272238E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.52237250E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     4.65053445E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.15369219E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.96007790E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     6.50533714E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.13643105E-02    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.13643105E-02    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.01293083E+02   # ~g
#    BR                NDA      ID1      ID2
     5.39280007E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     5.39280007E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     3.90440631E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     3.90440631E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     5.42191838E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     5.42191838E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     2.58657134E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     5.83054293E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     5.83426352E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     5.91872099E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     5.91872099E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.26996044E-03   # Gamma(h0)
     2.53134482E-03   2        22        22   # BR(h0 -> photon photon)
     1.40112991E-03   2        22        23   # BR(h0 -> photon Z)
     2.36355435E-02   2        23        23   # BR(h0 -> Z Z)
     2.05585791E-01   2       -24        24   # BR(h0 -> W W)
     8.10491114E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.26473037E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.34182919E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.75124410E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.49855771E-07   2        -2         2   # BR(h0 -> Up up)
     2.90819443E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.11661661E-07   2        -1         1   # BR(h0 -> Down down)
     2.21223337E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.88320354E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     4.26167529E-04   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     1.95510570E+01   # Gamma(HH)
     5.16883631E-10   2        22        22   # BR(HH -> photon photon)
     1.05155993E-07   2        22        23   # BR(HH -> photon Z)
     1.11619393E-05   2        23        23   # BR(HH -> Z Z)
     3.71132362E-06   2       -24        24   # BR(HH -> W W)
     3.00781807E-06   2        21        21   # BR(HH -> gluon gluon)
     3.35832736E-09   2       -11        11   # BR(HH -> Electron electron)
     1.49525084E-04   2       -13        13   # BR(HH -> Muon muon)
     4.39848013E-02   2       -15        15   # BR(HH -> Tau tau)
     2.01561359E-12   2        -2         2   # BR(HH -> Up up)
     3.90997751E-07   2        -4         4   # BR(HH -> Charm charm)
     3.08989131E-02   2        -6         6   # BR(HH -> Top top)
     2.63401599E-07   2        -1         1   # BR(HH -> Down down)
     9.52746612E-05   2        -3         3   # BR(HH -> Strange strange)
     2.91760151E-01   2        -5         5   # BR(HH -> Bottom bottom)
     4.65537855E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.86141819E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.86141819E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.93479007E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.35712175E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     3.32292385E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     2.59302220E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     2.32688788E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     8.35152068E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.01848153E-01   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     4.19330211E-05   2        25        25   # BR(HH -> h0 h0)
     1.78774418E-06   2  -1000011   1000011   # BR(HH -> Selectron1 selectron1)
     4.42974792E-12   2  -2000011   1000011   # BR(HH -> Selectron2 selectron1)
     4.42974792E-12   2  -1000011   2000011   # BR(HH -> Selectron1 selectron2)
     2.60647684E-06   2  -2000011   2000011   # BR(HH -> Selectron2 selectron2)
     1.75765941E-06   2  -1000013   1000013   # BR(HH -> Smuon1 smuon1)
     1.89277210E-07   2  -2000013   1000013   # BR(HH -> Smuon2 smuon1)
     1.89277210E-07   2  -1000013   2000013   # BR(HH -> Smuon1 smuon2)
     2.63760776E-06   2  -2000013   2000013   # BR(HH -> Smuon2 smuon2)
     1.02218697E-06   2  -1000015   1000015   # BR(HH -> Stau1 stau1)
     3.84211804E-03   2  -2000015   1000015   # BR(HH -> Stau2 stau1)
     3.84211804E-03   2  -1000015   2000015   # BR(HH -> Stau1 stau2)
DECAY        36     1.93577550E+01   # Gamma(A0)
     2.06776524E-08   2        22        22   # BR(A0 -> photon photon)
     6.14910743E-08   2        22        23   # BR(A0 -> photon Z)
     3.13995668E-05   2        21        21   # BR(A0 -> gluon gluon)
     3.30708572E-09   2       -11        11   # BR(A0 -> Electron electron)
     1.47243650E-04   2       -13        13   # BR(A0 -> Muon muon)
     4.33133457E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.89490400E-12   2        -2         2   # BR(A0 -> Up up)
     3.67566815E-07   2        -4         4   # BR(A0 -> Charm charm)
     2.95705953E-02   2        -6         6   # BR(A0 -> Top top)
     2.59374635E-07   2        -1         1   # BR(A0 -> Down down)
     9.38183105E-05   2        -3         3   # BR(A0 -> Strange strange)
     2.87295045E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     4.80394728E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.88008319E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.88008319E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.98850413E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.36826313E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.61605932E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     3.35976107E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     2.40086495E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.02765293E-01   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     8.44548670E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.74627434E-05   2        23        25   # BR(A0 -> Z h0)
     8.36175386E-38   2        25        25   # BR(A0 -> h0 h0)
     4.52703153E-12   2  -2000011   1000011   # BR(A0 -> Selectron2 selectron1)
     4.52703153E-12   2  -1000011   2000011   # BR(A0 -> Selectron1 selectron2)
     1.93544779E-07   2  -2000013   1000013   # BR(A0 -> Smuon2 smuon1)
     1.93544779E-07   2  -1000013   2000013   # BR(A0 -> Smuon1 smuon2)
     3.88153157E-03   2  -2000015   1000015   # BR(A0 -> Stau2 stau1)
     3.88153157E-03   2  -1000015   2000015   # BR(A0 -> Stau1 stau2)
DECAY        37     2.02823833E+01   # Gamma(Hp)
     3.94246612E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     1.68552740E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     4.76762944E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     2.59866756E-07   2        -1         2   # BR(Hp -> Down up)
     4.38303717E-06   2        -3         2   # BR(Hp -> Strange up)
     3.25790785E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.10130218E-08   2        -1         4   # BR(Hp -> Down charm)
     9.40586461E-05   2        -3         4   # BR(Hp -> Strange charm)
     4.56224261E-04   2        -5         4   # BR(Hp -> Bottom charm)
     2.06421477E-06   2        -1         6   # BR(Hp -> Down top)
     4.51451201E-05   2        -3         6   # BR(Hp -> Strange top)
     3.39231852E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.62326995E-03   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     5.74441966E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     9.25558289E-11   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.82094305E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.81360858E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.81308135E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.67739028E-05   2        24        25   # BR(Hp -> W h0)
     2.93614310E-10   2        24        35   # BR(Hp -> W HH)
     2.73854036E-10   2        24        36   # BR(Hp -> W A0)
     7.58498469E-12   2  -1000011   1000012   # BR(Hp -> Selectron1 snu_e1)
     9.84555764E-06   2  -2000011   1000012   # BR(Hp -> Selectron2 snu_e1)
     3.24249489E-07   2  -1000013   1000014   # BR(Hp -> Smuon1 snu_mu1)
     9.88611616E-06   2  -2000013   1000014   # BR(Hp -> Smuon2 snu_mu1)
     7.45027728E-03   2  -1000015   1000016   # BR(Hp -> Stau1 snu_tau1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.09537878E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.56391406E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.56300944E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00057877E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    5.81914513E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    6.39791401E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.09537878E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.56391406E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.56300944E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99986287E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.37128877E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99986287E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.37128877E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.27367695E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.81076916E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.08324825E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.37128877E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99986287E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.27589796E-04   # BR(b -> s gamma)
    2    1.58944925E-06   # BR(b -> s mu+ mu-)
    3    3.52560880E-05   # BR(b -> s nu nu)
    4    2.51205656E-15   # BR(Bd -> e+ e-)
    5    1.07312206E-10   # BR(Bd -> mu+ mu-)
    6    2.24649387E-08   # BR(Bd -> tau+ tau-)
    7    8.49454629E-14   # BR(Bs -> e+ e-)
    8    3.62886749E-09   # BR(Bs -> mu+ mu-)
    9    7.69723521E-07   # BR(Bs -> tau+ tau-)
   10    9.66753664E-05   # BR(B_u -> tau nu)
   11    9.98619523E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41914566E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93623185E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15697799E-03   # epsilon_K
   17    2.28165480E-15   # Delta(M_K)
   18    2.48013351E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29083251E-11   # BR(K^+ -> pi^+ nu nu)
   20   -9.95588401E-15   # Delta(g-2)_electron/2
   21   -4.25788260E-10   # Delta(g-2)_muon/2
   22   -1.55988701E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    3.13431168E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.96331295E-01   # C7
     0305 4322   00   2    -1.53417202E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.04849978E-01   # C8
     0305 6321   00   2    -1.91912788E-04   # C8'
 03051111 4133   00   0     1.62974013E+00   # C9 e+e-
 03051111 4133   00   2     1.62983425E+00   # C9 e+e-
 03051111 4233   00   2     3.76630317E-05   # C9' e+e-
 03051111 4137   00   0    -4.45243224E+00   # C10 e+e-
 03051111 4137   00   2    -4.45102176E+00   # C10 e+e-
 03051111 4237   00   2    -2.77441378E-04   # C10' e+e-
 03051313 4133   00   0     1.62974013E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62983410E+00   # C9 mu+mu-
 03051313 4233   00   2     3.76630167E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.45243224E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.45102190E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.77441393E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50506589E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     5.99306144E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50506589E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     5.99306153E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50506427E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     5.99333897E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85829624E-07   # C7
     0305 4422   00   2    -2.28233018E-06   # C7
     0305 4322   00   2    -2.37008608E-08   # C7'
     0305 6421   00   0     3.30488589E-07   # C8
     0305 6421   00   2     2.58787971E-06   # C8
     0305 6321   00   2     5.87091166E-08   # C8'
 03051111 4133   00   2    -5.67835812E-07   # C9 e+e-
 03051111 4233   00   2     7.63138477E-07   # C9' e+e-
 03051111 4137   00   2     1.57947502E-06   # C10 e+e-
 03051111 4237   00   2    -5.62212118E-06   # C10' e+e-
 03051313 4133   00   2    -5.67844191E-07   # C9 mu+mu-
 03051313 4233   00   2     7.63138292E-07   # C9' mu+mu-
 03051313 4137   00   2     1.57948503E-06   # C10 mu+mu-
 03051313 4237   00   2    -5.62212153E-06   # C10' mu+mu-
 03051212 4137   00   2     2.72446160E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     1.21449666E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     2.72450424E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     1.21449665E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.75990670E-09   # C11 nu_3 nu_3
 03051616 4237   00   2     1.21454110E-06   # C11' nu_3 nu_3
