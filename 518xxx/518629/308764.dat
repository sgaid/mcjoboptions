# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 18.08.2022,  13:53
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.20068209E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.56882464E+03  # scale for input parameters
    1    2.40531455E+02  # M_1
    2   -1.55346173E+03  # M_2
    3    3.50154654E+03  # M_3
   11   -1.91827889E+03  # A_t
   12    3.31179564E+03  # A_b
   13    1.97766308E+03  # A_tau
   23   -3.02249006E+03  # mu
   25    1.14832803E+01  # tan(beta)
   26    9.11616371E+02  # m_A, pole mass
   31    1.33575246E+03  # M_L11
   32    1.33575246E+03  # M_L22
   33    1.31960055E+03  # M_L33
   34    3.37020918E+02  # M_E11
   35    3.37020918E+02  # M_E22
   36    3.44938348E+02  # M_E33
   41    1.81658836E+03  # M_Q11
   42    1.81658836E+03  # M_Q22
   43    3.08588945E+03  # M_Q33
   44    2.72670388E+03  # M_U11
   45    2.72670388E+03  # M_U22
   46    1.98438099E+03  # M_U33
   47    1.92201552E+03  # M_D11
   48    1.92201552E+03  # M_D22
   49    1.73082749E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.56882464E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.56882464E+03  # (SUSY scale)
  1  1     8.41610341E-06   # Y_u(Q)^DRbar
  2  2     4.27538053E-03   # Y_c(Q)^DRbar
  3  3     1.01673289E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.56882464E+03  # (SUSY scale)
  1  1     1.94203600E-04   # Y_d(Q)^DRbar
  2  2     3.68986840E-03   # Y_s(Q)^DRbar
  3  3     1.92589117E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.56882464E+03  # (SUSY scale)
  1  1     3.38899483E-05   # Y_e(Q)^DRbar
  2  2     7.00736647E-03   # Y_mu(Q)^DRbar
  3  3     1.17852376E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.56882464E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -1.91827961E+03   # A_t(Q)^DRbar
Block Ad Q=  2.56882464E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     3.31179580E+03   # A_b(Q)^DRbar
Block Ae Q=  2.56882464E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.97766287E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.56882464E+03  # soft SUSY breaking masses at Q
   1    2.40531455E+02  # M_1
   2   -1.55346173E+03  # M_2
   3    3.50154654E+03  # M_3
  21   -8.36092554E+06  # M^2_(H,d)
  22   -9.04952100E+06  # M^2_(H,u)
  31    1.33575246E+03  # M_(L,11)
  32    1.33575246E+03  # M_(L,22)
  33    1.31960055E+03  # M_(L,33)
  34    3.37020918E+02  # M_(E,11)
  35    3.37020918E+02  # M_(E,22)
  36    3.44938348E+02  # M_(E,33)
  41    1.81658836E+03  # M_(Q,11)
  42    1.81658836E+03  # M_(Q,22)
  43    3.08588945E+03  # M_(Q,33)
  44    2.72670388E+03  # M_(U,11)
  45    2.72670388E+03  # M_(U,22)
  46    1.98438099E+03  # M_(U,33)
  47    1.92201552E+03  # M_(D,11)
  48    1.92201552E+03  # M_(D,22)
  49    1.73082749E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.17471787E+02  # h0
        35     9.11751941E+02  # H0
        36     9.11616371E+02  # A0
        37     9.21353874E+02  # H+
   1000001     1.93883548E+03  # ~d_L
   2000001     2.02691742E+03  # ~d_R
   1000002     1.93720197E+03  # ~u_L
   2000002     2.82487517E+03  # ~u_R
   1000003     1.93883195E+03  # ~s_L
   2000003     2.02691917E+03  # ~s_R
   1000004     1.93720151E+03  # ~c_L
   2000004     2.82487453E+03  # ~c_R
   1000005     1.83538817E+03  # ~b_1
   2000005     3.16684580E+03  # ~b_2
   1000006     2.08096914E+03  # ~t_1
   2000006     3.17105137E+03  # ~t_2
   1000011     1.35379117E+03  # ~e_L-
   2000011     3.60259647E+02  # ~e_R-
   1000012     1.35113496E+03  # ~nu_eL
   1000013     1.35379132E+03  # ~mu_L-
   2000013     3.60233771E+02  # ~mu_R-
   1000014     1.35113191E+03  # ~nu_muL
   1000015     3.57961656E+02  # ~tau_1-
   2000015     1.33737617E+03  # ~tau_2-
   1000016     1.33363954E+03  # ~nu_tauL
   1000021     3.47988146E+03  # ~g
   1000022     2.36412737E+02  # ~chi_10
   1000023     1.57418232E+03  # ~chi_20
   1000025     3.00595103E+03  # ~chi_30
   1000035     3.00802412E+03  # ~chi_40
   1000024     1.57439757E+03  # ~chi_1+
   1000037     3.00849081E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -8.57646035E-02   # alpha
Block Hmix Q=  2.56882464E+03  # Higgs mixing parameters
   1   -3.02249006E+03  # mu
   2    1.14832803E+01  # tan[beta](Q)
   3    2.43457583E+02  # v(Q)
   4    8.31044408E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     4.24131438E-02   # Re[R_st(1,1)]
   1  2     9.99100158E-01   # Re[R_st(1,2)]
   2  1    -9.99100158E-01   # Re[R_st(2,1)]
   2  2     4.24131438E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -1.53670127E-02   # Re[R_sb(1,1)]
   1  2     9.99881920E-01   # Re[R_sb(1,2)]
   2  1    -9.99881920E-01   # Re[R_sb(2,1)]
   2  2    -1.53670127E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -4.07088720E-02   # Re[R_sta(1,1)]
   1  2     9.99171050E-01   # Re[R_sta(1,2)]
   2  1    -9.99171050E-01   # Re[R_sta(2,1)]
   2  2    -4.07088720E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.99889959E-01   # Re[N(1,1)]
   1  2    -4.58394770E-05   # Re[N(1,2)]
   1  3    -1.48344733E-02   # Re[N(1,3)]
   1  4     7.67702536E-05   # Re[N(1,4)]
   2  1     5.83599717E-04   # Re[N(2,1)]
   2  2     9.99130491E-01   # Re[N(2,2)]
   2  3     3.61415443E-02   # Re[N(2,3)]
   2  4    -2.07776385E-02   # Re[N(2,4)]
   3  1     1.04331168E-02   # Re[N(3,1)]
   3  2    -1.08720468E-02   # Re[N(3,2)]
   3  3     7.06917853E-01   # Re[N(3,3)]
   3  4     7.07135135E-01   # Re[N(3,4)]
   4  1     1.05299135E-02   # Re[N(4,1)]
   4  2    -4.02499512E-02   # Re[N(4,2)]
   4  3     7.06215886E-01   # Re[N(4,3)]
   4  4    -7.06773079E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.98697754E-01   # Re[U(1,1)]
   1  2    -5.10176077E-02   # Re[U(1,2)]
   2  1    -5.10176077E-02   # Re[U(2,1)]
   2  2     9.98697754E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.99569076E-01   # Re[V(1,1)]
   1  2     2.93540681E-02   # Re[V(1,2)]
   2  1     2.93540681E-02   # Re[V(2,1)]
   2  2    -9.99569076E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.86684045E-01   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000011     1.59781158E+00   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.99999956E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.86514299E-01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000013     1.60083899E+00   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.98109079E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.88134834E-03    2     2000013        25   # BR(~mu^-_L -> ~mu^-_R h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.70703428E-01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
DECAY   2000015     3.76745280E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     4.20208297E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.80993155E-01    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     2.28899680E-02    2     1000015        36   # BR(~tau^-_2 -> ~tau^-_1 A^0)
     2.53091636E-01    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
     2.28169437E-02    2     1000015        35   # BR(~tau^-_2 -> ~tau^-_1 H^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.59482907E+00   # ~nu_e
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.59483933E+00   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.99991020E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     3.83473242E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     4.09820842E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     5.49783531E-01    2     1000015        24   # BR(~nu_tau -> ~tau^-_1 W^+)
     4.03956273E-02    2     1000015        37   # BR(~nu_tau -> ~tau^-_1 H^+)
DECAY   1000001     3.22670741E+00   # ~d_L
#    BR                NDA      ID1      ID2
     8.14141695E-02    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     3.06545821E-01    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     6.12040010E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
DECAY   2000001     1.10079132E+00   # ~d_R
#    BR                NDA      ID1      ID2
     9.99999944E-01    2     1000022         1   # BR(~d_R -> chi^0_1 d)
DECAY   1000003     3.22663974E+00   # ~s_L
#    BR                NDA      ID1      ID2
     8.14157559E-02    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     3.06547446E-01    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     6.12036798E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
DECAY   2000003     1.10079278E+00   # ~s_R
#    BR                NDA      ID1      ID2
     9.99999638E-01    2     1000022         3   # BR(~s_R -> chi^0_1 s)
DECAY   1000005     9.92886343E-01   # ~b_1
#    BR                NDA      ID1      ID2
     9.98761303E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     5.16832375E-04    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     7.21864600E-04    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     5.28775241E+01   # ~b_2
#    BR                NDA      ID1      ID2
     8.25805167E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.49078097E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.16627056E-04    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.14378905E-04    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.93355185E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.33676098E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     3.66038632E-01    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     1.40234972E-02    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     9.67213036E-03    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     1.57795593E-02    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
     9.68774271E-03    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   1000002     3.20822631E+00   # ~u_L
#    BR                NDA      ID1      ID2
     8.17258001E-02    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.06171791E-01    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.12102409E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
DECAY   2000002     6.21900292E+00   # ~u_R
#    BR                NDA      ID1      ID2
     9.99999836E-01    2     1000022         2   # BR(~u_R -> chi^0_1 u)
DECAY   1000004     3.20821144E+00   # ~c_L
#    BR                NDA      ID1      ID2
     8.17260699E-02    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.06170203E-01    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.12103727E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
DECAY   2000004     6.21900126E+00   # ~c_R
#    BR                NDA      ID1      ID2
     9.99999370E-01    2     1000022         4   # BR(~c_R -> chi^0_1 c)
DECAY   1000006     4.45760560E+00   # ~t_1
#    BR                NDA      ID1      ID2
     9.99864604E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     5.53270715E+01   # ~t_2
#    BR                NDA      ID1      ID2
     8.04937538E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.43022010E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.86831980E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.23726205E-04    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.70246250E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.80919336E-02    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     6.42679686E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.81929001E-01    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     8.99904135E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     1.80368967E-01    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     3.00727614E+00   # chi^+_1
#    BR                NDA      ID1      ID2
     1.56095044E-01    2    -1000011        12   # BR(chi^+_1 -> ~e^+_L nu_e)
     1.56094847E-01    2    -1000013        14   # BR(chi^+_1 -> ~mu^+_L nu_mu)
     5.11596868E-03    2    -1000015        16   # BR(chi^+_1 -> ~tau^+_1 nu_tau)
     1.77747898E-01    2    -2000015        16   # BR(chi^+_1 -> ~tau^+_2 nu_tau)
     1.59865261E-01    2     1000012       -11   # BR(chi^+_1 -> ~nu_e e^+)
     1.59869414E-01    2     1000014       -13   # BR(chi^+_1 -> ~nu_mu mu^+)
     1.83722359E-01    2     1000016       -15   # BR(chi^+_1 -> ~nu_tau tau^+)
     2.80233324E-04    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
     1.13111786E-03    2          37   1000022   # BR(chi^+_1 -> H^+ chi^0_1)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     6.30512455E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     3.47671761E-04    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     3.47671722E-04    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     6.23061402E-03    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     4.57667655E-04    2    -2000015        16   # BR(chi^+_2 -> ~tau^+_2 nu_tau)
     1.15327074E-04    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     1.30133609E-04    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     4.35962016E-03    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     1.86050782E-04    2     1000002        -1   # BR(chi^+_2 -> ~u_L d_bar)
     1.92650813E-04    2     1000004        -3   # BR(chi^+_2 -> ~c_L s_bar)
     3.99969309E-01    2     1000006        -5   # BR(chi^+_2 -> ~t_1 b_bar)
     5.60600797E-04    2    -1000001         2   # BR(chi^+_2 -> ~d^*_L u)
     5.69289257E-04    2    -1000003         4   # BR(chi^+_2 -> ~s^*_L c)
     2.12586967E-02    2    -1000005         6   # BR(chi^+_2 -> ~b^*_1 t)
     3.16409477E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     9.73827234E-02    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.51325040E-02    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     7.80194701E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     9.67720006E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     5.76969698E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     1.09563169E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     5.75285673E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     6.58850038E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.66550618E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     3.24270798E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     3.00260842E+00   # chi^0_2
#    BR                NDA      ID1      ID2
     7.81533103E-02    2     1000011       -11   # BR(chi^0_2 -> ~e^-_L e^+)
     7.81533103E-02    2    -1000011        11   # BR(chi^0_2 -> ~e^+_L e^-)
     7.81531835E-02    2     1000013       -13   # BR(chi^0_2 -> ~mu^-_L mu^+)
     7.81531835E-02    2    -1000013        13   # BR(chi^0_2 -> ~mu^+_L mu^-)
     2.56651109E-03    2     1000015       -15   # BR(chi^0_2 -> ~tau^-_1 tau^+)
     2.56651109E-03    2    -1000015        15   # BR(chi^0_2 -> ~tau^+_1 tau^-)
     8.89984151E-02    2     2000015       -15   # BR(chi^0_2 -> ~tau^-_2 tau^+)
     8.89984151E-02    2    -2000015        15   # BR(chi^0_2 -> ~tau^+_2 tau^-)
     7.98036674E-02    2     1000012       -12   # BR(chi^0_2 -> ~nu_e nu_bar_e)
     7.98036674E-02    2    -1000012        12   # BR(chi^0_2 -> ~nu^*_e nu_e)
     7.98056835E-02    2     1000014       -14   # BR(chi^0_2 -> ~nu_mu nu_bar_mu)
     7.98056835E-02    2    -1000014        14   # BR(chi^0_2 -> ~nu^*_mu nu_mu)
     9.17071017E-02    2     1000016       -16   # BR(chi^0_2 -> ~nu_tau nu_bar_tau)
     9.17071017E-02    2    -1000016        16   # BR(chi^0_2 -> ~nu^*_tau nu_tau)
     1.18304763E-04    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     3.37253218E-04    2     1000022        36   # BR(chi^0_2 -> chi^0_1 A^0)
     2.91868152E-04    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
     7.68639389E-04    2     1000022        35   # BR(chi^0_2 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     6.61090950E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     3.04009835E-03    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     3.04009835E-03    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     2.03699414E-03    2     2000015       -15   # BR(chi^0_3 -> ~tau^-_2 tau^+)
     2.03699414E-03    2    -2000015        15   # BR(chi^0_3 -> ~tau^+_2 tau^-)
     1.90850812E-01    2     1000006        -6   # BR(chi^0_3 -> ~t_1 t_bar)
     1.90850812E-01    2    -1000006         6   # BR(chi^0_3 -> ~t^*_1 t)
     9.87729308E-03    2     1000005        -5   # BR(chi^0_3 -> ~b_1 b_bar)
     9.87729308E-03    2    -1000005         5   # BR(chi^0_3 -> ~b^*_1 b)
     9.22765965E-02    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     9.22765965E-02    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     5.42560933E-02    2          37  -1000024   # BR(chi^0_3 -> H^+ chi^-_1)
     5.42560933E-02    2         -37   1000024   # BR(chi^0_3 -> H^- chi^+_1)
     1.52508520E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     8.69969328E-02    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     8.03456448E-03    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     3.04687036E-03    2     1000023        36   # BR(chi^0_3 -> chi^0_2 A^0)
     1.36563796E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     6.95387647E-02    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     1.61024269E-02    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
     7.13078006E-02    2     1000023        35   # BR(chi^0_3 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.28180889E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.81385108E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     3.98092901E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     3.98092901E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     6.47585938E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     3.05793614E-03    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     3.05793614E-03    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     2.18401006E-03    2     2000015       -15   # BR(chi^0_4 -> ~tau^-_2 tau^+)
     2.18401006E-03    2    -2000015        15   # BR(chi^0_4 -> ~tau^+_2 tau^-)
     1.37082185E-04    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     1.37082185E-04    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     1.37082498E-04    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     1.37082498E-04    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     1.38870965E-04    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     1.38870965E-04    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     1.54686901E-04    2     1000002        -2   # BR(chi^0_4 -> ~u_L u_bar)
     1.54686901E-04    2    -1000002         2   # BR(chi^0_4 -> ~u^*_L u)
     1.59096723E-04    2     1000004        -4   # BR(chi^0_4 -> ~c_L c_bar)
     1.59096723E-04    2    -1000004         4   # BR(chi^0_4 -> ~c^*_L c)
     1.96759602E-01    2     1000006        -6   # BR(chi^0_4 -> ~t_1 t_bar)
     1.96759602E-01    2    -1000006         6   # BR(chi^0_4 -> ~t^*_1 t)
     1.85961317E-04    2     1000001        -1   # BR(chi^0_4 -> ~d_L d_bar)
     1.85961317E-04    2    -1000001         1   # BR(chi^0_4 -> ~d^*_L d)
     1.89174868E-04    2     1000003        -3   # BR(chi^0_4 -> ~s_L s_bar)
     1.89174868E-04    2    -1000003         3   # BR(chi^0_4 -> ~s^*_L s)
     1.00551828E-02    2     1000005        -5   # BR(chi^0_4 -> ~b_1 b_bar)
     1.00551828E-02    2    -1000005         5   # BR(chi^0_4 -> ~b^*_1 b)
     9.55877871E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     9.55877871E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     7.57045300E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     7.57045300E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     1.52513702E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     6.46599240E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.16004578E-02    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     4.41610216E-03    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     1.96595435E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     9.97572878E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.15972223E-02    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     5.13704944E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.40778498E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     2.53012168E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     3.31067213E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     3.31067213E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     4.11080211E+02   # ~g
#    BR                NDA      ID1      ID2
     5.96656331E-02    2     1000002        -2   # BR(~g -> ~u_L u_bar)
     5.96656331E-02    2    -1000002         2   # BR(~g -> ~u^*_L u)
     1.45703300E-02    2     2000002        -2   # BR(~g -> ~u_R u_bar)
     1.45703300E-02    2    -2000002         2   # BR(~g -> ~u^*_R u)
     5.96656483E-02    2     1000004        -4   # BR(~g -> ~c_L c_bar)
     5.96656483E-02    2    -1000004         4   # BR(~g -> ~c^*_L c)
     1.45703335E-02    2     2000004        -4   # BR(~g -> ~c_R c_bar)
     1.45703335E-02    2    -2000004         4   # BR(~g -> ~c^*_R c)
     5.08033772E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     5.08033772E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     3.17523161E-03    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     3.17523161E-03    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     5.95752561E-02    2     1000001        -1   # BR(~g -> ~d_L d_bar)
     5.95752561E-02    2    -1000001         1   # BR(~g -> ~d^*_L d)
     5.46952934E-02    2     2000001        -1   # BR(~g -> ~d_R d_bar)
     5.46952934E-02    2    -2000001         1   # BR(~g -> ~d^*_R d)
     5.95754513E-02    2     1000003        -3   # BR(~g -> ~s_L s_bar)
     5.95754513E-02    2    -1000003         3   # BR(~g -> ~s^*_L s)
     5.46951963E-02    2     2000003        -3   # BR(~g -> ~s_R s_bar)
     5.46951963E-02    2    -2000003         3   # BR(~g -> ~s^*_R s)
     6.52830373E-02    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     6.52830373E-02    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     3.69677768E-03    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     3.69677768E-03    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.28564828E-03   # Gamma(h0)
     2.14466227E-03   2        22        22   # BR(h0 -> photon photon)
     8.35616786E-04   2        22        23   # BR(h0 -> photon Z)
     1.18813984E-02   2        23        23   # BR(h0 -> Z Z)
     1.13848295E-01   2       -24        24   # BR(h0 -> W W)
     7.13543726E-02   2        21        21   # BR(h0 -> gluon gluon)
     6.06446232E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.69752921E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.77556216E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.63716817E-07   2        -2         2   # BR(h0 -> Up up)
     3.17648718E-02   2        -4         4   # BR(h0 -> Charm charm)
     7.20373652E-07   2        -1         1   # BR(h0 -> Down down)
     2.60541098E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.89883978E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     3.05730702E+00   # Gamma(HH)
     3.45797282E-07   2        22        22   # BR(HH -> photon photon)
     5.83172544E-08   2        22        23   # BR(HH -> photon Z)
     3.54481854E-04   2        23        23   # BR(HH -> Z Z)
     5.24862470E-04   2       -24        24   # BR(HH -> W W)
     7.74170648E-05   2        21        21   # BR(HH -> gluon gluon)
     7.54971070E-09   2       -11        11   # BR(HH -> Electron electron)
     3.36025886E-04   2       -13        13   # BR(HH -> Muon muon)
     9.70691003E-02   2       -15        15   # BR(HH -> Tau tau)
     8.83068041E-12   2        -2         2   # BR(HH -> Up up)
     1.71223184E-06   2        -4         4   # BR(HH -> Charm charm)
     1.00916108E-01   2        -6         6   # BR(HH -> Top top)
     9.66516035E-07   2        -1         1   # BR(HH -> Down down)
     3.49555908E-04   2        -3         3   # BR(HH -> Strange strange)
     7.95460466E-01   2        -5         5   # BR(HH -> Bottom bottom)
     8.57088030E-05   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     3.79235863E-03   2        25        25   # BR(HH -> h0 h0)
     2.93981781E-05   2  -1000011   1000011   # BR(HH -> Selectron1 selectron1)
     2.92318392E-05   2  -1000013   1000013   # BR(HH -> Smuon1 smuon1)
     9.72194693E-04   2  -1000015   1000015   # BR(HH -> Stau1 stau1)
DECAY        36     3.03146532E+00   # Gamma(A0)
     6.14259304E-07   2        22        22   # BR(A0 -> photon photon)
     1.74314458E-07   2        22        23   # BR(A0 -> photon Z)
     2.90591764E-04   2        21        21   # BR(A0 -> gluon gluon)
     7.48998916E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.33367621E-04   2       -13        13   # BR(A0 -> Muon muon)
     9.63032742E-02   2       -15        15   # BR(A0 -> Tau tau)
     8.41369015E-12   2        -2         2   # BR(A0 -> Up up)
     1.63117428E-06   2        -4         4   # BR(A0 -> Charm charm)
     1.12860659E-01   2        -6         6   # BR(A0 -> Top top)
     9.58839158E-07   2        -1         1   # BR(A0 -> Down down)
     3.46779433E-04   2        -3         3   # BR(A0 -> Strange strange)
     7.89203167E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.19987077E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     5.38787921E-04   2        23        25   # BR(A0 -> Z h0)
     1.72058338E-35   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.68687203E+00   # Gamma(Hp)
     8.17515854E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.49513552E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     9.88616437E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     9.74699396E-07   2        -1         2   # BR(Hp -> Down up)
     1.62063728E-05   2        -3         2   # BR(Hp -> Strange up)
     8.87626932E-06   2        -5         2   # BR(Hp -> Bottom up)
     1.08945040E-07   2        -1         4   # BR(Hp -> Down charm)
     3.52622049E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.24299309E-03   2        -5         4   # BR(Hp -> Bottom charm)
     6.56997465E-06   2        -1         6   # BR(Hp -> Down top)
     1.43726500E-04   2        -3         6   # BR(Hp -> Strange top)
     8.98383602E-01   2        -5         6   # BR(Hp -> Bottom top)
     6.33069358E-04   2        24        25   # BR(Hp -> W h0)
     4.10651173E-08   2        24        35   # BR(Hp -> W HH)
     4.40479976E-08   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.74908925E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.31890818E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.31865726E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00019028E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    7.39319421E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    7.58347166E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.74908925E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.31890818E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.31865726E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99998791E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.20866375E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99998791E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.20866375E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26323857E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    6.42068769E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.15323478E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.20866375E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99998791E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.62267775E-04   # BR(b -> s gamma)
    2    1.58766629E-06   # BR(b -> s mu+ mu-)
    3    3.52629559E-05   # BR(b -> s nu nu)
    4    2.48799388E-15   # BR(Bd -> e+ e-)
    5    1.06284291E-10   # BR(Bd -> mu+ mu-)
    6    2.22505450E-08   # BR(Bd -> tau+ tau-)
    7    8.38560837E-14   # BR(Bs -> e+ e-)
    8    3.58232973E-09   # BR(Bs -> mu+ mu-)
    9    7.59873909E-07   # BR(Bs -> tau+ tau-)
   10    9.57355474E-05   # BR(B_u -> tau nu)
   11    9.88911552E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42518081E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93798705E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15981324E-03   # epsilon_K
   17    2.28168520E-15   # Delta(M_K)
   18    2.48087934E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29199765E-11   # BR(K^+ -> pi^+ nu nu)
   20   -1.49172497E-15   # Delta(g-2)_electron/2
   21   -6.37686620E-11   # Delta(g-2)_muon/2
   22   -1.95478632E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    2.27758838E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.30839217E-01   # C7
     0305 4322   00   2    -8.38870882E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.42751957E-01   # C8
     0305 6321   00   2    -9.41727986E-04   # C8'
 03051111 4133   00   0     1.61074834E+00   # C9 e+e-
 03051111 4133   00   2     1.61089448E+00   # C9 e+e-
 03051111 4233   00   2    -1.66069185E-05   # C9' e+e-
 03051111 4137   00   0    -4.43344044E+00   # C10 e+e-
 03051111 4137   00   2    -4.43278311E+00   # C10 e+e-
 03051111 4237   00   2     1.25899136E-04   # C10' e+e-
 03051313 4133   00   0     1.61074834E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61089430E+00   # C9 mu+mu-
 03051313 4233   00   2    -1.66070691E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.43344044E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43278328E+00   # C10 mu+mu-
 03051313 4237   00   2     1.25899284E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50520844E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -2.73230652E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50520844E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -2.73230327E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50520847E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -2.73138542E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85809486E-07   # C7
     0305 4422   00   2     5.19496404E-07   # C7
     0305 4322   00   2    -1.17460524E-06   # C7'
     0305 6421   00   0     3.30471340E-07   # C8
     0305 6421   00   2     2.21144087E-06   # C8
     0305 6321   00   2    -7.20726514E-07   # C8'
 03051111 4133   00   2    -1.09052405E-06   # C9 e+e-
 03051111 4233   00   2     7.92569986E-08   # C9' e+e-
 03051111 4137   00   2     8.42227417E-06   # C10 e+e-
 03051111 4237   00   2    -6.00134456E-07   # C10' e+e-
 03051313 4133   00   2    -1.09052210E-06   # C9 mu+mu-
 03051313 4233   00   2     7.92569908E-08   # C9' mu+mu-
 03051313 4137   00   2     8.42227567E-06   # C10 mu+mu-
 03051313 4237   00   2    -6.00134484E-07   # C10' mu+mu-
 03051212 4137   00   2    -1.80672112E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     1.30219592E-07   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.80672112E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     1.30219592E-07   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.80673754E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     1.30219573E-07   # C11' nu_3 nu_3
