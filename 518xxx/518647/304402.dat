# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 18.08.2022,  11:50
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.19705145E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.01990318E+03  # scale for input parameters
    1   -3.31741839E+02  # M_1
    2   -1.16302074E+03  # M_2
    3    3.97409830E+03  # M_3
   11   -6.95253852E+03  # A_t
   12   -3.27162116E+03  # A_b
   13    1.64032219E+03  # A_tau
   23    1.67261666E+02  # mu
   25    4.01142890E+00  # tan(beta)
   26    1.24608630E+03  # m_A, pole mass
   31    1.21124197E+03  # M_L11
   32    1.21124197E+03  # M_L22
   33    1.25516898E+03  # M_L33
   34    1.68556772E+03  # M_E11
   35    1.68556772E+03  # M_E22
   36    1.91739807E+03  # M_E33
   41    1.75787663E+03  # M_Q11
   42    1.75787663E+03  # M_Q22
   43    1.18800168E+03  # M_Q33
   44    3.10479956E+03  # M_U11
   45    3.10479956E+03  # M_U22
   46    3.58679682E+03  # M_U33
   47    1.32255618E+03  # M_D11
   48    1.32255618E+03  # M_D22
   49    4.69643656E+02  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.01990318E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.01990318E+03  # (SUSY scale)
  1  1     8.64096651E-06   # Y_u(Q)^DRbar
  2  2     4.38961099E-03   # Y_c(Q)^DRbar
  3  3     1.04389816E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.01990318E+03  # (SUSY scale)
  1  1     6.96532954E-05   # Y_d(Q)^DRbar
  2  2     1.32341261E-03   # Y_s(Q)^DRbar
  3  3     6.90742429E-02   # Y_b(Q)^DRbar
Block Ye Q=  2.01990318E+03  # (SUSY scale)
  1  1     1.21550094E-05   # Y_e(Q)^DRbar
  2  2     2.51327044E-03   # Y_mu(Q)^DRbar
  3  3     4.22690742E-02   # Y_tau(Q)^DRbar
Block Au Q=  2.01990318E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -6.95253864E+03   # A_t(Q)^DRbar
Block Ad Q=  2.01990318E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -3.27162113E+03   # A_b(Q)^DRbar
Block Ae Q=  2.01990318E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.64032218E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.01990318E+03  # soft SUSY breaking masses at Q
   1   -3.31741839E+02  # M_1
   2   -1.16302074E+03  # M_2
   3    3.97409830E+03  # M_3
  21    1.39956039E+06  # M^2_(H,d)
  22   -1.95435661E+05  # M^2_(H,u)
  31    1.21124197E+03  # M_(L,11)
  32    1.21124197E+03  # M_(L,22)
  33    1.25516898E+03  # M_(L,33)
  34    1.68556772E+03  # M_(E,11)
  35    1.68556772E+03  # M_(E,22)
  36    1.91739807E+03  # M_(E,33)
  41    1.75787663E+03  # M_(Q,11)
  42    1.75787663E+03  # M_(Q,22)
  43    1.18800168E+03  # M_(Q,33)
  44    3.10479956E+03  # M_(U,11)
  45    3.10479956E+03  # M_(U,22)
  46    3.58679682E+03  # M_(U,33)
  47    1.32255618E+03  # M_(D,11)
  48    1.32255618E+03  # M_(D,22)
  49    4.69643656E+02  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.14163787E+02  # h0
        35     1.24667619E+03  # H0
        36     1.24608630E+03  # A0
        37     1.24815481E+03  # H+
   1000001     1.78661481E+03  # ~d_L
   2000001     1.31027007E+03  # ~d_R
   1000002     1.78525500E+03  # ~u_L
   2000002     3.16274189E+03  # ~u_R
   1000003     1.78661455E+03  # ~s_L
   2000003     1.31027000E+03  # ~s_R
   1000004     1.78525485E+03  # ~c_L
   2000004     3.16274149E+03  # ~c_R
   1000005     2.38913207E+02  # ~b_1
   2000005     1.18175136E+03  # ~b_2
   1000006     1.14838945E+03  # ~t_1
   2000006     3.55280941E+03  # ~t_2
   1000011     1.22786128E+03  # ~e_L-
   2000011     1.68404642E+03  # ~e_R-
   1000012     1.22533576E+03  # ~nu_eL
   1000013     1.22786110E+03  # ~mu_L-
   2000013     1.68404612E+03  # ~mu_R-
   1000014     1.22533560E+03  # ~nu_muL
   1000015     1.27107597E+03  # ~tau_1-
   2000015     1.91628727E+03  # ~tau_2-
   1000016     1.26862887E+03  # ~nu_tauL
   1000021     3.87631486E+03  # ~g
   1000022     1.78417143E+02  # ~chi_10
   1000023     1.88647193E+02  # ~chi_20
   1000025     3.35476763E+02  # ~chi_30
   1000035     1.18311836E+03  # ~chi_40
   1000024     1.84755320E+02  # ~chi_1+
   1000037     1.18328782E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.36810819E-01   # alpha
Block Hmix Q=  2.01990318E+03  # Higgs mixing parameters
   1    1.67261666E+02  # mu
   2    4.01142890E+00  # tan[beta](Q)
   3    2.43673500E+02  # v(Q)
   4    1.55273107E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.95824648E-01   # Re[R_st(1,1)]
   1  2     9.12867527E-02   # Re[R_st(1,2)]
   2  1    -9.12867527E-02   # Re[R_st(2,1)]
   2  2     9.95824648E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     6.76165866E-03   # Re[R_sb(1,1)]
   1  2     9.99977140E-01   # Re[R_sb(1,2)]
   2  1    -9.99977140E-01   # Re[R_sb(2,1)]
   2  2     6.76165866E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -9.99999667E-01   # Re[R_sta(1,1)]
   1  2     8.16523480E-04   # Re[R_sta(1,2)]
   2  1    -8.16523480E-04   # Re[R_sta(2,1)]
   2  2    -9.99999667E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -1.39525983E-01   # Re[N(1,1)]
   1  2     3.90133394E-02   # Re[N(1,2)]
   1  3    -7.14193802E-01   # Re[N(1,3)]
   1  4    -6.84790240E-01   # Re[N(1,4)]
   2  1    -7.17360414E-02   # Re[N(2,1)]
   2  2     5.01611658E-02   # Re[N(2,2)]
   2  3     6.98113989E-01   # Re[N(2,3)]
   2  4    -7.10615688E-01   # Re[N(2,4)]
   3  1    -9.87610877E-01   # Re[N(3,1)]
   3  2    -1.25360471E-02   # Re[N(3,2)]
   3  3     5.02126300E-02   # Re[N(3,3)]
   3  4     1.48142819E-01   # Re[N(3,4)]
   4  1     3.34602466E-03   # Re[N(4,1)]
   4  2    -9.97900127E-01   # Re[N(4,2)]
   4  3     6.53939032E-03   # Re[N(4,3)]
   4  4    -6.43535250E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1     9.15344473E-03   # Re[U(1,1)]
   1  2     9.99958106E-01   # Re[U(1,2)]
   2  1    -9.99958106E-01   # Re[U(2,1)]
   2  2     9.15344473E-03   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.09705191E-02   # Re[V(1,1)]
   1  2     9.95853586E-01   # Re[V(1,2)]
   2  1     9.95853586E-01   # Re[V(2,1)]
   2  2    -9.09705191E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   1000011     1.44062672E+00   # ~e^-_L
#    BR                NDA      ID1      ID2
     4.53645671E-03    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     5.01223266E-04    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     9.37180766E-01    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     1.90570815E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.00493578E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     3.81239784E-02    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000011     7.81482818E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     2.06084224E-02    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     5.43307233E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.73955396E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000013     1.44077313E+00   # ~mu^-_L
#    BR                NDA      ID1      ID2
     4.58836160E-03    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     5.50937999E-04    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     9.37085521E-01    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     1.90549390E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.00432463E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     3.81198077E-02    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     7.81524012E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     2.06208211E-02    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     5.44565992E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     9.73903924E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
DECAY   1000015     1.76256155E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.63833293E-02    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.24032517E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     8.01588915E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     5.62108759E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     5.15454924E-04    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.12898173E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     9.19231656E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     2.37615587E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     8.82141739E-03    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     9.60022939E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     7.27043974E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
DECAY   1000012     1.48987342E+00   # ~nu_e
#    BR                NDA      ID1      ID2
     4.47175062E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.70148972E-02    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     8.21785044E-01    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     1.65911631E-02    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     5.72223209E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     3.26690689E-02    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
DECAY   1000014     1.49001972E+00   # ~nu_mu
#    BR                NDA      ID1      ID2
     4.47131091E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.70122408E-02    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     8.21704213E-01    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     1.65894126E-02    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     5.73155046E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     3.26655199E-02    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
DECAY   1000016     1.80608029E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     3.83028111E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.31477437E-02    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     7.09510852E-01    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     5.24301817E-02    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     7.29701295E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     1.03638282E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
DECAY   2000001     6.40348595E-01   # ~d_R
#    BR                NDA      ID1      ID2
     2.14221007E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     5.63759835E-03    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.72939675E-01    2     1000025         1   # BR(~d_R -> chi^0_3 d)
DECAY   1000001     7.68565581E+00   # ~d_L
#    BR                NDA      ID1      ID2
     4.08974282E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     3.96284223E-03    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.54047029E-02    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     3.21592824E-01    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.67808759E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     6.44782079E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
DECAY   2000003     6.40436024E-01   # ~s_R
#    BR                NDA      ID1      ID2
     2.14540190E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     5.67009697E-03    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.72806974E-01    2     1000025         3   # BR(~s_R -> chi^0_3 s)
DECAY   1000003     7.68637054E+00   # ~s_L
#    BR                NDA      ID1      ID2
     4.09339308E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     3.96632870E-03    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     2.54023569E-02    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     3.21562702E-01    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     2.54317895E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     6.44720887E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
DECAY   1000005     4.35729904E-03   # ~b_1
#    BR                NDA      ID1      ID2
     6.10882672E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     3.89117328E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
DECAY   2000005     2.34319878E+01   # ~b_2
#    BR                NDA      ID1      ID2
     3.15880456E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     3.07974850E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     5.02772805E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     9.86388652E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     9.47132303E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.39793410E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   1000002     7.75670258E+00   # ~u_L
#    BR                NDA      ID1      ID2
     1.99292603E-04    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.38455869E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     3.34639690E-02    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     3.16889436E-01    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.64098783E-02    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     6.31652865E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
DECAY   2000002     6.90764403E+00   # ~u_R
#    BR                NDA      ID1      ID2
     1.97786852E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     5.22438176E-03    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     9.74988464E-01    2     1000025         2   # BR(~u_R -> chi^0_3 u)
DECAY   1000004     7.75741437E+00   # ~c_L
#    BR                NDA      ID1      ID2
     2.39849770E-04    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.42807183E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     3.34628037E-02    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     3.16858408E-01    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.64162154E-02    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     6.31594651E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
DECAY   2000004     6.91004515E+00   # ~c_R
#    BR                NDA      ID1      ID2
     1.98536845E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     5.31060647E-03    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     9.74652550E-01    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     1.72817180E-04    2     1000024         3   # BR(~c_R -> chi^+_1 s)
DECAY   1000006     2.24459971E+01   # ~t_1
#    BR                NDA      ID1      ID2
     4.61769759E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     4.96640132E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     3.39411814E-02    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     5.72237163E-03    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.79230699E-03    2     1000005        24   # BR(~t_1 -> ~b_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.34249172E-04    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     5.71053772E+02   # ~t_2
#    BR                NDA      ID1      ID2
     6.38425980E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     6.82472160E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.50980855E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.33347177E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.13733811E-04    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.12534237E-01    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     1.79020829E-02    2     2000005        37   # BR(~t_2 -> ~b_2 H^+)
     1.58124682E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     9.10885180E-03    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     2.13381279E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     8.21643587E-03    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     3.68529609E-08   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.67801279E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.02877363E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.22606008E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.22436530E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     8.42788196E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     8.94629157E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     5.91166336E-03    2     1000006        -5   # BR(chi^+_2 -> ~t_1 b_bar)
     2.26847288E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.45261477E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.71311391E-02    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.42792746E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.43215180E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.71243713E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     6.66655491E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     4.75408654E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     3.91133259E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     4.01413601E-07   # chi^0_2
#    BR                NDA      ID1      ID2
     2.72226776E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.04107656E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     8.47342801E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.33483294E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.33349710E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     9.25317180E-03    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.01227336E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.00752546E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     1.92722983E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.78524788E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     1.01660539E-03    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     1.01660539E-03    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     6.08815495E-04    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     6.08815495E-04    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     3.38878226E-04    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     3.38878226E-04    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     3.37634547E-04    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     3.37634547E-04    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
     1.23084966E-04    3     1000024        15       -16   # BR(chi^0_2 -> chi^+_1 tau^- nu_bar_tau)
     1.23084966E-04    3    -1000024       -15        16   # BR(chi^0_2 -> chi^-_1 tau^+ nu_tau)
DECAY   1000025     6.92209953E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     9.59113161E-02    2     1000005        -5   # BR(chi^0_3 -> ~b_1 b_bar)
     9.59113161E-02    2    -1000005         5   # BR(chi^0_3 -> ~b^*_1 b)
     1.78647297E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     1.78647297E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     3.82494484E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.54888225E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     6.66644401E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.91055664E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.00739352E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.16593976E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.16593976E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.24674388E-01    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     8.82384285E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     4.27948693E-03    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     7.91797851E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.39742343E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.04995813E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.14386732E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     2.83367576E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.74704248E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     1.23112592E-04    3     1000025        12       -12   # BR(chi^0_4 -> chi^0_3 nu_e nu_bar_e)
     7.27878723E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     7.27878723E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     7.96057333E+02   # ~g
#    BR                NDA      ID1      ID2
     4.47370046E-02    2     1000002        -2   # BR(~g -> ~u_L u_bar)
     4.47370046E-02    2    -1000002         2   # BR(~g -> ~u^*_L u)
     8.05315289E-03    2     2000002        -2   # BR(~g -> ~u_R u_bar)
     8.05315289E-03    2    -2000002         2   # BR(~g -> ~u^*_R u)
     4.47370053E-02    2     1000004        -4   # BR(~g -> ~c_L c_bar)
     4.47370053E-02    2    -1000004         4   # BR(~g -> ~c^*_L c)
     8.05315071E-03    2     2000004        -4   # BR(~g -> ~c_R c_bar)
     8.05315071E-03    2    -2000004         4   # BR(~g -> ~c^*_R c)
     5.88806897E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     5.88806897E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.73437986E-03    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     1.73437986E-03    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     5.65394635E-02    2     2000001        -1   # BR(~g -> ~d_R d_bar)
     5.65394635E-02    2    -2000001         1   # BR(~g -> ~d^*_R d)
     4.47003035E-02    2     1000001        -1   # BR(~g -> ~d_L d_bar)
     4.47003035E-02    2    -1000001         1   # BR(~g -> ~d^*_L d)
     5.65394650E-02    2     2000003        -3   # BR(~g -> ~s_R s_bar)
     5.65394650E-02    2    -2000003         3   # BR(~g -> ~s^*_R s)
     4.47003105E-02    2     1000003        -3   # BR(~g -> ~s_L s_bar)
     4.47003105E-02    2    -1000003         3   # BR(~g -> ~s^*_L s)
     7.15184098E-02    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     7.15184098E-02    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     5.92952558E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     5.92952558E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.68033087E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     2.57687889E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.38731672E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     2.38731672E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     2.92122855E-03   # Gamma(h0)
     2.15120618E-03   2        22        22   # BR(h0 -> photon photon)
     6.30596543E-04   2        22        23   # BR(h0 -> photon Z)
     8.16347880E-03   2        23        23   # BR(h0 -> Z Z)
     8.25385137E-02   2       -24        24   # BR(h0 -> W W)
     7.24745647E-02   2        21        21   # BR(h0 -> gluon gluon)
     6.33132923E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.81621106E-04   2       -13        13   # BR(h0 -> Muon muon)
     8.11821517E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.78794753E-07   2        -2         2   # BR(h0 -> Up up)
     3.46883041E-02   2        -4         4   # BR(h0 -> Charm charm)
     7.44895503E-07   2        -1         1   # BR(h0 -> Down down)
     2.69410963E-04   2        -3         3   # BR(h0 -> Strange strange)
     7.17619222E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     4.29004753E+00   # Gamma(HH)
     1.19143147E-06   2        22        22   # BR(HH -> photon photon)
     2.02229720E-07   2        22        23   # BR(HH -> photon Z)
     8.76243802E-04   2        23        23   # BR(HH -> Z Z)
     8.99763634E-04   2       -24        24   # BR(HH -> W W)
     4.97849882E-04   2        21        21   # BR(HH -> gluon gluon)
     8.35003171E-10   2       -11        11   # BR(HH -> Electron electron)
     3.71682350E-05   2       -13        13   # BR(HH -> Muon muon)
     1.07339160E-02   2       -15        15   # BR(HH -> Tau tau)
     4.68949621E-11   2        -2         2   # BR(HH -> Up up)
     9.09494334E-06   2        -4         4   # BR(HH -> Charm charm)
     6.23425450E-01   2        -6         6   # BR(HH -> Top top)
     6.79727917E-08   2        -1         1   # BR(HH -> Down down)
     2.45860860E-05   2        -3         3   # BR(HH -> Strange strange)
     6.59055506E-02   2        -5         5   # BR(HH -> Bottom bottom)
     1.66567401E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.84566926E-02   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     5.64440617E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.71769644E-01   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     4.47267334E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     7.72603711E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     2.00507521E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     6.25057660E-03   2        25        25   # BR(HH -> h0 h0)
     1.52701027E-04   2  -1000005   1000005   # BR(HH -> Sbottom1 sbottom1)
DECAY        36     4.42353951E+00   # Gamma(A0)
     1.50095215E-06   2        22        22   # BR(A0 -> photon photon)
     3.43715063E-07   2        22        23   # BR(A0 -> photon Z)
     6.78691256E-04   2        21        21   # BR(A0 -> gluon gluon)
     7.97356240E-10   2       -11        11   # BR(A0 -> Electron electron)
     3.54924257E-05   2       -13        13   # BR(A0 -> Muon muon)
     1.02500227E-02   2       -15        15   # BR(A0 -> Tau tau)
     4.42081307E-11   2        -2         2   # BR(A0 -> Up up)
     8.57248877E-06   2        -4         4   # BR(A0 -> Charm charm)
     6.39088407E-01   2        -6         6   # BR(A0 -> Top top)
     6.49120575E-08   2        -1         1   # BR(A0 -> Down down)
     2.34790300E-05   2        -3         3   # BR(A0 -> Strange strange)
     6.29497477E-02   2        -5         5   # BR(A0 -> Bottom bottom)
     1.91524670E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     7.57165874E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.87189806E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     7.90573905E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.28308492E-02   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.65286123E-01   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     2.81153264E-05   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.16517563E-03   2        23        25   # BR(A0 -> Z h0)
     2.77396811E-36   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     4.53136091E+00   # Gamma(Hp)
     8.02396492E-10   2       -11        12   # BR(Hp -> Electron nu_e)
     3.43049557E-05   2       -13        14   # BR(Hp -> Muon nu_mu)
     9.70335979E-03   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.65940188E-08   2        -1         2   # BR(Hp -> Down up)
     9.44267674E-07   2        -3         2   # BR(Hp -> Strange up)
     6.05130064E-07   2        -5         2   # BR(Hp -> Bottom up)
     4.08839265E-07   2        -1         4   # BR(Hp -> Down charm)
     2.91575448E-05   2        -3         4   # BR(Hp -> Strange charm)
     8.47531780E-05   2        -5         4   # BR(Hp -> Bottom charm)
     4.30340306E-05   2        -1         6   # BR(Hp -> Down top)
     9.38313173E-04   2        -3         6   # BR(Hp -> Strange top)
     7.24041886E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.09955960E-03   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     3.80340133E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.62495778E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.14749851E-03   2        24        25   # BR(Hp -> W h0)
     2.12099655E-12   2        24        35   # BR(Hp -> W HH)
     1.13586103E-11   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.40700295E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.61508615E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.60915618E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00368514E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    5.84592289E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    6.21443718E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.40700295E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.61508615E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.60915618E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99943791E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    5.62093392E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99943791E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    5.62093392E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.23605424E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    5.51506652E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    2.57792947E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    5.62093392E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99943791E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.42551495E-04   # BR(b -> s gamma)
    2    1.59097440E-06   # BR(b -> s mu+ mu-)
    3    3.53162325E-05   # BR(b -> s nu nu)
    4    2.61348735E-15   # BR(Bd -> e+ e-)
    5    1.11645212E-10   # BR(Bd -> mu+ mu-)
    6    2.33718270E-08   # BR(Bd -> tau+ tau-)
    7    8.77928777E-14   # BR(Bs -> e+ e-)
    8    3.75050893E-09   # BR(Bs -> mu+ mu-)
    9    7.95515916E-07   # BR(Bs -> tau+ tau-)
   10    9.67424540E-05   # BR(B_u -> tau nu)
   11    9.99312512E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.44415380E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.94522855E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16732937E-03   # epsilon_K
   17    2.28176261E-15   # Delta(M_K)
   18    2.48639356E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.30280820E-11   # BR(K^+ -> pi^+ nu nu)
   20   -5.46187426E-16   # Delta(g-2)_electron/2
   21   -2.33511978E-11   # Delta(g-2)_muon/2
   22   -6.01177468E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -4.74505735E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.10664624E-01   # C7
     0305 4322   00   2    -4.34037365E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.24043249E-01   # C8
     0305 6321   00   2    -5.66365766E-04   # C8'
 03051111 4133   00   0     1.60228750E+00   # C9 e+e-
 03051111 4133   00   2     1.60273884E+00   # C9 e+e-
 03051111 4233   00   2     6.92038932E-06   # C9' e+e-
 03051111 4137   00   0    -4.42497961E+00   # C10 e+e-
 03051111 4137   00   2    -4.42941341E+00   # C10 e+e-
 03051111 4237   00   2    -5.29556126E-05   # C10' e+e-
 03051313 4133   00   0     1.60228750E+00   # C9 mu+mu-
 03051313 4133   00   2     1.60273882E+00   # C9 mu+mu-
 03051313 4233   00   2     6.92038838E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.42497961E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.42941343E+00   # C10 mu+mu-
 03051313 4237   00   2    -5.29556123E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50634660E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.15127811E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50634660E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.15127813E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50634672E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.15128372E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85825546E-07   # C7
     0305 4422   00   2    -2.35985551E-06   # C7
     0305 4322   00   2     1.03472740E-06   # C7'
     0305 6421   00   0     3.30485096E-07   # C8
     0305 6421   00   2    -7.14254699E-06   # C8
     0305 6321   00   2     4.94153769E-07   # C8'
 03051111 4133   00   2    -6.43284748E-06   # C9 e+e-
 03051111 4233   00   2     1.53025492E-07   # C9' e+e-
 03051111 4137   00   2     5.02568150E-05   # C10 e+e-
 03051111 4237   00   2    -1.17166438E-06   # C10' e+e-
 03051313 4133   00   2    -6.43284723E-06   # C9 mu+mu-
 03051313 4233   00   2     1.53025490E-07   # C9' mu+mu-
 03051313 4137   00   2     5.02568162E-05   # C10 mu+mu-
 03051313 4237   00   2    -1.17166439E-06   # C10' mu+mu-
 03051212 4137   00   2    -1.03390054E-05   # C11 nu_1 nu_1
 03051212 4237   00   2     2.54730779E-07   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.03390054E-05   # C11 nu_2 nu_2
 03051414 4237   00   2     2.54730779E-07   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.03367930E-05   # C11 nu_3 nu_3
 03051616 4237   00   2     2.54730907E-07   # C11' nu_3 nu_3
