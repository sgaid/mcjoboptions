# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 18.08.2022,  12:24
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.64402132E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    1.78112161E+03  # scale for input parameters
    1   -5.86569019E+02  # M_1
    2   -6.37081471E+02  # M_2
    3    2.78453669E+03  # M_3
   11   -6.42538120E+02  # A_t
   12    5.04763999E+02  # A_b
   13   -2.36745168E+03  # A_tau
   23    2.89489484E+03  # mu
   25    1.58056629E+01  # tan(beta)
   26    1.08325815E+03  # m_A, pole mass
   31    1.81708991E+03  # M_L11
   32    1.81708991E+03  # M_L22
   33    1.50765462E+03  # M_L33
   34    1.71370114E+03  # M_E11
   35    1.71370114E+03  # M_E22
   36    1.70126320E+03  # M_E33
   41    2.27852646E+03  # M_Q11
   42    2.27852646E+03  # M_Q22
   43    3.57540657E+03  # M_Q33
   44    2.72828903E+03  # M_U11
   45    2.72828903E+03  # M_U22
   46    7.67702718E+02  # M_U33
   47    3.09813917E+03  # M_D11
   48    3.09813917E+03  # M_D22
   49    2.58584164E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  1.78112161E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  1.78112161E+03  # (SUSY scale)
  1  1     8.40113627E-06   # Y_u(Q)^DRbar
  2  2     4.26777722E-03   # Y_c(Q)^DRbar
  3  3     1.01492474E+00   # Y_t(Q)^DRbar
Block Yd Q=  1.78112161E+03  # (SUSY scale)
  1  1     2.66827749E-04   # Y_d(Q)^DRbar
  2  2     5.06972722E-03   # Y_s(Q)^DRbar
  3  3     2.64609515E-01   # Y_b(Q)^DRbar
Block Ye Q=  1.78112161E+03  # (SUSY scale)
  1  1     4.65633933E-05   # Y_e(Q)^DRbar
  2  2     9.62783295E-03   # Y_mu(Q)^DRbar
  3  3     1.61924312E-01   # Y_tau(Q)^DRbar
Block Au Q=  1.78112161E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -6.42538160E+02   # A_t(Q)^DRbar
Block Ad Q=  1.78112161E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     5.04763969E+02   # A_b(Q)^DRbar
Block Ae Q=  1.78112161E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -2.36745163E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  1.78112161E+03  # soft SUSY breaking masses at Q
   1   -5.86569019E+02  # M_1
   2   -6.37081471E+02  # M_2
   3    2.78453669E+03  # M_3
  21   -7.28364817E+06  # M^2_(H,d)
  22   -8.45360911E+06  # M^2_(H,u)
  31    1.81708991E+03  # M_(L,11)
  32    1.81708991E+03  # M_(L,22)
  33    1.50765462E+03  # M_(L,33)
  34    1.71370114E+03  # M_(E,11)
  35    1.71370114E+03  # M_(E,22)
  36    1.70126320E+03  # M_(E,33)
  41    2.27852646E+03  # M_(Q,11)
  42    2.27852646E+03  # M_(Q,22)
  43    3.57540657E+03  # M_(Q,33)
  44    2.72828903E+03  # M_(U,11)
  45    2.72828903E+03  # M_(U,22)
  46    7.67702718E+02  # M_(U,33)
  47    3.09813917E+03  # M_(D,11)
  48    3.09813917E+03  # M_(D,22)
  49    2.58584164E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.14770734E+02  # h0
        35     1.08329953E+03  # H0
        36     1.08325815E+03  # A0
        37     1.08850704E+03  # H+
   1000001     2.34422596E+03  # ~d_L
   2000001     3.14550818E+03  # ~d_R
   1000002     2.34294362E+03  # ~u_L
   2000002     2.78305496E+03  # ~u_R
   1000003     2.34422578E+03  # ~s_L
   2000003     3.14550777E+03  # ~s_R
   1000004     2.34294369E+03  # ~c_L
   2000004     2.78305482E+03  # ~c_R
   1000005     2.64408717E+03  # ~b_1
   2000005     3.60904377E+03  # ~b_2
   1000006     8.78446229E+02  # ~t_1
   2000006     3.61136981E+03  # ~t_2
   1000011     1.82049900E+03  # ~e_L-
   2000011     1.71894714E+03  # ~e_R-
   1000012     1.81843666E+03  # ~nu_eL
   1000013     1.82051293E+03  # ~mu_L-
   2000013     1.71892219E+03  # ~mu_R-
   1000014     1.81843289E+03  # ~nu_muL
   1000015     1.50585305E+03  # ~tau_1-
   2000015     1.70665705E+03  # ~tau_2-
   1000016     1.50729207E+03  # ~nu_tauL
   1000021     2.88803277E+03  # ~g
   1000022     5.84954751E+02  # ~chi_10
   1000023     6.73046057E+02  # ~chi_20
   1000025     2.89211338E+03  # ~chi_30
   1000035     2.89241566E+03  # ~chi_40
   1000024     6.73206025E+02  # ~chi_1+
   1000037     2.89328531E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -6.15696586E-02   # alpha
Block Hmix Q=  1.78112161E+03  # Higgs mixing parameters
   1    2.89489484E+03  # mu
   2    1.58056629E+01  # tan[beta](Q)
   3    2.43931293E+02  # v(Q)
   4    1.17344822E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.66222514E-03   # Re[R_st(1,1)]
   1  2     9.99953320E-01   # Re[R_st(1,2)]
   2  1    -9.99953320E-01   # Re[R_st(2,1)]
   2  2     9.66222514E-03   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     1.53094224E-02   # Re[R_sb(1,1)]
   1  2     9.99882804E-01   # Re[R_sb(1,2)]
   2  1    -9.99882804E-01   # Re[R_sb(2,1)]
   2  2     1.53094224E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     9.91189530E-01   # Re[R_sta(1,1)]
   1  2     1.32451179E-01   # Re[R_sta(1,2)]
   2  1    -1.32451179E-01   # Re[R_sta(2,1)]
   2  2     9.91189530E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.99859274E-01   # Re[N(1,1)]
   1  2     9.64475866E-04   # Re[N(1,2)]
   1  3    -1.65760395E-02   # Re[N(1,3)]
   1  4    -2.39521850E-03   # Re[N(1,4)]
   2  1     1.43095767E-03   # Re[N(2,1)]
   2  2     9.99610583E-01   # Re[N(2,2)]
   2  3    -2.75003257E-02   # Re[N(2,3)]
   2  4    -4.51296096E-03   # Re[N(2,4)]
   3  1     1.00087700E-02   # Re[N(3,1)]
   3  2    -1.62680389E-02   # Re[N(3,2)]
   3  3    -7.06847383E-01   # Re[N(3,3)]
   3  4     7.07108162E-01   # Re[N(3,4)]
   4  1     1.33869061E-02   # Re[N(4,1)]
   4  2    -2.26517785E-02   # Re[N(4,2)]
   4  3    -7.06636926E-01   # Re[N(4,3)]
   4  4    -7.07086942E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.99243264E-01   # Re[U(1,1)]
   1  2     3.88960012E-02   # Re[U(1,2)]
   2  1    -3.88960012E-02   # Re[U(2,1)]
   2  2    -9.99243264E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.99979498E-01   # Re[V(1,1)]
   1  2     6.40339225E-03   # Re[V(1,2)]
   2  1     6.40339225E-03   # Re[V(2,1)]
   2  2    -9.99979498E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     6.75062130E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.99998122E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
DECAY   1000011     1.97331420E+01   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.28315362E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.02876431E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     6.04292033E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     6.75047756E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.99997358E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
DECAY   1000013     1.97333886E+01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.28314555E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.02876508E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     6.04292036E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.39654200E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.02762009E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.99588971E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     5.97649020E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     7.77285940E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.47017845E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.04515444E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     2.08386163E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     6.22002798E-02    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     2.87752974E-02    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     3.07164175E-02    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
DECAY   1000012     1.97095607E+01   # ~nu_e
#    BR                NDA      ID1      ID2
     9.34614056E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.01753182E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     6.04785412E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
DECAY   1000014     1.97094989E+01   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.34614155E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.01753105E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     6.04785479E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
DECAY   1000016     1.41169159E+01   # ~nu_tau
#    BR                NDA      ID1      ID2
     9.70873249E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     3.00541366E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     6.02371309E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000001     2.63134166E+01   # ~d_L
#    BR                NDA      ID1      ID2
     1.10497996E-02    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     3.29718326E-01    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     6.59231874E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
DECAY   2000001     7.75704697E+00   # ~d_R
#    BR                NDA      ID1      ID2
     2.10942460E-01    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     7.89055473E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000003     2.63133991E+01   # ~s_L
#    BR                NDA      ID1      ID2
     1.10498156E-02    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     3.29718522E-01    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     6.59231662E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
DECAY   2000003     7.75740468E+00   # ~s_R
#    BR                NDA      ID1      ID2
     2.10932747E-01    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     7.89016555E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000005     1.40831526E+00   # ~b_1
#    BR                NDA      ID1      ID2
     9.49397091E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     6.84288786E-03    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.35451762E-02    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.37053912E-04    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
     3.00777911E-02    2     1000006       -37   # BR(~b_1 -> ~t_1 H^-)
DECAY   2000005     1.33447388E+02   # ~b_2
#    BR                NDA      ID1      ID2
     3.61186233E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.10705402E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.42062414E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.42392778E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.20150302E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     6.90976432E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.76066186E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.85926754E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     2.88756661E-01    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     2.75359566E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     5.42112184E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   1000002     2.63220308E+01   # ~u_L
#    BR                NDA      ID1      ID2
     1.08019262E-02    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.29701324E-01    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.59496749E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
DECAY   2000002     5.67645099E+00   # ~u_R
#    BR                NDA      ID1      ID2
     9.99998013E-01    2     1000022         2   # BR(~u_R -> chi^0_1 u)
DECAY   1000004     2.63220264E+01   # ~c_L
#    BR                NDA      ID1      ID2
     1.08019189E-02    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.29701123E-01    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.59496958E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
DECAY   2000004     5.67644821E+00   # ~c_R
#    BR                NDA      ID1      ID2
     9.99997901E-01    2     1000022         4   # BR(~c_R -> chi^0_1 c)
DECAY   1000006     4.57123217E-01   # ~t_1
#    BR                NDA      ID1      ID2
     9.99999053E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.32643298E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.55239823E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.11136383E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     3.48909847E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     3.45026031E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.23373723E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.85982144E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.68165834E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     5.59439128E-03    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     9.36964894E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.45489329E-01    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     1.34981510E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     1.45566733E-01    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     9.55624923E-06   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99526305E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.01271592E-04    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.01273067E-04    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     2.49099208E-04    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.09118834E+02   # chi^+_2
#    BR                NDA      ID1      ID2
     3.00888532E-03    2    -2000015        16   # BR(chi^+_2 -> ~tau^+_2 nu_tau)
     3.66837736E-03    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     6.71682814E-01    2     1000006        -5   # BR(chi^+_2 -> ~t_1 b_bar)
     1.19598711E-03    2    -1000005         6   # BR(chi^+_2 -> ~b^*_1 t)
     1.85463216E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     4.96855044E-02    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.12482069E-02    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     3.92102881E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     4.95482446E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     4.45883436E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     5.47441734E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     4.44923531E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     4.29450755E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     9.24967904E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     2.86665567E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     3.19241410E-08   # chi^0_2
#    BR                NDA      ID1      ID2
     2.78750425E-03    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.41821737E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.41661355E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.78504013E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.78502910E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.48590125E-01    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     2.87511618E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     2.87508067E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     6.78164269E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     8.28139610E-02    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     1.12479363E+02   # chi^0_3
#    BR                NDA      ID1      ID2
     1.74270769E-03    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     1.74270769E-03    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     1.45835465E-03    2     2000015       -15   # BR(chi^0_3 -> ~tau^-_2 tau^+)
     1.45835465E-03    2    -2000015        15   # BR(chi^0_3 -> ~tau^+_2 tau^-)
     3.25709380E-01    2     1000006        -6   # BR(chi^0_3 -> ~t_1 t_bar)
     3.25709380E-01    2    -1000006         6   # BR(chi^0_3 -> ~t^*_1 t)
     7.22331035E-04    2     1000005        -5   # BR(chi^0_3 -> ~b_1 b_bar)
     7.22331035E-04    2    -1000005         5   # BR(chi^0_3 -> ~b^*_1 b)
     4.81691828E-02    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     4.81691828E-02    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     4.30742595E-02    2          37  -1000024   # BR(chi^0_3 -> H^+ chi^-_1)
     4.30742595E-02    2         -37   1000024   # BR(chi^0_3 -> H^- chi^+_1)
     1.16236185E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.17537152E-02    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     3.63275593E-03    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     1.11847080E-02    2     1000023        36   # BR(chi^0_3 -> chi^0_2 A^0)
     1.25759258E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     4.54295896E-02    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     7.44197304E-03    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
     2.67895644E-02    2     1000023        35   # BR(chi^0_3 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     6.68502260E-04    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.99665602E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     2.49659146E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     2.49659146E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.08660570E+02   # chi^0_4
#    BR                NDA      ID1      ID2
     1.79069128E-03    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     1.79069128E-03    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     1.52440913E-03    2     2000015       -15   # BR(chi^0_4 -> ~tau^-_2 tau^+)
     1.52440913E-03    2    -2000015        15   # BR(chi^0_4 -> ~tau^+_2 tau^-)
     3.38377795E-01    2     1000006        -6   # BR(chi^0_4 -> ~t_1 t_bar)
     3.38377795E-01    2    -1000006         6   # BR(chi^0_4 -> ~t^*_1 t)
     7.48435952E-04    2     1000005        -5   # BR(chi^0_4 -> ~b_1 b_bar)
     7.48435952E-04    2    -1000005         5   # BR(chi^0_4 -> ~b^*_1 b)
     4.98314132E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     4.98314132E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.91952972E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     3.91952972E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     6.71904817E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.69376192E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     2.91445145E-03    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     8.97442956E-03    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     1.01674217E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     3.67276265E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     9.85327426E-03    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     3.54715733E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     6.53346762E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     1.90264688E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     3.22785545E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     3.22785545E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.13184553E+02   # ~g
#    BR                NDA      ID1      ID2
     4.41335435E-02    2     1000002        -2   # BR(~g -> ~u_L u_bar)
     4.41335435E-02    2    -1000002         2   # BR(~g -> ~u^*_L u)
     1.92395962E-03    2     2000002        -2   # BR(~g -> ~u_R u_bar)
     1.92395962E-03    2    -2000002         2   # BR(~g -> ~u^*_R u)
     4.41334373E-02    2     1000004        -4   # BR(~g -> ~c_L c_bar)
     4.41334373E-02    2    -1000004         4   # BR(~g -> ~c^*_L c)
     1.92382902E-03    2     2000004        -4   # BR(~g -> ~c_R c_bar)
     1.92382902E-03    2    -2000004         4   # BR(~g -> ~c^*_R c)
     3.09945716E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     3.09945716E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     4.39476756E-02    2     1000001        -1   # BR(~g -> ~d_L d_bar)
     4.39476756E-02    2    -1000001         1   # BR(~g -> ~d^*_L d)
     4.39477012E-02    2     1000003        -3   # BR(~g -> ~s_L s_bar)
     4.39477012E-02    2    -1000003         3   # BR(~g -> ~s^*_L s)
     9.87964081E-03    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     9.87964081E-03    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     2.98072387E-03   # Gamma(h0)
     2.15359744E-03   2        22        22   # BR(h0 -> photon photon)
     6.69735500E-04   2        22        23   # BR(h0 -> photon Z)
     8.77898648E-03   2        23        23   # BR(h0 -> Z Z)
     8.79794846E-02   2       -24        24   # BR(h0 -> W W)
     7.48173057E-02   2        21        21   # BR(h0 -> gluon gluon)
     6.28497429E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.79559661E-04   2       -13        13   # BR(h0 -> Muon muon)
     8.05858764E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.77181970E-07   2        -2         2   # BR(h0 -> Up up)
     3.43756139E-02   2        -4         4   # BR(h0 -> Charm charm)
     7.36104980E-07   2        -1         1   # BR(h0 -> Down down)
     2.66232123E-04   2        -3         3   # BR(h0 -> Strange strange)
     7.10092689E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     3.56095048E+00   # Gamma(HH)
     9.57989628E-08   2        22        22   # BR(HH -> photon photon)
     2.96947997E-08   2        22        23   # BR(HH -> photon Z)
     5.19706398E-05   2        23        23   # BR(HH -> Z Z)
     6.36124153E-05   2       -24        24   # BR(HH -> W W)
     4.15375616E-05   2        21        21   # BR(HH -> gluon gluon)
     1.27389393E-08   2       -11        11   # BR(HH -> Electron electron)
     5.67019654E-04   2       -13        13   # BR(HH -> Muon muon)
     1.63246232E-01   2       -15        15   # BR(HH -> Tau tau)
     2.31469398E-12   2        -2         2   # BR(HH -> Up up)
     4.49002372E-07   2        -4         4   # BR(HH -> Charm charm)
     2.79652454E-02   2        -6         6   # BR(HH -> Top top)
     8.01508303E-07   2        -1         1   # BR(HH -> Down down)
     2.89914427E-04   2        -3         3   # BR(HH -> Strange strange)
     8.07581749E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.91330965E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     3.50324386E+00   # Gamma(A0)
     4.65437566E-07   2        22        22   # BR(A0 -> photon photon)
     3.43874782E-07   2        22        23   # BR(A0 -> photon Z)
     1.82290956E-04   2        21        21   # BR(A0 -> gluon gluon)
     1.27194053E-08   2       -11        11   # BR(A0 -> Electron electron)
     5.66150030E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.62998028E-01   2       -15        15   # BR(A0 -> Tau tau)
     2.18227635E-12   2        -2         2   # BR(A0 -> Up up)
     4.23195201E-07   2        -4         4   # BR(A0 -> Charm charm)
     2.94950845E-02   2        -6         6   # BR(A0 -> Top top)
     8.00301859E-07   2        -1         1   # BR(A0 -> Down down)
     2.89478186E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.06391574E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     7.53482091E-05   2        23        25   # BR(A0 -> Z h0)
     2.34430491E-37   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     3.47760420E+00   # Gamma(Hp)
     1.39902868E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.98128446E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.69183956E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     7.44128299E-07   2        -1         2   # BR(Hp -> Down up)
     1.24033247E-05   2        -3         2   # BR(Hp -> Strange up)
     8.52202604E-06   2        -5         2   # BR(Hp -> Bottom up)
     6.48966612E-08   2        -1         4   # BR(Hp -> Down charm)
     2.68849852E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.19338672E-03   2        -5         4   # BR(Hp -> Bottom charm)
     3.19990857E-06   2        -1         6   # BR(Hp -> Down top)
     7.01433103E-05   2        -3         6   # BR(Hp -> Strange top)
     8.28583152E-01   2        -5         6   # BR(Hp -> Bottom top)
     7.74320183E-05   2        24        25   # BR(Hp -> W h0)
     1.49271149E-09   2        24        35   # BR(Hp -> W HH)
     1.55294159E-09   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.49609478E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.49869370E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.49818980E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00020171E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.80119028E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    4.00289842E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.49609478E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.49869370E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.49818980E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99997393E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.60689085E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99997393E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.60689085E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.28071138E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    4.05290891E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.05606588E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.60689085E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99997393E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.48328779E-04   # BR(b -> s gamma)
    2    1.58713562E-06   # BR(b -> s mu+ mu-)
    3    3.52424929E-05   # BR(b -> s nu nu)
    4    2.54414591E-15   # BR(Bd -> e+ e-)
    5    1.08683011E-10   # BR(Bd -> mu+ mu-)
    6    2.27510762E-08   # BR(Bd -> tau+ tau-)
    7    8.57300096E-14   # BR(Bs -> e+ e-)
    8    3.66238291E-09   # BR(Bs -> mu+ mu-)
    9    7.76807857E-07   # BR(Bs -> tau+ tau-)
   10    9.55361573E-05   # BR(B_u -> tau nu)
   11    9.86851929E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42350578E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93729255E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15895065E-03   # epsilon_K
   17    2.28167644E-15   # Delta(M_K)
   18    2.47934917E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28840642E-11   # BR(K^+ -> pi^+ nu nu)
   20   -1.70503433E-15   # Delta(g-2)_electron/2
   21   -7.28952036E-11   # Delta(g-2)_muon/2
   22   -2.57093041E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    5.03646913E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.16621326E-01   # C7
     0305 4322   00   2    -5.20692689E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.29656469E-01   # C8
     0305 6321   00   2    -6.54550597E-04   # C8'
 03051111 4133   00   0     1.59788558E+00   # C9 e+e-
 03051111 4133   00   2     1.59782881E+00   # C9 e+e-
 03051111 4233   00   2    -3.10027560E-05   # C9' e+e-
 03051111 4137   00   0    -4.42057768E+00   # C10 e+e-
 03051111 4137   00   2    -4.41791768E+00   # C10 e+e-
 03051111 4237   00   2     2.39725412E-04   # C10' e+e-
 03051313 4133   00   0     1.59788558E+00   # C9 mu+mu-
 03051313 4133   00   2     1.59782836E+00   # C9 mu+mu-
 03051313 4233   00   2    -3.10030496E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.42057768E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.41791812E+00   # C10 mu+mu-
 03051313 4237   00   2     2.39725706E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50477066E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -5.21761624E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50477066E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -5.21760985E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50477066E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -5.21581077E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85832187E-07   # C7
     0305 4422   00   2     3.50479534E-07   # C7
     0305 4322   00   2     1.23190664E-06   # C7'
     0305 6421   00   0     3.30490784E-07   # C8
     0305 6421   00   2     7.16731180E-07   # C8
     0305 6321   00   2     8.66534585E-07   # C8'
 03051111 4133   00   2    -8.94447309E-07   # C9 e+e-
 03051111 4233   00   2    -1.13127490E-08   # C9' e+e-
 03051111 4137   00   2     6.86002990E-06   # C10 e+e-
 03051111 4237   00   2     9.03434727E-08   # C10' e+e-
 03051313 4133   00   2    -8.94444922E-07   # C9 mu+mu-
 03051313 4233   00   2    -1.13127458E-08   # C9' mu+mu-
 03051313 4137   00   2     6.86003229E-06   # C10 mu+mu-
 03051313 4237   00   2     9.03434797E-08   # C10' mu+mu-
 03051212 4137   00   2    -1.49638872E-06   # C11 nu_1 nu_1
 03051212 4237   00   2    -1.96761862E-08   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.49638872E-06   # C11 nu_2 nu_2
 03051414 4237   00   2    -1.96761862E-08   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.49645238E-06   # C11 nu_3 nu_3
 03051616 4237   00   2    -1.96764410E-08   # C11' nu_3 nu_3
