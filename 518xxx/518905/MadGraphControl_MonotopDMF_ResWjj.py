#import MadGraphControl.MadGraph_NNPDF23nnloas0119qed_Base_Fragment
import MadGraphControl.MadGraphUtils

MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':263000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
    'pdf_variations':[263000], # list of pdfs ids for which all variations (error sets) will be included as weights
    'alternative_pdfs':None, # list of pdfs ids for which only the central set will be included as weights
    'scale_variations':[0.5,1,2], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
}

from MadGraphControl.MadGraphUtils import *
print "%%%%%%%%%%%%%%%%%%%%%%%%%  Jobfile   %%%%%%%%%%%%%%%%%%%%%%%%"
print phys_short
config_names = phys_short.split('_')
print "%%%%%%%%%%%%%%%%%%%%%%%% Config Names %%%%%%%%%%%%%%%%%%%%%%%"
print config_names
mMed = int(config_names[2][4:])
print "%%%%%%%%%%%%%%%%%%%%%%%%% Mediator Mass %%%%%%%%%%%%%%%%%%%%%"
print mMed
mDM = int(config_names[3][2:])
print "%%%%%%%%%%%%%%%%%%%%%%%%  Chi Mass %%%%%%%%%%%%%%%%%%%%%%%%%%"
print mDM
coupling_lambdas = float(config_names[4][2:].replace ('p','.'))
print "%%%%%%%%%%%%%%%%%%%%%%%%  Lambda %%%%%%%%%%%%%%%%%%%%%%%%%%"
print coupling_lambdas
coupling_yphis = float(config_names[5][2:].replace ('p','.'))
print "%%%%%%%%%%%%%%%%%%%%%%%%  Chi y_Phis %%%%%%%%%%%%%%%%%%%%%%%%%%"
print coupling_yphis


# nevents = runArgs.maxEvents*1.0 if runArgs.maxEvents>0 else 1.2*evgenConfig.nEventsPerJob
enhanced_scale = (mMed >= 4500 and mDM == 1)
scale = 1.2 if not enhanced_scale else 20.

nevents = int (runArgs.maxEvents*scale) if runArgs.maxEvents>0 else int (scale*evgenConfig.nEventsPerJob)
# nevents = 30000
ebeam1 = float(runArgs.ecmEnergy/2)
ebeam2 = float(runArgs.ecmEnergy/2)

process="""
import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/MonotopDMF_UFO -modelname
define j = g u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > t chi, (t > b w+, w+ > j j)
add process p p > t~ chi, (t~ > b~ w-, w- > j j)
output -f"""

process_dir = new_process(process)

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version'   :'3.0',
             'cut_decays'    :'F',
             'asrwgtflavor'  :'5',
             'ebeam1'        :ebeam1,
             'ebeam2'        :ebeam2,
             'iseed'         :runArgs.randomSeed,
             'small_width_treatment'  :'1e-12',
             'nevents'       :int(nevents)}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

#Parameter card

masses={'1000006': mMed, #phi mass
        '1000022': mDM}  #chi mass

couplings={'2': coupling_lambdas,
           '3': coupling_yphis }

if mDM >=100:
    decays={'1000006': 'DECAY 1000006 Auto', #phi width
            '1000022': 'DECAY 1000022 0.00'}  #chi width
else:
    decays={'1000006': 'DECAY 1000006 Auto', #phi width
            '1000022': 'DECAY 1000022 Auto'}  #chi width


params={}
params['MASS']=masses
params['COUPX']=couplings
params['DECAY']=decays

modify_param_card(param_card_input=None,process_dir=process_dir,params=params)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  

#### Shower 
evgenConfig.description = 'MadGraph_resonant_monotop'
evgenConfig.contact  = ["paolo.sabatini@cern.ch"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
