# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 18.08.2022,  11:58
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.73182691E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    1.17037250E+03  # scale for input parameters
    1   -2.30849353E+02  # M_1
    2   -6.32059469E+02  # M_2
    3    1.47457243E+03  # M_3
   11    7.87677416E+02  # A_t
   12    1.28946784E+03  # A_b
   13    3.92038381E+03  # A_tau
   23    2.62389892E+02  # mu
   25    5.52575972E+00  # tan(beta)
   26    6.22381305E+02  # m_A, pole mass
   31    1.87181164E+03  # M_L11
   32    1.87181164E+03  # M_L22
   33    1.22324571E+03  # M_L33
   34    9.79469924E+02  # M_E11
   35    9.79469924E+02  # M_E22
   36    1.30431212E+03  # M_E33
   41    3.38130895E+03  # M_Q11
   42    3.38130895E+03  # M_Q22
   43    2.12786765E+02  # M_Q33
   44    2.99079452E+02  # M_U11
   45    2.99079452E+02  # M_U22
   46    3.17643515E+03  # M_U33
   47    2.05012964E+03  # M_D11
   48    2.05012964E+03  # M_D22
   49    3.96533162E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  1.17037250E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  1.17037250E+03  # (SUSY scale)
  1  1     8.52056163E-06   # Y_u(Q)^DRbar
  2  2     4.32844531E-03   # Y_c(Q)^DRbar
  3  3     1.02935228E+00   # Y_t(Q)^DRbar
Block Yd Q=  1.17037250E+03  # (SUSY scale)
  1  1     9.46107458E-05   # Y_d(Q)^DRbar
  2  2     1.79760417E-03   # Y_s(Q)^DRbar
  3  3     9.38242131E-02   # Y_b(Q)^DRbar
Block Ye Q=  1.17037250E+03  # (SUSY scale)
  1  1     1.65102670E-05   # Y_e(Q)^DRbar
  2  2     3.41379958E-03   # Y_mu(Q)^DRbar
  3  3     5.74144929E-02   # Y_tau(Q)^DRbar
Block Au Q=  1.17037250E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     7.87677413E+02   # A_t(Q)^DRbar
Block Ad Q=  1.17037250E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.28946790E+03   # A_b(Q)^DRbar
Block Ae Q=  1.17037250E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     3.92038374E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  1.17037250E+03  # soft SUSY breaking masses at Q
   1   -2.30849353E+02  # M_1
   2   -6.32059469E+02  # M_2
   3    1.47457243E+03  # M_3
  21    3.05492931E+05  # M^2_(H,d)
  22   -3.11821052E+05  # M^2_(H,u)
  31    1.87181164E+03  # M_(L,11)
  32    1.87181164E+03  # M_(L,22)
  33    1.22324571E+03  # M_(L,33)
  34    9.79469924E+02  # M_(E,11)
  35    9.79469924E+02  # M_(E,22)
  36    1.30431212E+03  # M_(E,33)
  41    3.38130895E+03  # M_(Q,11)
  42    3.38130895E+03  # M_(Q,22)
  43    2.12786765E+02  # M_(Q,33)
  44    2.99079452E+02  # M_(U,11)
  45    2.99079452E+02  # M_(U,22)
  46    3.17643515E+03  # M_(U,33)
  47    2.05012964E+03  # M_(D,11)
  48    2.05012964E+03  # M_(D,22)
  49    3.96533162E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.10995087E+02  # h0
        35     6.23289297E+02  # H0
        36     6.22381305E+02  # A0
        37     6.28545399E+02  # H+
   1000001     3.40865976E+03  # ~d_L
   2000001     2.07364524E+03  # ~d_R
   1000002     3.40790205E+03  # ~u_L
   2000002     2.83705919E+02  # ~u_R
   1000003     3.40865986E+03  # ~s_L
   2000003     2.07364536E+03  # ~s_R
   1000004     3.40790221E+03  # ~c_L
   2000004     2.83713491E+02  # ~c_R
   1000005     4.08940284E+02  # ~b_1
   2000005     3.99166148E+03  # ~b_2
   1000006     4.27730609E+02  # ~t_1
   2000006     3.20241704E+03  # ~t_2
   1000011     1.87113553E+03  # ~e_L-
   2000011     9.97748784E+02  # ~e_R-
   1000012     1.86913492E+03  # ~nu_eL
   1000013     1.87113545E+03  # ~mu_L-
   2000013     9.97748763E+02  # ~mu_R-
   1000014     1.86913489E+03  # ~nu_muL
   1000015     1.22030475E+03  # ~tau_1-
   2000015     1.31846395E+03  # ~tau_2-
   1000016     1.21746244E+03  # ~nu_tauL
   1000021     1.61579819E+03  # ~g
   1000022     2.20376570E+02  # ~chi_10
   1000023     2.75721550E+02  # ~chi_20
   1000025     2.76739500E+02  # ~chi_30
   1000035     6.66180630E+02  # ~chi_40
   1000024     2.68402606E+02  # ~chi_1+
   1000037     6.66350155E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.81939350E-01   # alpha
Block Hmix Q=  1.17037250E+03  # Higgs mixing parameters
   1    2.62389892E+02  # mu
   2    5.52575972E+00  # tan[beta](Q)
   3    2.44730227E+02  # v(Q)
   4    3.87358489E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.99941558E-01   # Re[R_st(1,1)]
   1  2     1.08111275E-02   # Re[R_st(1,2)]
   2  1    -1.08111275E-02   # Re[R_st(2,1)]
   2  2    -9.99941558E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     1.00000000E+00   # Re[R_sb(1,1)]
   1  2     7.79529142E-06   # Re[R_sb(1,2)]
   2  1    -7.79529142E-06   # Re[R_sb(2,1)]
   2  2     1.00000000E+00   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -9.99851872E-01   # Re[R_sta(1,1)]
   1  2     1.72114637E-02   # Re[R_sta(1,2)]
   2  1    -1.72114637E-02   # Re[R_sta(2,1)]
   2  2    -9.99851872E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -8.81816814E-01   # Re[N(1,1)]
   1  2     4.01900179E-02   # Re[N(1,2)]
   1  3    -3.76804840E-01   # Re[N(1,3)]
   1  4    -2.80716907E-01   # Re[N(1,4)]
   2  1     4.66444324E-01   # Re[N(2,1)]
   2  2     1.12281715E-01   # Re[N(2,2)]
   2  3    -6.09243285E-01   # Re[N(2,3)]
   2  4    -6.31383504E-01   # Re[N(2,4)]
   3  1     6.84343900E-02   # Re[N(3,1)]
   3  2    -7.28812847E-02   # Re[N(3,2)]
   3  3    -6.96955106E-01   # Re[N(3,3)]
   3  4     7.10111704E-01   # Re[N(3,4)]
   4  1     1.20637571E-02   # Re[N(4,1)]
   4  2    -9.90184779E-01   # Re[N(4,2)]
   4  3    -3.30803816E-02   # Re[N(4,3)]
   4  4    -1.35256263E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -4.78664335E-02   # Re[U(1,1)]
   1  2     9.98853745E-01   # Re[U(1,2)]
   2  1    -9.98853745E-01   # Re[U(2,1)]
   2  2    -4.78664335E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     1.92922014E-01   # Re[V(1,1)]
   1  2     9.81214093E-01   # Re[V(1,2)]
   2  1     9.81214093E-01   # Re[V(2,1)]
   2  2    -1.92922014E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     4.47802588E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     7.87685803E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     2.07796737E-01    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     4.46741742E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     2.11710753E+01   # ~e^-_L
#    BR                NDA      ID1      ID2
     7.02384581E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     4.86443316E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     4.91020821E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.87075404E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.71001146E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     5.91840774E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     4.47842341E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     7.87622254E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     2.07794870E-01    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     4.48843811E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
DECAY   1000013     2.11714929E+01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     7.02399102E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.86506281E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     5.00535897E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.87069744E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.70997765E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     5.91829060E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     9.50463399E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.90002422E-02    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     6.94789892E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     4.51674352E-03    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.69457452E-01    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.20126957E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     5.55345304E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     6.37626938E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.65702206E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.11031945E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.02042686E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.66543643E-04    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.26662813E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     1.72145258E-04    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
DECAY   1000012     2.12186964E+01   # ~nu_e
#    BR                NDA      ID1      ID2
     9.85541177E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     6.95406093E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     4.44250465E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     2.93489293E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.76835005E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.68876523E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
DECAY   1000014     2.12191116E+01   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.85521875E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     6.95392474E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     4.44241764E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.93483543E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     2.77025266E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.68865394E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
DECAY   1000016     9.56669606E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.36983684E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     9.44901577E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     6.03352989E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.73077855E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     4.53005783E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.29155337E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
DECAY   2000001     2.63876295E+01   # ~d_R
#    BR                NDA      ID1      ID2
     3.33493099E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.21090408E-03    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     1.98221153E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.57236416E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     2.03853422E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.83873562E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.25599115E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     6.70435403E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     3.33087837E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.35849580E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.93347435E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     2.63878945E+01   # ~s_R
#    BR                NDA      ID1      ID2
     3.33496686E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.21262408E-03    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     2.00581178E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.57227105E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     2.03854907E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.83886557E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.26109513E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     6.70430552E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     3.39005733E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.35848817E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.93341697E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     6.25489747E-02   # ~b_1
#    BR                NDA      ID1      ID2
     6.38263332E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.32041905E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.29694763E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
DECAY   2000005     2.24021503E+02   # ~b_2
#    BR                NDA      ID1      ID2
     8.12816571E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     3.29145228E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.54747021E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     3.07351538E-03    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     9.83293971E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     3.25484296E-04    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     1.63493145E-04    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     1.63500352E-04    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   2000002     7.79285133E-02   # ~u_R
#    BR                NDA      ID1      ID2
     9.94463934E-01    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     5.44649492E-03    2     1000023         2   # BR(~u_R -> chi^0_2 u)
DECAY   1000002     2.03836722E+02   # ~u_L
#    BR                NDA      ID1      ID2
     9.96180348E-04    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     2.77140300E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     2.67525995E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     6.64525671E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     5.40999720E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.31070734E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.93031592E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.79258513E-02   # ~c_R
#    BR                NDA      ID1      ID2
     9.94552843E-01    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     5.33857918E-03    2     1000023         4   # BR(~c_R -> chi^0_2 c)
DECAY   1000004     2.03838116E+02   # ~c_L
#    BR                NDA      ID1      ID2
     9.96657831E-04    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     2.77384608E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     2.70628973E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     6.64521690E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     5.41101814E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.31069845E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.93025800E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     2.13923915E-01   # ~t_1
#    BR                NDA      ID1      ID2
     5.80298754E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     4.19580582E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
#    BR                NDA      ID1      ID2       ID3
     1.20563331E-04    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     2.86006422E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.74080739E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     9.72400926E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.16533435E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     4.21369052E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.23418302E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     8.62097857E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     4.86698315E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     8.67526437E-03    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     3.45191484E-03    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     4.31437905E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.72284666E-03    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     5.96318629E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     1.73952187E-03    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     2.72574274E-04   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.33801117E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.32896050E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.11294418E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.11292849E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.10715566E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     9.01053781E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     3.22923555E-01    2     1000006        -5   # BR(chi^+_2 -> ~t_1 b_bar)
     1.95951594E-01    2    -1000005         6   # BR(chi^+_2 -> ~b^*_1 t)
     1.18850332E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     1.16629440E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.22393228E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     1.21202615E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.04938104E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.74881034E-04    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.23195406E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     2.06688532E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     1.60434380E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.07167331E-05   # chi^0_2
#    BR                NDA      ID1      ID2
     1.23583595E-02    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     2.97293457E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     2.95001262E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     7.76164312E-02    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     7.76145291E-02    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     7.57256674E-02    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     1.59054196E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     1.59049460E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     1.61468452E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.04452794E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     2.15262495E-03    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     2.15262495E-03    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     1.86184133E-03    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     1.86184133E-03    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     7.17509623E-04    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     7.17509623E-04    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     7.16766156E-04    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     7.16766156E-04    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
     5.41402461E-04    3     1000024        15       -16   # BR(chi^0_2 -> chi^+_1 tau^- nu_bar_tau)
     5.41402461E-04    3    -1000024       -15        16   # BR(chi^0_2 -> chi^-_1 tau^+ nu_tau)
DECAY   1000025     4.75129597E-04   # chi^0_3
#    BR                NDA      ID1      ID2
     6.39785010E-03    2     1000022        22   # BR(chi^0_3 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.20847702E-01    3     1000022         2        -2   # BR(chi^0_3 -> chi^0_1 u u_bar)
     1.20162125E-01    3     1000022         4        -4   # BR(chi^0_3 -> chi^0_1 c c_bar)
     1.51725059E-01    3     1000022         1        -1   # BR(chi^0_3 -> chi^0_1 d d_bar)
     1.51720991E-01    3     1000022         3        -3   # BR(chi^0_3 -> chi^0_1 s s_bar)
     1.43712965E-01    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
     3.41961727E-02    3     1000022        11       -11   # BR(chi^0_3 -> chi^0_1 e^- e^+)
     3.41947006E-02    3     1000022        13       -13   # BR(chi^0_3 -> chi^0_1 mu^- mu^+)
     3.38092943E-02    3     1000022        15       -15   # BR(chi^0_3 -> chi^0_1 tau^- tau^+)
     2.03017244E-01    3     1000022        12       -12   # BR(chi^0_3 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000035     1.26768677E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.37832277E-01    2     1000006        -6   # BR(chi^0_4 -> ~t_1 t_bar)
     1.37832277E-01    2    -1000006         6   # BR(chi^0_4 -> ~t^*_1 t)
     1.32642832E-01    2     1000005        -5   # BR(chi^0_4 -> ~b_1 b_bar)
     1.32642832E-01    2    -1000005         5   # BR(chi^0_4 -> ~b^*_1 b)
     9.54927970E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     9.54927970E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.80543929E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     2.04377098E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     6.78208673E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     3.49129132E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     4.98424988E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.17432354E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.22759420E-04    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     6.16307169E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     1.94360735E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.94360735E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.74721111E+02   # ~g
#    BR                NDA      ID1      ID2
     1.28559470E-01    2     2000002        -2   # BR(~g -> ~u_R u_bar)
     1.28559470E-01    2    -2000002         2   # BR(~g -> ~u^*_R u)
     1.28559028E-01    2     2000004        -4   # BR(~g -> ~c_R c_bar)
     1.28559028E-01    2    -2000004         4   # BR(~g -> ~c^*_R c)
     1.18695666E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.18695666E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.19895993E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.19895993E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
     2.24123013E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     2.43463425E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.78809356E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.78809356E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     2.95672488E-03   # Gamma(h0)
     1.86105802E-03   2        22        22   # BR(h0 -> photon photon)
     3.94775659E-04   2        22        23   # BR(h0 -> photon Z)
     4.87580431E-03   2        23        23   # BR(h0 -> Z Z)
     5.11411678E-02   2       -24        24   # BR(h0 -> W W)
     7.21244914E-02   2        21        21   # BR(h0 -> gluon gluon)
     6.60225496E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.93669520E-04   2       -13        13   # BR(h0 -> Muon muon)
     8.46373846E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.72898462E-07   2        -2         2   # BR(h0 -> Up up)
     3.35417760E-02   2        -4         4   # BR(h0 -> Charm charm)
     7.80765045E-07   2        -1         1   # BR(h0 -> Down down)
     2.82384616E-04   2        -3         3   # BR(h0 -> Strange strange)
     7.50846528E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.26307230E+00   # Gamma(HH)
     1.17471867E-06   2        22        22   # BR(HH -> photon photon)
     3.01143509E-07   2        22        23   # BR(HH -> photon Z)
     2.62111284E-03   2        23        23   # BR(HH -> Z Z)
     5.16853062E-03   2       -24        24   # BR(HH -> W W)
     8.44154500E-04   2        21        21   # BR(HH -> gluon gluon)
     2.57326734E-09   2       -11        11   # BR(HH -> Electron electron)
     1.14518898E-04   2       -13        13   # BR(HH -> Muon muon)
     3.29833460E-02   2       -15        15   # BR(HH -> Tau tau)
     5.21746337E-11   2        -2         2   # BR(HH -> Up up)
     1.01139751E-05   2        -4         4   # BR(HH -> Charm charm)
     4.71683500E-01   2        -6         6   # BR(HH -> Top top)
     2.30876113E-07   2        -1         1   # BR(HH -> Down down)
     8.35075423E-05   2        -3         3   # BR(HH -> Strange strange)
     2.16123084E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.59072850E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     8.20756997E-02   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     6.39015198E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     3.23069723E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.93111301E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     2.90662984E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     6.40343377E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     2.57768664E-02   2        25        25   # BR(HH -> h0 h0)
     1.49912002E-03   2  -1000002   1000002   # BR(HH -> Sup1 sup1)
     1.49811316E-03   2  -1000004   1000004   # BR(HH -> Scharm1 scharm1)
DECAY        36     1.47912241E+00   # Gamma(A0)
     1.72379599E-06   2        22        22   # BR(A0 -> photon photon)
     1.39444980E-06   2        22        23   # BR(A0 -> photon Z)
     1.19431055E-03   2        21        21   # BR(A0 -> gluon gluon)
     2.17822949E-09   2       -11        11   # BR(A0 -> Electron electron)
     9.69383659E-05   2       -13        13   # BR(A0 -> Muon muon)
     2.79214942E-02   2       -15        15   # BR(A0 -> Tau tau)
     3.99885880E-11   2        -2         2   # BR(A0 -> Up up)
     7.75013376E-06   2        -4         4   # BR(A0 -> Charm charm)
     5.24218959E-01   2        -6         6   # BR(A0 -> Top top)
     1.95473525E-07   2        -1         1   # BR(A0 -> Down down)
     7.07024970E-05   2        -3         3   # BR(A0 -> Strange strange)
     1.83064497E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     6.23928270E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     7.92093996E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     5.47652225E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     3.13612617E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.37751343E-02   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     7.42629656E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.06569042E-02   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     3.83498698E-03   2        23        25   # BR(A0 -> Z h0)
     4.62934771E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.35664030E+00   # Gamma(Hp)
     2.51720570E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     1.07618400E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     3.04401395E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.95528874E-07   2        -1         2   # BR(Hp -> Down up)
     3.23239954E-06   2        -3         2   # BR(Hp -> Strange up)
     2.01340263E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.07681388E-07   2        -1         4   # BR(Hp -> Down charm)
     7.91484421E-05   2        -3         4   # BR(Hp -> Strange charm)
     2.81959328E-04   2        -5         4   # BR(Hp -> Bottom charm)
     3.74999491E-05   2        -1         6   # BR(Hp -> Down top)
     8.17709453E-04   2        -3         6   # BR(Hp -> Strange top)
     7.52512514E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.38806075E-01   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     3.30665949E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     6.92141676E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     4.39064417E-03   2        24        25   # BR(Hp -> W h0)
     3.98658535E-09   2        24        35   # BR(Hp -> W HH)
     8.83320366E-09   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.03236989E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    3.05016506E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    3.05340205E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.98939875E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.38104801E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.27503547E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.03236989E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    3.05016506E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    3.05340205E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99991553E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    8.44737400E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99991553E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    8.44737400E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.34526206E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    3.34404250E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.89439512E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    8.44737400E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99991553E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.85776136E-04   # BR(b -> s gamma)
    2    1.59099410E-06   # BR(b -> s mu+ mu-)
    3    3.53221027E-05   # BR(b -> s nu nu)
    4    2.50990231E-15   # BR(Bd -> e+ e-)
    5    1.07220179E-10   # BR(Bd -> mu+ mu-)
    6    2.24453013E-08   # BR(Bd -> tau+ tau-)
    7    8.45187204E-14   # BR(Bs -> e+ e-)
    8    3.61063709E-09   # BR(Bs -> mu+ mu-)
    9    7.65833896E-07   # BR(Bs -> tau+ tau-)
   10    9.63290234E-05   # BR(B_u -> tau nu)
   11    9.95041933E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.45575611E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.94925661E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.17259394E-03   # epsilon_K
   17    2.28181839E-15   # Delta(M_K)
   18    2.48446377E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.30092981E-11   # BR(K^+ -> pi^+ nu nu)
   20   -1.06674778E-15   # Delta(g-2)_electron/2
   21   -4.56068424E-11   # Delta(g-2)_muon/2
   22   -2.00008639E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    1.07659633E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.53559709E-01   # C7
     0305 4322   00   2    -1.24345577E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.66042217E-01   # C8
     0305 6321   00   2    -1.37178155E-03   # C8'
 03051111 4133   00   0     1.58290887E+00   # C9 e+e-
 03051111 4133   00   2     1.58448967E+00   # C9 e+e-
 03051111 4233   00   2     1.96078224E-06   # C9' e+e-
 03051111 4137   00   0    -4.40560097E+00   # C10 e+e-
 03051111 4137   00   2    -4.41062638E+00   # C10 e+e-
 03051111 4237   00   2    -1.62693567E-05   # C10' e+e-
 03051313 4133   00   0     1.58290887E+00   # C9 mu+mu-
 03051313 4133   00   2     1.58448965E+00   # C9 mu+mu-
 03051313 4233   00   2     1.96075439E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.40560097E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.41062640E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.62693330E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50647162E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     3.56005851E-06   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50647162E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     3.56006430E-06   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50647127E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     3.56205974E-06   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85821393E-07   # C7
     0305 4422   00   2    -1.72425644E-05   # C7
     0305 4322   00   2    -4.50593261E-07   # C7'
     0305 6421   00   0     3.30481539E-07   # C8
     0305 6421   00   2    -1.35921684E-05   # C8
     0305 6321   00   2    -4.20018918E-07   # C8'
 03051111 4133   00   2     3.94371547E-06   # C9 e+e-
 03051111 4233   00   2     1.66621965E-07   # C9' e+e-
 03051111 4137   00   2    -1.59235090E-06   # C10 e+e-
 03051111 4237   00   2    -1.33118744E-06   # C10' e+e-
 03051313 4133   00   2     3.94371502E-06   # C9 mu+mu-
 03051313 4233   00   2     1.66621941E-07   # C9' mu+mu-
 03051313 4137   00   2    -1.59235094E-06   # C10 mu+mu-
 03051313 4237   00   2    -1.33118749E-06   # C10' mu+mu-
 03051212 4137   00   2     7.97655026E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     2.90839219E-07   # C11' nu_1 nu_1
 03051414 4137   00   2     7.97655028E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     2.90839219E-07   # C11' nu_2 nu_2
 03051616 4137   00   2     7.91469505E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     2.90845727E-07   # C11' nu_3 nu_3
