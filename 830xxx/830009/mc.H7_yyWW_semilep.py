evgenConfig.description = "gammagamma->WW with Budnev parameterization, leptonic decays, central lepton filter pt>15 GeV, "
evgenConfig.keywords = ["2lepton", "diboson", "SM", "WW", "electroweak", "exclusive"]
evgenConfig.contact = ["Kristin Lohwasser <kristin@cern.ch"]
evgenConfig.generators  = ["Herwig7", "EvtGen"]
evgenConfig.tune        = "H7.2-Default"

## Initialise Herwig7 for run with built-in/old-style matrix elements 
include("Herwig7_i/Herwig7_BuiltinME.py") 

Herwig7Config.pdf_gammagamma_cmds()

include("./Herwig7_QED_EvtGen_Common.py")

commands = """
#do /Herwig/Particles/W-:PrintDecayModes
#do /Herwig/Particles/W+:SelectDecayModes W+->nu_e,e+; W+->nu_mu,mu+; W+->nu_tau,tau+;
#do /Herwig/Particles/W-:SelectDecayModes W-->nu_ebar,e-; W-->nu_mubar,mu-; W-->nu_taubar,tau-;

# Selected the hard process
cd /Herwig/MatrixElements
insert SubProcess:MatrixElements 0 MEgg2WW
"""

Herwig7Config.add_commands(commands)
del commands

Herwig7Config.run()

include('GeneratorFilters/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 3500.
MultiLeptonFilter.Etacut = 4.5
MultiLeptonFilter.NLeptons = 1

