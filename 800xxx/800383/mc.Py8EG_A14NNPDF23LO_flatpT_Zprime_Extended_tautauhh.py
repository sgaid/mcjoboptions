evgenConfig.description = "a8 Z'->tautau->hh with A14 tune and NNPDF23LO PDF"
evgenConfig.process = "Z' -> tauh + tauh"
evgenConfig.contact = ["fdibello@cern.ch"] 
evgenConfig.keywords    = [ 'BSM', 'Zprime', 'heavyBoson', 'SSM', 'resonance', 'electroweak']
evgenConfig.generators += [ 'Pythia8' ]

include( "Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py" )

genSeq.Pythia8.UserHooks += ["ZprimeFlatpT"]

genSeq.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",  # create Z bosons
"PhaseSpace:mHatMin = 25.0",         # minimum inv.mass cut
"32:onMode = off",                     # switch off all Z' decays
"32:onIfAny = 15",
"32:m0 = 4000.0"]


#Only Z' - no gamma/Z
genSeq.Pythia8.Commands += ["Zprime:gmZmode= 3",
                            "ZprimeFlatpT:MaxSHat=13000.",
                            "ZprimeFlatpT:DoDecayWeightBelow=0."]

testSeq.TestHepMC.MaxVtxDisp = 1500.0

include("Pythia8_i/Pythia8_ShowerWeights.py")


# ... Filter Z'->tautau->Children
from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilterExtended
filtSeq += XtoVVDecayFilterExtended("TauTauHadHadFilter")
filtSeq.TauTauHadHadFilter.PDGGrandParent = 32
filtSeq.TauTauHadHadFilter.PDGParent = 15
filtSeq.TauTauHadHadFilter.StatusParent = 2
filtSeq.TauTauHadHadFilter.PDGChild1 = [211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]
filtSeq.TauTauHadHadFilter.PDGChild2 = [211,213,215,311,321,323,10232,10323,20213,20232,20323,30213,100213,100323,1000213]
