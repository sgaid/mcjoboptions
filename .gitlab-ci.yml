#-----------------------------------------------------
# WORKFLOW
#-----------------------------------------------------
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "parent_pipeline"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"
      when: never

#-----------------------------------------------------
# VARIABLES
#-----------------------------------------------------
variables:
  GIT_SSL_NO_VERIFY: "true"

#-----------------------------------------------------
# PIPELINE STAGES
#-----------------------------------------------------
stages:
  - check_commit
  - setup
  - run
  - cleanup
  - allow_merge
  - auto_mr

#-----------------------------------------------------
# CHECK COMMIT: NOTIFY CHANGES
#-----------------------------------------------------
notify_changes:
  stage: check_commit
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_COMMIT_MESSAGE =~ /\[skip modfiles\]/
    - if: $CI_COMMIT_MESSAGE =~ /pipeline succeeded. Removing log.generate files/ && $GITLAB_USER_ID == "15691"
      when: never
  image: atlasadc/atlas-grid-centos7
  tags:
    - cvmfs
  variables:
    ATLAS_LOCAL_ROOT_BASE: /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
  before_script:
    - echo ${K8S_SECRET_SERVICE_PASSWORD} | kinit ${SERVICE_ACCOUNT}@CERN.CH
    - mkdir /root/.globus
    - echo $GRID_USERCERT | base64 -di > /root/.globus/usercert.pem
    - echo $GRID_USERKEY  | base64 -di > /root/.globus/userkey.pem
    - chmod 400 /root/.globus/userkey.pem
  allow_failure:
    exit_codes: 1
  script:
    - |
      source scripts/notify.sh
      ret=$?
      exit $ret

#-----------------------------------------------------
# CHECK COMMIT: CHECK JO CONSISTENCY
#-----------------------------------------------------
check_jo_consistency:
  stage: check_commit
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /pipeline succeeded. Removing log.generate files/ && $GITLAB_USER_ID == "15691"
      when: never
    - when: always
  image:
    name: python:3.6.10 # for f-string support
    entrypoint: [""] # so that we start from bash and not from a python shell (default in image)
  before_script:
    - git config --global user.name "mcgensvc" && git config --global user.email "mcgensvc@cern.ch"
    - git pull origin master >& /dev/null
  script:
    - python -B scripts/check_jo_consistency_main.py

#-----------------------------------------------------
# CHECK COMMIT: CHECK UNIQUE CONTROL FILE
#-----------------------------------------------------
check_unique_file:
  stage: check_commit
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /pipeline succeeded. Removing log.generate files/ && $GITLAB_USER_ID == "15691"
      when: never
    - when: always
  script:
    - ./scripts/check_unique_controlFile.sh
  
#-----------------------------------------------------
# CHECK COMMIT: CHECK UNIQUE PHYSICS SHORT
#-----------------------------------------------------
check_unique_physicsShort:
  stage: check_commit
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /pipeline succeeded. Removing log.generate files/ && $GITLAB_USER_ID == "15691"
      when: never
    - when: always
  script:
    - ./scripts/check_unique_physicsShort.sh

#-----------------------------------------------------
# CHECK COMMIT: CHECK OTHER FILES FOR MODIFICATIONS/DELETIONS
#-----------------------------------------------------
check_modified_files:
  stage: check_commit
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /\[skip modfiles\]/
      when: never
    - if: $CI_COMMIT_MESSAGE =~ /pipeline succeeded. Removing log.generate files/ && $GITLAB_USER_ID == "15691"
      when: never
    - when: always
  before_script:
    - git config --global user.name "mcgensvc" && git config --global user.email "mcgensvc@cern.ch"
  script:
    - ./scripts/check_modified_files.sh

#-----------------------------------------------------
# CHECK COMMIT: CHECK TYPE OF ADDED FILES
#-----------------------------------------------------
check_added_files:
  stage: check_commit
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /pipeline succeeded. Removing log.generate files/ && $GITLAB_USER_ID == "15691"
      when: never
    - when: always
  image:
    name: python:3.6.10 # for f-string support
    entrypoint: [""] # so that we start from bash and not from a python shell (default in image)
  script:
    - ./scripts/check_added_files

#-----------------------------------------------------
# CHECK COMMIT: CHECK GRID FILE READABILITY
#-----------------------------------------------------
check_grid_readable:
  stage: check_commit
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /pipeline succeeded. Removing log.generate files/ && $GITLAB_USER_ID == "15691"
      when: never
    - when: always
  tags:
    - cvmfs
  # Have to use private image built from gitlab-registry.cern.ch/dss/eos:4.4.47
  # since official image doesn't have correct entrypoint
  image: gitlab-registry.cern.ch/atlas-physics/pmg/mcjoboptions:eos_bash
  before_script:
    - echo ${K8S_SECRET_SERVICE_PASSWORD} | kinit ${SERVICE_ACCOUNT}@CERN.CH
  script:
    - ./scripts/check_grid_readable.sh

#-----------------------------------------------------
# CHECK COMMIT: CHECK NUMBER OF FILES IN GRIDPACKS
#-----------------------------------------------------
check_grid_nfiles:
  stage: check_commit
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /pipeline succeeded. Removing log.generate files/ && $GITLAB_USER_ID == "15691"
      when: never
    - if: $CI_COMMIT_MESSAGE =~ /\[skip nfiles\]/
      when: never
    - when: always
  tags:
    - cvmfs
  # Have to use private image built from gitlab-registry.cern.ch/dss/eos:4.4.47
  # since official image doesn't have correct entrypoint
  image: gitlab-registry.cern.ch/atlas-physics/pmg/mcjoboptions:eos_bash
  before_script:
    - echo ${K8S_SECRET_SERVICE_PASSWORD} | kinit ${SERVICE_ACCOUNT}@CERN.CH
  script:
    - ./scripts/check_grid_nfiles.sh

#-----------------------------------------------------
# CHECK COMMIT: CHECK GRID FILE SIZE
#-----------------------------------------------------
check_grid_size:
  stage: check_commit
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /pipeline succeeded. Removing log.generate files/ && $GITLAB_USER_ID == "15691"
      when: never
    - when: always
  tags:
    - cvmfs
  # Have to use private image built from gitlab-registry.cern.ch/dss/eos:4.4.47
  # since official image doesn't have correct entrypoint
  image: gitlab-registry.cern.ch/atlas-physics/pmg/mcjoboptions:eos_bash
  before_script:
      - echo ${K8S_SECRET_SERVICE_PASSWORD} | kinit ${SERVICE_ACCOUNT}@CERN.CH
  script:
    - ./scripts/check_grid_size.sh
  allow_failure: true

#-----------------------------------------------------
# SETUP ATHENA: GENERATE ATHENA JOBS
#-----------------------------------------------------
setup_athena:
  stage: setup
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /pipeline succeeded. Removing log.generate files/ && $GITLAB_USER_ID == "15691"
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_MESSAGE =~ /\[skip athena\]/
      when: never
    - when: on_success
  artifacts:
    paths:
      - .run_athena.yml
  script:
    - python scripts/generateYaml.py 5 # generate 5 athena jobs
    - echo "Automatically generated CI config file:"
    - cat .run_athena.yml

#-----------------------------------------------------
# RUN ATHENA: RUN ATHENA PARENT JOB
#-----------------------------------------------------
run_child_pipeline:
  stage: run
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /pipeline succeeded. Removing log.generate files/ && $GITLAB_USER_ID == "15691"
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_MESSAGE =~ /\[skip athena\]/
      when: never
    - when: on_success
  trigger:
    include:
      - artifact: .run_athena.yml
        job: setup_athena
    strategy: depend

#-----------------------------------------------------
# CLEAN UP: REMOVE LOG.GENERATE.SHORT FILES
#-----------------------------------------------------
remove_logs:
  stage: cleanup
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /pipeline succeeded. Removing log.generate files/ && $GITLAB_USER_ID == "15691"
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: never
    - when: on_success
  before_script:
    - git config --global user.name "mcgensvc" && git config --global user.email "mcgensvc@cern.ch"
    - git checkout -b ${CI_COMMIT_REF_NAME} || echo "Branch ${CI_COMMIT_REF_NAME} already exists, checking it out" && git checkout ${CI_COMMIT_REF_NAME}
  script:
    - ./scripts/removeLogs.sh

#-------------------------------------------------------------------
# ALLOW MERGE: DUMMY JOB THAT RUNS ONLY IF EVERYTHING ELSE SUCCEEDED
#-------------------------------------------------------------------
allow_merge:
  stage: allow_merge
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: never
    - if: $CI_COMMIT_MESSAGE =~ /pipeline succeeded. Removing log.generate files/ && $GITLAB_USER_ID == "15691"
  script:
    - echo "Pipeline successful"
    
#-------------------------------------------------------------------
# CREATE MR: OPEN MR FOR THE BRANCH THAT HAS BEEN COMMITTED
#-------------------------------------------------------------------
create_mr:
  stage: auto_mr
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_MESSAGE =~ /\[auto\]/
  script:
    - ./scripts/merge_request_api.sh -o

#-------------------------------------------------------------------
# MERGE MR: MERGE OPEN MR CREATED THROUGH PREVIOUS STEP
#-------------------------------------------------------------------
merge_mr:
  stage: auto_mr
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" &&  $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^dsid_auto/
  script:
    - ./scripts/merge_request_api.sh -a
    - ./scripts/merge_request_api.sh -m
