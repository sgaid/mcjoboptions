evgenConfig.description = "Single muon +/- with fixed pT = 1 GeV, flat eta (between -4.3 and 4.3), and flat phi"
evgenConfig.keywords = ["singleParticle", "muon"]
evgenConfig.contact = ["ohm@cern.ch"]
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG
genSeq += PG.ParticleGun()
evgenConfig.generators += ["ParticleGun"]

genSeq.ParticleGun.sampler.pid = {13,-13}
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=PG.InvSampler(1000, 800000), eta=[-4.3, 4.3])
