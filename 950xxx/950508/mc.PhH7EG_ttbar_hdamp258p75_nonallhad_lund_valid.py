#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = ''''POWHEG+Herwig7 ttbar production, pp collisions, with Powheg hdamp equal 1.5*top mass, 
A14 tune, at least one lepton filter, ME NNPDF30 NLO, A14 NNPDF23 LO, Lund string hadronisation model
''' 
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'yoran.yeh@cern.ch']
evgenConfig.nEventsPerJob   = 40000
evgenConfig.inputFilesPerJob = 5 
evgenConfig.generators += ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.tune = "H7.1-Default"

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO", usepwhglhereader=True)
Herwig7Config.tune_commands()

# set up Pythia8 (Lund string) hadronisation plugin
include("Herwig7_i/Herwig7_TheP8I.py")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

