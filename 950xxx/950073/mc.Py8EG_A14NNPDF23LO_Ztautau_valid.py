# adapted for rel 21.6 from MC15.429110

# JO for test of Pythia internal PDF info

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'Pythia 8 Z->tautau production to test PDF info'
evgenConfig.keywords    = [ 'SM', 'Z', 'tau', 'lepton']
evgenConfig.contact = ['giancarlo.panizzo@cern.ch']

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Photospp.py')

genSeq.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", # create Z bosons
                            "PhaseSpace:mHatMin = 60.", # lower invariant mass
                            "23:onMode = off", # switch off all Z decays
                            "23:onIfAny = 15"] # switch on Z->tautau decays]

