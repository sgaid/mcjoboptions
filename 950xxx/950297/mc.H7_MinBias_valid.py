## Provide config information
evgenConfig.generators += ["Herwig7"]
evgenConfig.tune        = "Default"
evgenConfig.description = "Herwig7 minbias sample with MMHT2014 PDF and corresponding tune"
evgenConfig.keywords    = ["SM","QCD", "dijet"]
evgenConfig.contact     = ["dominic.hirschbuehl@cern.ch"]
evgenConfig.nEventsPerJob = 10000


## Configure Herwig/Matchbox
## These are the commands corresponding to what would go
## into the regular Herwig infile

from Herwig7_i.Herwig7_iConf import Herwig7
from Herwig7_i.Herwig7ConfigBuiltinME import Hw7ConfigBuiltinME

genSeq += Herwig7()

generator = Hw7ConfigBuiltinME(genSeq, runArgs, run_name="HerwigBuiltinME")
generator.me_pdf_commands(order="NLO", name="MMHT2014nlo68cl")
generator.shower_pdf_commands(order="LO", name="MMHT2014lo68cl")

## ------------------
## Hard process setup
## ------------------

generator.add_commands("""
read snippets/MB.in

insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEMinBias
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0

set /Herwig/Generators/EventGenerator:EventHandler:Cuts /Herwig/Cuts/MinBiasCuts
""")

## run the generator
generator.run()



