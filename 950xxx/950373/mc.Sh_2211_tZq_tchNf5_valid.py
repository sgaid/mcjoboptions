include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")
include("Sherpa_i/Fusing_Fragmentation.py")

evgenConfig.description = "Sherpa tZq Production"
evgenConfig.keywords = ["SM", "top" ]
evgenConfig.contact = [ "atlas-generators-sherpa@cern.ch" , "shuhui.huang@cern.ch" ]
evgenConfig.nEventsPerJob = 10000
genSeq.Sherpa_i.RunCard="""
(run){

  # me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN
  EVENT_GENERATION_MODE Weighted
  LOOPGEN:=OpenLoops
  HARD_DECAYS On

  # scales, tags for scale variations
  # SCALES STRICT_METS:
  #   use CKKW clustering scale for real/MC@NLO emission
  # CORESCALE SingleTop:
  #   use Mandelstam \hat{t} for t-channel 2->2 core process
  SCF:=1.; FSF:=SCF; RSF:=SCF; QSF:=SCF
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2}
  CORE_SCALE SingleTop

  # disable hadronic W decays
  HDH_STATUS[24,2,-1] 0
  HDH_STATUS[24,4,-3] 0
  HDH_STATUS[-24,-2,1] 0
  HDH_STATUS[-24,-4,3] 0

  # choose EW Gmu input scheme
  EW_SCHEME 3

  # required for using top-quark in ME
  WIDTH[6]  0. 
  WIDTH[23] 0. 
  
}(run)

(processes){
  Process 93 93 -> 6 93 23
  NLO_QCD_Mode MC@NLO
  Order (*,3)
  ME_Generator Amegic
  RS_ME_Generator Comix
  Loop_Generator LOOPGEN
  Min_N_TChannels 1  # require t-channel W
  End process
}(processes)
"""
genSeq.Sherpa_i.NCores = 24
genSeq.Sherpa_i.OpenLoopsLibs = [ "pptzj", "pptzjj" ]
genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.", "WIDTH[23]=0.","EW_SCHEME=3" ]
