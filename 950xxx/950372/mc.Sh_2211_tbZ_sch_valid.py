# SHERPA run card for s-channel single top-quark production at MC@NLO
# and N_f = 5
# inherit from https://sherpa.hepforge.org/doc/SHERPA-MC-2.2.10.html#s_002dchannel-single_002dtop-production
include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")
include("Sherpa_i/Fusing_Fragmentation.py")

evgenConfig.description = "Sherpa tbZ Production"
evgenConfig.keywords = ["SM", "top" ]
evgenConfig.contact = [ "atlas-generators-sherpa@cern.ch" , "shuhui.huang@cern.ch" ]
evgenConfig.nEventsPerJob = 10000
genSeq.Sherpa_i.RunCard="""
(run){

  # me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN
  EVENT_GENERATION_MODE Weighted
  LOOPGEN:=OpenLoops
  HARD_DECAYS On

  # scales, tags for scale variations
  # SCALES STRICT_METS:
  #   use CKKW clustering scale for real/MC@NLO emission
  # CORESCALE SingleTop:
  #   use Mandelstam \hat{s} for s-channel 2->2 core process
  SCF:=1.; FSF:=SCF; RSF:=SCF; QSF:=SCF
  rscl2:=MPerp2(p[3])
  fscl2:=MPerp2(p[2])
  SCALES VAR{FSF*fscl2}{RSF*rscl2}{QSF*fscl2}

  # disable hadronic W decays
  HDH_STATUS[24,2,-1] 0
  HDH_STATUS[24,4,-3] 0
  HDH_STATUS[-24,-2,1] 0
  HDH_STATUS[-24,-4,3] 0

  # choose EW Gmu input scheme
  EW_SCHEME 3

  # required for using top-quark in ME
  WIDTH[6]  0.
  WIDTH[23] 0.

  # there is no bottom in the initial-state in s-channel production
  PARTICLE_CONTAINER 900 lj 1 -1 2 -2 3 -3 4 -4 21
}(run)

(processes){
  Process 900 900 -> 6 93 23
  NLO_QCD_Mode MC@NLO
  Order (*,3)
  ME_Generator Amegic
  RS_ME_Generator Comix
  Loop_Generator LOOPGEN
  Max_N_TChannels 0  # require s-channel W
  End process
}(processes)
"""
genSeq.Sherpa_i.NCores = 24
genSeq.Sherpa_i.OpenLoopsLibs = [ "pptzj", "pptzjj" ]
genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.", "WIDTH[23]=0.","EW_SCHEME=3" ]
