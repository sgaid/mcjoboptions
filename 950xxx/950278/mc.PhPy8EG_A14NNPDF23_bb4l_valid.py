evgenConfig.generators  += ["Powheg","Pythia8","EvtGen"]
evgenConfig.description  = 'Powheg bb4l test production with A14 tune'
evgenConfig.keywords    += [ 'SM', 'top', 'WW', 'lepton']
evgenConfig.contact      = [ 'Simone Amoroso <simone.amoroso@cern.ch>' ]
evgenConfig.nEventsPerJob=2000

# -------------------------------------------------------------- # Load ATLAS defaults for the Powheg bblvlv process # ----------------------------
include("PowhegControl/PowhegControl_bblvlv_Common.py")
PowhegConfig.width_t=1.3292779909405845
PowhegConfig.twidth_phsp=1.3292779909405845
PowhegConfig.hdamp=258.75 
PowhegConfig.for_reweighting=1
PowhegConfig.PDF=[303600 ,260000, 25200, 13165, 90900, 265000, 266000, 303400]

# -------------------------------------------------------------- # Generate events # --------------------------------------------------------------
PowhegConfig.generate()


################################################################
#                      adding UserHook                         #
################################################################         
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg.py")

genSeq.Pythia8.Commands += [ 'UncertaintyBands:overSampleFSR=10' ]
genSeq.Pythia8.Commands += [ 'UncertaintyBands:FSRpTmin2Fac=8' ]
genSeq.Pythia8.Commands += [ 'UncertaintyBands:overSampleISR=10' ]

if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    print 'UserHook present'
    genSeq.Pythia8.UserHooks += ['PowhegBB4Ltms']

genSeq.Pythia8.Commands += ["POWHEG:veto=1"]
genSeq.Pythia8.Commands += ["POWHEG:vetoCount = 3"]
genSeq.Pythia8.Commands += ["POWHEG:pThard = 0"]
genSeq.Pythia8.Commands += ["POWHEG:pTemt = 0"]
genSeq.Pythia8.Commands += ["POWHEG:emitted = 0"]
genSeq.Pythia8.Commands += ["POWHEG:pTdef = 1"]
genSeq.Pythia8.Commands += ["POWHEG:nFinal = -1"]
genSeq.Pythia8.Commands += ["POWHEG:MPIveto = 1"]
genSeq.Pythia8.Commands += ["POWHEG:QEDveto = 1"]

genSeq.Pythia8.Commands += ["POWHEG:bb4l:FSREmission:veto = 1"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:onlyDistance1 = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:vetoQED = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:vetoAtPL = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:dryRunFSR = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:FSREmission:vetoDipoleFrame = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:pTpythiaVeto = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:ScaleResonance:veto = 0"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:PartonLevel:veto = 0"]
#genSeq.Pythia8.Commands += ["POWHEG:bb4l:PartonLevel:excludeFSRConflicting = false"]                                                                                                                              
genSeq.Pythia8.Commands += ["POWHEG:bb4l:pTminVeto = 0.8"]
genSeq.Pythia8.Commands += ["POWHEG:bb4l:DEBUG = 0"]

include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
filtSeq.TTbarWToLeptonFilter.Ptcut = 10000.
