import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

process = """
import model loop_sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > t t~ [QCD]
output -f"""

process_dir = new_process(process)

#Fetch default NLO run_card.dat and set parameters
settings = {'parton_shower':'HERWIG6',
            'nevents':int(nevents)}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# Reweighting setup
reweight_card=process_dir+'/Cards/reweight_card.dat'
reweight_card_f = open(reweight_card,'w')
reweight_card_f.write("""change mode NLO
launch --rwgt_info=aS_1
set aS 1
launch --rwgt_info=aS_1p1
set aS 1.1
launch --rwgt_info=aS_1p2
set aS 1.2""")
reweight_card_f.close()

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  

evgenConfig.generators = ["aMcAtNlo"]

############################
# Shower JOs will go here
theApp.finalize()
theApp.exit()
