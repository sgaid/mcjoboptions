# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 08.09.2021,  00:25
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    8.46188107E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.31660690E+03  # scale for input parameters
    1    4.62631380E+01  # M_1
    2    1.57984169E+03  # M_2
    3    3.25225440E+03  # M_3
   11    4.42854055E+02  # A_t
   12   -1.79153949E+03  # A_b
   13    1.83342768E+03  # A_tau
   23    2.22697742E+02  # mu
   25    8.05134518E+00  # tan(beta)
   26    4.16082335E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.54645457E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.93266813E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.56443024E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.31660690E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.31660690E+03  # (SUSY scale)
  1  1     8.44879475E-06   # Y_u(Q)^DRbar
  2  2     4.29198773E-03   # Y_c(Q)^DRbar
  3  3     1.02068227E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.31660690E+03  # (SUSY scale)
  1  1     1.36692112E-04   # Y_d(Q)^DRbar
  2  2     2.59715013E-03   # Y_s(Q)^DRbar
  3  3     1.35555742E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.31660690E+03  # (SUSY scale)
  1  1     2.38537731E-05   # Y_e(Q)^DRbar
  2  2     4.93220374E-03   # Y_mu(Q)^DRbar
  3  3     8.29515530E-02   # Y_tau(Q)^DRbar
Block Au Q=  3.31660690E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     4.42854058E+02   # A_t(Q)^DRbar
Block Ad Q=  3.31660690E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.79153941E+03   # A_b(Q)^DRbar
Block Ae Q=  3.31660690E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.83342767E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.31660690E+03  # soft SUSY breaking masses at Q
   1    4.62631380E+01  # M_1
   2    1.57984169E+03  # M_2
   3    3.25225440E+03  # M_3
  21    1.69423410E+07  # M^2_(H,d)
  22    4.28945324E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.54645457E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.93266813E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.56443024E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.19141046E+02  # h0
        35     4.16054152E+03  # H0
        36     4.16082335E+03  # A0
        37     4.16333515E+03  # H+
   1000001     1.01004225E+04  # ~d_L
   2000001     1.00769836E+04  # ~d_R
   1000002     1.01000628E+04  # ~u_L
   2000002     1.00800741E+04  # ~u_R
   1000003     1.01004234E+04  # ~s_L
   2000003     1.00769841E+04  # ~s_R
   1000004     1.01000637E+04  # ~c_L
   2000004     1.00800754E+04  # ~c_R
   1000005     2.69957331E+03  # ~b_1
   2000005     3.64285934E+03  # ~b_2
   1000006     3.01788750E+03  # ~t_1
   2000006     3.64489442E+03  # ~t_2
   1000011     1.00201081E+04  # ~e_L-
   2000011     1.00089324E+04  # ~e_R-
   1000012     1.00193513E+04  # ~nu_eL
   1000013     1.00201092E+04  # ~mu_L-
   2000013     1.00089346E+04  # ~mu_R-
   1000014     1.00193525E+04  # ~nu_muL
   1000015     1.00095890E+04  # ~tau_1-
   2000015     1.00204446E+04  # ~tau_2-
   1000016     1.00196870E+04  # ~nu_tauL
   1000021     3.68956330E+03  # ~g
   1000022     4.25454683E+01  # ~chi_10
   1000023     2.38829487E+02  # ~chi_20
   1000025     2.38977189E+02  # ~chi_30
   1000035     1.68613505E+03  # ~chi_40
   1000024     2.33745206E+02  # ~chi_1+
   1000037     1.68610154E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.17781454E-01   # alpha
Block Hmix Q=  3.31660690E+03  # Higgs mixing parameters
   1    2.22697742E+02  # mu
   2    8.05134518E+00  # tan[beta](Q)
   3    2.43166298E+02  # v(Q)
   4    1.73124509E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -6.88006742E-03   # Re[R_st(1,1)]
   1  2     9.99976332E-01   # Re[R_st(1,2)]
   2  1    -9.99976332E-01   # Re[R_st(2,1)]
   2  2    -6.88006742E-03   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     1.37361116E-03   # Re[R_sb(1,1)]
   1  2     9.99999057E-01   # Re[R_sb(1,2)]
   2  1    -9.99999057E-01   # Re[R_sb(2,1)]
   2  2     1.37361116E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -8.22313502E-04   # Re[R_sta(1,1)]
   1  2     9.99999662E-01   # Re[R_sta(1,2)]
   2  1    -9.99999662E-01   # Re[R_sta(2,1)]
   2  2    -8.22313502E-04   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.78674600E-01   # Re[N(1,1)]
   1  2     3.95269773E-03   # Re[N(1,2)]
   1  3    -1.96882340E-01   # Re[N(1,3)]
   1  4     5.84615110E-02   # Re[N(1,4)]
   2  1     1.80828671E-01   # Re[N(2,1)]
   2  2     4.31792584E-02   # Re[N(2,2)]
   2  3    -6.90413806E-01   # Re[N(2,3)]
   2  4     6.99117530E-01   # Re[N(2,4)]
   3  1     9.74424858E-02   # Re[N(3,1)]
   3  2    -2.60028037E-02   # Re[N(3,2)]
   3  3    -6.95995454E-01   # Re[N(3,3)]
   3  4    -7.10928367E-01   # Re[N(3,4)]
   4  1     1.40766550E-03   # Re[N(4,1)]
   4  2    -9.98721073E-01   # Re[N(4,2)]
   4  3    -1.25079360E-02   # Re[N(4,3)]
   4  4     4.89672134E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.76770076E-02   # Re[U(1,1)]
   1  2     9.99843749E-01   # Re[U(1,2)]
   2  1     9.99843749E-01   # Re[U(2,1)]
   2  2     1.76770076E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -6.92674037E-02   # Re[V(1,1)]
   1  2     9.97598129E-01   # Re[V(1,2)]
   2  1     9.97598129E-01   # Re[V(2,1)]
   2  2     6.92674037E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02870668E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.57848638E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     3.26644873E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.48500136E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.37521264E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.63408568E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     6.25911004E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.17433905E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01570954E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     2.00207530E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.05411437E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02967549E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.57668082E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     3.27040576E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     9.52977514E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
DECAY   1000013     1.37526125E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.63391815E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     6.27567779E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.34487315E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01560338E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     2.00200477E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.05390115E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.30278562E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.09377002E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     4.32905591E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     2.15114485E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.57976580E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     1.38896545E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.58813670E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.08960518E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     4.98879416E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.98596383E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.00537905E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.99436866E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.37524593E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.89829380E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     9.14171639E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     1.95168419E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02447786E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.07381313E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02629607E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.37529454E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.89798030E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     9.14139432E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     1.95161543E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02437135E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.10891219E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.02608395E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.38899793E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.81048995E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     9.05151124E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.93242610E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.99464554E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.29044406E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.96688528E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.02069646E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.94966887E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     3.05208460E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.90656471E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.26147975E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.94472307E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.14951247E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.77396922E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.15681341E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.24473698E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.02072388E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.94971535E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     3.06273554E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.90652039E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.26153120E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.94478160E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.15851117E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.77392891E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.15680556E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.24467974E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     3.44144558E+00   # ~b_1
#    BR                NDA      ID1      ID2
     4.30637623E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.48581089E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.40701068E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.80061179E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     1.06081366E+02   # ~b_2
#    BR                NDA      ID1      ID2
     5.27402355E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     5.95494490E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     6.31737379E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     9.31332158E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     6.99280940E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.89531998E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     4.41734067E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     6.19249068E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.48162106E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.18731838E-03    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     3.44769409E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.63651634E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.26132226E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.77600652E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.47487116E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.76807145E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     5.86864473E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.15160217E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.24444134E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.19256542E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.48158138E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.19021733E-03    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     3.47776483E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.63640153E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.26137340E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.77601154E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.49972956E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.76803256E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     5.88719085E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.15159418E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.24438403E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.29607020E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.09697560E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.33547928E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.39408556E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     6.13789183E-04    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.74218406E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.24155155E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.06348198E+02   # ~t_2
#    BR                NDA      ID1      ID2
     6.92021218E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     3.43082839E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     3.54519427E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     9.42387746E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.43256767E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.85226766E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.20725428E-04    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.41239509E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.47575837E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99997250E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     1.60659244E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     6.86537075E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.42605501E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.45277447E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.43288387E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.41592162E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.10026840E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     8.34252614E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     7.17330320E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.08976429E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     3.74450946E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     6.25544815E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     1.91263447E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     7.54504825E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.45455795E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.68425441E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.33158760E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.33158760E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     5.98586272E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.72984405E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     6.87204440E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.68629176E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.07188356E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.67092240E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     4.20001449E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     7.71358303E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     7.71358303E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     3.57440001E+01   # ~g
#    BR                NDA      ID1      ID2
     1.63322098E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.63322098E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     3.29815482E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     3.29815482E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     9.63206011E-04    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     9.63206011E-04    2    -2000005         5   # BR(~g -> ~b^*_2 b)
     1.33852884E-04    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.34429502E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     4.28796540E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     4.45551060E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     9.60543485E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     7.56126155E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     7.56126155E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.56984986E-03   # Gamma(h0)
     2.10038841E-03   2        22        22   # BR(h0 -> photon photon)
     9.22655882E-04   2        22        23   # BR(h0 -> photon Z)
     1.38585724E-02   2        23        23   # BR(h0 -> Z Z)
     1.28721799E-01   2       -24        24   # BR(h0 -> W W)
     6.86532461E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.60750794E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.04947215E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.90863285E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.32224519E-07   2        -2         2   # BR(h0 -> Up up)
     2.56588714E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.37916286E-07   2        -1         1   # BR(h0 -> Down down)
     1.94551009E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.16832653E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     1.83765313E-01   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     4.74121709E+01   # Gamma(HH)
     9.58767360E-08   2        22        22   # BR(HH -> photon photon)
     7.55129425E-08   2        22        23   # BR(HH -> photon Z)
     5.07115546E-06   2        23        23   # BR(HH -> Z Z)
     1.02369140E-06   2       -24        24   # BR(HH -> W W)
     5.36407911E-06   2        21        21   # BR(HH -> gluon gluon)
     8.51093981E-10   2       -11        11   # BR(HH -> Electron electron)
     3.78981188E-05   2       -13        13   # BR(HH -> Muon muon)
     1.09459991E-02   2       -15        15   # BR(HH -> Tau tau)
     2.71739122E-12   2        -2         2   # BR(HH -> Up up)
     5.27193347E-07   2        -4         4   # BR(HH -> Charm charm)
     3.90911651E-02   2        -6         6   # BR(HH -> Top top)
     6.01180061E-08   2        -1         1   # BR(HH -> Down down)
     2.17450534E-05   2        -3         3   # BR(HH -> Strange strange)
     5.62133159E-02   2        -5         5   # BR(HH -> Bottom bottom)
     3.57625559E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.62070090E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     2.62070090E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.92610242E-05   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     7.40490375E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     3.38610144E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     5.52558804E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     9.24128143E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     7.28632941E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     2.88146016E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     9.01061755E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     2.74464414E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     1.63677241E-01   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     9.74669211E-06   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     3.09819039E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     4.71542551E+01   # Gamma(A0)
     1.34310163E-07   2        22        22   # BR(A0 -> photon photon)
     1.18284724E-07   2        22        23   # BR(A0 -> photon Z)
     1.51416358E-05   2        21        21   # BR(A0 -> gluon gluon)
     8.24695865E-10   2       -11        11   # BR(A0 -> Electron electron)
     3.67226649E-05   2       -13        13   # BR(A0 -> Muon muon)
     1.06065050E-02   2       -15        15   # BR(A0 -> Tau tau)
     2.50200293E-12   2        -2         2   # BR(A0 -> Up up)
     4.85391082E-07   2        -4         4   # BR(A0 -> Charm charm)
     3.62163242E-02   2        -6         6   # BR(A0 -> Top top)
     5.82527963E-08   2        -1         1   # BR(A0 -> Down down)
     2.10703467E-05   2        -3         3   # BR(A0 -> Strange strange)
     5.44688018E-02   2        -5         5   # BR(A0 -> Bottom bottom)
     4.10099571E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.63122572E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     2.63122572E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     3.48314333E-04   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     8.59572466E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     5.57822230E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     3.31050715E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.02621009E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.19954871E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.14457654E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.61795334E-01   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.71758309E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     9.21520666E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     1.74717866E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     1.12358062E-05   2        23        25   # BR(A0 -> Z h0)
     1.16674200E-40   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     4.82924707E+01   # Gamma(Hp)
     1.02083711E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     4.36439747E-05   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.23450020E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     6.26837692E-08   2        -1         2   # BR(Hp -> Down up)
     1.06189247E-06   2        -3         2   # BR(Hp -> Strange up)
     6.76539108E-07   2        -5         2   # BR(Hp -> Bottom up)
     3.02717813E-08   2        -1         4   # BR(Hp -> Down charm)
     2.31716404E-05   2        -3         4   # BR(Hp -> Strange charm)
     9.47406033E-05   2        -5         4   # BR(Hp -> Bottom charm)
     3.00004760E-06   2        -1         6   # BR(Hp -> Down top)
     6.54446525E-05   2        -3         6   # BR(Hp -> Strange top)
     1.09772409E-01   2        -5         6   # BR(Hp -> Bottom top)
     9.65080748E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.91573245E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     5.56783643E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.47874023E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     6.14921900E-05   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     2.50637257E-01   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     2.57833752E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     3.91090542E-10   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.09944563E-05   2        24        25   # BR(Hp -> W h0)
     4.79745010E-12   2        24        35   # BR(Hp -> W HH)
     2.81889231E-12   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.08929016E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    6.49152302E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    6.48241592E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00140489E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.40214548E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.54263474E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.08929016E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    6.49152302E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    6.48241592E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99966493E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    3.35072306E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99966493E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    3.35072306E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.27200230E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.02241726E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.40856187E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    3.35072306E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99966493E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.22973661E-04   # BR(b -> s gamma)
    2    1.58960485E-06   # BR(b -> s mu+ mu-)
    3    3.52440690E-05   # BR(b -> s nu nu)
    4    2.55843415E-15   # BR(Bd -> e+ e-)
    5    1.09293402E-10   # BR(Bd -> mu+ mu-)
    6    2.28794851E-08   # BR(Bd -> tau+ tau-)
    7    8.61679158E-14   # BR(Bs -> e+ e-)
    8    3.68109062E-09   # BR(Bs -> mu+ mu-)
    9    7.80791191E-07   # BR(Bs -> tau+ tau-)
   10    9.67836320E-05   # BR(B_u -> tau nu)
   11    9.99737865E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42372486E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93792788E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15886547E-03   # epsilon_K
   17    2.28167388E-15   # Delta(M_K)
   18    2.47921013E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28865461E-11   # BR(K^+ -> pi^+ nu nu)
   20    7.80832515E-17   # Delta(g-2)_electron/2
   21    3.33830421E-12   # Delta(g-2)_muon/2
   22    9.44362444E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -9.33305348E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.91468540E-01   # C7
     0305 4322   00   2    -7.04823122E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.00002239E-01   # C8
     0305 6321   00   2    -1.02116547E-04   # C8'
 03051111 4133   00   0     1.61967351E+00   # C9 e+e-
 03051111 4133   00   2     1.62026700E+00   # C9 e+e-
 03051111 4233   00   2     1.59037967E-05   # C9' e+e-
 03051111 4137   00   0    -4.44236561E+00   # C10 e+e-
 03051111 4137   00   2    -4.43985512E+00   # C10 e+e-
 03051111 4237   00   2    -1.17906801E-04   # C10' e+e-
 03051313 4133   00   0     1.61967351E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62026699E+00   # C9 mu+mu-
 03051313 4233   00   2     1.59037963E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44236561E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43985512E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.17906801E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50480741E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     2.55350744E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50480741E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     2.55350744E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50480741E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     2.55350858E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85822342E-07   # C7
     0305 4422   00   2     2.74906639E-06   # C7
     0305 4322   00   2     1.21228611E-07   # C7'
     0305 6421   00   0     3.30482352E-07   # C8
     0305 6421   00   2    -1.05532330E-06   # C8
     0305 6321   00   2    -5.84475060E-09   # C8'
 03051111 4133   00   2     2.71294280E-07   # C9 e+e-
 03051111 4233   00   2     3.03163272E-07   # C9' e+e-
 03051111 4137   00   2    -2.27114886E-07   # C10 e+e-
 03051111 4237   00   2    -2.24885471E-06   # C10' e+e-
 03051313 4133   00   2     2.71294181E-07   # C9 mu+mu-
 03051313 4233   00   2     3.03163268E-07   # C9' mu+mu-
 03051313 4137   00   2    -2.27114786E-07   # C10 mu+mu-
 03051313 4237   00   2    -2.24885472E-06   # C10' mu+mu-
 03051212 4137   00   2     6.71174796E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     4.87035760E-07   # C11' nu_1 nu_1
 03051414 4137   00   2     6.71174838E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     4.87035760E-07   # C11' nu_2 nu_2
 03051616 4137   00   2     6.71186611E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     4.87035759E-07   # C11' nu_3 nu_3
