# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  20:02
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.66239111E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.99347131E+03  # scale for input parameters
    1   -4.66829048E+01  # M_1
    2   -1.39014411E+03  # M_2
    3    2.16292947E+03  # M_3
   11   -3.45607330E+03  # A_t
   12    1.86849005E+03  # A_b
   13   -8.42997267E+02  # A_tau
   23   -2.50237626E+02  # mu
   25    2.56304774E+01  # tan(beta)
   26    1.35155393E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.66184980E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.39080393E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.94322184E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.99347131E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.99347131E+03  # (SUSY scale)
  1  1     8.39075127E-06   # Y_u(Q)^DRbar
  2  2     4.26250165E-03   # Y_c(Q)^DRbar
  3  3     1.01367015E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.99347131E+03  # (SUSY scale)
  1  1     4.32153257E-04   # Y_d(Q)^DRbar
  2  2     8.21091188E-03   # Y_s(Q)^DRbar
  3  3     4.28560614E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.99347131E+03  # (SUSY scale)
  1  1     7.54139035E-05   # Y_e(Q)^DRbar
  2  2     1.55932034E-02   # Y_mu(Q)^DRbar
  3  3     2.62252030E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.99347131E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -3.45607621E+03   # A_t(Q)^DRbar
Block Ad Q=  2.99347131E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.86849025E+03   # A_b(Q)^DRbar
Block Ae Q=  2.99347131E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -8.42997032E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  2.99347131E+03  # soft SUSY breaking masses at Q
   1   -4.66829048E+01  # M_1
   2   -1.39014411E+03  # M_2
   3    2.16292947E+03  # M_3
  21    1.63617749E+06  # M^2_(H,d)
  22    9.13318615E+04  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.66184980E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.39080393E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.94322184E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23588766E+02  # h0
        35     1.35156090E+03  # H0
        36     1.35155393E+03  # A0
        37     1.35573651E+03  # H+
   1000001     1.01117451E+04  # ~d_L
   2000001     1.00878843E+04  # ~d_R
   1000002     1.01113830E+04  # ~u_L
   2000002     1.00911447E+04  # ~u_R
   1000003     1.01117487E+04  # ~s_L
   2000003     1.00878899E+04  # ~s_R
   1000004     1.01113867E+04  # ~c_L
   2000004     1.00911463E+04  # ~c_R
   1000005     3.69211361E+03  # ~b_1
   2000005     3.99283513E+03  # ~b_2
   1000006     2.42276372E+03  # ~t_1
   2000006     3.69861510E+03  # ~t_2
   1000011     1.00202933E+04  # ~e_L-
   2000011     1.00088373E+04  # ~e_R-
   1000012     1.00195310E+04  # ~nu_eL
   1000013     1.00203080E+04  # ~mu_L-
   2000013     1.00088657E+04  # ~mu_R-
   1000014     1.00195456E+04  # ~nu_muL
   1000015     1.00169037E+04  # ~tau_1-
   2000015     1.00244765E+04  # ~tau_2-
   1000016     1.00236788E+04  # ~nu_tauL
   1000021     2.57080781E+03  # ~g
   1000022     4.50217134E+01  # ~chi_10
   1000023     2.61915823E+02  # ~chi_20
   1000025     2.64113044E+02  # ~chi_30
   1000035     1.48701570E+03  # ~chi_40
   1000024     2.58623721E+02  # ~chi_1+
   1000037     1.48697967E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.80549203E-02   # alpha
Block Hmix Q=  2.99347131E+03  # Higgs mixing parameters
   1   -2.50237626E+02  # mu
   2    2.56304774E+01  # tan[beta](Q)
   3    2.43243919E+02  # v(Q)
   4    1.82669803E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     6.43030773E-02   # Re[R_st(1,1)]
   1  2     9.97930416E-01   # Re[R_st(1,2)]
   2  1    -9.97930416E-01   # Re[R_st(2,1)]
   2  2     6.43030773E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99969391E-01   # Re[R_sb(1,1)]
   1  2     7.82418000E-03   # Re[R_sb(1,2)]
   2  1    -7.82418000E-03   # Re[R_sb(2,1)]
   2  2    -9.99969391E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -6.57397867E-02   # Re[R_sta(1,1)]
   1  2     9.97836801E-01   # Re[R_sta(1,2)]
   2  1    -9.97836801E-01   # Re[R_sta(2,1)]
   2  2    -6.57397867E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.83410876E-01   # Re[N(1,1)]
   1  2    -2.38344975E-03   # Re[N(1,2)]
   1  3    -1.77468095E-01   # Re[N(1,3)]
   1  4     3.74492187E-02   # Re[N(1,4)]
   2  1     1.52241692E-01   # Re[N(2,1)]
   2  2     4.76161983E-02   # Re[N(2,2)]
   2  3     6.95049639E-01   # Re[N(2,3)]
   2  4    -7.01042912E-01   # Re[N(2,4)]
   3  1    -9.86013131E-02   # Re[N(3,1)]
   3  2     3.13608144E-02   # Re[N(3,2)]
   3  3    -6.96616387E-01   # Re[N(3,3)]
   3  4    -7.09943582E-01   # Re[N(3,4)]
   4  1     1.81600201E-03   # Re[N(4,1)]
   4  2    -9.98370430E-01   # Re[N(4,2)]
   4  3     1.16912022E-02   # Re[N(4,3)]
   4  4    -5.58256374E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.65316653E-02   # Re[U(1,1)]
   1  2    -9.99863343E-01   # Re[U(1,2)]
   2  1    -9.99863343E-01   # Re[U(2,1)]
   2  2     1.65316653E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     7.89745012E-02   # Re[V(1,1)]
   1  2     9.96876636E-01   # Re[V(1,2)]
   2  1     9.96876636E-01   # Re[V(2,1)]
   2  2    -7.89745012E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02865018E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.67139541E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     2.31477671E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.70951538E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.39146818E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.66871156E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     5.26306215E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.43797149E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01582998E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.73019364E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.06150008E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03833430E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.65313617E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     2.35669178E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.01565402E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     9.59400362E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.39195443E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.66679263E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     5.42922286E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     3.12492122E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01478171E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.72959177E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.05939228E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     7.80312228E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     6.31141070E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     9.86741931E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     8.89368221E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.88647364E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.75570140E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     3.79130071E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.52577396E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.09774745E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     4.86781302E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     4.49446457E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.74220165E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.51128350E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.39150657E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.82654666E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.59752573E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     2.23505856E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02724048E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.94812660E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02467548E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.39199277E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.82347653E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.59627442E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     2.23428115E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02618791E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     4.29437155E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.02258163E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.52909522E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.03565557E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     3.27517777E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     2.03479136E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.75609121E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     9.31435593E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.48528455E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.01834419E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.76033060E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.85767243E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.91975887E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.27532512E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.69448738E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.28553137E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.13474183E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.02935722E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.43841853E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.01861865E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.76063691E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.95034748E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.91937798E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.27550035E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.69496682E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.36470561E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.13463529E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.02933609E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.43824368E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.25776728E+02   # ~b_1
#    BR                NDA      ID1      ID2
     4.24506289E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.85823823E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.88102687E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     5.03814861E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.27471462E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.02906329E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     3.42556480E-01    2     1000021         5   # BR(~b_1 -> ~g b)
     1.15046529E-01    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     1.39182466E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.86168047E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     5.06122965E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     5.06809611E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.03529993E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     7.76159121E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.66686425E-04    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
DECAY   2000002     7.19023714E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.03090219E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     7.25439289E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     3.04291212E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.68661149E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.27519867E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.60466311E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     2.99916686E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.12797185E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     6.70038907E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.02319254E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.43816177E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.19031110E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.03087209E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     7.27921039E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     3.06841832E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.68651337E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.27537367E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.60463589E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.02078450E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.12786635E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     6.86341889E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.02317136E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.43798690E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.00803807E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.12868949E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.34259447E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.37927347E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.34258618E-03    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.72394263E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.78905379E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.29880911E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.07844146E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.59489423E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.64684681E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     4.84901668E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     6.23217627E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     9.70055116E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.28280349E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     5.69177864E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     7.97318776E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.78262282E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99967099E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     1.35429392E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     2.45013965E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.44302019E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     3.81321604E-04    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     2.43294473E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.42287369E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     8.96364701E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     7.31293515E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     7.22265243E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     7.60246902E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.27027503E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     4.43280699E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     5.56693515E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.03444018E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     6.79588781E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.20401966E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.46013356E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.27731257E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.27731257E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     6.73047809E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.59512532E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.23316483E-04    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     1.57389533E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.36533017E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     2.52204230E-04    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     3.82686813E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     3.21876598E-04    3     1000023         5        -5   # BR(chi^0_4 -> chi^0_2 b b_bar)
     3.62306246E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     3.52197815E-04    3     1000025         5        -5   # BR(chi^0_4 -> chi^0_3 b b_bar)
     7.52155996E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     7.52155996E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.09700726E+00   # ~g
#    BR                NDA      ID1      ID2
     2.10111148E-04    2     1000022        21   # BR(~g -> chi^0_1 g)
     1.54060738E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.91055591E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     4.59850016E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     5.63977925E-04    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     2.27261815E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     2.85191113E-03    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     2.35417100E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     2.86149874E-03    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     6.64051151E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     4.81752765E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.39212032E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.39212032E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     8.67290243E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     8.67290243E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.59920000E-03   # Gamma(h0)
     2.41217621E-03   2        22        22   # BR(h0 -> photon photon)
     1.41976783E-03   2        22        23   # BR(h0 -> photon Z)
     2.49312174E-02   2        23        23   # BR(h0 -> Z Z)
     2.11857495E-01   2       -24        24   # BR(h0 -> W W)
     7.56352221E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.86664909E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.16476514E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.24161492E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.35400556E-07   2        -2         2   # BR(h0 -> Up up)
     2.62776155E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.64493079E-07   2        -1         1   # BR(h0 -> Down down)
     2.04163756E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.43008346E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     5.16206658E-02   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     1.39401179E+01   # Gamma(HH)
     8.07866061E-08   2        22        22   # BR(HH -> photon photon)
     7.78816265E-08   2        22        23   # BR(HH -> photon Z)
     1.09039940E-05   2        23        23   # BR(HH -> Z Z)
     1.00572789E-05   2       -24        24   # BR(HH -> W W)
     2.59247109E-05   2        21        21   # BR(HH -> gluon gluon)
     9.66835695E-09   2       -11        11   # BR(HH -> Electron electron)
     4.30374892E-04   2       -13        13   # BR(HH -> Muon muon)
     1.24281371E-01   2       -15        15   # BR(HH -> Tau tau)
     3.25224375E-13   2        -2         2   # BR(HH -> Up up)
     6.30779097E-08   2        -4         4   # BR(HH -> Charm charm)
     4.61497397E-03   2        -6         6   # BR(HH -> Top top)
     7.89251830E-07   2        -1         1   # BR(HH -> Down down)
     2.85467609E-04   2        -3         3   # BR(HH -> Strange strange)
     7.59381246E-01   2        -5         5   # BR(HH -> Bottom bottom)
     4.49051805E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     6.77875614E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     4.42390053E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     5.04017456E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     2.58811461E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     2.30227584E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     2.41681573E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     7.07311473E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.36666174E+01   # Gamma(A0)
     2.68155135E-07   2        22        22   # BR(A0 -> photon photon)
     1.32904426E-07   2        22        23   # BR(A0 -> photon Z)
     6.93313679E-05   2        21        21   # BR(A0 -> gluon gluon)
     9.62996217E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.28665790E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.23788736E-01   2       -15        15   # BR(A0 -> Tau tau)
     3.14054319E-13   2        -2         2   # BR(A0 -> Up up)
     6.09075592E-08   2        -4         4   # BR(A0 -> Charm charm)
     4.79364405E-03   2        -6         6   # BR(A0 -> Top top)
     7.86117875E-07   2        -1         1   # BR(A0 -> Down down)
     2.84334178E-04   2        -3         3   # BR(A0 -> Strange strange)
     7.56379494E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     5.47514193E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     7.17454040E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     5.40127670E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     4.25875689E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     3.57137053E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     2.16923096E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     2.46318786E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.49628835E-05   2        23        25   # BR(A0 -> Z h0)
     5.00449367E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.48162986E+01   # Gamma(Hp)
     1.07260531E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.58572265E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.29709959E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     7.62447567E-07   2        -1         2   # BR(Hp -> Down up)
     1.27451516E-05   2        -3         2   # BR(Hp -> Strange up)
     8.19638348E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.85836321E-08   2        -1         4   # BR(Hp -> Down charm)
     2.74840880E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.14778544E-03   2        -5         4   # BR(Hp -> Bottom charm)
     3.53963061E-07   2        -1         6   # BR(Hp -> Down top)
     8.10997514E-06   2        -3         6   # BR(Hp -> Strange top)
     7.66547831E-01   2        -5         6   # BR(Hp -> Bottom top)
     9.75343768E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     4.27742981E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     5.00931366E-06   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.39772389E-05   2        24        25   # BR(Hp -> W h0)
     1.16333166E-10   2        24        35   # BR(Hp -> W HH)
     1.17306937E-10   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.52326726E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    6.56969045E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    6.56921372E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00007257E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.44968145E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.52225219E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.52326726E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    6.56969045E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    6.56921372E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999114E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    8.86141099E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999114E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    8.86141099E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26340582E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    5.03273773E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.11802226E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    8.86141099E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999114E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.57002780E-04   # BR(b -> s gamma)
    2    1.58665638E-06   # BR(b -> s mu+ mu-)
    3    3.52347108E-05   # BR(b -> s nu nu)
    4    1.96263672E-15   # BR(Bd -> e+ e-)
    5    8.38413155E-11   # BR(Bd -> mu+ mu-)
    6    1.75339898E-08   # BR(Bd -> tau+ tau-)
    7    6.62541831E-14   # BR(Bs -> e+ e-)
    8    2.83036689E-09   # BR(Bs -> mu+ mu-)
    9    5.99771329E-07   # BR(Bs -> tau+ tau-)
   10    9.44773003E-05   # BR(B_u -> tau nu)
   11    9.75914342E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42622249E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93763206E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16018613E-03   # epsilon_K
   17    2.28168988E-15   # Delta(M_K)
   18    2.47915730E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28832758E-11   # BR(K^+ -> pi^+ nu nu)
   20    2.73042162E-16   # Delta(g-2)_electron/2
   21    1.16734306E-11   # Delta(g-2)_muon/2
   22    3.30390686E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.30378529E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.27790046E-01   # C7
     0305 4322   00   2    -5.26097066E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.30506892E-01   # C8
     0305 6321   00   2    -5.69972435E-04   # C8'
 03051111 4133   00   0     1.61638913E+00   # C9 e+e-
 03051111 4133   00   2     1.61690927E+00   # C9 e+e-
 03051111 4233   00   2     1.04384992E-04   # C9' e+e-
 03051111 4137   00   0    -4.43908124E+00   # C10 e+e-
 03051111 4137   00   2    -4.43567687E+00   # C10 e+e-
 03051111 4237   00   2    -7.83151892E-04   # C10' e+e-
 03051313 4133   00   0     1.61638913E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61690917E+00   # C9 mu+mu-
 03051313 4233   00   2     1.04384001E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.43908124E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43567697E+00   # C10 mu+mu-
 03051313 4237   00   2    -7.83150984E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50461338E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.69739101E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50461338E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.69739311E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50461338E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.69798590E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85813673E-07   # C7
     0305 4422   00   2     4.89810430E-06   # C7
     0305 4322   00   2    -1.33550561E-06   # C7'
     0305 6421   00   0     3.30474927E-07   # C8
     0305 6421   00   2     1.06831552E-05   # C8
     0305 6321   00   2    -4.06380801E-07   # C8'
 03051111 4133   00   2    -2.69338292E-07   # C9 e+e-
 03051111 4233   00   2     2.94439514E-06   # C9' e+e-
 03051111 4137   00   2     4.37101288E-06   # C10 e+e-
 03051111 4237   00   2    -2.21053153E-05   # C10' e+e-
 03051313 4133   00   2    -2.69339119E-07   # C9 mu+mu-
 03051313 4233   00   2     2.94439476E-06   # C9' mu+mu-
 03051313 4137   00   2     4.37101405E-06   # C10 mu+mu-
 03051313 4237   00   2    -2.21053164E-05   # C10' mu+mu-
 03051212 4137   00   2    -9.27357402E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     4.79107883E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -9.27357359E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     4.79107883E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -9.27345417E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     4.79107882E-06   # C11' nu_3 nu_3
