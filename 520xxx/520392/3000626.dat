# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  15:19
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.82917528E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.97143399E+03  # scale for input parameters
    1   -1.72960708E+02  # M_1
    2   -1.06847228E+03  # M_2
    3    4.37045942E+03  # M_3
   11    5.04389870E+03  # A_t
   12    1.18738499E+03  # A_b
   13    2.55281688E+02  # A_tau
   23   -1.91959805E+02  # mu
   25    1.74367382E+01  # tan(beta)
   26    1.04484219E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.62400612E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.27023993E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.16221307E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.97143399E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.97143399E+03  # (SUSY scale)
  1  1     8.39814909E-06   # Y_u(Q)^DRbar
  2  2     4.26625974E-03   # Y_c(Q)^DRbar
  3  3     1.01456387E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.97143399E+03  # (SUSY scale)
  1  1     2.94258539E-04   # Y_d(Q)^DRbar
  2  2     5.59091225E-03   # Y_s(Q)^DRbar
  3  3     2.91812264E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.97143399E+03  # (SUSY scale)
  1  1     5.13502669E-05   # Y_e(Q)^DRbar
  2  2     1.06176066E-02   # Y_mu(Q)^DRbar
  3  3     1.78570676E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.97143399E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     5.04389871E+03   # A_t(Q)^DRbar
Block Ad Q=  3.97143399E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.18738500E+03   # A_b(Q)^DRbar
Block Ae Q=  3.97143399E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     2.55281688E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.97143399E+03  # soft SUSY breaking masses at Q
   1   -1.72960708E+02  # M_1
   2   -1.06847228E+03  # M_2
   3    4.37045942E+03  # M_3
  21   -6.84379896E+04  # M^2_(H,d)
  22    2.64102140E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.62400612E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.27023993E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.16221307E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.03376783E+02  # h0
        35     1.27219484E+02  # H0
        36     1.04484219E+02  # A0
        37     1.71060848E+02  # H+
   1000001     1.00995708E+04  # ~d_L
   2000001     1.00752587E+04  # ~d_R
   1000002     1.00992087E+04  # ~u_L
   2000002     1.00783841E+04  # ~u_R
   1000003     1.00995716E+04  # ~s_L
   2000003     1.00752598E+04  # ~s_R
   1000004     1.00992096E+04  # ~c_L
   2000004     1.00783847E+04  # ~c_R
   1000005     2.34769603E+03  # ~b_1
   2000005     4.71133282E+03  # ~b_2
   1000006     3.34369557E+03  # ~t_1
   2000006     4.71702271E+03  # ~t_2
   1000011     1.00210497E+04  # ~e_L-
   2000011     1.00089199E+04  # ~e_R-
   1000012     1.00202851E+04  # ~nu_eL
   1000013     1.00210525E+04  # ~mu_L-
   2000013     1.00089253E+04  # ~mu_R-
   1000014     1.00202879E+04  # ~nu_muL
   1000015     1.00104352E+04  # ~tau_1-
   2000015     1.00218400E+04  # ~tau_2-
   1000016     1.00210658E+04  # ~nu_tauL
   1000021     4.84746771E+03  # ~g
   1000022     1.54703321E+02  # ~chi_10
   1000023     2.14215568E+02  # ~chi_20
   1000025     2.27263460E+02  # ~chi_30
   1000035     1.15186893E+03  # ~chi_40
   1000024     2.08227128E+02  # ~chi_1+
   1000037     1.15108799E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.36412075E+00   # alpha
Block Hmix Q=  3.97143399E+03  # Higgs mixing parameters
   1   -1.91959805E+02  # mu
   2    1.74367382E+01  # tan[beta](Q)
   3    2.43014001E+02  # v(Q)
   4    1.09169520E+04  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -6.22814383E-02   # Re[R_st(1,1)]
   1  2     9.98058627E-01   # Re[R_st(1,2)]
   2  1    -9.98058627E-01   # Re[R_st(2,1)]
   2  2    -6.22814383E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -5.74162501E-04   # Re[R_sb(1,1)]
   1  2     9.99999835E-01   # Re[R_sb(1,2)]
   2  1    -9.99999835E-01   # Re[R_sb(2,1)]
   2  2    -5.74162501E-04   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -2.74789226E-02   # Re[R_sta(1,1)]
   1  2     9.99622383E-01   # Re[R_sta(1,2)]
   2  1    -9.99622383E-01   # Re[R_sta(2,1)]
   2  2    -2.74789226E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     8.37625041E-01   # Re[N(1,1)]
   1  2    -2.85229139E-02   # Re[N(1,2)]
   1  3    -4.33704367E-01   # Re[N(1,3)]
   1  4     3.30864409E-01   # Re[N(1,4)]
   2  1    -7.75683478E-02   # Re[N(2,1)]
   2  2     3.98134309E-02   # Re[N(2,2)]
   2  3    -6.96088618E-01   # Re[N(2,3)]
   2  4    -7.12642041E-01   # Re[N(2,4)]
   3  1     5.40699274E-01   # Re[N(3,1)]
   3  2     5.62165819E-02   # Re[N(3,2)]
   3  3     5.71906538E-01   # Re[N(3,3)]
   3  4    -6.14334520E-01   # Re[N(3,4)]
   4  1     3.42603191E-03   # Re[N(4,1)]
   4  2    -9.97216641E-01   # Re[N(4,2)]
   4  3     1.68543788E-02   # Re[N(4,3)]
   4  4    -7.25476547E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -2.38281623E-02   # Re[U(1,1)]
   1  2    -9.99716069E-01   # Re[U(1,2)]
   2  1    -9.99716069E-01   # Re[U(2,1)]
   2  2     2.38281623E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     1.02655785E-01   # Re[V(1,1)]
   1  2     9.94716940E-01   # Re[V(1,2)]
   2  1     9.94716940E-01   # Re[V(2,1)]
   2  2    -1.02655785E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02588605E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     7.01731289E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     6.01520671E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.92242058E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.41444172E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     5.47086896E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.70934418E-02    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.00858394E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     3.53807721E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.06984843E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03037424E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     7.01189472E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     6.22586083E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.92127268E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     4.45602772E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.41466668E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     5.47298727E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.71394631E-02    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.00810683E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     3.53751558E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.06888498E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     6.30213562E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     5.81332940E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     5.27505549E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     2.63966967E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     3.07218716E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.01037463E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     6.04855826E-04    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.47742676E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     5.93267631E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.11858680E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     5.04515762E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.87952580E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.61342009E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.80921870E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.41448307E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     7.06141001E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.05663403E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     1.68574904E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.03042041E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.56609887E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.00863636E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.41470801E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     7.06028919E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.05630759E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     1.68548147E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02993946E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.72367828E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.00768362E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.47811462E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.75794941E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.96825133E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.61330495E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.90020148E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     4.92308373E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.75068220E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.75325792E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.29892165E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     3.45619177E-03    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.88173559E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.03534534E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.32427187E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     2.11639846E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.18137168E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     7.14400504E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.43426812E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.82395519E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.75338391E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.30117410E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     3.46039519E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.88147605E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.03544537E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.32618467E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     2.16659980E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.21525919E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     7.14388750E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.43424509E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.82382687E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     9.07996609E+00   # ~b_1
#    BR                NDA      ID1      ID2
     1.82287584E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.09517686E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.81846806E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.26157361E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.25545288E-04    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.93413851E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.11078661E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.02233710E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.36156059E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     9.43802162E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.90371612E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.94676795E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.73523127E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.03181998E-04    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     9.62737702E-04    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     1.00761965E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     4.92518118E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.20468459E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     2.74705455E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.33462873E-02    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.54331639E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.03516772E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.07139799E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.71121892E-03    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     7.12648468E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.55108911E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.41994793E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.82356906E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.92525440E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.20471823E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.78461308E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.33488802E-02    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.54317515E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.03526754E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.07204309E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.71347596E-03    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     7.12637001E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.56142589E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.41992464E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.82344070E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.42721051E+02   # ~t_1
#    BR                NDA      ID1      ID2
     9.29894998E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.42552764E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.88974269E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     7.13173089E-04    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.73325219E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.42552632E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.03754690E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.89191872E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.37547998E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.83952901E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     9.22840759E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.89054648E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.81182640E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.85017626E-03    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     8.28202160E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     7.78370178E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     1.24654858E-01    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     6.16542194E-04   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.33691653E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.32971391E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.11230986E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.11230157E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.10875814E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     2.00134120E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     2.30460165E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     1.30383826E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.05985259E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     4.48668599E-02    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     1.11911687E-01    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     8.44628943E-02    2          37   1000025   # BR(chi^+_2 -> H^+ chi^0_3)
     1.28556347E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.18096060E-01    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     1.39941112E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     1.04564132E-01    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     2.61455846E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     5.47267990E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     2.90388075E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     2.11994896E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     7.86433282E-04   # chi^0_2
#    BR                NDA      ID1      ID2
     4.06922316E-03    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.18420933E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.17828202E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.51838680E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.51837065E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.50010248E-01    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.42653381E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.42657841E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     3.43870069E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     2.03053787E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     4.39700875E-05   # chi^0_3
#    BR                NDA      ID1      ID2
     4.35574654E-03    2     1000022        22   # BR(chi^0_3 -> chi^0_1 photon)
     5.50215632E-03    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     5.40068098E-02    3     1000022         2        -2   # BR(chi^0_3 -> chi^0_1 u u_bar)
     5.38820289E-02    3     1000022         4        -4   # BR(chi^0_3 -> chi^0_1 c c_bar)
     6.92285805E-02    3     1000022         1        -1   # BR(chi^0_3 -> chi^0_1 d d_bar)
     6.93043369E-02    3     1000022         3        -3   # BR(chi^0_3 -> chi^0_1 s s_bar)
     2.55643809E-01    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
     1.56064396E-02    3     1000022        11       -11   # BR(chi^0_3 -> chi^0_1 e^- e^+)
     1.56662529E-02    3     1000022        13       -13   # BR(chi^0_3 -> chi^0_1 mu^- mu^+)
     3.23136992E-02    3     1000022        15       -15   # BR(chi^0_3 -> chi^0_1 tau^- tau^+)
     9.25995153E-02    3     1000022        12       -12   # BR(chi^0_3 -> chi^0_1 nu_e nu_bar_e)
     2.31538562E-03    3     1000023         2        -2   # BR(chi^0_3 -> chi^0_2 u u_bar)
     2.04120454E-03    3     1000023         4        -4   # BR(chi^0_3 -> chi^0_2 c c_bar)
     2.96879279E-03    3     1000023         1        -1   # BR(chi^0_3 -> chi^0_2 d d_bar)
     2.96698749E-03    3     1000023         3        -3   # BR(chi^0_3 -> chi^0_2 s s_bar)
     7.77053888E-04    3     1000023         5        -5   # BR(chi^0_3 -> chi^0_2 b b_bar)
     6.69976216E-04    3     1000023        11       -11   # BR(chi^0_3 -> chi^0_2 e^- e^+)
     6.69340695E-04    3     1000023        13       -13   # BR(chi^0_3 -> chi^0_2 mu^- mu^+)
     5.13353905E-04    3     1000023        15       -15   # BR(chi^0_3 -> chi^0_2 tau^- tau^+)
     3.97011446E-03    3     1000023        12       -12   # BR(chi^0_3 -> chi^0_2 nu_e nu_bar_e)
     5.31168090E-02    3     1000024         1        -2   # BR(chi^0_3 -> chi^+_1 d u_bar)
     5.31168090E-02    3    -1000024        -1         2   # BR(chi^0_3 -> chi^-_1 d_bar u)
     5.19876268E-02    3     1000024         3        -4   # BR(chi^0_3 -> chi^+_1 s c_bar)
     5.19876268E-02    3    -1000024        -3         4   # BR(chi^0_3 -> chi^-_1 s_bar c)
     1.77055625E-02    3     1000024        11       -12   # BR(chi^0_3 -> chi^+_1 e^- nu_bar_e)
     1.77055625E-02    3    -1000024       -11        12   # BR(chi^0_3 -> chi^-_1 e^+ nu_e)
     1.77037469E-02    3     1000024        13       -14   # BR(chi^0_3 -> chi^+_1 mu^- nu_bar_mu)
     1.77037469E-02    3    -1000024       -13        14   # BR(chi^0_3 -> chi^-_1 mu^+ nu_mu)
     1.69854623E-02    3     1000024        15       -16   # BR(chi^0_3 -> chi^+_1 tau^- nu_bar_tau)
     1.69854623E-02    3    -1000024       -15        16   # BR(chi^0_3 -> chi^-_1 tau^+ nu_tau)
DECAY   1000035     2.13023879E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.22962071E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.22962071E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.17146837E-01    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     1.17146837E-01    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     6.47294525E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     8.90123957E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     2.66415624E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.67373128E-02    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     3.19821239E-02    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     2.68585718E-02    2     1000025        36   # BR(chi^0_4 -> chi^0_3 A^0)
     3.37003826E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     4.59287239E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     7.74609878E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     6.02508890E-03    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     1.09916876E-01    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     3.99448263E-02    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     2.20530814E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     1.12821177E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     8.93473170E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     3.41662799E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     3.41662799E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.25485753E+02   # ~g
#    BR                NDA      ID1      ID2
     1.59044318E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.59044318E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     3.34969631E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     3.34969631E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.75248232E-03    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.75248232E-03    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     5.24196623E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     2.56142199E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.99225625E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.00863289E-03    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     2.10675019E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.10675019E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     9.02884873E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     9.02884873E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     7.02296726E-01   # Gamma(h0)
     1.59874793E-06   2        22        22   # BR(h0 -> photon photon)
     2.00662423E-07   2        22        23   # BR(h0 -> photon Z)
     3.55927643E-07   2        23        23   # BR(h0 -> Z Z)
     3.71996759E-06   2       -24        24   # BR(h0 -> W W)
     1.06455818E-03   2        21        21   # BR(h0 -> gluon gluon)
     7.42496315E-09   2       -11        11   # BR(h0 -> Electron electron)
     3.30256576E-04   2       -13        13   # BR(h0 -> Muon muon)
     9.51711111E-02   2       -15        15   # BR(h0 -> Tau tau)
     2.71955901E-11   2        -2         2   # BR(h0 -> Up up)
     5.27701804E-06   2        -4         4   # BR(h0 -> Charm charm)
     8.94391481E-07   2        -1         1   # BR(h0 -> Down down)
     3.23492564E-04   2        -3         3   # BR(h0 -> Strange strange)
     9.03098527E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     3.93780857E-02   # Gamma(HH)
     2.10970041E-04   2        22        22   # BR(HH -> photon photon)
     2.05137229E-04   2        22        23   # BR(HH -> photon Z)
     3.33783598E-03   2        23        23   # BR(HH -> Z Z)
     2.64211084E-02   2       -24        24   # BR(HH -> W W)
     1.20733013E-02   2        21        21   # BR(HH -> gluon gluon)
     7.29347318E-09   2       -11        11   # BR(HH -> Electron electron)
     3.24428548E-04   2       -13        13   # BR(HH -> Muon muon)
     9.35537568E-02   2       -15        15   # BR(HH -> Tau tau)
     1.26673750E-08   2        -2         2   # BR(HH -> Up up)
     2.45846246E-03   2        -4         4   # BR(HH -> Charm charm)
     8.47232093E-07   2        -1         1   # BR(HH -> Down down)
     3.06439423E-04   2        -3         3   # BR(HH -> Strange strange)
     8.61098869E-01   2        -5         5   # BR(HH -> Bottom bottom)
     8.82379038E-06   2        23        36   # BR(HH -> Z A0)
DECAY        36     7.45630464E-01   # Gamma(A0)
     4.27621441E-07   2        22        22   # BR(A0 -> photon photon)
     3.64381200E-10   2        22        23   # BR(A0 -> photon Z)
     1.24309511E-03   2        21        21   # BR(A0 -> gluon gluon)
     7.41349587E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.29748951E-04   2       -13        13   # BR(A0 -> Muon muon)
     9.51386431E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.53252917E-12   2        -2         2   # BR(A0 -> Up up)
     2.96247502E-07   2        -4         4   # BR(A0 -> Charm charm)
     8.91317573E-07   2        -1         1   # BR(A0 -> Down down)
     3.22381350E-04   2        -3         3   # BR(A0 -> Strange strange)
     9.02964509E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.25548882E-12   2        23        25   # BR(A0 -> Z h0)
DECAY        37     1.19825222E-01   # Gamma(Hp)
     7.89902866E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     3.37707889E-03   2       -13        14   # BR(Hp -> Muon nu_mu)
     9.55025190E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     7.63563707E-06   2        -1         2   # BR(Hp -> Down up)
     1.23348854E-04   2        -3         2   # BR(Hp -> Strange up)
     8.15224106E-05   2        -5         2   # BR(Hp -> Bottom up)
     4.85748846E-07   2        -1         4   # BR(Hp -> Down charm)
     2.75476169E-03   2        -3         4   # BR(Hp -> Strange charm)
     1.14147978E-02   2        -5         4   # BR(Hp -> Bottom charm)
     1.74314825E-04   2        -5         6   # BR(Hp -> Bottom top)
     1.36761984E-02   2        24        25   # BR(Hp -> W h0)
     9.77445212E-05   2        24        35   # BR(Hp -> W HH)
     1.32668421E-02   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    2.92194592E+02    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.28452468E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    3.04039839E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    4.22485645E-02    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    9.61040478E-01    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.28904266E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    2.92194592E+02    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.28452468E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    3.04039839E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    6.80731552E-02        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    9.31926845E-01        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    6.80731552E-02        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    9.31926845E-01        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    5.21706176E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.56401101E+00        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.43586970E+00        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    9.31926845E-01        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    6.80731552E-02        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    5.96113514E-04   # BR(b -> s gamma)
    2    1.63581689E-06   # BR(b -> s mu+ mu-)
    3    3.52861881E-05   # BR(b -> s nu nu)
    4    2.71956976E-15   # BR(Bd -> e+ e-)
    5    1.16176864E-10   # BR(Bd -> mu+ mu-)
    6    2.43164275E-08   # BR(Bd -> tau+ tau-)
    7    9.21787246E-14   # BR(Bs -> e+ e-)
    8    3.93786998E-09   # BR(Bs -> mu+ mu-)
    9    8.35125389E-07   # BR(Bs -> tau+ tau-)
   10    4.01712945E-05   # BR(B_u -> tau nu)
   11    4.14954092E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42804300E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.92894194E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16239241E-03   # epsilon_K
   17    2.28179244E-15   # Delta(M_K)
   18    2.48213449E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29561995E-11   # BR(K^+ -> pi^+ nu nu)
   20    1.61416291E-16   # Delta(g-2)_electron/2
   21    6.90105582E-12   # Delta(g-2)_muon/2
   22    1.95242763E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    4.47359505E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -4.40041681E-01   # C7
     0305 4322   00   2    -4.83326467E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -3.13193247E-01   # C8
     0305 6321   00   2    -4.16080253E-03   # C8'
 03051111 4133   00   0     1.62608810E+00   # C9 e+e-
 03051111 4133   00   2     1.62667674E+00   # C9 e+e-
 03051111 4233   00   2    -2.17387287E-04   # C9' e+e-
 03051111 4137   00   0    -4.44878020E+00   # C10 e+e-
 03051111 4137   00   2    -4.45035330E+00   # C10 e+e-
 03051111 4237   00   2     1.61200341E-03   # C10' e+e-
 03051313 4133   00   0     1.62608810E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62667670E+00   # C9 mu+mu-
 03051313 4233   00   2    -2.17523646E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44878020E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.45035334E+00   # C10 mu+mu-
 03051313 4237   00   2     1.61214032E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50569143E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -3.48623906E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50569143E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -3.48594294E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50569143E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -3.40249421E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85825417E-07   # C7
     0305 4422   00   2     3.47969539E-06   # C7
     0305 4322   00   2     4.14737469E-07   # C7'
     0305 6421   00   0     3.30484986E-07   # C8
     0305 6421   00   2     5.22029509E-06   # C8
     0305 6321   00   2     2.39495251E-07   # C8'
 03051111 4133   00   2     2.55635407E-07   # C9 e+e-
 03051111 4233   00   2     1.30090680E-06   # C9' e+e-
 03051111 4137   00   2    -1.05217732E-07   # C10 e+e-
 03051111 4237   00   2    -9.62467186E-06   # C10' e+e-
 03051313 4133   00   2     2.55634921E-07   # C9 mu+mu-
 03051313 4233   00   2     1.30090674E-06   # C9' mu+mu-
 03051313 4137   00   2    -1.05217256E-07   # C10 mu+mu-
 03051313 4237   00   2    -9.62467206E-06   # C10' mu+mu-
 03051212 4137   00   2     4.22390273E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     2.08147959E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     4.22390496E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     2.08147959E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     4.22453299E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     2.08147959E-06   # C11' nu_3 nu_3
