# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 08.09.2021,  16:48
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.92701476E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.74286280E+03  # scale for input parameters
    1    2.36747054E+02  # M_1
    2    4.03166961E+02  # M_2
    3    3.90281431E+03  # M_3
   11   -7.69031150E+03  # A_t
   12    1.50380750E+03  # A_b
   13   -1.73314466E+03  # A_tau
   23   -3.15051134E+02  # mu
   25    3.83130741E+01  # tan(beta)
   26    8.63838105E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.25400976E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.37337330E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.98371895E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.74286280E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.74286280E+03  # (SUSY scale)
  1  1     8.38722757E-06   # Y_u(Q)^DRbar
  2  2     4.26071161E-03   # Y_c(Q)^DRbar
  3  3     1.01324446E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.74286280E+03  # (SUSY scale)
  1  1     6.45722135E-04   # Y_d(Q)^DRbar
  2  2     1.22687206E-02   # Y_s(Q)^DRbar
  3  3     6.40354018E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.74286280E+03  # (SUSY scale)
  1  1     1.12683234E-04   # Y_e(Q)^DRbar
  2  2     2.32993190E-02   # Y_mu(Q)^DRbar
  3  3     3.91856217E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.74286280E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -7.69031116E+03   # A_t(Q)^DRbar
Block Ad Q=  2.74286280E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.50380745E+03   # A_b(Q)^DRbar
Block Ae Q=  2.74286280E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.73314467E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.74286280E+03  # soft SUSY breaking masses at Q
   1    2.36747054E+02  # M_1
   2    4.03166961E+02  # M_2
   3    3.90281431E+03  # M_3
  21    3.66615253E+05  # M^2_(H,d)
  22    2.48725379E+04  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.25400976E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.37337330E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.98371895E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.26682936E+02  # h0
        35     8.63620729E+02  # H0
        36     8.63838105E+02  # A0
        37     8.71961241E+02  # H+
   1000001     1.00851589E+04  # ~d_L
   2000001     1.00607842E+04  # ~d_R
   1000002     1.00848809E+04  # ~u_L
   2000002     1.00635727E+04  # ~u_R
   1000003     1.00851674E+04  # ~s_L
   2000003     1.00607987E+04  # ~s_R
   1000004     1.00848893E+04  # ~c_L
   2000004     1.00635744E+04  # ~c_R
   1000005     3.24834207E+03  # ~b_1
   2000005     5.04143098E+03  # ~b_2
   1000006     2.29133176E+03  # ~t_1
   2000006     3.28337279E+03  # ~t_2
   1000011     1.00212634E+04  # ~e_L-
   2000011     1.00090902E+04  # ~e_R-
   1000012     1.00205055E+04  # ~nu_eL
   1000013     1.00213005E+04  # ~mu_L-
   2000013     1.00091615E+04  # ~mu_R-
   1000014     1.00205422E+04  # ~nu_muL
   1000015     1.00293385E+04  # ~tau_1-
   2000015     1.00321686E+04  # ~tau_2-
   1000016     1.00310715E+04  # ~nu_tauL
   1000021     4.32804551E+03  # ~g
   1000022     2.28589640E+02  # ~chi_10
   1000023     3.07241291E+02  # ~chi_20
   1000025     3.22825336E+02  # ~chi_30
   1000035     4.66574010E+02  # ~chi_40
   1000024     2.99715324E+02  # ~chi_1+
   1000037     4.66607110E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.60758230E-02   # alpha
Block Hmix Q=  2.74286280E+03  # Higgs mixing parameters
   1   -3.15051134E+02  # mu
   2    3.83130741E+01  # tan[beta](Q)
   3    2.43301937E+02  # v(Q)
   4    7.46216272E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     1.97666425E-01   # Re[R_st(1,1)]
   1  2     9.80269343E-01   # Re[R_st(1,2)]
   2  1    -9.80269343E-01   # Re[R_st(2,1)]
   2  2     1.97666425E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99998001E-01   # Re[R_sb(1,1)]
   1  2     1.99956981E-03   # Re[R_sb(1,2)]
   2  1    -1.99956981E-03   # Re[R_sb(2,1)]
   2  2    -9.99998001E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -3.50741197E-01   # Re[R_sta(1,1)]
   1  2     9.36472430E-01   # Re[R_sta(1,2)]
   2  1    -9.36472430E-01   # Re[R_sta(2,1)]
   2  2    -3.50741197E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.21704718E-01   # Re[N(1,1)]
   1  2    -8.06056481E-02   # Re[N(1,2)]
   1  3    -3.09205193E-01   # Re[N(1,3)]
   1  4    -2.19898367E-01   # Re[N(1,4)]
   2  1    -3.78752667E-01   # Re[N(2,1)]
   2  2    -3.56678235E-01   # Re[N(2,2)]
   2  3    -6.13630200E-01   # Re[N(2,3)]
   2  4    -5.93957096E-01   # Re[N(2,4)]
   3  1    -5.75937460E-02   # Re[N(3,1)]
   3  2     7.73913094E-02   # Re[N(3,2)]
   3  3    -6.97106433E-01   # Re[N(3,3)]
   3  4     7.10447863E-01   # Re[N(3,4)]
   4  1    -6.07436513E-02   # Re[N(4,1)]
   4  2     9.27520324E-01   # Re[N(4,2)]
   4  3    -2.04677177E-01   # Re[N(4,3)]
   4  4    -3.06795551E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -2.88556947E-01   # Re[U(1,1)]
   1  2    -9.57462735E-01   # Re[U(1,2)]
   2  1     9.57462735E-01   # Re[U(2,1)]
   2  2    -2.88556947E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -4.32200895E-01   # Re[V(1,1)]
   1  2     9.01777349E-01   # Re[V(1,2)]
   2  1     9.01777349E-01   # Re[V(2,1)]
   2  2     4.32200895E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02327274E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     8.49655362E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.43352402E-01    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     3.31407364E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     3.67811168E-03    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44364487E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     5.17559984E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     9.53007949E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     6.62795430E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.43721709E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     5.07903310E-02    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     5.57768371E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.04488421E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     8.46226270E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.43544592E-01    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     4.33897324E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     3.75175261E-03    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     1.96077466E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
     1.77638239E-04    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.44473035E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     5.17888482E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     9.55110926E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.02557806E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.43570742E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     5.07523589E-02    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     5.57351380E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.19347659E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     3.82310696E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.82828687E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.10585744E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     8.76361031E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.92530120E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     2.29811435E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.67245742E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     6.26161578E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.32741370E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.00359252E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.20745466E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.76291214E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.80774843E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44369486E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     1.00201333E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     7.26376149E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.56649283E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     2.80316269E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     1.13930855E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     4.94721288E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44478020E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.00126428E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     7.25833154E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.56382674E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.80106727E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     1.14531127E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     4.94413560E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.75104653E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.27008305E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     5.99513152E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     2.94359919E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.31359692E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.54175694E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.22825053E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.33010541E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.94321879E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.50895375E-03    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.89473990E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.63877082E+02   # ~d_L
#    BR                NDA      ID1      ID2
     4.00084775E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.58327857E-03    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     5.12667706E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.86212484E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.11153676E-02    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.22070489E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.98096101E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.33071967E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.94758450E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.52996388E-03    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.89362691E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.63911736E+02   # ~s_L
#    BR                NDA      ID1      ID2
     4.00497512E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.60004190E-03    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     5.34640151E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.86201292E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.11192461E-02    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.22065245E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.98055722E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.47030649E+02   # ~b_1
#    BR                NDA      ID1      ID2
     1.36444973E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     4.44641934E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     5.15143128E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     5.31185338E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.22103892E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.51080602E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     4.62745861E-01    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
     1.32810798E-03    2     1000006       -37   # BR(~b_1 -> ~t_1 H^-)
DECAY   2000005     1.15749355E+02   # ~b_2
#    BR                NDA      ID1      ID2
     5.42233591E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.36156223E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.71408634E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.49209976E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.22261317E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.94137830E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.37771639E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     5.50740546E-04    2     2000006       -24   # BR(~b_2 -> ~t_2 W^-)
     2.24652445E-03    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     1.42485288E-02    2     2000006       -37   # BR(~b_2 -> ~t_2 H^-)
     2.99982581E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     7.96369342E-03    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     5.11685624E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
     7.96420595E-03    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   2000002     5.50126480E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.46694515E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     5.84942316E-03    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.35228441E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     1.50087447E-04    2     1000035         2   # BR(~u_R -> chi^0_4 u)
     9.59195809E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.63871039E+02   # ~u_L
#    BR                NDA      ID1      ID2
     4.65996733E-04    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.20052025E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     3.00747981E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     5.59482924E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     2.49359565E-02    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.08282304E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.98061500E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.50133875E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.46693078E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     5.85167365E-03    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.38553978E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     1.50704865E-04    2     1000035         4   # BR(~c_R -> chi^0_4 c)
     9.59183168E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.63905651E+02   # ~c_L
#    BR                NDA      ID1      ID2
     4.66237957E-04    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.20065154E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     3.03495347E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     5.59459391E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     2.49761913E-02    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.08280508E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.98021113E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     9.39733084E+01   # ~t_1
#    BR                NDA      ID1      ID2
     7.63049239E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.83189915E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.43022763E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.17041784E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.32114586E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     4.36294370E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.74061695E+02   # ~t_2
#    BR                NDA      ID1      ID2
     7.45777017E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     8.92298742E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.17753287E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     7.60476883E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     8.70737707E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.11611281E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.19804945E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     7.51814805E-04    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     2.89629984E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     6.39584295E-04    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     6.70618039E-04   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.33516622E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.33216633E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.11170469E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.11170148E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.10926128E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     2.21645621E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.80312112E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.56438117E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     3.93767033E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.23565781E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.23422342E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     7.99331503E-05   # chi^0_2
#    BR                NDA      ID1      ID2
     1.59316682E-03    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.18745366E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.18500957E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.52245044E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.52243497E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.49450884E-01    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.43314236E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.43308042E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     3.41478286E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     2.03558959E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     1.52563505E-04    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     1.52563505E-04    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     1.32843608E-04    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     1.32843608E-04    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
DECAY   1000025     1.36391984E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     9.99534908E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     2.62127043E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.23647382E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.23647382E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.17500665E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.02066733E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     3.32052499E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     4.98261738E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     9.17439303E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.18047141E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000021     1.12485902E+02   # ~g
#    BR                NDA      ID1      ID2
     2.34059360E-04    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     2.34059360E-04    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     2.81796120E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.81796120E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.08934832E-01    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     1.08934832E-01    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.08599241E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.08599241E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
     1.19043179E-04    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.49237104E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.39767389E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.39767389E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.80376234E-03   # Gamma(h0)
     2.52995363E-03   2        22        22   # BR(h0 -> photon photon)
     1.76793798E-03   2        22        23   # BR(h0 -> photon Z)
     3.47036123E-02   2        23        23   # BR(h0 -> Z Z)
     2.77472165E-01   2       -24        24   # BR(h0 -> W W)
     7.63036510E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.70365691E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.09227916E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.03294272E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.27260392E-07   2        -2         2   # BR(h0 -> Up up)
     2.47000879E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.43344957E-07   2        -1         1   # BR(h0 -> Down down)
     1.96514542E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.21786748E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     2.09967690E+01   # Gamma(HH)
     3.01843527E-07   2        22        22   # BR(HH -> photon photon)
     8.80285134E-07   2        22        23   # BR(HH -> photon Z)
     2.27333565E-06   2        23        23   # BR(HH -> Z Z)
     3.54886282E-06   2       -24        24   # BR(HH -> W W)
     5.85807129E-05   2        21        21   # BR(HH -> gluon gluon)
     8.77863286E-09   2       -11        11   # BR(HH -> Electron electron)
     3.90716697E-04   2       -13        13   # BR(HH -> Muon muon)
     1.12819805E-01   2       -15        15   # BR(HH -> Tau tau)
     7.78957023E-14   2        -2         2   # BR(HH -> Up up)
     1.51048715E-08   2        -4         4   # BR(HH -> Charm charm)
     9.82897385E-04   2        -6         6   # BR(HH -> Top top)
     7.67041319E-07   2        -1         1   # BR(HH -> Down down)
     2.77393161E-04   2        -3         3   # BR(HH -> Strange strange)
     6.78449722E-01   2        -5         5   # BR(HH -> Bottom bottom)
     3.06426543E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     4.54873423E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     4.54873423E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     7.01414503E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.54725385E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.69988931E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     8.09964525E-04   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     2.40398302E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     8.63511193E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     6.33818958E-03   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     5.65193304E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     3.49141320E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     1.52002126E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     2.02926530E+01   # Gamma(A0)
     5.00684052E-07   2        22        22   # BR(A0 -> photon photon)
     2.67843998E-06   2        22        23   # BR(A0 -> photon Z)
     8.80235036E-05   2        21        21   # BR(A0 -> gluon gluon)
     8.86626623E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.94617153E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.13948086E-01   2       -15        15   # BR(A0 -> Tau tau)
     7.68989043E-14   2        -2         2   # BR(A0 -> Up up)
     1.48931614E-08   2        -4         4   # BR(A0 -> Charm charm)
     1.17564090E-03   2        -6         6   # BR(A0 -> Top top)
     7.74671775E-07   2        -1         1   # BR(A0 -> Down down)
     2.80152587E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.85221164E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     5.49346528E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     3.03601315E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     3.03601315E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     9.32599626E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.30577974E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.19997328E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.92405511E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     4.35633576E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     5.55293271E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     2.11010633E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.45905874E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     9.45050683E-03   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     3.58310392E-06   2        23        25   # BR(A0 -> Z h0)
     1.14242262E-35   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.15883701E+01   # Gamma(Hp)
     1.03006127E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.40383354E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.24564511E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     7.79080916E-07   2        -1         2   # BR(Hp -> Down up)
     1.29427654E-05   2        -3         2   # BR(Hp -> Strange up)
     7.55862820E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.65254537E-08   2        -1         4   # BR(Hp -> Down charm)
     2.80735760E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.05847429E-03   2        -5         4   # BR(Hp -> Bottom charm)
     7.22627056E-08   2        -1         6   # BR(Hp -> Down top)
     1.95552658E-06   2        -3         6   # BR(Hp -> Strange top)
     6.74380007E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.98858222E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     4.84417803E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     7.39988530E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     5.56984947E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.39974074E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     3.95579166E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     4.78602047E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     5.12232343E-06   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     3.49813254E-06   2        24        25   # BR(Hp -> W h0)
     2.52556194E-09   2        24        35   # BR(Hp -> W HH)
     2.21328869E-09   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.98544388E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.46789310E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.46789165E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00000099E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    6.80257559E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    6.81249193E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.98544388E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.46789310E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.46789165E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    1.00000000E+00        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    3.61121495E-10        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    1.00000000E+00        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    3.61121495E-10        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.24543899E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.26873564E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.55748092E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    3.61121495E-10        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    1.00000000E+00        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.92844539E-04   # BR(b -> s gamma)
    2    1.58912569E-06   # BR(b -> s mu+ mu-)
    3    3.52765048E-05   # BR(b -> s nu nu)
    4    8.62907319E-16   # BR(Bd -> e+ e-)
    5    3.68473905E-11   # BR(Bd -> mu+ mu-)
    6    6.84203817E-09   # BR(Bd -> tau+ tau-)
    7    2.90732949E-14   # BR(Bs -> e+ e-)
    8    1.24151628E-09   # BR(Bs -> mu+ mu-)
    9    2.34314994E-07   # BR(Bs -> tau+ tau-)
   10    8.39624549E-05   # BR(B_u -> tau nu)
   11    8.67300015E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42047737E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93005792E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15884011E-03   # epsilon_K
   17    2.28168724E-15   # Delta(M_K)
   18    2.48169942E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29446558E-11   # BR(K^+ -> pi^+ nu nu)
   20   -2.50411432E-16   # Delta(g-2)_electron/2
   21   -1.07059494E-11   # Delta(g-2)_muon/2
   22   -3.03473362E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    8.19339963E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.63378384E-01   # C7
     0305 4322   00   2    -1.02121776E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.62844010E-01   # C8
     0305 6321   00   2    -1.04695257E-03   # C8'
 03051111 4133   00   0     1.61371508E+00   # C9 e+e-
 03051111 4133   00   2     1.61427075E+00   # C9 e+e-
 03051111 4233   00   2     1.71342361E-04   # C9' e+e-
 03051111 4137   00   0    -4.43640718E+00   # C10 e+e-
 03051111 4137   00   2    -4.43713432E+00   # C10 e+e-
 03051111 4237   00   2    -1.29168018E-03   # C10' e+e-
 03051313 4133   00   0     1.61371508E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61427053E+00   # C9 mu+mu-
 03051313 4233   00   2     1.71322098E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.43640718E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43713454E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.29166027E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50550988E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     2.80126256E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50550988E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     2.80130633E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50550988E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     2.81363876E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85779619E-07   # C7
     0305 4422   00   2    -1.08871833E-05   # C7
     0305 4322   00   2    -3.32580270E-06   # C7'
     0305 6421   00   0     3.30445757E-07   # C8
     0305 6421   00   2     6.80612486E-06   # C8
     0305 6321   00   2    -1.48291557E-06   # C8'
 03051111 4133   00   2    -2.34253485E-07   # C9 e+e-
 03051111 4233   00   2     7.35155713E-06   # C9' e+e-
 03051111 4137   00   2     9.86769052E-06   # C10 e+e-
 03051111 4237   00   2    -5.54522621E-05   # C10' e+e-
 03051313 4133   00   2    -2.34257780E-07   # C9 mu+mu-
 03051313 4233   00   2     7.35155553E-06   # C9' mu+mu-
 03051313 4137   00   2     9.86769440E-06   # C10 mu+mu-
 03051313 4237   00   2    -5.54522670E-05   # C10' mu+mu-
 03051212 4137   00   2    -2.10321128E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     1.20259250E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -2.10321108E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     1.20259250E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -2.10315338E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     1.20259250E-05   # C11' nu_3 nu_3
