# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  19:24
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.87839554E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.04653579E+03  # scale for input parameters
    1   -1.98214678E+02  # M_1
    2   -4.75765721E+02  # M_2
    3    4.59460068E+03  # M_3
   11    1.34745818E+03  # A_t
   12    2.51302885E+02  # A_b
   13   -4.67517487E+02  # A_tau
   23   -1.99537547E+02  # mu
   25    3.77094043E+01  # tan(beta)
   26    5.95664937E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.06109042E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.80613452E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.44723582E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.04653579E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.04653579E+03  # (SUSY scale)
  1  1     8.38731971E-06   # Y_u(Q)^DRbar
  2  2     4.26075841E-03   # Y_c(Q)^DRbar
  3  3     1.01325559E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.04653579E+03  # (SUSY scale)
  1  1     6.35554967E-04   # Y_d(Q)^DRbar
  2  2     1.20755444E-02   # Y_s(Q)^DRbar
  3  3     6.30271373E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.04653579E+03  # (SUSY scale)
  1  1     1.10908989E-04   # Y_e(Q)^DRbar
  2  2     2.29324614E-02   # Y_mu(Q)^DRbar
  3  3     3.85686275E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.04653579E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     1.34745820E+03   # A_t(Q)^DRbar
Block Ad Q=  4.04653579E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     2.51302884E+02   # A_b(Q)^DRbar
Block Ae Q=  4.04653579E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -4.67517488E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  4.04653579E+03  # soft SUSY breaking masses at Q
   1   -1.98214678E+02  # M_1
   2   -4.75765721E+02  # M_2
   3    4.59460068E+03  # M_3
  21    2.94702859E+05  # M^2_(H,d)
  22    3.28020472E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.06109042E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.80613452E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.44723582E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23200571E+02  # h0
        35     5.95663858E+02  # H0
        36     5.95664937E+02  # A0
        37     6.07712051E+02  # H+
   1000001     1.00991605E+04  # ~d_L
   2000001     1.00743737E+04  # ~d_R
   1000002     1.00987913E+04  # ~u_L
   2000002     1.00776161E+04  # ~u_R
   1000003     1.00991631E+04  # ~s_L
   2000003     1.00743782E+04  # ~s_R
   1000004     1.00987939E+04  # ~c_L
   2000004     1.00776167E+04  # ~c_R
   1000005     3.58805859E+03  # ~b_1
   2000005     4.18608147E+03  # ~b_2
   1000006     3.90880210E+03  # ~t_1
   2000006     4.18912278E+03  # ~t_2
   1000011     1.00215307E+04  # ~e_L-
   2000011     1.00088058E+04  # ~e_R-
   1000012     1.00207573E+04  # ~nu_eL
   1000013     1.00215427E+04  # ~mu_L-
   2000013     1.00088287E+04  # ~mu_R-
   1000014     1.00207691E+04  # ~nu_muL
   1000015     1.00153414E+04  # ~tau_1-
   2000015     1.00249447E+04  # ~tau_2-
   1000016     1.00241275E+04  # ~nu_tauL
   1000021     5.06145161E+03  # ~g
   1000022     1.73546361E+02  # ~chi_10
   1000023     2.25046675E+02  # ~chi_20
   1000025     2.35822881E+02  # ~chi_30
   1000035     5.36186309E+02  # ~chi_40
   1000024     2.12177192E+02  # ~chi_1+
   1000037     5.35615230E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.77163744E-02   # alpha
Block Hmix Q=  4.04653579E+03  # Higgs mixing parameters
   1   -1.99537547E+02  # mu
   2    3.77094043E+01  # tan[beta](Q)
   3    2.42949372E+02  # v(Q)
   4    3.54816717E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -7.20056703E-02   # Re[R_st(1,1)]
   1  2     9.97404223E-01   # Re[R_st(1,2)]
   2  1    -9.97404223E-01   # Re[R_st(2,1)]
   2  2    -7.20056703E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -3.35534470E-03   # Re[R_sb(1,1)]
   1  2     9.99994371E-01   # Re[R_sb(1,2)]
   2  1    -9.99994371E-01   # Re[R_sb(2,1)]
   2  2    -3.35534470E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -6.41700077E-02   # Re[R_sta(1,1)]
   1  2     9.97938981E-01   # Re[R_sta(1,2)]
   2  1    -9.97938981E-01   # Re[R_sta(2,1)]
   2  2    -6.41700077E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     7.42447981E-01   # Re[N(1,1)]
   1  2    -9.90200465E-02   # Re[N(1,2)]
   1  3    -5.16504108E-01   # Re[N(1,3)]
   1  4     4.14957265E-01   # Re[N(1,4)]
   2  1     7.28026943E-02   # Re[N(2,1)]
   2  2    -7.54251421E-02   # Re[N(2,2)]
   2  3     6.92523908E-01   # Re[N(2,3)]
   2  4     7.13737664E-01   # Re[N(2,4)]
   3  1     6.65476605E-01   # Re[N(3,1)]
   3  2     1.55145637E-01   # Re[N(3,2)]
   3  3     4.97596926E-01   # Re[N(3,3)]
   3  4    -5.34292073E-01   # Re[N(3,4)]
   4  1     2.47315832E-02   # Re[N(4,1)]
   4  2    -9.80018321E-01   # Re[N(4,2)]
   4  3     7.76623632E-02   # Re[N(4,3)]
   4  4    -1.81441441E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.09661752E-01   # Re[U(1,1)]
   1  2    -9.93968963E-01   # Re[U(1,2)]
   2  1    -9.93968963E-01   # Re[U(2,1)]
   2  2     1.09661752E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     2.56485523E-01   # Re[V(1,1)]
   1  2     9.66548072E-01   # Re[V(1,2)]
   2  1     9.66548072E-01   # Re[V(2,1)]
   2  2    -2.56485523E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02487644E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     5.51356195E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     5.29930537E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     4.42735799E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     6.08652022E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44169649E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     2.70832565E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     4.07935288E-04    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     7.96967937E-02    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.84349344E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     7.35214895E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.01110522E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.04581071E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     5.49623275E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     6.27159839E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     4.41413225E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     6.18573129E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     2.04851434E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.44274570E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     2.72573465E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     7.55813718E-04    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     7.98186729E-02    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.84147255E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     7.34681103E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.00674101E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.09773049E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     3.34434404E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.28443824E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     2.64968469E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.99172759E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.71954042E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     1.73588251E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     6.18029855E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     8.43375658E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.11398573E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.38340682E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.66642933E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.01453765E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44174250E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     7.51116970E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.99397981E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     1.22682217E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.00072130E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     4.02144742E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.68339497E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44279160E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     7.50571692E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.99108036E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     1.22593155E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.99854294E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     4.09025327E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.67935608E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.73859991E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.23077164E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     3.31314349E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.01769121E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.48920934E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.01781309E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.73499984E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.50046954E+02   # ~d_R
#    BR                NDA      ID1      ID2
     6.88492968E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     5.52860072E-03    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.87512428E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.81022691E+02   # ~d_L
#    BR                NDA      ID1      ID2
     4.08820445E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.97190276E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.02641438E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     7.37235890E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.83845171E-03    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.50322640E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.69327283E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.50105656E+02   # ~s_R
#    BR                NDA      ID1      ID2
     6.90129900E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     5.54389633E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.87384673E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.81055827E+02   # ~s_L
#    BR                NDA      ID1      ID2
     4.10137867E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     6.21246561E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.15071897E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     7.37197048E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.84420518E-03    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.50314519E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.69283874E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     5.81494260E+01   # ~b_1
#    BR                NDA      ID1      ID2
     1.49016930E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.32017535E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.34569297E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.65402137E-03    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.76517339E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     5.22487831E-03    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.72222714E+02   # ~b_2
#    BR                NDA      ID1      ID2
     5.65267323E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     9.24966219E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     4.74989220E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.01488771E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.61952997E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.37163255E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.76723575E-03    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     4.67249741E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.65341482E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     2.55030654E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     2.13069134E-02    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.51874614E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.81004406E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.98179846E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.72279808E-03    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     7.24098140E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.00570658E-02    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.42142281E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.69285633E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.67257044E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.65350763E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.58991204E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     2.13087992E-02    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.51859805E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.81037508E+02   # ~c_L
#    BR                NDA      ID1      ID2
     3.01357885E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.72426462E-03    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     7.24059067E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.01061266E-02    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.42134823E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.69242217E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.66890233E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.17116913E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.44242723E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.53984456E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.01150468E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.54108437E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.03479623E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.72348379E+02   # ~t_2
#    BR                NDA      ID1      ID2
     7.83473881E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.48250840E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.52089365E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.18300331E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.92597661E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.06034909E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.36955358E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.95016610E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.62446755E-04   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.34203200E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.32614788E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.11401733E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.11399181E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.10381098E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     4.30751946E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     6.86137133E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.50923679E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.18728084E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.47182144E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.13407514E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.12352848E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     4.81567762E-04    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     3.33427697E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     5.28068078E-04   # chi^0_2
#    BR                NDA      ID1      ID2
     7.69296411E-03    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.18812389E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.17972900E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.52340410E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.52335274E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.42841435E-01    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.43788017E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.43769221E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     3.38527253E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     2.03725556E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     2.85732746E-04    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     2.85732746E-04    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     2.72414226E-04    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     2.72414226E-04    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
DECAY   1000025     3.97010302E-05   # chi^0_3
#    BR                NDA      ID1      ID2
     3.38598013E-03    2     1000022        22   # BR(chi^0_3 -> chi^0_1 photon)
     3.08023791E-03    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     1.39137181E-02    3     1000022         2        -2   # BR(chi^0_3 -> chi^0_1 u u_bar)
     1.38633434E-02    3     1000022         4        -4   # BR(chi^0_3 -> chi^0_1 c c_bar)
     1.78334662E-02    3     1000022         1        -1   # BR(chi^0_3 -> chi^0_1 d d_bar)
     1.78332660E-02    3     1000022         3        -3   # BR(chi^0_3 -> chi^0_1 s s_bar)
     1.77377350E-02    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
     4.01734491E-03    3     1000022        11       -11   # BR(chi^0_3 -> chi^0_1 e^- e^+)
     4.01737654E-03    3     1000022        13       -13   # BR(chi^0_3 -> chi^0_1 mu^- mu^+)
     4.02555111E-03    3     1000022        15       -15   # BR(chi^0_3 -> chi^0_1 tau^- tau^+)
     2.38550296E-02    3     1000022        12       -12   # BR(chi^0_3 -> chi^0_1 nu_e nu_bar_e)
     7.52153987E-04    3     1000023         2        -2   # BR(chi^0_3 -> chi^0_2 u u_bar)
     6.24900194E-04    3     1000023         4        -4   # BR(chi^0_3 -> chi^0_2 c c_bar)
     9.64410413E-04    3     1000023         1        -1   # BR(chi^0_3 -> chi^0_2 d d_bar)
     9.63541382E-04    3     1000023         3        -3   # BR(chi^0_3 -> chi^0_2 s s_bar)
     1.00268100E-04    3     1000023         5        -5   # BR(chi^0_3 -> chi^0_2 b b_bar)
     2.17641212E-04    3     1000023        11       -11   # BR(chi^0_3 -> chi^0_2 e^- e^+)
     2.17332354E-04    3     1000023        13       -13   # BR(chi^0_3 -> chi^0_2 mu^- mu^+)
     1.45753590E-04    3     1000023        15       -15   # BR(chi^0_3 -> chi^0_2 tau^- tau^+)
     1.28968887E-03    3     1000023        12       -12   # BR(chi^0_3 -> chi^0_2 nu_e nu_bar_e)
     1.46285302E-01    3     1000024         1        -2   # BR(chi^0_3 -> chi^+_1 d u_bar)
     1.46285302E-01    3    -1000024        -1         2   # BR(chi^0_3 -> chi^-_1 d_bar u)
     1.44292541E-01    3     1000024         3        -4   # BR(chi^0_3 -> chi^+_1 s c_bar)
     1.44292541E-01    3    -1000024        -3         4   # BR(chi^0_3 -> chi^-_1 s_bar c)
     4.87614895E-02    3     1000024        11       -12   # BR(chi^0_3 -> chi^+_1 e^- nu_bar_e)
     4.87614895E-02    3    -1000024       -11        12   # BR(chi^0_3 -> chi^-_1 e^+ nu_e)
     4.87583106E-02    3     1000024        13       -14   # BR(chi^0_3 -> chi^+_1 mu^- nu_bar_mu)
     4.87583106E-02    3    -1000024       -13        14   # BR(chi^0_3 -> chi^-_1 mu^+ nu_mu)
     4.74829869E-02    3     1000024        15       -16   # BR(chi^0_3 -> chi^+_1 tau^- nu_bar_tau)
     4.74829869E-02    3    -1000024       -15        16   # BR(chi^0_3 -> chi^-_1 tau^+ nu_tau)
DECAY   1000035     5.29293647E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.36968965E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.36968965E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     9.35456210E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     2.04030794E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.95225411E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     3.53058082E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.46028236E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.10382301E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.39543111E-04    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     6.39543111E-04    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     9.10131699E+01   # ~g
#    BR                NDA      ID1      ID2
     1.36136835E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.36136835E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     7.78240726E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     7.78240726E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     2.03685882E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.03685882E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     8.21765981E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     8.21765981E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.67719031E-03   # Gamma(h0)
     2.33717297E-03   2        22        22   # BR(h0 -> photon photon)
     1.34119494E-03   2        22        23   # BR(h0 -> photon Z)
     2.32122716E-02   2        23        23   # BR(h0 -> Z Z)
     1.98805797E-01   2       -24        24   # BR(h0 -> W W)
     7.28739835E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.38739192E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.39639768E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.90930898E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.33194073E-07   2        -2         2   # BR(h0 -> Up up)
     2.58490182E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.25694407E-07   2        -1         1   # BR(h0 -> Down down)
     2.26300257E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.06020767E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.41025402E+01   # Gamma(HH)
     3.38941160E-07   2        22        22   # BR(HH -> photon photon)
     4.06149154E-07   2        22        23   # BR(HH -> photon Z)
     8.81899574E-06   2        23        23   # BR(HH -> Z Z)
     1.77471463E-05   2       -24        24   # BR(HH -> W W)
     1.30074209E-04   2        21        21   # BR(HH -> gluon gluon)
     9.05609540E-09   2       -11        11   # BR(HH -> Electron electron)
     4.03020615E-04   2       -13        13   # BR(HH -> Muon muon)
     1.16362967E-01   2       -15        15   # BR(HH -> Tau tau)
     7.93480345E-14   2        -2         2   # BR(HH -> Up up)
     1.53854541E-08   2        -4         4   # BR(HH -> Charm charm)
     7.18206311E-04   2        -6         6   # BR(HH -> Top top)
     8.29568179E-07   2        -1         1   # BR(HH -> Down down)
     3.00057490E-04   2        -3         3   # BR(HH -> Strange strange)
     8.38070297E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.13991775E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.45657697E-02   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     9.19353277E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     9.03938023E-05   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     9.70273895E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     6.60541991E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.08015743E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     8.24867266E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.40801773E+01   # Gamma(A0)
     6.44165299E-07   2        22        22   # BR(A0 -> photon photon)
     4.84143093E-07   2        22        23   # BR(A0 -> photon Z)
     1.61485492E-04   2        21        21   # BR(A0 -> gluon gluon)
     8.94145410E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.97918827E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.14894159E-01   2       -15        15   # BR(A0 -> Tau tau)
     6.61169858E-14   2        -2         2   # BR(A0 -> Up up)
     1.28257930E-08   2        -4         4   # BR(A0 -> Charm charm)
     9.19534491E-04   2        -6         6   # BR(A0 -> Top top)
     8.19066312E-07   2        -1         1   # BR(A0 -> Down down)
     2.96258903E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.27522758E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.04580543E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.28608636E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     4.80426360E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.99992466E-04   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.67283226E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     2.97241838E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     2.82252842E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.49634640E-05   2        23        25   # BR(A0 -> Z h0)
     4.35416398E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.33305433E+01   # Gamma(Hp)
     1.13400472E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.84822406E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.37133105E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     9.02901241E-07   2        -1         2   # BR(Hp -> Down up)
     1.49227412E-05   2        -3         2   # BR(Hp -> Strange up)
     1.00044865E-05   2        -5         2   # BR(Hp -> Bottom up)
     4.21462615E-08   2        -1         4   # BR(Hp -> Down charm)
     3.25415446E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.40097433E-03   2        -5         4   # BR(Hp -> Bottom charm)
     8.00676737E-08   2        -1         6   # BR(Hp -> Down top)
     2.14646409E-06   2        -3         6   # BR(Hp -> Strange top)
     8.20757787E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.17638849E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.79997892E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.52886387E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.72325921E-05   2        24        25   # BR(Hp -> W h0)
     2.55014592E-08   2        24        35   # BR(Hp -> W HH)
     2.54901448E-08   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.09286439E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.42190631E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.42199917E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99934694E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    7.68540806E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    7.03235290E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.09286439E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.42190631E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.42199917E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99998550E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.44962540E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99998550E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.44962540E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26002579E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    3.89672599E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.67716981E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.44962540E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99998550E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.91107990E-04   # BR(b -> s gamma)
    2    1.58750636E-06   # BR(b -> s mu+ mu-)
    3    3.52489382E-05   # BR(b -> s nu nu)
    4    2.39814111E-15   # BR(Bd -> e+ e-)
    5    1.02445955E-10   # BR(Bd -> mu+ mu-)
    6    2.14512478E-08   # BR(Bd -> tau+ tau-)
    7    8.25922597E-14   # BR(Bs -> e+ e-)
    8    3.52834128E-09   # BR(Bs -> mu+ mu-)
    9    7.48543974E-07   # BR(Bs -> tau+ tau-)
   10    7.50517691E-05   # BR(B_u -> tau nu)
   11    7.75256042E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41798104E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.92613732E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15730634E-03   # epsilon_K
   17    2.28168649E-15   # Delta(M_K)
   18    2.47961553E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28959270E-11   # BR(K^+ -> pi^+ nu nu)
   20    2.93889033E-16   # Delta(g-2)_electron/2
   21    1.25647601E-11   # Delta(g-2)_muon/2
   22    3.56044562E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.18998765E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.58570981E-01   # C7
     0305 4322   00   2    -1.37683078E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.71317047E-01   # C8
     0305 6321   00   2    -1.50604237E-03   # C8'
 03051111 4133   00   0     1.62738424E+00   # C9 e+e-
 03051111 4133   00   2     1.62762509E+00   # C9 e+e-
 03051111 4233   00   2    -6.18825077E-05   # C9' e+e-
 03051111 4137   00   0    -4.45007635E+00   # C10 e+e-
 03051111 4137   00   2    -4.44801810E+00   # C10 e+e-
 03051111 4237   00   2     4.59281341E-04   # C10' e+e-
 03051313 4133   00   0     1.62738424E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62762498E+00   # C9 mu+mu-
 03051313 4233   00   2    -6.19459773E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.45007635E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44801821E+00   # C10 mu+mu-
 03051313 4237   00   2     4.59344540E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50490641E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -9.92991150E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50490641E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -9.92854010E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50490642E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -9.54207021E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85821848E-07   # C7
     0305 4422   00   2     4.72887353E-06   # C7
     0305 4322   00   2    -4.30255307E-08   # C7'
     0305 6421   00   0     3.30481928E-07   # C8
     0305 6421   00   2     4.64467674E-06   # C8
     0305 6321   00   2     4.77848656E-08   # C8'
 03051111 4133   00   2     4.24449615E-07   # C9 e+e-
 03051111 4233   00   2     5.72542936E-06   # C9' e+e-
 03051111 4137   00   2     2.16560562E-07   # C10 e+e-
 03051111 4237   00   2    -4.23254716E-05   # C10' e+e-
 03051313 4133   00   2     4.24446995E-07   # C9 mu+mu-
 03051313 4233   00   2     5.72542798E-06   # C9' mu+mu-
 03051313 4137   00   2     2.16563346E-07   # C10 mu+mu-
 03051313 4237   00   2    -4.23254757E-05   # C10' mu+mu-
 03051212 4137   00   2    -2.36993502E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     9.15091765E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -2.36992141E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     9.15091765E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -2.36612838E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     9.15091763E-06   # C11' nu_3 nu_3
