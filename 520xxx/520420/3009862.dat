# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  22:45
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.92402088E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.96356206E+03  # scale for input parameters
    1    6.11558624E+01  # M_1
    2   -1.05204250E+03  # M_2
    3    2.84164603E+03  # M_3
   11   -5.93909052E+03  # A_t
   12    4.90506139E+02  # A_b
   13    8.96093147E+02  # A_tau
   23    2.38604832E+02  # mu
   25    3.81667675E+01  # tan(beta)
   26    4.70371178E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.58423670E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.38347523E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.30369418E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.96356206E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.96356206E+03  # (SUSY scale)
  1  1     8.38724950E-06   # Y_u(Q)^DRbar
  2  2     4.26072275E-03   # Y_c(Q)^DRbar
  3  3     1.01324711E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.96356206E+03  # (SUSY scale)
  1  1     6.43257990E-04   # Y_d(Q)^DRbar
  2  2     1.22219018E-02   # Y_s(Q)^DRbar
  3  3     6.37910358E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.96356206E+03  # (SUSY scale)
  1  1     1.12253223E-04   # Y_e(Q)^DRbar
  2  2     2.32104063E-02   # Y_mu(Q)^DRbar
  3  3     3.90360852E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.96356206E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -5.93909654E+03   # A_t(Q)^DRbar
Block Ad Q=  3.96356206E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     4.90505812E+02   # A_b(Q)^DRbar
Block Ae Q=  3.96356206E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     8.96092807E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.96356206E+03  # soft SUSY breaking masses at Q
   1    6.11558624E+01  # M_1
   2   -1.05204250E+03  # M_2
   3    2.84164603E+03  # M_3
  21    2.21722584E+07  # M^2_(H,d)
  22    2.81195126E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.58423670E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.38347523E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.30369418E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.27130818E+02  # h0
        35     4.70234516E+03  # H0
        36     4.70371178E+03  # A0
        37     4.70292070E+03  # H+
   1000001     1.01117072E+04  # ~d_L
   2000001     1.00871700E+04  # ~d_R
   1000002     1.01113452E+04  # ~u_L
   2000002     1.00900891E+04  # ~u_R
   1000003     1.01117093E+04  # ~s_L
   2000003     1.00871736E+04  # ~s_R
   1000004     1.01113474E+04  # ~c_L
   2000004     1.00900897E+04  # ~c_R
   1000005     3.63236898E+03  # ~b_1
   2000005     4.36565234E+03  # ~b_2
   1000006     3.61762089E+03  # ~t_1
   2000006     4.34258444E+03  # ~t_2
   1000011     1.00208999E+04  # ~e_L-
   2000011     1.00091906E+04  # ~e_R-
   1000012     1.00201344E+04  # ~nu_eL
   1000013     1.00209097E+04  # ~mu_L-
   2000013     1.00092093E+04  # ~mu_R-
   1000014     1.00201441E+04  # ~nu_muL
   1000015     1.00145400E+04  # ~tau_1-
   2000015     1.00237294E+04  # ~tau_2-
   1000016     1.00229102E+04  # ~nu_tauL
   1000021     3.32121557E+03  # ~g
   1000022     5.91934805E+01  # ~chi_10
   1000023     2.56347394E+02  # ~chi_20
   1000025     2.64288547E+02  # ~chi_30
   1000035     1.13891120E+03  # ~chi_40
   1000024     2.55931931E+02  # ~chi_1+
   1000037     1.13789000E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.55034231E-02   # alpha
Block Hmix Q=  3.96356206E+03  # Higgs mixing parameters
   1    2.38604832E+02  # mu
   2    3.81667675E+01  # tan[beta](Q)
   3    2.42928169E+02  # v(Q)
   4    2.21249045E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.89991600E-01   # Re[R_st(1,1)]
   1  2     1.41126300E-01   # Re[R_st(1,2)]
   2  1    -1.41126300E-01   # Re[R_st(2,1)]
   2  2     9.89991600E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99995852E-01   # Re[R_sb(1,1)]
   1  2     2.88035618E-03   # Re[R_sb(1,2)]
   2  1    -2.88035618E-03   # Re[R_sb(2,1)]
   2  2     9.99995852E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     7.83017849E-02   # Re[R_sta(1,1)]
   1  2     9.96929702E-01   # Re[R_sta(1,2)]
   2  1    -9.96929702E-01   # Re[R_sta(2,1)]
   2  2     7.83017849E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.82789290E-01   # Re[N(1,1)]
   1  2    -3.51724232E-03   # Re[N(1,2)]
   1  3    -1.78962843E-01   # Re[N(1,3)]
   1  4     4.56633563E-02   # Re[N(1,4)]
   2  1     9.52085979E-02   # Re[N(2,1)]
   2  2     6.26174210E-02   # Re[N(2,2)]
   2  3    -7.03156090E-01   # Re[N(2,3)]
   2  4    -7.01844637E-01   # Re[N(2,4)]
   3  1     1.58280815E-01   # Re[N(3,1)]
   3  2    -4.20018513E-02   # Re[N(3,2)]
   3  3    -6.87991467E-01   # Re[N(3,3)]
   3  4     7.07001252E-01   # Re[N(3,4)]
   4  1     2.77826355E-03   # Re[N(4,1)]
   4  2    -9.97147197E-01   # Re[N(4,2)]
   4  3    -1.45449439E-02   # Re[N(4,3)]
   4  4    -7.40148213E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -2.05987664E-02   # Re[U(1,1)]
   1  2     9.99787823E-01   # Re[U(1,2)]
   2  1    -9.99787823E-01   # Re[U(2,1)]
   2  2    -2.05987664E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     1.04744058E-01   # Re[V(1,1)]
   1  2     9.94499212E-01   # Re[V(1,2)]
   2  1     9.94499212E-01   # Re[V(2,1)]
   2  2    -1.04744058E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02867697E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.65917796E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     9.05385037E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.50207825E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.41526233E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.70571067E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     4.00813713E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     5.63607545E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01026612E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     2.64130533E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.07080407E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.05011250E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.61887718E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.00643015E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.59186574E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     2.12051799E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.41633634E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.70154606E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.37952215E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     9.21596676E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.00798809E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     2.63930501E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.06620681E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.11332253E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     4.48042871E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.39171613E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.35676880E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.67823827E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.72045688E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     3.38471026E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.71535607E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.57085342E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     9.05013705E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     8.71385666E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.47422360E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     3.31569165E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.98897600E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.41530719E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.47460470E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     4.98431807E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02790272E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.82887355E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.00607486E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.41638110E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.46818737E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     4.98054375E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02561002E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     7.58058689E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.00153022E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.71916668E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.97866717E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     4.10448907E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.49345327E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.82060181E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.94667918E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.38572577E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.51750099E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     2.20699900E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.91181700E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.66804835E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.70686550E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.20923659E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.85592339E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.63122173E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.13114668E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.28410541E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.38632624E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.51820217E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.03008703E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     2.42804790E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.91089031E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.66838633E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.70804206E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.40214306E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     3.04051226E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.63097559E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.13109758E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.28374284E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.49441035E+02   # ~b_1
#    BR                NDA      ID1      ID2
     9.52126426E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     9.65501423E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     9.26078535E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     8.65996276E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.83922583E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.79207428E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     5.15911013E-02    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     1.33703105E+02   # ~b_2
#    BR                NDA      ID1      ID2
     2.59581414E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.29951577E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.24755626E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.61560846E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.27621783E-04    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     4.57320757E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.04062624E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.07490858E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     6.55744045E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.31874252E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     3.11081481E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     8.59692364E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.65641542E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.66790821E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.84973056E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.67022273E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.61994823E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.27205043E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.11918973E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.28381677E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.55751348E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.31870678E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     3.13811442E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     8.62457450E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.65630827E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.66824595E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.84965936E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.69349368E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.61970408E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.31100511E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.11914085E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.28345419E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.50723986E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.30125265E-03    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     6.84298638E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.56055702E-04    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     2.37804589E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.35312062E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     8.80458895E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.90983461E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.74038766E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     1.80924055E-02    2     1000021         4   # BR(~t_1 -> ~g c)
     4.33241509E-02    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
     3.99838132E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     3.08855643E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.93584030E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.40742073E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.46384329E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.84173816E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.79922895E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     9.54682260E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     4.92563817E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     7.46205601E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.48460016E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99991665E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     9.77582219E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     3.81493350E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.50456042E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.43891219E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.46429074E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.40120654E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.94258054E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     5.37203819E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     4.88969940E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.94715537E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     6.91402298E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     3.08564421E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     1.18670827E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     4.13102234E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     5.86890303E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.09685989E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.24227132E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.24227132E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.62985318E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     6.88737289E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.52270655E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.93013370E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.47944912E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.63504664E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.02390061E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     2.07709627E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     5.60179538E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     5.60179538E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     5.79157583E-01   # ~g
#    BR                NDA      ID1      ID2
     1.60711475E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.31623596E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     1.36994648E-04    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     1.36994738E-04    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     7.51837361E-03    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     6.99675657E-03    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.49086685E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     6.41667672E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.51302368E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     6.14909713E-02    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.51854868E-04    3     1000035         2        -2   # BR(~g -> chi^0_4 u u_bar)
     1.51854878E-04    3     1000035         4        -4   # BR(~g -> chi^0_4 c c_bar)
     2.41435038E-02    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.52133078E-04    3     1000035         1        -1   # BR(~g -> chi^0_4 d d_bar)
     1.52132991E-04    3     1000035         3        -3   # BR(~g -> chi^0_4 s s_bar)
     2.82626178E-02    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.21110498E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.21110498E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.34781083E-04    3     1000037         1        -2   # BR(~g -> chi^+_2 d u_bar)
     1.34781083E-04    3    -1000037        -1         2   # BR(~g -> chi^-_2 d_bar u)
     1.34781158E-04    3     1000037         3        -4   # BR(~g -> chi^+_2 s c_bar)
     1.34781158E-04    3    -1000037        -3         4   # BR(~g -> chi^-_2 s_bar c)
     3.01512312E-02    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     3.01512312E-02    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.86615856E-03   # Gamma(h0)
     2.52443472E-03   2        22        22   # BR(h0 -> photon photon)
     1.80798124E-03   2        22        23   # BR(h0 -> photon Z)
     3.60416600E-02   2        23        23   # BR(h0 -> Z Z)
     2.85762160E-01   2       -24        24   # BR(h0 -> W W)
     7.64975951E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.52029029E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.01071631E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.79786482E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.28321674E-07   2        -2         2   # BR(h0 -> Up up)
     2.49058529E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.21626678E-07   2        -1         1   # BR(h0 -> Down down)
     1.88660150E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.01421782E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     1.26694989E-02   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     1.30977186E+02   # Gamma(HH)
     1.56288546E-08   2        22        22   # BR(HH -> photon photon)
     3.30890392E-08   2        22        23   # BR(HH -> photon Z)
     6.28787818E-08   2        23        23   # BR(HH -> Z Z)
     1.10907222E-08   2       -24        24   # BR(HH -> W W)
     2.87088440E-06   2        21        21   # BR(HH -> gluon gluon)
     7.36447942E-09   2       -11        11   # BR(HH -> Electron electron)
     3.27948390E-04   2       -13        13   # BR(HH -> Muon muon)
     9.47221347E-02   2       -15        15   # BR(HH -> Tau tau)
     5.16220672E-14   2        -2         2   # BR(HH -> Up up)
     1.00151827E-08   2        -4         4   # BR(HH -> Charm charm)
     7.20409726E-04   2        -6         6   # BR(HH -> Top top)
     5.09329542E-07   2        -1         1   # BR(HH -> Down down)
     1.84231008E-04   2        -3         3   # BR(HH -> Strange strange)
     4.67254998E-01   2        -5         5   # BR(HH -> Bottom bottom)
     3.69185318E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.29757018E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.29757018E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.34406383E-04   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     2.78058373E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     2.09708325E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.51776688E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     4.17194351E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     3.26781774E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.11709502E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     6.65237309E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     2.34058300E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     6.02634496E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     6.74630718E-05   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     4.34750671E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.26024467E+02   # Gamma(A0)
     2.49959367E-10   2        22        22   # BR(A0 -> photon photon)
     7.28393305E-09   2        22        23   # BR(A0 -> photon Z)
     6.23790977E-06   2        21        21   # BR(A0 -> gluon gluon)
     7.13881324E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.17894142E-04   2       -13        13   # BR(A0 -> Muon muon)
     9.18182057E-02   2       -15        15   # BR(A0 -> Tau tau)
     4.65255069E-14   2        -2         2   # BR(A0 -> Up up)
     9.02624792E-09   2        -4         4   # BR(A0 -> Charm charm)
     6.51338001E-04   2        -6         6   # BR(A0 -> Top top)
     4.93698456E-07   2        -1         1   # BR(A0 -> Down down)
     1.78575938E-04   2        -3         3   # BR(A0 -> Strange strange)
     4.52914395E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     3.79932837E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.34942047E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.34942047E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.04965592E-04   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     2.97322953E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.96637224E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.76809760E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     4.51666427E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     3.10063863E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.01269410E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     6.55650670E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     2.73342266E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     6.60946665E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     5.28320826E-05   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     1.67102663E-07   2        23        25   # BR(A0 -> Z h0)
     4.15634136E-14   2        23        35   # BR(A0 -> Z HH)
     7.71027553E-41   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.45206479E+02   # Gamma(Hp)
     8.24716996E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.52592275E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     9.97331865E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.94271421E-07   2        -1         2   # BR(Hp -> Down up)
     8.38478958E-06   2        -3         2   # BR(Hp -> Strange up)
     5.34725068E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.36686525E-08   2        -1         4   # BR(Hp -> Down charm)
     1.78146392E-04   2        -3         4   # BR(Hp -> Strange charm)
     7.48806585E-04   2        -5         4   # BR(Hp -> Bottom charm)
     5.18178985E-08   2        -1         6   # BR(Hp -> Down top)
     1.39591482E-06   2        -3         6   # BR(Hp -> Strange top)
     5.05143034E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.77614441E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     7.69307167E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.24637051E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.17954040E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     7.32875322E-05   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.11568019E-01   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     1.17532150E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     3.57806140E-10   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.44991344E-07   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.47919774E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.45675422E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.45670214E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00003575E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    6.50729993E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    6.86482138E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.47919774E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.45675422E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.45670214E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999522E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    4.78017358E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999522E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    4.78017358E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26612803E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    4.66820363E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.10294278E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    4.78017358E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999522E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.10715242E-04   # BR(b -> s gamma)
    2    1.59170299E-06   # BR(b -> s mu+ mu-)
    3    3.52532113E-05   # BR(b -> s nu nu)
    4    3.02200639E-15   # BR(Bd -> e+ e-)
    5    1.29095744E-10   # BR(Bd -> mu+ mu-)
    6    2.69704143E-08   # BR(Bd -> tau+ tau-)
    7    1.01172262E-13   # BR(Bs -> e+ e-)
    8    4.32204553E-09   # BR(Bs -> mu+ mu-)
    9    9.14961901E-07   # BR(Bs -> tau+ tau-)
   10    9.64286511E-05   # BR(B_u -> tau nu)
   11    9.96071049E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41875620E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93586627E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15694662E-03   # epsilon_K
   17    2.28165475E-15   # Delta(M_K)
   18    2.48039603E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29129428E-11   # BR(K^+ -> pi^+ nu nu)
   20   -3.62778833E-16   # Delta(g-2)_electron/2
   21   -1.55100763E-11   # Delta(g-2)_muon/2
   22   -4.39876642E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.45986712E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.76689610E-01   # C7
     0305 4322   00   2     1.88865045E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -9.23088676E-02   # C8
     0305 6321   00   2    -6.39808271E-05   # C8'
 03051111 4133   00   0     1.62667620E+00   # C9 e+e-
 03051111 4133   00   2     1.62688987E+00   # C9 e+e-
 03051111 4233   00   2     4.55269023E-04   # C9' e+e-
 03051111 4137   00   0    -4.44936830E+00   # C10 e+e-
 03051111 4137   00   2    -4.44788211E+00   # C10 e+e-
 03051111 4237   00   2    -3.36945803E-03   # C10' e+e-
 03051313 4133   00   0     1.62667620E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62688973E+00   # C9 mu+mu-
 03051313 4233   00   2     4.55268865E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44936830E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44788225E+00   # C10 mu+mu-
 03051313 4237   00   2    -3.36945829E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50503052E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     7.28605077E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50503052E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     7.28605089E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50503052E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     7.28608373E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85830125E-07   # C7
     0305 4422   00   2    -6.68385609E-06   # C7
     0305 4322   00   2     1.46831128E-06   # C7'
     0305 6421   00   0     3.30489018E-07   # C8
     0305 6421   00   2    -1.83410446E-05   # C8
     0305 6321   00   2     3.60036739E-07   # C8'
 03051111 4133   00   2    -3.07525708E-10   # C9 e+e-
 03051111 4233   00   2     8.46500024E-06   # C9' e+e-
 03051111 4137   00   2     4.13909779E-06   # C10 e+e-
 03051111 4237   00   2    -6.26508771E-05   # C10' e+e-
 03051313 4133   00   2    -3.11437467E-10   # C9 mu+mu-
 03051313 4233   00   2     8.46499838E-06   # C9' mu+mu-
 03051313 4137   00   2     4.13910233E-06   # C10 mu+mu-
 03051313 4237   00   2    -6.26508827E-05   # C10' mu+mu-
 03051212 4137   00   2    -8.63290117E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.35475048E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -8.63289924E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.35475048E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -8.63235791E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.35475048E-05   # C11' nu_3 nu_3
