# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 12.05.2022,  16:48
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.76413477E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.44881834E+03  # scale for input parameters
    1   -9.71512809E+01  # M_1
    2    8.66998372E+02  # M_2
    3    1.73779010E+03  # M_3
   11    4.83742826E+02  # A_t
   12    5.35531213E+02  # A_b
   13    7.57096346E+02  # A_tau
   23    3.45325679E+02  # mu
   25    2.60984099E+00  # tan(beta)
   26    1.17672939E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.37398082E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.87123452E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.12085759E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.44881834E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.44881834E+03  # (SUSY scale)
  1  1     8.97877984E-06   # Y_u(Q)^DRbar
  2  2     4.56122016E-03   # Y_c(Q)^DRbar
  3  3     1.08470872E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.44881834E+03  # (SUSY scale)
  1  1     4.70881489E-05   # Y_d(Q)^DRbar
  2  2     8.94674829E-04   # Y_s(Q)^DRbar
  3  3     4.66966884E-02   # Y_b(Q)^DRbar
Block Ye Q=  3.44881834E+03  # (SUSY scale)
  1  1     8.21722632E-06   # Y_e(Q)^DRbar
  2  2     1.69906179E-03   # Y_mu(Q)^DRbar
  3  3     2.85754242E-02   # Y_tau(Q)^DRbar
Block Au Q=  3.44881834E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     4.83742810E+02   # A_t(Q)^DRbar
Block Ad Q=  3.44881834E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     5.35531218E+02   # A_b(Q)^DRbar
Block Ae Q=  3.44881834E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     7.57096353E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.44881834E+03  # soft SUSY breaking masses at Q
   1   -9.71512809E+01  # M_1
   2    8.66998372E+02  # M_2
   3    1.73779010E+03  # M_3
  21   -1.33319267E+05  # M^2_(H,d)
  22    1.00836386E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.37398082E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.87123452E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.12085759E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     8.82833776E+01  # h0
        35     1.47598783E+02  # H0
        36     1.17672939E+02  # A0
        37     1.67618268E+02  # H+
   1000001     1.01182955E+04  # ~d_L
   2000001     1.00933348E+04  # ~d_R
   1000002     1.01180077E+04  # ~u_L
   2000002     1.00972303E+04  # ~u_R
   1000003     1.01182961E+04  # ~s_L
   2000003     1.00933348E+04  # ~s_R
   1000004     1.01180083E+04  # ~c_L
   2000004     1.00972316E+04  # ~c_R
   1000005     2.43208530E+03  # ~b_1
   2000005     4.17360073E+03  # ~b_2
   1000006     2.43570311E+03  # ~t_1
   2000006     4.88333242E+03  # ~t_2
   1000011     1.00213789E+04  # ~e_L-
   2000011     1.00082897E+04  # ~e_R-
   1000012     1.00206870E+04  # ~nu_eL
   1000013     1.00213790E+04  # ~mu_L-
   2000013     1.00082899E+04  # ~mu_R-
   1000014     1.00206871E+04  # ~nu_muL
   1000015     1.00083578E+04  # ~tau_1-
   2000015     1.00214139E+04  # ~tau_2-
   1000016     1.00207218E+04  # ~nu_tauL
   1000021     2.11896241E+03  # ~g
   1000022     1.00420001E+02  # ~chi_10
   1000023     3.48848385E+02  # ~chi_20
   1000025     3.56556018E+02  # ~chi_30
   1000035     9.37646058E+02  # ~chi_40
   1000024     3.46345220E+02  # ~chi_1+
   1000037     9.37566427E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -8.97002065E-01   # alpha
Block Hmix Q=  3.44881834E+03  # Higgs mixing parameters
   1    3.45325679E+02  # mu
   2    2.60984099E+00  # tan[beta](Q)
   3    2.43202063E+02  # v(Q)
   4    1.38469206E+04  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.99999537E-01   # Re[R_st(1,1)]
   1  2     9.62084384E-04   # Re[R_st(1,2)]
   2  1    -9.62084384E-04   # Re[R_st(2,1)]
   2  2    -9.99999537E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99999994E-01   # Re[R_sb(1,1)]
   1  2     1.10637841E-04   # Re[R_sb(1,2)]
   2  1    -1.10637841E-04   # Re[R_sb(2,1)]
   2  2     9.99999994E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     8.76793980E-04   # Re[R_sta(1,1)]
   1  2     9.99999616E-01   # Re[R_sta(1,2)]
   2  1    -9.99999616E-01   # Re[R_sta(2,1)]
   2  2     8.76793980E-04   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.93620516E-01   # Re[N(1,1)]
   1  2    -3.97890351E-03   # Re[N(1,2)]
   1  3     1.11974785E-01   # Re[N(1,3)]
   1  4    -1.28096146E-02   # Re[N(1,4)]
   2  1    -8.85085700E-02   # Re[N(2,1)]
   2  2    -1.25945245E-01   # Re[N(2,2)]
   2  3     7.01287769E-01   # Re[N(2,3)]
   2  4    -6.96059978E-01   # Re[N(2,4)]
   3  1    -6.96809247E-02   # Re[N(3,1)]
   3  2     2.65029079E-02   # Re[N(3,2)]
   3  3     7.00460741E-01   # Re[N(3,3)]
   3  4     7.09786527E-01   # Re[N(3,4)]
   4  1     5.39185276E-03   # Re[N(4,1)]
   4  2    -9.91675128E-01   # Re[N(4,2)]
   4  3    -7.07945057E-02   # Re[N(4,3)]
   4  4     1.07422095E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.97846544E-02   # Re[U(1,1)]
   1  2     9.95009057E-01   # Re[U(1,2)]
   2  1     9.95009057E-01   # Re[U(2,1)]
   2  2     9.97846544E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -1.51815489E-01   # Re[V(1,1)]
   1  2     9.88408851E-01   # Re[V(1,2)]
   2  1     9.88408851E-01   # Re[V(2,1)]
   2  2     1.51815489E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02764021E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.87310652E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     7.81654630E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     4.84423124E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.42634673E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.58154331E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     9.25742735E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.97084771E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.14362852E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.01663278E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02775498E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.87288278E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     7.82197702E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     4.84971586E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
DECAY   1000013     1.42635248E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.58151382E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     9.25936949E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.97083595E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.14360379E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.01660857E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.06011770E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.81025607E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     9.34799055E-03    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     6.38157772E-03    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     3.18644728E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     1.42797487E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.57304121E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     9.80435561E-03    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     5.94829548E-04    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.96754845E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     6.13203178E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     6.00983526E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.42635127E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.84211025E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.90876454E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     1.25265921E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.00534142E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     1.42199506E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.93663381E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.42635703E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.84207466E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.90875686E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     1.25265417E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.00532932E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     1.42238780E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.93661032E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.42798015E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.83205484E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.90659389E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.25123469E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     3.00192405E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.53296022E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.92999616E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.33655185E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.58158123E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.92320972E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.62911446E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.68894218E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     6.25100884E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     4.99841764E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.02537687E-03    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.00447348E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.46151106E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.33655504E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.58158066E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.92320539E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.62915842E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.68893601E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     6.25189073E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     4.99839258E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.03010059E-03    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.00446954E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.46146854E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     8.88408605E+01   # ~b_1
#    BR                NDA      ID1      ID2
     3.94535993E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.96278592E-03    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     7.32805406E-04    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     8.60612571E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     5.95298725E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.86593243E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     1.25405823E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     1.84178518E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.24880185E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     5.74417058E-04    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     5.36002089E-04    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     9.56283327E-04    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     9.85400722E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     7.50897946E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.96413860E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     2.34680280E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.45441183E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.69977635E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.62902400E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.54360222E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.03390719E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     4.97900273E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     2.37345167E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.91172921E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.46131492E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.50906365E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.96410572E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.37368222E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.48236079E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.69966867E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.62906766E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.54359525E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.03624679E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     4.97898343E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     2.37362365E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.91167986E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.46127235E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     8.72519560E+01   # ~t_1
#    BR                NDA      ID1      ID2
     3.73534135E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     3.01753245E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     3.11814581E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     9.45155668E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     6.47502698E-03    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.74301017E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     1.06597786E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
     8.07435470E-04    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     4.91639061E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.18624484E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.11442080E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.15670315E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.46449615E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.24903896E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.93278347E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.14643906E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     1.79594268E-03    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     8.98136556E-04    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     1.28723244E-03    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     1.97916674E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     6.72384651E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
     3.26288428E-01    2          37   1000022   # BR(chi^+_1 -> H^+ chi^0_1)
#    BR                NDA      ID1      ID2       ID3
     1.32692023E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.72188834E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     6.97491438E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     1.67857467E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.71491129E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     1.24064440E-03    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     6.03735268E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     1.53870282E-01    2          37   1000025   # BR(chi^+_2 -> H^+ chi^0_3)
     1.65316906E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     6.12259246E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     1.76914547E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     3.86950553E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     9.56965675E-04    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.20634049E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     1.49180504E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     4.11161452E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     1.45570226E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     1.38588567E-01    2     1000022        36   # BR(chi^0_2 -> chi^0_1 A^0)
     7.10598355E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
     5.24069346E-03    2     1000022        35   # BR(chi^0_2 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     3.42986011E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.96254796E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.15463576E-02    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     7.60926578E-03    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     6.64570687E-01    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.77137567E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.62084515E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.62084515E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     5.75768532E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     5.75768532E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     6.60230435E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     6.56804695E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.61389671E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     2.45334277E-04    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     2.94172282E-02    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     5.85970814E-03    2     1000025        36   # BR(chi^0_4 -> chi^0_3 A^0)
     1.21191642E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.73582044E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     2.32628631E-03    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     8.79717383E-04    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     1.88988987E-03    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     1.74257371E-01    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     3.05350499E-04    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     2.93152637E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     1.43496646E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.43496646E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.27669863E-01   # ~g
#    BR                NDA      ID1      ID2
     3.52454300E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     3.80532180E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     3.24263506E-03    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     3.44541682E-03    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.76869269E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.44375697E-03    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.86509079E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     5.65367852E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     3.36427886E-02    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     3.68072490E-02    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.03546117E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.03546117E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     7.11379014E-02    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     7.11379014E-02    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     8.92137828E-03   # Gamma(h0)
     2.41425451E-04   2        22        22   # BR(h0 -> photon photon)
     1.07327293E-05   2        23        23   # BR(h0 -> Z Z)
     5.90540490E-05   2       -24        24   # BR(h0 -> W W)
     4.32432266E-03   2        21        21   # BR(h0 -> gluon gluon)
     7.44215750E-09   2       -11        11   # BR(h0 -> Electron electron)
     3.31005290E-04   2       -13        13   # BR(h0 -> Muon muon)
     9.53235819E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.71372486E-08   2        -2         2   # BR(h0 -> Up up)
     3.32488125E-03   2        -4         4   # BR(h0 -> Charm charm)
     9.18871503E-07   2        -1         1   # BR(h0 -> Down down)
     3.32337636E-04   2        -3         3   # BR(h0 -> Strange strange)
     8.96051716E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.20560158E-02   # Gamma(HH)
     3.21797830E-04   2        22        22   # BR(HH -> photon photon)
     1.06515902E-03   2        22        23   # BR(HH -> photon Z)
     2.60978061E-02   2        23        23   # BR(HH -> Z Z)
     2.11650809E-01   2       -24        24   # BR(HH -> W W)
     3.47309962E-02   2        21        21   # BR(HH -> gluon gluon)
     5.86923808E-09   2       -11        11   # BR(HH -> Electron electron)
     2.61087340E-04   2       -13        13   # BR(HH -> Muon muon)
     7.53085861E-02   2       -15        15   # BR(HH -> Tau tau)
     2.96432969E-08   2        -2         2   # BR(HH -> Up up)
     5.75264817E-03   2        -4         4   # BR(HH -> Charm charm)
     6.60741292E-07   2        -1         1   # BR(HH -> Down down)
     2.38982434E-04   2        -3         3   # BR(HH -> Strange strange)
     6.43347615E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.22381613E-03   2        23        36   # BR(HH -> Z A0)
DECAY        36     1.66974735E-02   # Gamma(A0)
     8.75624774E-06   2        22        22   # BR(A0 -> photon photon)
     2.25396146E-07   2        22        23   # BR(A0 -> photon Z)
     2.95385046E-03   2        21        21   # BR(A0 -> gluon gluon)
     7.80596580E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.47218106E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.00191697E-01   2       -15        15   # BR(A0 -> Tau tau)
     2.94694042E-09   2        -2         2   # BR(A0 -> Up up)
     5.73746028E-04   2        -4         4   # BR(A0 -> Charm charm)
     9.14295412E-07   2        -1         1   # BR(A0 -> Down down)
     3.30688411E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.95314414E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.78479654E-04   2        23        25   # BR(A0 -> Z h0)
DECAY        37     5.12336361E-03   # Gamma(Hp)
     4.13376004E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     1.76731013E-03   2       -13        14   # BR(Hp -> Muon nu_mu)
     4.99784185E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.00557412E-06   2        -1         2   # BR(Hp -> Down up)
     6.44018193E-05   2        -3         2   # BR(Hp -> Strange up)
     4.11988762E-05   2        -5         2   # BR(Hp -> Bottom up)
     1.45672026E-04   2        -1         4   # BR(Hp -> Down charm)
     4.68856183E-03   2        -3         4   # BR(Hp -> Strange charm)
     5.77335091E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.22428241E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.99575479E-01   2        24        25   # BR(Hp -> W h0)
     5.34736980E-04   2        24        35   # BR(Hp -> W HH)
     6.51928152E-02   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    4.77018238E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    3.04108761E+00    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    6.81126999E+00        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    4.46478794E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    7.00336705E-01    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.46815499E-01        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    4.77018238E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    3.04108761E+00    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    6.81126999E+00        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    7.43482644E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.56517356E-01        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    7.43482644E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.56517356E-01        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    4.85007567E-01        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    9.13806343E-01        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    3.17468753E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.56517356E-01        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    7.43482644E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    6.06225885E-04   # BR(b -> s gamma)
    2    1.71342869E-06   # BR(b -> s mu+ mu-)
    3    3.68018044E-05   # BR(b -> s nu nu)
    4    2.72368908E-15   # BR(Bd -> e+ e-)
    5    1.16352906E-10   # BR(Bd -> mu+ mu-)
    6    2.43573394E-08   # BR(Bd -> tau+ tau-)
    7    9.17651679E-14   # BR(Bs -> e+ e-)
    8    3.92020503E-09   # BR(Bs -> mu+ mu-)
    9    8.31508897E-07   # BR(Bs -> tau+ tau-)
   10    9.51362498E-05   # BR(B_u -> tau nu)
   11    9.82721037E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.98436856E-01   # |Delta(M_Bd)| [ps^-1] 
   13    2.13797751E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.39319621E-03   # epsilon_K
   17    2.28415827E-15   # Delta(M_K)
   18    2.58881084E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.54863338E-11   # BR(K^+ -> pi^+ nu nu)
   20    1.05688246E-17   # Delta(g-2)_electron/2
   21    4.51850268E-13   # Delta(g-2)_muon/2
   22    1.27824922E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -9.02663904E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -4.48632442E-01   # C7
     0305 4322   00   2    -4.99276257E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -3.17549005E-01   # C8
     0305 6321   00   2    -4.29665666E-03   # C8'
 03051111 4133   00   0     1.62117177E+00   # C9 e+e-
 03051111 4133   00   2     1.63657553E+00   # C9 e+e-
 03051111 4233   00   2    -3.22296585E-06   # C9' e+e-
 03051111 4137   00   0    -4.44386388E+00   # C10 e+e-
 03051111 4137   00   2    -4.59327868E+00   # C10 e+e-
 03051111 4237   00   2     2.48364382E-05   # C10' e+e-
 03051313 4133   00   0     1.62117177E+00   # C9 mu+mu-
 03051313 4133   00   2     1.63657553E+00   # C9 mu+mu-
 03051313 4233   00   2    -3.22304169E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.44386388E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.59327869E+00   # C10 mu+mu-
 03051313 4237   00   2     2.48365143E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.53770173E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -5.37808900E-06   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.53770173E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -5.37807251E-06   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.53770173E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -5.37342566E-06   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85822078E-07   # C7
     0305 4422   00   2     3.89489679E-06   # C7
     0305 4322   00   2     7.77737233E-08   # C7'
     0305 6421   00   0     3.30482126E-07   # C8
     0305 6421   00   2     1.12239477E-06   # C8
     0305 6321   00   2     9.18099365E-09   # C8'
 03051111 4133   00   2     1.27738578E-06   # C9 e+e-
 03051111 4233   00   2     6.21065528E-08   # C9' e+e-
 03051111 4137   00   2     5.32464072E-08   # C10 e+e-
 03051111 4237   00   2    -4.49644226E-07   # C10' e+e-
 03051313 4133   00   2     1.27738576E-06   # C9 mu+mu-
 03051313 4233   00   2     6.21065527E-08   # C9' mu+mu-
 03051313 4137   00   2     5.32464369E-08   # C10 mu+mu-
 03051313 4237   00   2    -4.49644227E-07   # C10' mu+mu-
 03051212 4137   00   2     3.10040144E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     9.73355847E-08   # C11' nu_1 nu_1
 03051414 4137   00   2     3.10040158E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     9.73355847E-08   # C11' nu_2 nu_2
 03051616 4137   00   2     3.10043982E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     9.73355846E-08   # C11' nu_3 nu_3
