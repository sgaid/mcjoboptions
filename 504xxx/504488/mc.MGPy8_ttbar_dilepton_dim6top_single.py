from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

# --------------------------------------------------------------
# Metadata 
# --------------------------------------------------------------
evgenConfig.description   = 'dileptonic ttbar with SMEFTatNLO-NLO'
evgenConfig.generators    = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.keywords     += ['ttbar','dim6top']
evgenConfig.contact       = ['baptiste.ravina@cern.ch']
evgenConfig.nEventsPerJob = 10000

# --------------------------------------------------------------
# Setting up the process 
# --------------------------------------------------------------
jo_name = get_physics_short()

process='''
    set stdout_level DEBUG
    import model dim6top_LO_UFO
    define p = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define vl = ve vm vt
    define l- = e- mu- ta-
    define vl~ = ve~ vm~ vt~
    generate p p > t t~ QED=0 QCD=2 FCNC=0, (t > w+ b FCNC=0,  w+ > l+ vl FCNC=0 DIM6=0), (t~ > w- b~ FCNC=0, w- > l- vl~ FCNC=0 DIM6=0) @0 DIM6=1
    output -f
'''

process_dir = new_process(process)

# Fix f2py
modify_config_card(process_dir=process_dir,settings={'f2py_compiler':'f2py2','f2py_compiler_py2':'f2py2'})
# --------------------------------------------------------------
# run_card
# --------------------------------------------------------------
nevents = runArgs.maxEvents if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob
nevents *= 1.1 # safety factor

settings = {
    'nevents'               : nevents,
    'maxjetflavor'          : '5',
    'fixed_ren_scale'       : 'True',
    'fixed_fac_scale'       : 'True',
    'pdlabel'               : '"lhapdf"',
    'lhaid'                 : '260000',
    'use_syst'              : 'True',
    'systematics_program'   : 'systematics',
    'systematics_arguments' : ['--mur=0.5,1.0,2.0','--muf=0.5,1.0,2.0','--weight_info=MUR%(mur).1f_MUF%(muf).1f_PDF%(pdf)i_DYNSCALE%(dyn)i','--dyn=3','--pdf=errorset,13100@0,25200@0,265000@0,266000@0'],
    'ptj'                   : '0.0',
    'ptl'                   : '0.0',
    'etaj'                  : '-1.0',
    'etal'                  : '-1.0',
    'drjj'                  : '0.0',
    'drll'                  : '0.0',
    'drjl'                  : '0.0',
    'dynamical_scale_choice': '3',
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# --------------------------------------------------------------
#  param_card
# --------------------------------------------------------------
# Set ATLAS SM parameters
params = dict() 
params['mass'] = dict()
params['mass']['6'] = '1.725000e+02'
params['mass']['23'] = '9.118760e+01'
params['mass']['24'] = '8.039900e+01'
params['mass']['25'] = '1.250000e+02'
params['yukawa'] = dict()
params['yukawa']['6'] = '1.725000e+02'
params['DECAY'] = dict()
params['DECAY']['23'] = 'DECAY  23   2.495200e+00'
params['DECAY']['24'] = '''DECAY  24   2.085000e+00
    3.377000e-01   2   -1   2
    3.377000e-01   2   -3   4
    1.082000e-01   2  -11  12
    1.082000e-01   2  -13  14
    1.082000e-01   2  -15  16'''
params['DECAY']['25'] = 'DECAY  25   6.382339e-03'
params['sminputs'] = dict()
params['sminputs']['1'] = '1.323489e+02' # change aewm1 to restore correct W mass

modify_param_card(process_dir=process_dir,params=params)

# Reset dim6top parameters
params = dict() 
params['dim6'] = dict()
params['dim6']['1'] = '1.000000e+03'
# Write the updated param card
modify_param_card(process_dir=process_dir,params=params)

reweight_card=process_dir+'/Cards/reweight_card.dat'
reweight_card_f = open(reweight_card,'w')
reweight_card_f.write('''
launch --rwgt_info=reset_do_not_use
'''+process_dir+'/Cards/param_card.dat'+'''
launch --rwgt_info=ctq8_m3p2
set DIM6 55 -3.2
launch --rwgt_info=ctq8_p6p8
set DIM6 55 6.8
launch --rwgt_info=ctq8_m1p6
set DIM6 55 -1.6
launch --rwgt_info=ctq8_p3p4
set DIM6 55 3.4
launch --rwgt_info=cqd1_m1p9
set DIM6 61 -1.9
launch --rwgt_info=cqd1_p5p0
set DIM6 61 5.0
launch --rwgt_info=cqd1_m0p95
set DIM6 61 -0.9
launch --rwgt_info=cqd1_p2p5
set DIM6 61 2.5
launch --rwgt_info=cqd8_m38p0
set DIM6 54 -38.0
launch --rwgt_info=cqd8_p16p0
set DIM6 54 16.0
launch --rwgt_info=cqd8_m3p8
set DIM6 54 -3.8
launch --rwgt_info=cqd8_p1p6
set DIM6 54 1.6
launch --rwgt_info=ctq1_m6p1
set DIM6 62 -6.1
launch --rwgt_info=ctq1_p15p0
set DIM6 62 15.0
launch --rwgt_info=ctq1_m3p05
set DIM6 62 -3.0
launch --rwgt_info=ctq1_p1p5
set DIM6 62 1.5
launch --rwgt_info=ctqqu8_m10p0
set DIM6 76 -10.0
launch --rwgt_info=ctqqu8_p10p0
set DIM6 76 10.0
launch --rwgt_info=ctqqu8_m5p0
set DIM6 76 -5.0
launch --rwgt_info=ctqqu8_p5p0
set DIM6 76 5.0
launch --rwgt_info=ctqqu8i_m10p0
set DIM6 77 -10.0
launch --rwgt_info=ctqqu8i_p10p0
set DIM6 77 10.0
launch --rwgt_info=ctqqu8i_m5p0
set DIM6 77 -5.0
launch --rwgt_info=ctqqu8i_p5p0
set DIM6 77 5.0
launch --rwgt_info=ctu1_m8p8
set DIM6 63 -8.8
launch --rwgt_info=ctu1_p17p0
set DIM6 63 17.0
launch --rwgt_info=ctu1_m4p4
set DIM6 63 -4.4
launch --rwgt_info=ctu1_p1p7
set DIM6 63 1.7
launch --rwgt_info=ctd8_m9p5
set DIM6 57 -9.5
launch --rwgt_info=ctd8_p45p0
set DIM6 57 45.0
launch --rwgt_info=ctd8_m4p75
set DIM6 57 -4.8
launch --rwgt_info=ctd8_p4p5
set DIM6 57 4.5
launch --rwgt_info=cqq13_m1p0
set DIM6 58 -1.0
launch --rwgt_info=cqq13_p0p8
set DIM6 58 0.8
launch --rwgt_info=cqq13_m2p0
set DIM6 58 -2.0
launch --rwgt_info=cqq13_p1p6
set DIM6 58 1.6
launch --rwgt_info=cqu8_m13p0
set DIM6 53 -13.0
launch --rwgt_info=cqu8_p16p0
set DIM6 53 16.0
launch --rwgt_info=cqu8_m1p3
set DIM6 53 -1.3
launch --rwgt_info=cqu8_p1p6
set DIM6 53 1.6
launch --rwgt_info=cqq11_m17p0
set DIM6 59 -17.0
launch --rwgt_info=cqq11_p7p5
set DIM6 59 7.5
launch --rwgt_info=cqq11_m1p7
set DIM6 59 -1.7
launch --rwgt_info=cqq11_p3p75
set DIM6 59 3.8
launch --rwgt_info=ctd1_m18p0
set DIM6 64 -18.0
launch --rwgt_info=ctd1_p9p0
set DIM6 64 9.0
launch --rwgt_info=ctd1_m1p8
set DIM6 64 -1.8
launch --rwgt_info=ctd1_p4p5
set DIM6 64 4.5
launch --rwgt_info=cqtqd8i_m10p0
set DIM6 85 -10.0
launch --rwgt_info=cqtqd8i_p10p0
set DIM6 85 10.0
launch --rwgt_info=cqtqd8i_m5p0
set DIM6 85 -5.0
launch --rwgt_info=cqtqd8i_p5p0
set DIM6 85 5.0
launch --rwgt_info=ctqqu1_m10p0
set DIM6 74 -10.0
launch --rwgt_info=ctqqu1_p10p0
set DIM6 74 10.0
launch --rwgt_info=ctqqu1_m5p0
set DIM6 74 -5.0
launch --rwgt_info=ctqqu1_p5p0
set DIM6 74 5.0
launch --rwgt_info=ctqqu1i_m10p0
set DIM6 75 -10.0
launch --rwgt_info=ctqqu1i_p10p0
set DIM6 75 10.0
launch --rwgt_info=ctqqu1i_m5p0
set DIM6 75 -5.0
launch --rwgt_info=ctqqu1i_p5p0
set DIM6 75 5.0
launch --rwgt_info=cqq83_m0p8
set DIM6 51 -0.8
launch --rwgt_info=cqq83_p1p3
set DIM6 51 1.3
launch --rwgt_info=cqq83_m1p6
set DIM6 51 -1.6
launch --rwgt_info=cqq83_p0p65
set DIM6 51 0.7
launch --rwgt_info=ctu8_m22p0
set DIM6 56 -22.0
launch --rwgt_info=ctu8_p9p4
set DIM6 56 9.4
launch --rwgt_info=ctu8_m2p2
set DIM6 56 -2.2
launch --rwgt_info=ctu8_p4p7
set DIM6 56 4.7
launch --rwgt_info=cqtqd8_m15p0
set DIM6 84 -15.0
launch --rwgt_info=cqtqd8_p9p2
set DIM6 84 9.2
launch --rwgt_info=cqtqd8_m1p5
set DIM6 84 -1.5
launch --rwgt_info=cqtqd8_p4p6
set DIM6 84 4.6
launch --rwgt_info=cqtqd1_m19p0
set DIM6 82 -19.0
launch --rwgt_info=cqtqd1_p9p2
set DIM6 82 9.2
launch --rwgt_info=cqtqd1_m1p9
set DIM6 82 -1.9
launch --rwgt_info=cqtqd1_p4p6
set DIM6 82 4.6
launch --rwgt_info=cqtqd1i_m10p0
set DIM6 83 -10.0
launch --rwgt_info=cqtqd1i_p10p0
set DIM6 83 10.0
launch --rwgt_info=cqtqd1i_m5p0
set DIM6 83 -5.0
launch --rwgt_info=cqtqd1i_p5p0
set DIM6 83 5.0
launch --rwgt_info=cpq3_m8p9
set DIM6 5 -8.9
launch --rwgt_info=cpq3_p7p0
set DIM6 5 7.0
launch --rwgt_info=cpq3_m4p45
set DIM6 5 -4.5
launch --rwgt_info=cpq3_p3p5
set DIM6 5 3.5
launch --rwgt_info=cqq81_m7p5
set DIM6 52 -7.5
launch --rwgt_info=cqq81_p7p6
set DIM6 52 7.6
launch --rwgt_info=cqq81_m3p75
set DIM6 52 -3.8
launch --rwgt_info=cqq81_p3p8
set DIM6 52 3.8
launch --rwgt_info=cqu1_m14p0
set DIM6 60 -14.0
launch --rwgt_info=cqu1_p8p8
set DIM6 60 8.8
launch --rwgt_info=cqu1_m1p4
set DIM6 60 -1.4
launch --rwgt_info=cqu1_p4p4
set DIM6 60 4.4
launch --rwgt_info=ctg_m0p4
set DIM6 16 -0.4
launch --rwgt_info=ctg_p0p4
set DIM6 16 0.4
launch --rwgt_info=ctg_m0p8
set DIM6 16 -0.8
launch --rwgt_info=ctg_p0p8
set DIM6 16 0.8
launch --rwgt_info=ctgi_m10p0
set DIM6 17 -10.0
launch --rwgt_info=ctgi_p10p0
set DIM6 17 10.0
launch --rwgt_info=ctgi_m5p0
set DIM6 17 -5.0
launch --rwgt_info=ctgi_p5p0
set DIM6 17 5.0
launch --rwgt_info=cbw_m1p3
set DIM6 14 -1.3
launch --rwgt_info=cbw_p4p8
set DIM6 14 4.8
launch --rwgt_info=cbw_m0p65
set DIM6 14 -0.7
launch --rwgt_info=cbw_p2p4
set DIM6 14 2.4
launch --rwgt_info=cbwi_m10p0
set DIM6 15 -10.0
launch --rwgt_info=cbwi_p10p0
set DIM6 15 10.0
launch --rwgt_info=cbwi_m5p0
set DIM6 15 -5.0
launch --rwgt_info=cbwi_p5p0
set DIM6 15 5.0
launch --rwgt_info=ctw_m2p0
set DIM6 10 -2.0
launch --rwgt_info=ctw_p1p0
set DIM6 10 1.0
launch --rwgt_info=ctw_m1p0
set DIM6 10 -1.0
launch --rwgt_info=ctw_p2p0
set DIM6 10 2.0
launch --rwgt_info=ctwi_m10p0
set DIM6 12 -10.0
launch --rwgt_info=ctwi_p10p0
set DIM6 12 10.0
launch --rwgt_info=ctwi_m5p0
set DIM6 12 -5.0
launch --rwgt_info=ctwi_p5p0
set DIM6 12 5.0
launch --rwgt_info=cptb_m8p9
set DIM6 8 -8.9
launch --rwgt_info=cptb_p7p0
set DIM6 8 7.0
launch --rwgt_info=cptb_m4p45
set DIM6 8 -4.5
launch --rwgt_info=cptb_p3p5
set DIM6 8 3.5
launch --rwgt_info=cptbi_m10p0
set DIM6 9 -10.0
launch --rwgt_info=cptbi_p10p0
set DIM6 9 10.0
launch --rwgt_info=cptbi_m5p0
set DIM6 9 -5.0
launch --rwgt_info=cptbi_p5p0
set DIM6 9 5.0
''')
reweight_card_f.close()

print_cards()

# --------------------------------------------------------------
# Generate
# --------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# --------------------------------------------------------------
# Run Pythia 8 Showering
# --------------------------------------------------------------
check_reset_proc_number(opts)
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
