from MadGraphControl.MadGraphUtils import * 
from MadGraphControl.MadGraphParamHelpers import set_top_params
from MadGraphControl.DiagramRemoval import do_DRX

evgenConfig.nEventsPerJob = 5000
nevents = int(1.4*runArgs.maxEvents) if runArgs.maxEvents > 0 else int(1.4*evgenConfig.nEventsPerJob)

DR_mode=1
mode=0


gen_process = """
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define w = w+ w-
define ttbar = t t~
generate p p > ttbar w Z [QCD]
output -f
"""
process_dir = new_process(gen_process)

set_top_params(process_dir,mTop=172.5,FourFS=False)

#Call DR code
do_DRX(DR_mode,process_dir)
    
run_settings = { 
    'lhe_version':'3.0',
    'pdlabel'    : "'lhapdf'",
    'lhaid'         : 260000,
    'maxjetflavor'  : 5,
    'parton_shower' : 'PYTHIA8',
    'fixed_ren_scale' : "False",
    'fixed_fac_scale' : "False",
    'muR_ref_fixed' : 172.5,
    'muF1_ref_fixed' : 172.5,
    'muF2_ref_fixed' : 172.5,
    'fixed_QES_scale' : "True",
    'QES_ref_fixed' : 172.5,
    'reweight_scale': '.true.',
    'rw_Rscale_down':  0.5,  
    'rw_Rscale_up'  :  2.0,  
    'rw_Fscale_down':  0.5,  
    'rw_Fscale_up'  :  2.0,  
    'reweight_PDF'  : '.true.',
    'PDF_set_min'   : 260001,
    'PDF_set_max'   : 260100,
    'mll_sf'        : -1,
    'req_acc' :0.001,
}

# Set up the process

# Set up the run card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_settings)

madspin_card_loc=process_dir+'/Cards/madspin_card.dat'                                                                                                                                   
mscard = open(madspin_card_loc,'w')  
mscard.write("""
  set max_weight_ps_point 500
  set Nevents_for_max_weigth 500
  set BW_cut 50
 set seed %i
 # specify the decay for the final state particles
 define q = u d s c b
 define q~ = u~ d~ s~ c~ b~
 define l+ = e+ mu+ ta+
 define l- = e- mu- ta-
 define v = ve vm vt
 define v~ = ve~ vm~ vt~
 decay t > w+ b, w+ > all all
 decay t~ > w- b~, w- > all all
 decay w+ > all all
 decay w- > all all
 decay z > v v~
 # running the actual code
 launch"""%(runArgs.randomSeed))
mscard.close()



# Generate the events
generate(process_dir=process_dir,runArgs=runArgs)

# Remember to set saveProcDir to FALSE before sending for production!!
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)


#### Shower
## run Pythia8 on-the-fly -----------------------------------------------------
## Provide config information
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]
evgenConfig.description = "MG5aMCatNLO/MadSpin/Pythia8 tWZ_Ztonunu DR1, DR applied only to MG code"
evgenConfig.keywords    = ["SM","top"]
evgenConfig.contact     = ["olga.bylund@cern.ch"]
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")


