process="tWmcb"
include("MadGraphControl_ttWcbWll.py")


# --------------------------------------------------------------
# Run Pythia 8 Showering
# --------------------------------------------------------------
   
evgenConfig.generators += ["aMcAtNlo","Pythia8"]
evgenConfig.description = 'MG5_aMC@NLO+Pythia8+EvtGen for tt, W->cb and W->leptons, OTF, A14 NNPDF 2.3 LO, ME NNPDF 3.0 NLO, using scale sqrt(sum_i mT(i)**2/2)), for i = top quarks'
evgenConfig.keywords+=['SM', 'top', 'ttbar']
evgenConfig.contact = ['anna.ivina@cern.ch','ian.connelly@cern.ch', 'steffen.henkelmann@cern.ch']
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=outputDS    
evgenConfig.nEventsPerJob = 1000


include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
