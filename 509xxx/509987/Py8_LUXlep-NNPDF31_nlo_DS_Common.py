## Config for Py8 with LUX pdf

include("Pythia8_i/Pythia8_Base_Fragment.py")

genSeq.Pythia8.Commands += [
    "PDF:pSet= LHAPDF6:LUXlep-NNPDF31_nlo_as_0118_luxqed",
    "BeamRemnants:unresolvedHadron=2",
    "PartonLevel:MPI = off",
    "PartonLevel:ISR = on",
    "SpaceShower:dipoleRecoil = on",

    "BeamRemnants:primordialKThard = 1.5",
    "SpaceShower:pTmaxMatch = 0.",
    "SpaceShower:pTmaxFudge = 1.0",
    "SpaceShower:pTdampMatch  = 1",
    "SpaceShower:pTdampFudge = 1.5", 

     "BeamRemnants:primordialKTremnant = 0.1" 
 ]

evgenConfig.tune = "NNPDF23_QED"

#EvtGen for b fragmentation as default.  No EvtGen is available in "nonStandard"
include("Pythia8_i/Pythia8_EvtGen.py")

