#-------------------------------------------------------------------------------------------------
### JO file to generate W* events
#-------------------------------------------------------------------------------------------------

from MadGraphControl.MadGraphUtils import *

# Define number of generated LHE events
nevents=10*(runArgs.maxEvents)
mode=0


dosml=False
dodil=False

do4FS=False
do5FS=False


##Getting the Physics short, as while running in release 21, need to pass the directory itself##
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()

title_str=str(phys_short)
print title_str 
           
for s in title_str.split("_"):
    ss=s
#    print ss
    if ss=='sml':
        dosml=True
    if ss=='dil':
        dodil=True
if (not dosml and not dodil) or (dosml and dodil):
    raise RuntimeError("No decay mode was identified, check jobOption name %s"%title_str)

for s in title_str.split("_"):
    ss=s.replace("FS","")
#    print ss
    if ss.isdigit() and int(ss)==4:
        do4FS=True
    if ss.isdigit() and int(ss)==5:
        do5FS=True
if (not do4FS and not do5FS) or (do4FS and do5FS):
    raise RuntimeError("Flavour scheme does not recognised, check jobOption name %s"%title_str)

if do4FS:
  process="""
    import model ehsm
    define p = g u c d s u~ c~ d~ s~
    generate p p > Ws- > t t~ b b~
    output -f
    """
if do5FS:
  process="""
    import model ehsm
    define p = g u c d s u~ c~ d~ s~ b b~
    generate p p > Ws- > t t~ b
    output -f
    """

#------------------------------------------------------------------------------------
# JOB OPTION NAME MUST CONTAIN THE MASS WE WANT TO SIMULATE IN FORMAT LIKE: *_M700_*
#------------------------------------------------------------------------------------

mWs=0
for s in title_str.split("_"):
    ss=s.replace("M","")
    if ss.isdigit():
        mWs = int(ss)
if mWs==0:
        raise RuntimeError("Wstar mass does not set, check joOption name %s"%title_str)
print mWs


#Fetch default LO run_card.dat and set parameters
extras = { 'lhe_version':'3.0',
           'cut_decays' :'F',
           'pdlabel'    :"'lhapdf'",
           'lhaid'      :'247000',
           'use_syst'   : 'False'
         }

extras['nevents'] = nevents


# set up process
process_dir = new_process(process)
print "process_dir", process_dir

#------------------------------------------------
# Run Card
#--------------------------------------------------
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

masses = {'4000023':str(mWs)+'  #  MWs'}

params = {}
params['MASS'] = masses
print masses

#------------------------------------------------
# Param Card
#--------------------------------------------------
modify_param_card(process_dir=process_dir,params=params)

print_cards()

runName='run_01'     
    
generate(process_dir=process_dir, runArgs=runArgs)
outputDS=arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)
 

#### Showering with Pythia8 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

evgenConfig.description = 'MadGraph_Wstar'
evgenConfig.contact = ['Mihail Chizhov < Mihail.Chizhov@cern.ch>']
evgenConfig.keywords+=['top','bottom']
#evgenConfig.inputfilecheck = runName
#runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'
runArgs.inputGeneratorFile=outputDS


# JOB OPTION NAME MUST CONTAIN THE DECAY MODE WE WANT TO SIMULATE IN FORMAT LIKE: *_sml.py or *_dil.py

if dosml:
#    evgenConfig.keywords+=['semileptonic']
    from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter   # lep filter
    filtSeq += TTbarWToLeptonFilter("TTbarWToLeptonFilter")

    TTbarWToLeptonFilter = filtSeq.TTbarWToLeptonFilter

    TTbarWToLeptonFilter.NumLeptons = -1
    TTbarWToLeptonFilter.Ptcut = 0.

    filtSeq.Expression = "(TTbarWToLeptonFilter)"

if dodil:
#    evgenConfig.keywords+=['dileptonic']
    from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter   # lep filter
    filtSeq += TTbarWToLeptonFilter("TTbarWToLeptonFilter")

    TTbarWToLeptonFilter = filtSeq.TTbarWToLeptonFilter

    TTbarWToLeptonFilter.NumLeptons = 2
    TTbarWToLeptonFilter.Ptcut = 0.

    filtSeq.Expression = "(TTbarWToLeptonFilter)"    
    evgenConfig.minevents=2000
                                                     
