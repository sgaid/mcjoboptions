import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

gridpack_mode = True

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------------------------------
# Due to the low filter efficiency, the number of generated events are set to safefactor times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
safefactor=2
nevents=100*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor
mode=0

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
extras = { 'lhe_version':'2.0', 
    'cut_decays':'F', 
    'pta':'20',
    'ptl':'7',
    'ptamax':'-1',
    'ptlmax':'-1',
    'etal':'3',
    'etaa':'3',
    'etaamin':'0',
    'etalmin':'0',
    'mmaa':'95',
    'mmaamax':'160',
    'draa':'0.4',
    'dral':'0.4',
    'draamax':'-1.0',
    'dralmax':'-1.0',
    'nevents':int(nevents)}

#---------------------------------------------------------------------------------------------------
# Generating lv lv a a  process with MadGraph
# Parameters are set above
#---------------------------------------------------------------------------------------------------
process="""
        import model sm
        define l+ = e+ mu+ ta+
        define l- = e- mu- ta-
        define vl = ve vm vt
        define vl~ = ve~ vm~ vt~
        
        define p = g u c d s u~ c~ d~ s~
        define j = g u c d s u~ c~ d~ s~
         
        generate p p > l+ vl l- vl~ a a  QCD=0 
        output -f"""

process_dir = new_process(process)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# https://gitlab.cern.ch/atlas/athena/-/tree/master/Generators/MadGraphControl
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used 
#---------------------------------------------------------------------------------------------------
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# https://gitlab.cern.ch/atlas/athena/-/tree/master/Generators/MadGraphControl
#---------------------------------------------------------------------------------------------------
modify_param_card(process_dir=process_dir)
#---------------------------------------------------------------------------------------------------
# Printing cards   
#---------------------------------------------------------------------------------------------------
print_cards()

#---------------------------------------------------------------------------------------------------
# Generate events                                                               
#---------------------------------------------------------------------------------------------------     
generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)

#---------------------------------------------------------------------------------------------------
# Move output files into the appropriate place, with the appropriate name
#---------------------------------------------------------------------------------------------------   
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=2,saveProcDir=True)

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.description = "SM p p > lv lv j j gam gam, qcd=0 at 13TeV"
evgenConfig.keywords = ["SM","diphoton"]
evgenConfig.contact = ['Shuiting Xin<Shuiting.Xin@cern.ch>']
evgenConfig.nEventsPerJob = 10000

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")


from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter

filtSeq += MultiLeptonFilter("LepTwoFilter")
filtSeq.LepTwoFilter.NLeptons = 2
filtSeq.LepTwoFilter.Ptcut = 7000
filtSeq.LepTwoFilter.Etacut = 3
filtSeq.Expression = "LepTwoFilter"
