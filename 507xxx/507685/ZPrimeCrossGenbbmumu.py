# Job options for vector leptoquark (U1) pair production
# To be used for MC16 in r21.6 (tested with 21.6.45)

from MadGraphControl.MadGraphUtils import *
import os
import re
import math
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

THIS_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
job_option_name = [ f for f in os.listdir(THIS_DIR) if (f.startswith('mc') and f.endswith('.py'))][0]
print job_option_name

matchesMass = re.search("M([0-9]+).*\.py", job_option_name)
if matchesMass is None:
    raise RuntimeError("Cannot find mass string in job option name: {:s}.".format(job_option_name))
else:
    zpmass = float(matchesMass.group(1))

# ecmEnergy given when running Gen_tf.py
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

mgproc = """
import model Zpll_LH_UFO_CrossGen
define p1 = g u d c s b u~ d~ c~ s~ b~ ## 5-flavour scheme
define j1 = g u d c s b u~ d~ c~ s~ b~ ## any jet
define j2 = b b~ ## only b's
define p3 = b b~
define p4 = s s~

# General case (inclusive)
generate p1 p1 > zp j2 j2 NP=2 QCD=2 QED=0, zp > mu+ mu- 
add process p1 p1 > zp j2 NP=2 QCD=1 QED=0, zp > mu+ mu-
output -f
"""

process_dir = new_process(process=mgproc)

settings = {
    'nevents'      : 4.5*runArgs.maxEvents,
    'lhe_version': '2.0',
    'cut_decays' : 'F',
    'ickkw' : 0,
    'xqcut': 0.,
    'drjj' : 0.0,
    'maxjetflavor': 5,
    'dparameter': 0.4,
    'ktdurham': 30.,
}
modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)

# Parameters
parameters = {
    'DECAY':{
        '9000005' : 'Auto' #str(zpmass/(8*math.pi))
    },
    'mass':{
        '9000005' : '{:e}'.format(zpmass)
        },
}
modify_param_card(process_dir=process_dir, params=parameters)

pdgfile = open("pdgid_extras.txt", "w+")
pdgfile.write("""9000005
-9000005
""")
pdgfile.close()


# Print cards on screen
print_cards(run_card='run_card.dat', param_card='param_card.dat')

generate(process_dir=process_dir, runArgs=runArgs)

arrange_output(process_dir=process_dir, lhe_version=3, saveProcDir=True, runArgs=runArgs)

# Metadata
evgenConfig.description = ('ZPrime+bbbar to mu+ mu-')
evgenConfig.keywords+=['BSM','exotic','zprime']
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.process = 'pp -> zp b b~'
evgenConfig.contact = ["Roy S. Brener <roy.brener@cern.ch>"]

PYTHIA8_nJetMax=2
PYTHIA8_Dparameter=0.4
PYTHIA8_Process="guess"
PYTHIA8_TMS=30.
PYTHIA8_nQuarksMerge=5

# Shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")


genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]
