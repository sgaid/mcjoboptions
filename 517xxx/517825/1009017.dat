# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.05.2022,  10:22
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.59768663E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.56176184E+03  # scale for input parameters
    1    2.42261206E+02  # M_1
    2    8.56892125E+02  # M_2
    3    3.72134149E+03  # M_3
   11    7.63317338E+03  # A_t
   12    1.64140353E+03  # A_b
   13    1.48060130E+03  # A_tau
   23    1.85596899E+03  # mu
   25    5.45578883E+01  # tan(beta)
   26    2.24753321E+03  # m_A, pole mass
   31    1.21652870E+03  # M_L11
   32    1.21652870E+03  # M_L22
   33    1.15804590E+03  # M_L33
   34    2.19179653E+02  # M_E11
   35    2.19179653E+02  # M_E22
   36    1.75454520E+03  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.58813951E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.77283472E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.27738808E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.56176184E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.56176184E+03  # (SUSY scale)
  1  1     8.38578041E-06   # Y_u(Q)^DRbar
  2  2     4.25997645E-03   # Y_c(Q)^DRbar
  3  3     1.01306963E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.56176184E+03  # (SUSY scale)
  1  1     9.19350858E-04   # Y_d(Q)^DRbar
  2  2     1.74676663E-02   # Y_s(Q)^DRbar
  3  3     9.11707968E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.56176184E+03  # (SUSY scale)
  1  1     1.60433448E-04   # Y_e(Q)^DRbar
  2  2     3.31725487E-02   # Y_mu(Q)^DRbar
  3  3     5.57907697E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.56176184E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     7.63317342E+03   # A_t(Q)^DRbar
Block Ad Q=  3.56176184E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.64140339E+03   # A_b(Q)^DRbar
Block Ae Q=  3.56176184E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.48060131E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.56176184E+03  # soft SUSY breaking masses at Q
   1    2.42261206E+02  # M_1
   2    8.56892125E+02  # M_2
   3    3.72134149E+03  # M_3
  21    1.74705611E+06  # M^2_(H,d)
  22   -3.38868155E+06  # M^2_(H,u)
  31    1.21652870E+03  # M_(L,11)
  32    1.21652870E+03  # M_(L,22)
  33    1.15804590E+03  # M_(L,33)
  34    2.19179653E+02  # M_(E,11)
  35    2.19179653E+02  # M_(E,22)
  36    1.75454520E+03  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.58813951E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.77283472E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.27738808E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.28887805E+02  # h0
        35     2.24748539E+03  # H0
        36     2.24753321E+03  # A0
        37     2.25022901E+03  # H+
   1000001     1.00997436E+04  # ~d_L
   2000001     1.00753180E+04  # ~d_R
   1000002     1.00993879E+04  # ~u_L
   2000002     1.00783554E+04  # ~u_R
   1000003     1.00997523E+04  # ~s_L
   2000003     1.00753290E+04  # ~s_R
   1000004     1.00993949E+04  # ~c_L
   2000004     1.00783563E+04  # ~c_R
   1000005     2.70252961E+03  # ~b_1
   2000005     3.39486872E+03  # ~b_2
   1000006     2.69283341E+03  # ~t_1
   2000006     4.71107768E+03  # ~t_2
   1000011     1.22614065E+03  # ~e_L-
   2000011     2.44363120E+02  # ~e_R-
   1000012     1.22322730E+03  # ~nu_eL
   1000013     1.22613893E+03  # ~mu_L-
   2000013     2.43973943E+02  # ~mu_R-
   1000014     1.22318379E+03  # ~nu_muL
   1000015     1.14592997E+03  # ~tau_1-
   2000015     1.73803701E+03  # ~tau_2-
   1000016     1.15122168E+03  # ~nu_tauL
   1000021     4.17331653E+03  # ~g
   1000022     2.38815620E+02  # ~chi_10
   1000023     9.06696546E+02  # ~chi_20
   1000025     1.86314734E+03  # ~chi_30
   1000035     1.86592628E+03  # ~chi_40
   1000024     9.06732220E+02  # ~chi_1+
   1000037     1.86669918E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.79512293E-02   # alpha
Block Hmix Q=  3.56176184E+03  # Higgs mixing parameters
   1    1.85596899E+03  # mu
   2    5.45578883E+01  # tan[beta](Q)
   3    2.43042663E+02  # v(Q)
   4    5.05140553E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.97581557E-01   # Re[R_st(1,1)]
   1  2     6.95056667E-02   # Re[R_st(1,2)]
   2  1    -6.95056667E-02   # Re[R_st(2,1)]
   2  2    -9.97581557E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99375159E-01   # Re[R_sb(1,1)]
   1  2     3.53453123E-02   # Re[R_sb(1,2)]
   2  1    -3.53453123E-02   # Re[R_sb(2,1)]
   2  2     9.99375159E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     9.94421379E-01   # Re[R_sta(1,1)]
   1  2     1.05480427E-01   # Re[R_sta(1,2)]
   2  1    -1.05480427E-01   # Re[R_sta(2,1)]
   2  2     9.94421379E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.99705945E-01   # Re[N(1,1)]
   1  2    -4.00529708E-04   # Re[N(1,2)]
   1  3     2.39964264E-02   # Re[N(1,3)]
   1  4    -3.46899430E-03   # Re[N(1,4)]
   2  1    -1.82154446E-03   # Re[N(2,1)]
   2  2    -9.98100112E-01   # Re[N(2,2)]
   2  3     5.53121595E-02   # Re[N(2,3)]
   2  4    -2.70815999E-02   # Re[N(2,4)]
   3  1     1.44994235E-02   # Re[N(3,1)]
   3  2    -1.99978728E-02   # Re[N(3,2)]
   3  3    -7.06618382E-01   # Re[N(3,3)]
   3  4    -7.07163569E-01   # Re[N(3,4)]
   4  1    -1.93512666E-02   # Re[N(4,1)]
   4  2     5.82759875E-02   # Re[N(4,2)]
   4  3     7.05021417E-01   # Re[N(4,3)]
   4  4    -7.06522639E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.96918448E-01   # Re[U(1,1)]
   1  2     7.84449420E-02   # Re[U(1,2)]
   2  1     7.84449420E-02   # Re[U(2,1)]
   2  2     9.96918448E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.99253003E-01   # Re[V(1,1)]
   1  2     3.86450054E-02   # Re[V(1,2)]
   2  1     3.86450054E-02   # Re[V(2,1)]
   2  2     9.99253003E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     2.47259117E-03   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000011     4.73162933E+00   # ~e^-_L
#    BR                NDA      ID1      ID2
     3.00659842E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.33814464E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     4.65525550E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     2.14403386E-03   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
DECAY   1000013     4.76078424E+00   # ~mu^-_L
#    BR                NDA      ID1      ID2
     2.98821191E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.32383707E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     4.62670903E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.12419947E-03    2     2000013        25   # BR(~mu^-_L -> ~mu^-_R h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     3.49105170E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     3.94518163E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.02884247E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     4.02597590E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     2.17186303E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     3.82774456E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     3.15127247E-04    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     5.89056603E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     3.14127244E-01    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     1.57432849E-01    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     1.44761268E-01    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
DECAY   1000012     4.69216614E+00   # ~nu_e
#    BR                NDA      ID1      ID2
     3.03262775E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.31616352E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     4.65120873E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
DECAY   1000014     4.69145240E+00   # ~nu_mu
#    BR                NDA      ID1      ID2
     3.03296414E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.31603160E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     4.65100426E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
DECAY   1000016     3.51538178E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     3.77068346E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.06613159E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     4.16318493E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.51413410E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.01835650E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.89810266E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.80805467E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.07634802E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     6.39628323E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.32178058E-04    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.27705765E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     7.49652569E-04    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.05242044E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.51528294E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.01815185E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.02771142E-04    2    -1000037         4   # BR(~s_R -> chi^-_2 c)
     9.89606061E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.80866901E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.07621409E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     6.39573871E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     2.73796421E-04    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.27694357E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     7.54580461E-04    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.05170651E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     5.53194616E+01   # ~b_1
#    BR                NDA      ID1      ID2
     7.59228173E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.71562938E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.10910150E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.09801092E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.34455475E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.65678063E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     6.19471129E+01   # ~b_2
#    BR                NDA      ID1      ID2
     3.03436695E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.07895179E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.21318195E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.20220825E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.16187067E-03    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.38334607E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.67026093E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.28295794E-02    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     4.70096934E-02    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     5.68576041E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.95166203E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.60461663E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.80788708E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.05772341E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     6.40452814E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.83128497E-04    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.28303075E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.82163756E-04    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.05210148E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.68582888E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.95161465E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.60450200E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.80849915E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.05753985E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     6.40395703E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.85608313E-04    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.28292174E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     2.65396148E-04    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.05138735E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     5.84574931E+01   # ~t_1
#    BR                NDA      ID1      ID2
     2.25044848E-03    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     6.56553272E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     5.67876893E-02    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     1.54823883E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.26083970E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.28391439E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     3.17743586E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.05756701E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.59674994E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     4.01718527E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.58834270E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     7.76713358E-04    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     8.46621794E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     8.37680696E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.57777668E-03    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.69130982E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     4.20478607E-02    2     1000021         6   # BR(~t_2 -> ~g t)
     2.57392129E-01    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.18051896E-04    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     1.29499003E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.05143808E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.79999724E-03   # chi^+_1
#    BR                NDA      ID1      ID2
     2.91956358E-02    2    -2000013        14   # BR(chi^+_1 -> ~mu^+_R nu_mu)
     7.29981203E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.18843987E-02    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
     6.56839303E-02    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     6.56919052E-02    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     9.75569994E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.52599565E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.07099944E-03    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     1.28567953E-03    2    -2000013        14   # BR(chi^+_2 -> ~mu^+_R nu_mu)
     1.07100400E-03    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     6.72602719E-03    2    -2000015        16   # BR(chi^+_2 -> ~tau^+_2 nu_tau)
     2.61809766E-04    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     6.95008021E-04    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     1.44803263E-01    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     8.02902170E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.46904986E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.43687168E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.54564804E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     8.17230696E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.93436453E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     3.11370323E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     7.11433274E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.83209047E-03   # chi^0_2
#    BR                NDA      ID1      ID2
     3.54822431E-03    2     2000011       -11   # BR(chi^0_2 -> ~e^-_R e^+)
     3.54822431E-03    2    -2000011        11   # BR(chi^0_2 -> ~e^+_R e^-)
     1.78064870E-02    2     2000013       -13   # BR(chi^0_2 -> ~mu^-_R mu^+)
     1.78064870E-02    2    -2000013        13   # BR(chi^0_2 -> ~mu^+_R mu^-)
     1.72719617E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     5.55752692E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.29846839E-03    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
     5.49568450E-04    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.20579956E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.20592024E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     4.97871694E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.10987274E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     1.83305996E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     5.88450738E-04    2     2000013       -13   # BR(chi^0_3 -> ~mu^-_R mu^+)
     5.88450738E-04    2    -2000013        13   # BR(chi^0_3 -> ~mu^+_R mu^-)
     1.89305741E-04    2     1000013       -13   # BR(chi^0_3 -> ~mu^-_L mu^+)
     1.89305741E-04    2    -1000013        13   # BR(chi^0_3 -> ~mu^+_L mu^-)
     6.03606278E-02    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     6.03606278E-02    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     2.65643153E-03    2     2000015       -15   # BR(chi^0_3 -> ~tau^-_2 tau^+)
     2.65643153E-03    2    -2000015        15   # BR(chi^0_3 -> ~tau^+_2 tau^-)
     2.05932025E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.05932025E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     4.31083474E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.83099149E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     3.82512815E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.79038190E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.62844153E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.02213709E-04    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
     2.59177435E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     6.05451980E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     6.05451980E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.50955512E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.12349562E-04    2     2000011       -11   # BR(chi^0_4 -> ~e^-_R e^+)
     1.12349562E-04    2    -2000011        11   # BR(chi^0_4 -> ~e^+_R e^-)
     2.01827707E-04    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     2.01827707E-04    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     7.62010282E-04    2     2000013       -13   # BR(chi^0_4 -> ~mu^-_R mu^+)
     7.62010282E-04    2    -2000013        13   # BR(chi^0_4 -> ~mu^+_R mu^-)
     4.18859434E-04    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     4.18859434E-04    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     7.27865111E-02    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     7.27865111E-02    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     3.38042924E-03    2     2000015       -15   # BR(chi^0_4 -> ~tau^-_2 tau^+)
     3.38042924E-03    2    -2000015        15   # BR(chi^0_4 -> ~tau^+_2 tau^-)
     4.16445659E-04    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     4.16445659E-04    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     4.16490314E-04    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     4.16490314E-04    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     4.91258965E-04    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     4.91258965E-04    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     2.43022384E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.43022384E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.92163685E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     2.60504700E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     4.97745711E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.31723529E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.91582148E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     1.20785709E-04    3     1000022         5        -5   # BR(chi^0_4 -> chi^0_1 b b_bar)
     3.26294432E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     6.89743476E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     6.89743476E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.13631237E+02   # ~g
#    BR                NDA      ID1      ID2
     6.61792571E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     6.61792571E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.88000678E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.88000678E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.83216624E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.83216624E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     6.22213073E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     6.22213073E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.00105196E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     1.38339173E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.37030986E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.42155562E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.42155562E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     4.00188265E-03   # Gamma(h0)
     2.57302732E-03   2        22        22   # BR(h0 -> photon photon)
     2.02963608E-03   2        22        23   # BR(h0 -> photon Z)
     4.28762126E-02   2        23        23   # BR(h0 -> Z Z)
     3.29474536E-01   2       -24        24   # BR(h0 -> W W)
     7.68015163E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.25560953E-09   2       -11        11   # BR(h0 -> Electron electron)
     1.89301058E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.45775130E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.19541841E-07   2        -2         2   # BR(h0 -> Up up)
     2.32037215E-02   2        -4         4   # BR(h0 -> Charm charm)
     4.89045310E-07   2        -1         1   # BR(h0 -> Down down)
     1.76875460E-04   2        -3         3   # BR(h0 -> Strange strange)
     4.68097048E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     4.46567608E+01   # Gamma(HH)
     2.56820128E-08   2        22        22   # BR(HH -> photon photon)
     9.19202485E-08   2        22        23   # BR(HH -> photon Z)
     4.63252591E-07   2        23        23   # BR(HH -> Z Z)
     2.07921387E-07   2       -24        24   # BR(HH -> W W)
     1.36381386E-05   2        21        21   # BR(HH -> gluon gluon)
     2.48194102E-08   2       -11        11   # BR(HH -> Electron electron)
     1.10497013E-03   2       -13        13   # BR(HH -> Muon muon)
     3.29022361E-01   2       -15        15   # BR(HH -> Tau tau)
     1.23874535E-14   2        -2         2   # BR(HH -> Up up)
     2.40269520E-09   2        -4         4   # BR(HH -> Charm charm)
     6.68175551E-05   2        -6         6   # BR(HH -> Top top)
     1.42008489E-06   2        -1         1   # BR(HH -> Down down)
     5.13457287E-04   2        -3         3   # BR(HH -> Strange strange)
     6.63309015E-01   2        -5         5   # BR(HH -> Bottom bottom)
     6.75267706E-04   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     6.13504198E-05   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     3.68521233E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     3.65465554E-03   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     8.65112911E-04   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     3.36223141E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     6.09968759E-06   2        25        25   # BR(HH -> h0 h0)
     5.44730133E-08   2  -1000011   1000011   # BR(HH -> Selectron1 selectron1)
     1.96778111E-12   2  -2000011   1000011   # BR(HH -> Selectron2 selectron1)
     1.96778111E-12   2  -1000011   2000011   # BR(HH -> Selectron1 selectron2)
     5.10278378E-08   2  -1000013   1000013   # BR(HH -> Smuon1 smuon1)
     8.41107057E-08   2  -2000013   1000013   # BR(HH -> Smuon2 smuon1)
     8.41107057E-08   2  -1000013   2000013   # BR(HH -> Smuon1 smuon2)
DECAY        36     4.30642582E+01   # Gamma(A0)
     7.05518471E-08   2        22        22   # BR(A0 -> photon photon)
     2.18222496E-07   2        22        23   # BR(A0 -> photon Z)
     2.36264511E-05   2        21        21   # BR(A0 -> gluon gluon)
     2.47741408E-08   2       -11        11   # BR(A0 -> Electron electron)
     1.10295950E-03   2       -13        13   # BR(A0 -> Muon muon)
     3.28424662E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.25335885E-14   2        -2         2   # BR(A0 -> Up up)
     2.43111332E-09   2        -4         4   # BR(A0 -> Charm charm)
     6.99001505E-05   2        -6         6   # BR(A0 -> Top top)
     1.41751257E-06   2        -1         1   # BR(A0 -> Down down)
     5.12527030E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.62108372E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.71865703E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     6.74281360E-05   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     4.75454512E-04   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     9.28943033E-04   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     3.70908977E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     8.55804544E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     6.69204772E-07   2        23        25   # BR(A0 -> Z h0)
     7.13658716E-41   2        25        25   # BR(A0 -> h0 h0)
     2.02553592E-12   2  -2000011   1000011   # BR(A0 -> Selectron2 selectron1)
     2.02553592E-12   2  -1000011   2000011   # BR(A0 -> Selectron1 selectron2)
     8.65997124E-08   2  -2000013   1000013   # BR(A0 -> Smuon2 smuon1)
     8.65997124E-08   2  -1000013   2000013   # BR(A0 -> Smuon1 smuon2)
DECAY        37     4.90274855E+01   # Gamma(Hp)
     2.37829066E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     1.01679353E-03   2       -13        14   # BR(Hp -> Muon nu_mu)
     2.87606874E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.45507757E-06   2        -1         2   # BR(Hp -> Down up)
     2.44668965E-05   2        -3         2   # BR(Hp -> Strange up)
     7.47424788E-06   2        -5         2   # BR(Hp -> Bottom up)
     6.80812082E-08   2        -1         4   # BR(Hp -> Down charm)
     5.24140721E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.04666184E-03   2        -5         4   # BR(Hp -> Bottom charm)
     3.87912809E-08   2        -1         6   # BR(Hp -> Down top)
     1.61520375E-06   2        -3         6   # BR(Hp -> Strange top)
     7.04941174E-01   2        -5         6   # BR(Hp -> Bottom top)
     7.53086796E-04   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     4.07538451E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     2.26462227E-09   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     5.90603570E-07   2        24        25   # BR(Hp -> W h0)
     4.31385655E-12   2        24        35   # BR(Hp -> W HH)
     3.95079306E-12   2        24        36   # BR(Hp -> W A0)
     3.50091256E-12   2  -1000011   1000012   # BR(Hp -> Selectron1 snu_e1)
     1.49680378E-07   2  -1000013   1000014   # BR(Hp -> Smuon1 snu_mu1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.59406647E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.97660377E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.97656318E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00001364E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.22320270E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.35957929E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.59406647E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.97660377E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.97656318E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999859E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.41281008E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999859E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.41281008E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26293996E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.96299610E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.63302292E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.41281008E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999859E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.42027878E-04   # BR(b -> s gamma)
    2    1.58831195E-06   # BR(b -> s mu+ mu-)
    3    3.52562245E-05   # BR(b -> s nu nu)
    4    5.39299138E-15   # BR(Bd -> e+ e-)
    5    2.30380222E-10   # BR(Bd -> mu+ mu-)
    6    4.80994338E-08   # BR(Bd -> tau+ tau-)
    7    1.82750626E-13   # BR(Bs -> e+ e-)
    8    7.80702180E-09   # BR(Bs -> mu+ mu-)
    9    1.65135672E-06   # BR(Bs -> tau+ tau-)
   10    9.11086367E-05   # BR(B_u -> tau nu)
   11    9.41117337E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42238390E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93439080E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15886492E-03   # epsilon_K
   17    2.28167795E-15   # Delta(M_K)
   18    2.48047612E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29157958E-11   # BR(K^+ -> pi^+ nu nu)
   20    1.74683966E-14   # Delta(g-2)_electron/2
   21    7.47425354E-10   # Delta(g-2)_muon/2
   22    9.01515983E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.93764724E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.12071661E-01   # C7
     0305 4322   00   2    -2.65306654E-04   # C7'
     0305 6421   00   0    -9.52278347E-02   # C8
     0305 6421   00   2    -1.17273530E-01   # C8
     0305 6321   00   2    -3.16352193E-04   # C8'
 03051111 4133   00   0     1.62368511E+00   # C9 e+e-
 03051111 4133   00   2     1.62379753E+00   # C9 e+e-
 03051111 4233   00   2     7.89634351E-04   # C9' e+e-
 03051111 4137   00   0    -4.44637721E+00   # C10 e+e-
 03051111 4137   00   2    -4.44521693E+00   # C10 e+e-
 03051111 4237   00   2    -5.87044678E-03   # C10' e+e-
 03051313 4133   00   0     1.62368511E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62379559E+00   # C9 mu+mu-
 03051313 4233   00   2     7.89629752E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44637721E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44521886E+00   # C10 mu+mu-
 03051313 4237   00   2    -5.87044885E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50511614E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.27025985E-03   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50511614E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.27026046E-03   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50511617E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.27043293E-03   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85605859E-07   # C7
     0305 4422   00   2     2.54823193E-05   # C7
     0305 4322   00   2    -1.00307684E-06   # C7'
     0305 6421   00   0     3.30296920E-07   # C8
     0305 6421   00   2    -1.66162295E-05   # C8
     0305 6321   00   2    -9.29244746E-07   # C8'
 03051111 4133   00   2    -4.06292635E-07   # C9 e+e-
 03051111 4233   00   2     1.61781160E-05   # C9' e+e-
 03051111 4137   00   2     2.53320702E-06   # C10 e+e-
 03051111 4237   00   2    -1.20279490E-04   # C10' e+e-
 03051313 4133   00   2    -4.06288695E-07   # C9 mu+mu-
 03051313 4233   00   2     1.61780847E-05   # C9' mu+mu-
 03051313 4137   00   2     2.53321861E-06   # C10 mu+mu-
 03051313 4237   00   2    -1.20279577E-04   # C10' mu+mu-
 03051212 4137   00   2    -2.41582650E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     2.60263586E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -2.41582475E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     2.60263586E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -2.41174190E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     2.60263575E-05   # C11' nu_3 nu_3
