import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.keywords = ["SM", "AQGC"]
evgenConfig.contact = ["Daniel Wilbern <daniel.wilbern@cern.ch>"]
evgenConfig.description = "MadGraph+Pythia8 for WWy AQGC with param {}^2=={}".format(EFT_param, EFT_decomposition)
evgenConfig.nEventsPerJob = 5000

# Process card
process = "import model SM_Ltotal_Ind5v2020v2_UFO\n"
process += "generate p p > w+ w- a M0=0 M1=0 M2=0 M3=0 M4=0 M5=0 M7=0 T0=0 T1=0 T2=0 T5=0 T6=0 T7=0"
process += "\noutput -f"
process_dir = new_process(process)

# Run card
evtMultiplier = 70
run_settings = {
    "nevents": evtMultiplier*runArgs.maxEvents*1.1 if runArgs.maxEvents > 0 else evtMultiplier*1.1*evgenConfig.nEventsPerJob,
    "dynamical_scale_choice": 3}
modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=run_settings)

# Param card
param_settings = {"ANOINPUTS": {
    "1": "1e-20 # S0",
    "2": "1e-20 # S1",
    "3": "1e-20 # S2",
    "4": "1e-20 # M0",
    "5": "1e-20 # M1",
    "6": "1e-20 # M2",
    "7": "1e-20 # M3",
    "8": "1e-20 # M4",
    "9": "1e-20 # M5",
    "10": "1e-20 # M6",
    "11": "1e-20 # M7",
    "12": "1e-20 # T0",
    "13": "1e-20 # T1",
    "14": "1e-20 # T2",
    "15": "1e-20 # T3",
    "16": "1e-20 # T4",
    "17": "1e-20 # T5",
    "18": "1e-20 # T6",
    "19": "1e-20 # T7",
    "20": "1e-20 # T8",
    "21": "1e-20 # T9"}
    }

modify_param_card(process_dir=process_dir, params=param_settings)

# Generate LHE file OTF
generate(process_dir=process_dir, runArgs=runArgs)
arrange_output(process_dir=process_dir, runArgs=runArgs, lhe_version=3, saveProcDir=False)

# Shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
check_reset_proc_number(opts) # Force serial processing
include("Pythia8_i/Pythia8_MadGraph.py")

# Generator filters
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
filtSeq += PhotonFilter()
filtSeq.PhotonFilter.Ptcut = 15000.
filtSeq.PhotonFilter.Etacut = 2.5
filtSeq.PhotonFilter.NPhotons = 1

from GeneratorFilters.GeneratorFiltersConf import ElectronFilter
filtSeq += ElectronFilter()
filtSeq.ElectronFilter.Ptcut = 15000.
filtSeq.ElectronFilter.Etacut = 2.5

from GeneratorFilters.GeneratorFiltersConf import MuonFilter
filtSeq += MuonFilter()
filtSeq.MuonFilter.Ptcut = 15000.
filtSeq.MuonFilter.Etacut = 2.5

