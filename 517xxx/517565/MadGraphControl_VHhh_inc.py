#import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# Generate A->ZH, H->bb, Z->ll or Z->vv

# parse the job arguments to get mA,mH and Z decay mode
phys_short = get_physics_short()
# mA = float(phys_short.split('_')[2][2:])
mH = float(phys_short.split('_')[2][2:])



wA = 0.001


print "mH ",mH



# a safe margin for the number of generated events
nevents=int(runArgs.maxEvents*7.0) 
mode=0 

# create the process string to be copied to proc_card_mg5.dat
process="""
import model 2HDM_GF
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~ a
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define v = z w- w+
# Specify process(es) to run
define all = e+ e- mu+ mu- ta+ ta- u u~ d d~ c c~ s s~ b b~ ve vm vt ve~ vm~ vt~
generate p p > z > z h2 / h3, z > all all, (h2 > h1 h1)
add process     p p > w- > w- h2 / h3 h+ h- l+ l- t t~, w- > all all, (h2 > h1 h1) 
add process  p p > w+ > w+ h2 / h3 h+ h- l+ l- t t~, w+ > all all, (h2 > h1 h1) 
output -f
"""

print 'process string: ',process
# generate     p p > w- > w- h2 / h3 h+ h- l+ l- t t~, w- > all all, (h2 > h1 h1) 
# add process  p p > w+ > w+ h2 / h3 h+ h- l+ l- t t~, w+ > all all, (h2 > h1 h1) 
#p p > z > z h2 / h3, z > all all, (h2 > h1 h1)
#---------------------------------------------------------------------------------------------------
# Set masses in param_card.dat
#---------------------------------------------------------------------------------------------------
mh1=125
mh2=mH
masses ={'25':mh1,  
         '35':mh2}
decays ={'25':'DECAY 25 4.070000e-03 # Wh1',  
         '35':'DECAY 35 1.000000e-03 # Wh2'}
params = {}
params['mass'] = masses
params['decay'] = decays

print masses


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


#Fetch default LO run_card.dat and set parameters (extras)
extras = { 'lhe_version':'3.0', 
           #'cut_decays':'F', 
           'pdlabel':"'lhapdf'"} # NNPDF23_lo_as_0130_qed
extras['nevents'] = nevents

# set up process
process_dir = new_process(process) 

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
#---------------------------------------------------------------------------------------------------
modify_param_card(process_dir=process_dir,params=params)
#build_param_card(param_card_old='param_card_AZH.dat',param_card_new='param_card_new.dat',params=params)
#madspin_card = process_dir+'/Cards/madspin_card.dat'
# if os.access(madspin_card,os.R_OK):
#     os.unlink(madspin_card)
#mscard = open(madspin_card,'w')

#mscard.write("""
# set BW_cut 10000                # cut on how far the particle can be off-shell
# set max_weight_ps_point 400
#set seed %i
#decay z > all all  
#decay w- > all all  
#decay w+ > all all   
#decay h2 > h1 h1
#launch"""%runArgs.randomSeed)
#mscard.close()
print_cards()

# and the generation
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")


# Shower
evgenConfig.description = 'mass splitting V->ZH(%s GeV)->lep' % (mH)
evgenConfig.contact = ['Merve Nazlim Agaras <merve.nazlim.agaras@cern.ch>']
evgenConfig.keywords += ['exotic', 'multilepton']
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
#evgenConfig.inputfilecheck = runName
#evgenConfig.inputfilecheck = ""
# runArgs.inputGeneratorFile=output_DS

# include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
# include("Pythia8_i/Pythia8_MadGraph.py")

# Lepton filter
include('GeneratorFilters/MultiLeptonFilter.py')
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10000.
MultiLeptonFilter.NLeptons = 2


