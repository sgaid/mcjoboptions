# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.05.2022,  06:00
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.31824515E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.66157302E+03  # scale for input parameters
    1   -2.93144219E+02  # M_1
    2   -1.16915416E+03  # M_2
    3    2.82574746E+03  # M_3
   11    6.14896486E+03  # A_t
   12    8.51201534E+02  # A_b
   13    5.44468177E+02  # A_tau
   23   -1.34922152E+03  # mu
   25    5.43754065E+01  # tan(beta)
   26    2.13723403E+03  # m_A, pole mass
   31    2.28306951E+02  # M_L11
   32    2.28306951E+02  # M_L22
   33    7.49286480E+02  # M_L33
   34    1.07648806E+03  # M_E11
   35    1.07648806E+03  # M_E22
   36    1.74748428E+03  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.93078550E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.66800663E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.53641046E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.66157302E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.66157302E+03  # (SUSY scale)
  1  1     8.38578988E-06   # Y_u(Q)^DRbar
  2  2     4.25998126E-03   # Y_c(Q)^DRbar
  3  3     1.01307077E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.66157302E+03  # (SUSY scale)
  1  1     9.16276906E-04   # Y_d(Q)^DRbar
  2  2     1.74092612E-02   # Y_s(Q)^DRbar
  3  3     9.08659571E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.66157302E+03  # (SUSY scale)
  1  1     1.59897020E-04   # Y_e(Q)^DRbar
  2  2     3.30616326E-02   # Y_mu(Q)^DRbar
  3  3     5.56042270E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.66157302E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     6.14896487E+03   # A_t(Q)^DRbar
Block Ad Q=  3.66157302E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     8.51201458E+02   # A_b(Q)^DRbar
Block Ae Q=  3.66157302E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     5.44468177E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.66157302E+03  # soft SUSY breaking masses at Q
   1   -2.93144219E+02  # M_1
   2   -1.16915416E+03  # M_2
   3    2.82574746E+03  # M_3
  21    3.32638765E+06  # M^2_(H,d)
  22   -1.70970307E+06  # M^2_(H,u)
  31    2.28306951E+02  # M_(L,11)
  32    2.28306951E+02  # M_(L,22)
  33    7.49286480E+02  # M_(L,33)
  34    1.07648806E+03  # M_(E,11)
  35    1.07648806E+03  # M_(E,22)
  36    1.74748428E+03  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.93078550E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.66800663E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.53641046E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.27357533E+02  # h0
        35     2.13653102E+03  # H0
        36     2.13723403E+03  # A0
        37     2.14810851E+03  # H+
   1000001     1.01093173E+04  # ~d_L
   2000001     1.00848481E+04  # ~d_R
   1000002     1.01089640E+04  # ~u_L
   2000002     1.00884288E+04  # ~u_R
   1000003     1.01093255E+04  # ~s_L
   2000003     1.00848605E+04  # ~s_R
   1000004     1.01089713E+04  # ~c_L
   2000004     1.00884297E+04  # ~c_R
   1000005     2.60073221E+03  # ~b_1
   2000005     4.91256550E+03  # ~b_2
   1000006     2.72609941E+03  # ~t_1
   2000006     4.91805873E+03  # ~t_2
   1000011     3.01396210E+02  # ~e_L-
   2000011     1.07863927E+03  # ~e_R-
   1000012     2.90341110E+02  # ~nu_eL
   1000013     3.01198316E+02  # ~mu_L-
   2000013     1.07859495E+03  # ~mu_R-
   1000014     2.90267512E+02  # ~nu_muL
   1000015     7.54477687E+02  # ~tau_1-
   2000015     1.72776439E+03  # ~tau_2-
   1000016     7.61954692E+02  # ~nu_tauL
   1000021     3.28189253E+03  # ~g
   1000022     2.88869714E+02  # ~chi_10
   1000023     1.20587846E+03  # ~chi_20
   1000025     1.35488775E+03  # ~chi_30
   1000035     1.37560096E+03  # ~chi_40
   1000024     1.20593248E+03  # ~chi_1+
   1000037     1.37618811E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.89177838E-02   # alpha
Block Hmix Q=  3.66157302E+03  # Higgs mixing parameters
   1   -1.34922152E+03  # mu
   2    5.43754065E+01  # tan[beta](Q)
   3    2.43017958E+02  # v(Q)
   4    4.56776930E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -4.91272208E-02   # Re[R_st(1,1)]
   1  2     9.98792529E-01   # Re[R_st(1,2)]
   2  1    -9.98792529E-01   # Re[R_st(2,1)]
   2  2    -4.91272208E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -1.23057405E-02   # Re[R_sb(1,1)]
   1  2     9.99924282E-01   # Re[R_sb(1,2)]
   2  1    -9.99924282E-01   # Re[R_sb(2,1)]
   2  2    -1.23057405E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -9.98455259E-01   # Re[R_sta(1,1)]
   1  2     5.55616455E-02   # Re[R_sta(1,2)]
   2  1    -5.55616455E-02   # Re[R_sta(2,1)]
   2  2    -9.98455259E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.99353519E-01   # Re[N(1,1)]
   1  2    -8.42197199E-04   # Re[N(1,2)]
   1  3    -3.49944028E-02   # Re[N(1,3)]
   1  4     8.19923934E-03   # Re[N(1,4)]
   2  1    -1.09899152E-02   # Re[N(2,1)]
   2  2    -9.46786853E-01   # Re[N(2,2)]
   2  3    -2.41189825E-01   # Re[N(2,3)]
   2  4     2.12841127E-01   # Re[N(2,4)]
   3  1     1.89126927E-02   # Re[N(3,1)]
   3  2    -2.11485767E-02   # Re[N(3,2)]
   3  3     7.06332707E-01   # Re[N(3,3)]
   3  4     7.07311215E-01   # Re[N(3,4)]
   4  1    -2.85320323E-02   # Re[N(4,1)]
   4  2     3.21164574E-01   # Re[N(4,2)]
   4  3    -6.64602865E-01   # Re[N(4,3)]
   4  4     6.74049161E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.29756675E-01   # Re[U(1,1)]
   1  2    -3.68174586E-01   # Re[U(1,2)]
   2  1    -3.68174586E-01   # Re[U(2,1)]
   2  2     9.29756675E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.44444000E-01   # Re[V(1,1)]
   1  2     3.28672375E-01   # Re[V(1,2)]
   2  1     3.28672375E-01   # Re[V(2,1)]
   2  2    -9.44444000E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   1000011     2.49748532E-03   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.99884481E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000011     4.66417991E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.99999917E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000013     2.42210843E-03   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.99887474E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     4.68056857E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.96450418E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     3.54958171E-03    2     1000013        25   # BR(~mu^-_R -> ~mu^-_L h^0)
DECAY   1000015     7.03090962E-01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
DECAY   2000015     2.22957833E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     3.66358801E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     4.88365278E-03    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     3.54017846E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.95908814E-02    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.17233827E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.78276446E-02    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
     2.50930217E-01    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     1.26444434E-01    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     1.16839202E-01    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
DECAY   1000012     3.73472657E-05   # ~nu_e
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
DECAY   1000014     3.37216201E-05   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
DECAY   1000016     7.03215591E-01   # ~nu_tau
#    BR                NDA      ID1      ID2
     9.99803410E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.41974981E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.74457292E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.91243991E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.69612937E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.84536068E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.01547801E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     5.92809084E-03    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     9.71342749E-02    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.50988169E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.29805057E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.42093082E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.74309084E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.91063465E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.69675951E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.84530769E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.01551865E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     5.96120913E-03    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     9.71268848E-02    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.51016869E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.29738110E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     4.64215912E+01   # ~b_1
#    BR                NDA      ID1      ID2
     3.17856182E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     3.46522334E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.43600104E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.09511401E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     7.90975632E-02    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     4.01353080E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     4.01095359E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.90949921E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     5.23347184E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     8.57559248E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     8.08110318E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     1.03141539E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.04451385E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.96402900E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.60508963E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.95145172E-03    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     5.66193886E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     7.48539823E-04    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     5.57383394E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
     7.48274972E-04    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   2000002     6.59178085E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.40775686E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.65879834E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.69597815E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.81072777E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     5.05702920E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.56436888E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.00225659E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.20326290E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.29778696E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.59185161E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.40772049E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.65869545E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.69660701E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.81058137E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     5.05664124E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.56598541E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.00227938E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.20974296E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.29711739E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     6.88201027E+01   # ~t_1
#    BR                NDA      ID1      ID2
     8.57050403E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.52503573E-04    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     2.84878945E-02    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.28836496E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.97888900E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     6.74627257E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     3.91352988E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     4.23874540E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.61526907E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     4.86470062E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.00568665E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     9.65289973E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     9.88809752E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.53478804E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.81841525E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     1.07611983E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.42085691E-03    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     7.62042070E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.10484331E-03    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     1.27852380E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     1.09527272E-03    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     2.02324415E+01   # chi^+_1
#    BR                NDA      ID1      ID2
     1.99339058E-01    2    -1000011        12   # BR(chi^+_1 -> ~e^+_L nu_e)
     1.99373929E-01    2    -1000013        14   # BR(chi^+_1 -> ~mu^+_L nu_mu)
     8.68476157E-02    2    -1000015        16   # BR(chi^+_1 -> ~tau^+_1 nu_tau)
     2.07665197E-01    2     1000012       -11   # BR(chi^+_1 -> ~nu_e e^+)
     2.07757610E-01    2     1000014       -13   # BR(chi^+_1 -> ~nu_mu mu^+)
     9.37002298E-02    2     1000016       -15   # BR(chi^+_1 -> ~nu_tau tau^+)
     5.21381979E-03    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     8.96868359E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     8.29786096E-02    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     8.29895860E-02    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     2.14584015E-04    2    -2000013        14   # BR(chi^+_2 -> ~mu^+_R nu_mu)
     3.47755587E-02    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     6.66086754E-02    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     6.79255472E-02    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     2.30635480E-01    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     9.38337154E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.03459348E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     7.98837763E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     5.44424422E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.20628548E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.05795662E+01   # chi^0_2
#    BR                NDA      ID1      ID2
     1.02869156E-01    2     1000011       -11   # BR(chi^0_2 -> ~e^-_L e^+)
     1.02869156E-01    2    -1000011        11   # BR(chi^0_2 -> ~e^+_L e^-)
     1.02919050E-01    2     1000013       -13   # BR(chi^0_2 -> ~mu^-_L mu^+)
     1.02919050E-01    2    -1000013        13   # BR(chi^0_2 -> ~mu^+_L mu^-)
     4.83832378E-02    2     1000015       -15   # BR(chi^0_2 -> ~tau^-_1 tau^+)
     4.83832378E-02    2    -1000015        15   # BR(chi^0_2 -> ~tau^+_1 tau^-)
     1.01314281E-01    2     1000012       -12   # BR(chi^0_2 -> ~nu_e nu_bar_e)
     1.01314281E-01    2    -1000012        12   # BR(chi^0_2 -> ~nu^*_e nu_e)
     1.01320602E-01    2     1000014       -14   # BR(chi^0_2 -> ~nu_mu nu_bar_mu)
     1.01320602E-01    2    -1000014        14   # BR(chi^0_2 -> ~nu^*_mu nu_mu)
     4.12022766E-02    2     1000016       -16   # BR(chi^0_2 -> ~nu_tau nu_bar_tau)
     4.12022766E-02    2    -1000016        16   # BR(chi^0_2 -> ~nu^*_tau nu_tau)
     1.09813248E-03    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     2.83872388E-03    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     5.95930939E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     1.16966660E-03    2     1000013       -13   # BR(chi^0_3 -> ~mu^-_L mu^+)
     1.16966660E-03    2    -1000013        13   # BR(chi^0_3 -> ~mu^+_L mu^-)
     1.92911274E-04    2     2000013       -13   # BR(chi^0_3 -> ~mu^-_R mu^+)
     1.92911274E-04    2    -2000013        13   # BR(chi^0_3 -> ~mu^+_R mu^-)
     1.65331904E-01    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     1.65331904E-01    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     4.44844769E-04    2     1000012       -12   # BR(chi^0_3 -> ~nu_e nu_bar_e)
     4.44844769E-04    2    -1000012        12   # BR(chi^0_3 -> ~nu^*_e nu_e)
     4.44866476E-04    2     1000014       -14   # BR(chi^0_3 -> ~nu_mu nu_bar_mu)
     4.44866476E-04    2    -1000014        14   # BR(chi^0_3 -> ~nu^*_mu nu_mu)
     2.28462466E-04    2     1000016       -16   # BR(chi^0_3 -> ~nu_tau nu_bar_tau)
     2.28462466E-04    2    -1000016        16   # BR(chi^0_3 -> ~nu^*_tau nu_tau)
     9.31228426E-02    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     9.31228426E-02    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     1.15726190E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.81305291E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     9.53444267E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     8.36072624E-02    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     1.17803970E-04    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     1.74333631E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
DECAY   1000035     8.07188236E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     3.18113335E-02    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     3.18113335E-02    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     3.25589476E-02    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     3.25589476E-02    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     1.73222856E-04    2     2000013       -13   # BR(chi^0_4 -> ~mu^-_R mu^+)
     1.73222856E-04    2    -2000013        13   # BR(chi^0_4 -> ~mu^+_R mu^-)
     1.25178285E-01    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     1.25178285E-01    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     3.87470084E-02    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     3.87470084E-02    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     3.87488400E-02    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     3.87488400E-02    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     2.03949183E-02    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     2.03949183E-02    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     1.28929794E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.28929794E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.03048076E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     5.12184935E-04    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     6.85003567E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     6.60860605E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.32035982E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
DECAY   1000021     2.27456567E+01   # ~g
#    BR                NDA      ID1      ID2
     5.10441344E-04    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     5.10441344E-04    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     2.03209439E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.03209439E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.95595571E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.95595571E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.11287535E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     1.04365142E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.83409428E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.83409428E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     4.02454766E-03   # Gamma(h0)
     2.42577505E-03   2        22        22   # BR(h0 -> photon photon)
     1.76698425E-03   2        22        23   # BR(h0 -> photon Z)
     3.55786686E-02   2        23        23   # BR(h0 -> Z Z)
     2.80911519E-01   2       -24        24   # BR(h0 -> W W)
     7.37750882E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.65140641E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.06904938E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.97500400E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.30564355E-07   2        -2         2   # BR(h0 -> Up up)
     2.53400322E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.36828616E-07   2        -1         1   # BR(h0 -> Down down)
     1.94159489E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.20050156E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.53484734E+02   # Gamma(HH)
     2.24559231E-08   2        22        22   # BR(HH -> photon photon)
     3.68864534E-08   2        22        23   # BR(HH -> photon Z)
     2.04191648E-07   2        23        23   # BR(HH -> Z Z)
     9.84899288E-08   2       -24        24   # BR(HH -> W W)
     2.13143853E-05   2        21        21   # BR(HH -> gluon gluon)
     7.76126646E-09   2       -11        11   # BR(HH -> Electron electron)
     3.45531631E-04   2       -13        13   # BR(HH -> Muon muon)
     9.35763794E-02   2       -15        15   # BR(HH -> Tau tau)
     1.49323729E-14   2        -2         2   # BR(HH -> Up up)
     2.89676472E-09   2        -4         4   # BR(HH -> Charm charm)
     2.88409997E-04   2        -6         6   # BR(HH -> Top top)
     4.55915809E-07   2        -1         1   # BR(HH -> Down down)
     1.64963695E-04   2        -3         3   # BR(HH -> Strange strange)
     9.00129669E-01   2        -5         5   # BR(HH -> Bottom bottom)
     3.26563238E-05   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     4.37378736E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     3.52991192E-03   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.45604155E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     2.03114937E-07   2        25        25   # BR(HH -> h0 h0)
     2.84576005E-08   2  -1000011   1000011   # BR(HH -> Selectron1 selectron1)
     3.38257435E-13   2  -2000011   1000011   # BR(HH -> Selectron2 selectron1)
     3.38257435E-13   2  -1000011   2000011   # BR(HH -> Selectron1 selectron2)
     2.74343668E-08   2  -1000013   1000013   # BR(HH -> Smuon1 smuon1)
     1.44589969E-08   2  -2000013   1000013   # BR(HH -> Smuon2 smuon1)
     1.44589969E-08   2  -1000013   2000013   # BR(HH -> Smuon1 smuon2)
     1.63728496E-05   2  -1000015   1000015   # BR(HH -> Stau1 stau1)
DECAY        36     1.37971612E+02   # Gamma(A0)
     6.58507686E-09   2        22        22   # BR(A0 -> photon photon)
     1.13469735E-08   2        22        23   # BR(A0 -> photon Z)
     3.03634307E-05   2        21        21   # BR(A0 -> gluon gluon)
     7.75777407E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.45377073E-04   2       -13        13   # BR(A0 -> Muon muon)
     9.35281844E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.38372837E-14   2        -2         2   # BR(A0 -> Up up)
     2.68470077E-09   2        -4         4   # BR(A0 -> Charm charm)
     2.78512673E-04   2        -6         6   # BR(A0 -> Top top)
     4.55586201E-07   2        -1         1   # BR(A0 -> Down down)
     1.64844293E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.99539931E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     3.99949708E-05   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     7.95098159E-04   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.97197705E-03   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     3.30488939E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     3.11699526E-07   2        23        25   # BR(A0 -> Z h0)
     1.22911165E-37   2        25        25   # BR(A0 -> h0 h0)
     3.70428158E-13   2  -2000011   1000011   # BR(A0 -> Selectron2 selectron1)
     3.70428158E-13   2  -1000011   2000011   # BR(A0 -> Selectron1 selectron2)
     1.58371263E-08   2  -2000013   1000013   # BR(A0 -> Smuon2 smuon1)
     1.58371263E-08   2  -1000013   2000013   # BR(A0 -> Smuon1 smuon2)
DECAY        37     1.60345570E+02   # Gamma(Hp)
     6.26608990E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     2.67894913E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     7.57758647E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.52528896E-07   2        -1         2   # BR(Hp -> Down up)
     7.61173944E-06   2        -3         2   # BR(Hp -> Strange up)
     9.73447313E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.11664712E-08   2        -1         4   # BR(Hp -> Down charm)
     1.63153322E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.36317406E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.25932330E-08   2        -1         6   # BR(Hp -> Down top)
     5.13667729E-07   2        -3         6   # BR(Hp -> Strange top)
     9.16988544E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.11259025E-03   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     4.30101401E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     2.72698321E-07   2        24        25   # BR(Hp -> W h0)
     1.77365107E-09   2        24        35   # BR(Hp -> W HH)
     1.29549825E-09   2        24        36   # BR(Hp -> W A0)
     1.06063616E-07   2  -1000011   1000012   # BR(Hp -> Selectron1 snu_e1)
     6.53861167E-13   2  -2000011   1000012   # BR(Hp -> Selectron2 snu_e1)
     1.04692825E-07   2  -1000013   1000014   # BR(Hp -> Smuon1 snu_mu1)
     2.79512398E-08   2  -2000013   1000014   # BR(Hp -> Smuon2 snu_mu1)
     8.89737865E-06   2  -1000015   1000016   # BR(Hp -> Stau1 snu_tau1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.05837750E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.95662645E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.95668483E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99980256E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.57960880E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.38216637E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.05837750E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.95662645E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.95668483E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999720E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.80041618E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999720E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.80041618E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25890122E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.09826385E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.64790034E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.80041618E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999720E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.16476291E-04   # BR(b -> s gamma)
    2    1.59079568E-06   # BR(b -> s mu+ mu-)
    3    3.52526532E-05   # BR(b -> s nu nu)
    4    6.17900038E-15   # BR(Bd -> e+ e-)
    5    2.63917766E-10   # BR(Bd -> mu+ mu-)
    6    5.28420677E-08   # BR(Bd -> tau+ tau-)
    7    2.17154233E-13   # BR(Bs -> e+ e-)
    8    9.27531599E-09   # BR(Bs -> mu+ mu-)
    9    1.88019877E-06   # BR(Bs -> tau+ tau-)
   10    9.43130752E-05   # BR(B_u -> tau nu)
   11    9.74217959E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41812828E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93853782E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15696312E-03   # epsilon_K
   17    2.28165881E-15   # Delta(M_K)
   18    2.47983526E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29017000E-11   # BR(K^+ -> pi^+ nu nu)
   20    3.00015654E-14   # Delta(g-2)_electron/2
   21    1.28423046E-09   # Delta(g-2)_muon/2
   22    1.13279814E-07   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    9.74275958E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.82907945E-01   # C7
     0305 4322   00   2    -9.36231671E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -9.84142234E-02   # C8
     0305 6321   00   2    -1.96508148E-04   # C8'
 03051111 4133   00   0     1.62465095E+00   # C9 e+e-
 03051111 4133   00   2     1.62486913E+00   # C9 e+e-
 03051111 4233   00   2     4.65342926E-04   # C9' e+e-
 03051111 4137   00   0    -4.44734305E+00   # C10 e+e-
 03051111 4137   00   2    -4.44578201E+00   # C10 e+e-
 03051111 4237   00   2    -3.45358522E-03   # C10' e+e-
 03051313 4133   00   0     1.62465095E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62486187E+00   # C9 mu+mu-
 03051313 4233   00   2     4.65338556E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44734305E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44578927E+00   # C10 mu+mu-
 03051313 4237   00   2    -3.45358528E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50501928E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     7.47122916E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50501928E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     7.47123629E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50501948E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     7.47324977E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85882743E-07   # C7
     0305 4422   00   2     1.82747617E-06   # C7
     0305 4322   00   2     2.33841868E-06   # C7'
     0305 6421   00   0     3.30534090E-07   # C8
     0305 6421   00   2     2.26906936E-05   # C8
     0305 6321   00   2     1.54941706E-06   # C8'
 03051111 4133   00   2    -1.53380458E-07   # C9 e+e-
 03051111 4233   00   2     1.05773038E-05   # C9' e+e-
 03051111 4137   00   2     3.41634392E-07   # C10 e+e-
 03051111 4237   00   2    -7.85113949E-05   # C10' e+e-
 03051313 4133   00   2    -1.53396263E-07   # C9 mu+mu-
 03051313 4233   00   2     1.05772848E-05   # C9' mu+mu-
 03051313 4137   00   2     3.41607416E-07   # C10 mu+mu-
 03051313 4237   00   2    -7.85114550E-05   # C10' mu+mu-
 03051212 4137   00   2     4.51766036E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     1.69846412E-05   # C11' nu_1 nu_1
 03051414 4137   00   2     4.51757048E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     1.69846412E-05   # C11' nu_2 nu_2
 03051616 4137   00   2     4.86603929E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     1.69846482E-05   # C11' nu_3 nu_3
