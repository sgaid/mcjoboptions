# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.05.2022,  15:07
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.80454111E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.51726919E+03  # scale for input parameters
    1    3.47921086E+02  # M_1
    2    7.16124312E+02  # M_2
    3    2.55904930E+03  # M_3
   11   -2.62548853E+03  # A_t
   12    8.73301703E+02  # A_b
   13   -1.52610658E+03  # A_tau
   23   -4.73349861E+02  # mu
   25    4.73852034E+01  # tan(beta)
   26    8.33862393E+02  # m_A, pole mass
   31    9.96962749E+02  # M_L11
   32    9.96962749E+02  # M_L22
   33    9.09254194E+02  # M_L33
   34    1.20883996E+03  # M_E11
   35    1.20883996E+03  # M_E22
   36    1.42029026E+03  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.39140439E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.57534107E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.40389919E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.51726919E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.51726919E+03  # (SUSY scale)
  1  1     8.38623897E-06   # Y_u(Q)^DRbar
  2  2     4.26020940E-03   # Y_c(Q)^DRbar
  3  3     1.01312503E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.51726919E+03  # (SUSY scale)
  1  1     7.98528150E-04   # Y_d(Q)^DRbar
  2  2     1.51720348E-02   # Y_s(Q)^DRbar
  3  3     7.91889702E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.51726919E+03  # (SUSY scale)
  1  1     1.39349001E-04   # Y_e(Q)^DRbar
  2  2     2.88129539E-02   # Y_mu(Q)^DRbar
  3  3     4.84586485E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.51726919E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -2.62548948E+03   # A_t(Q)^DRbar
Block Ad Q=  3.51726919E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     8.73301828E+02   # A_b(Q)^DRbar
Block Ae Q=  3.51726919E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.52610626E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.51726919E+03  # soft SUSY breaking masses at Q
   1    3.47921086E+02  # M_1
   2    7.16124312E+02  # M_2
   3    2.55904930E+03  # M_3
  21    6.94027032E+05  # M^2_(H,d)
  22    6.53481875E+04  # M^2_(H,u)
  31    9.96962749E+02  # M_(L,11)
  32    9.96962749E+02  # M_(L,22)
  33    9.09254194E+02  # M_(L,33)
  34    1.20883996E+03  # M_(E,11)
  35    1.20883996E+03  # M_(E,22)
  36    1.42029026E+03  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.39140439E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.57534107E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.40389919E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23146773E+02  # h0
        35     8.33874591E+02  # H0
        36     8.33862393E+02  # A0
        37     8.40817585E+02  # H+
   1000001     1.01113915E+04  # ~d_L
   2000001     1.00863501E+04  # ~d_R
   1000002     1.01110300E+04  # ~u_L
   2000002     1.00900161E+04  # ~u_R
   1000003     1.01113983E+04  # ~s_L
   2000003     1.00863623E+04  # ~s_R
   1000004     1.01110367E+04  # ~c_L
   2000004     1.00900170E+04  # ~c_R
   1000005     3.43742714E+03  # ~b_1
   2000005     3.46207953E+03  # ~b_2
   1000006     3.42411919E+03  # ~t_1
   2000006     3.61295325E+03  # ~t_2
   1000011     1.00812979E+03  # ~e_L-
   2000011     1.21056769E+03  # ~e_R-
   1000012     1.00456655E+03  # ~nu_eL
   1000013     1.00809290E+03  # ~mu_L-
   2000013     1.21051918E+03  # ~mu_R-
   1000014     1.00453465E+03  # ~nu_muL
   1000015     9.06895585E+02  # ~tau_1-
   2000015     1.40080525E+03  # ~tau_2-
   1000016     9.03564810E+02  # ~nu_tauL
   1000021     3.00380919E+03  # ~g
   1000022     3.38284523E+02  # ~chi_10
   1000023     4.70534075E+02  # ~chi_20
   1000025     4.77538492E+02  # ~chi_30
   1000035     7.72302322E+02  # ~chi_40
   1000024     4.66068041E+02  # ~chi_1+
   1000037     7.72393511E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.14391986E-02   # alpha
Block Hmix Q=  3.51726919E+03  # Higgs mixing parameters
   1   -4.73349861E+02  # mu
   2    4.73852034E+01  # tan[beta](Q)
   3    2.43071695E+02  # v(Q)
   4    6.95326490E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.53246803E-01   # Re[R_st(1,1)]
   1  2     3.02192875E-01   # Re[R_st(1,2)]
   2  1    -3.02192875E-01   # Re[R_st(2,1)]
   2  2     9.53246803E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.41663643E-01   # Re[R_sb(1,1)]
   1  2     3.36555468E-01   # Re[R_sb(1,2)]
   2  1    -3.36555468E-01   # Re[R_sb(2,1)]
   2  2    -9.41663643E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -9.99493790E-01   # Re[R_sta(1,1)]
   1  2     3.18145107E-02   # Re[R_sta(1,2)]
   2  1    -3.18145107E-02   # Re[R_sta(2,1)]
   2  2    -9.99493790E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.72368647E-01   # Re[N(1,1)]
   1  2    -2.43451669E-02   # Re[N(1,2)]
   1  3    -1.89767348E-01   # Re[N(1,3)]
   1  4    -1.33771749E-01   # Re[N(1,4)]
   2  1    -2.29578658E-01   # Re[N(2,1)]
   2  2    -1.85702045E-01   # Re[N(2,2)]
   2  3    -6.78074095E-01   # Re[N(2,3)]
   2  4    -6.73070511E-01   # Re[N(2,4)]
   3  1    -3.85672770E-02   # Re[N(3,1)]
   3  2     4.70200181E-02   # Re[N(3,2)]
   3  3    -7.03088460E-01   # Re[N(3,3)]
   3  4     7.08497213E-01   # Re[N(3,4)]
   4  1    -1.74762448E-02   # Re[N(4,1)]
   4  2     9.81178466E-01   # Re[N(4,2)]
   4  3    -9.93503576E-02   # Re[N(4,3)]
   4  4    -1.64659971E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.39461701E-01   # Re[U(1,1)]
   1  2    -9.90227466E-01   # Re[U(1,2)]
   2  1     9.90227466E-01   # Re[U(2,1)]
   2  2    -1.39461701E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -2.31551655E-01   # Re[V(1,1)]
   1  2     9.72822610E-01   # Re[V(1,2)]
   2  1     9.72822610E-01   # Re[V(2,1)]
   2  2     2.31551655E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   1000011     3.42278454E+00   # ~e^-_L
#    BR                NDA      ID1      ID2
     2.50260050E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     7.54060855E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     5.44275089E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.08980354E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     3.11762033E-02    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     4.33633032E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000011     5.12617960E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.53542999E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     4.50710485E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     1.25844593E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     1.27439352E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000013     3.43230630E+00   # ~mu^-_L
#    BR                NDA      ID1      ID2
     2.49688644E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     7.65579786E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.98497565E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.08356781E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     3.10873409E-02    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     4.32324280E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.15498135E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.48284329E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     4.60999803E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.61800249E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.40152560E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     2.75904147E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000015     4.18488505E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     2.05406351E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.99087939E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     2.60193554E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     6.80704849E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.72263548E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.40015316E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     1.71507791E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     3.53936123E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.54222217E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.48651262E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     3.17266824E-03    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.91966369E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     6.16479292E-03    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
     2.10809959E-02    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     1.02329643E-02    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     1.05726086E-02    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
DECAY   1000012     3.46696213E+00   # ~nu_e
#    BR                NDA      ID1      ID2
     2.96466441E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.08997305E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.49412200E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     2.09273931E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     8.42214300E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     4.03454103E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
DECAY   1000014     3.47651249E+00   # ~nu_mu
#    BR                NDA      ID1      ID2
     2.95637824E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.08127669E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.48428339E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.08654095E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     8.68699477E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     4.02272574E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
DECAY   1000016     4.18412077E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     2.07812774E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.00748625E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     2.25756834E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     6.77022728E-02    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     5.88707989E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     1.31511910E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
DECAY   2000001     6.66808219E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.96672387E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     4.43253134E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.91574682E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.96865024E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.17362425E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.16635788E-03    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.61541529E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.34803096E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     2.16332550E-03    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.08254516E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.32600326E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.66901055E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.96810667E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     4.74810479E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.91438315E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.96915342E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.17557069E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.19281248E-03    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.90047409E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.34775336E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     2.16750507E-03    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.08248000E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.32548530E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.57394757E+02   # ~b_1
#    BR                NDA      ID1      ID2
     2.45513298E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.26671757E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.25111796E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     6.77647326E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.06305452E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.53846283E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     9.57486499E-02    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     1.09291372E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.40520542E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.69995513E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.94824364E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     3.05586864E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.72678960E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     6.42357945E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.53654628E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     6.84003477E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.10771043E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.72873164E-03    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     9.67135440E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.96852134E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.23395803E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     2.85493302E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.28050082E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     5.96362802E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.04480332E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.32572501E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.84010783E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.10768680E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.73111737E-03    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     9.67125186E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.96902409E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.23396294E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     2.85681913E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.28018367E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     6.01983338E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.04474922E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.32520702E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.66088237E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.22466269E-04    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     2.10453412E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.78729412E-04    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     1.89293009E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.05560496E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     6.55101034E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.98964322E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.14203209E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     5.37061684E-03    2     1000021         4   # BR(~t_1 -> ~g c)
     9.52883941E-02    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
     4.36331260E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.76173327E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.77241317E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.82131098E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.98287686E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     3.42643112E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.36690912E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     6.56934326E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.24785226E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     8.82358783E-03    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     6.23821303E-04    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     5.35214564E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     5.62364797E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.02545596E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99997294E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     5.17376907E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     1.00485119E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.56533211E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.85230479E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.42062116E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.13972687E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.96906507E-04    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     5.64301297E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     7.80497848E-02   # chi^0_2
#    BR                NDA      ID1      ID2
     1.05965161E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     8.94023283E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     4.40107288E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     7.46710535E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.53252954E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     6.37133495E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.02656112E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.02656112E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.37691317E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.29163946E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     2.26615020E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     6.74047291E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.61002339E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.92342621E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.35310258E-04    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     4.35310258E-04    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     5.72801047E-01   # ~g
#    BR                NDA      ID1      ID2
     1.00842878E-04    2     1000022        21   # BR(~g -> chi^0_1 g)
     1.60432165E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.38635189E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     1.24022348E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     1.16180215E-02    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.09553211E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     8.56106032E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.19419249E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     9.07377842E-02    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.20716909E-04    3     1000035         2        -2   # BR(~g -> chi^0_4 u u_bar)
     1.20717210E-04    3     1000035         4        -4   # BR(~g -> chi^0_4 c c_bar)
     2.85796779E-02    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.22224981E-04    3     1000035         1        -1   # BR(~g -> chi^0_4 d d_bar)
     1.22227558E-04    3     1000035         3        -3   # BR(~g -> chi^0_4 s s_bar)
     2.34322287E-02    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.04645832E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.04645832E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     2.43073473E-04    3     1000037         1        -2   # BR(~g -> chi^+_2 d u_bar)
     2.43073473E-04    3    -1000037        -1         2   # BR(~g -> chi^-_2 d_bar u)
     2.43076300E-04    3     1000037         3        -4   # BR(~g -> chi^+_2 s c_bar)
     2.43076300E-04    3    -1000037        -3         4   # BR(~g -> chi^-_2 s_bar c)
     5.22213093E-02    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     5.22213093E-02    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.41831613E-03   # Gamma(h0)
     2.49556340E-03   2        22        22   # BR(h0 -> photon photon)
     1.43284065E-03   2        22        23   # BR(h0 -> photon Z)
     2.47971210E-02   2        23        23   # BR(h0 -> Z Z)
     2.12611118E-01   2       -24        24   # BR(h0 -> W W)
     7.86722877E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.20748257E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.31637183E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.67741439E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.40511148E-07   2        -2         2   # BR(h0 -> Up up)
     2.72694540E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.08001766E-07   2        -1         1   # BR(h0 -> Down down)
     2.19899349E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.85495181E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     2.55222621E+01   # Gamma(HH)
     1.81449446E-08   2        22        22   # BR(HH -> photon photon)
     1.73465057E-09   2        22        23   # BR(HH -> photon Z)
     1.71587235E-06   2        23        23   # BR(HH -> Z Z)
     2.76629993E-06   2       -24        24   # BR(HH -> W W)
     8.54956764E-05   2        21        21   # BR(HH -> gluon gluon)
     8.95228719E-09   2       -11        11   # BR(HH -> Electron electron)
     3.98441326E-04   2       -13        13   # BR(HH -> Muon muon)
     1.13913899E-01   2       -15        15   # BR(HH -> Tau tau)
     4.64712838E-14   2        -2         2   # BR(HH -> Up up)
     9.01126964E-09   2        -4         4   # BR(HH -> Charm charm)
     5.78360790E-04   2        -6         6   # BR(HH -> Top top)
     9.01608044E-07   2        -1         1   # BR(HH -> Down down)
     3.26080852E-04   2        -3         3   # BR(HH -> Strange strange)
     8.80671954E-01   2        -5         5   # BR(HH -> Bottom bottom)
     5.72285373E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     2.71317021E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     3.15653723E-03   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     2.02070410E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     2.48169570E+01   # Gamma(A0)
     2.37696397E-07   2        22        22   # BR(A0 -> photon photon)
     2.46393474E-07   2        22        23   # BR(A0 -> photon Z)
     1.13303578E-04   2        21        21   # BR(A0 -> gluon gluon)
     8.93075236E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.97482848E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.13645331E-01   2       -15        15   # BR(A0 -> Tau tau)
     4.56625825E-14   2        -2         2   # BR(A0 -> Up up)
     8.84185545E-09   2        -4         4   # BR(A0 -> Charm charm)
     6.95521032E-04   2        -6         6   # BR(A0 -> Top top)
     8.99339441E-07   2        -1         1   # BR(A0 -> Down down)
     3.25260407E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.78496154E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.69453530E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     4.50275095E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.25531822E-04   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     2.72790445E-06   2        23        25   # BR(A0 -> Z h0)
     2.13093859E-36   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.73399357E+01   # Gamma(Hp)
     1.17400187E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.01922453E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.41971047E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.92863913E-07   2        -1         2   # BR(Hp -> Down up)
     1.48265577E-05   2        -3         2   # BR(Hp -> Strange up)
     9.59884715E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.13872096E-08   2        -1         4   # BR(Hp -> Down charm)
     3.21753989E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.34417639E-03   2        -5         4   # BR(Hp -> Bottom charm)
     3.67268947E-08   2        -1         6   # BR(Hp -> Down top)
     1.23335903E-06   2        -3         6   # BR(Hp -> Strange top)
     8.50743923E-01   2        -5         6   # BR(Hp -> Bottom top)
     5.08796967E-03   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.56486865E-06   2        24        25   # BR(Hp -> W h0)
     7.97226600E-10   2        24        35   # BR(Hp -> W HH)
     8.04249467E-10   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.03235586E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.24532515E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.24535750E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99985590E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    4.59773493E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    4.45363377E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.03235586E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.24532515E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.24535750E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999885E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.14715295E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999885E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.14715295E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26125678E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    3.72907136E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.87351541E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.14715295E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999885E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.78921851E-04   # BR(b -> s gamma)
    2    1.58709572E-06   # BR(b -> s mu+ mu-)
    3    3.52465966E-05   # BR(b -> s nu nu)
    4    1.12174281E-15   # BR(Bd -> e+ e-)
    5    4.78951106E-11   # BR(Bd -> mu+ mu-)
    6    8.62262144E-09   # BR(Bd -> tau+ tau-)
    7    3.82542282E-14   # BR(Bs -> e+ e-)
    8    1.63345237E-09   # BR(Bs -> mu+ mu-)
    9    3.01886653E-07   # BR(Bs -> tau+ tau-)
   10    7.87764940E-05   # BR(B_u -> tau nu)
   11    8.13731024E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41931272E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.92629436E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15802591E-03   # epsilon_K
   17    2.28168771E-15   # Delta(M_K)
   18    2.47931979E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28893995E-11   # BR(K^+ -> pi^+ nu nu)
   20   -1.16943213E-14   # Delta(g-2)_electron/2
   21   -4.99965603E-10   # Delta(g-2)_muon/2
   22   -1.48313221E-07   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.83536908E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.49275469E-01   # C7
     0305 4322   00   2    -9.86142439E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.52102416E-01   # C8
     0305 6321   00   2    -1.03469371E-03   # C8'
 03051111 4133   00   0     1.62288202E+00   # C9 e+e-
 03051111 4133   00   2     1.62324454E+00   # C9 e+e-
 03051111 4233   00   2     2.01707178E-04   # C9' e+e-
 03051111 4137   00   0    -4.44557412E+00   # C10 e+e-
 03051111 4137   00   2    -4.44330612E+00   # C10 e+e-
 03051111 4237   00   2    -1.49866842E-03   # C10' e+e-
 03051313 4133   00   0     1.62288202E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62324179E+00   # C9 mu+mu-
 03051313 4233   00   2     2.01655166E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44557412E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44330887E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.49862177E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50487332E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     3.24338611E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50487332E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     3.24349586E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50487335E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     3.27442257E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85803243E-07   # C7
     0305 4422   00   2    -1.79774404E-05   # C7
     0305 4322   00   2    -1.74688135E-06   # C7'
     0305 6421   00   0     3.30465992E-07   # C8
     0305 6421   00   2     1.41501727E-05   # C8
     0305 6321   00   2    -2.55818809E-07   # C8'
 03051111 4133   00   2    -1.95381660E-07   # C9 e+e-
 03051111 4233   00   2     1.04241936E-05   # C9' e+e-
 03051111 4137   00   2     7.43359500E-07   # C10 e+e-
 03051111 4237   00   2    -7.75571859E-05   # C10' e+e-
 03051313 4133   00   2    -1.95399135E-07   # C9 mu+mu-
 03051313 4233   00   2     1.04241702E-05   # C9' mu+mu-
 03051313 4137   00   2     7.43365163E-07   # C10 mu+mu-
 03051313 4237   00   2    -7.75572586E-05   # C10' mu+mu-
 03051212 4137   00   2     8.82233779E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     1.67850123E-05   # C11' nu_1 nu_1
 03051414 4137   00   2     8.82235157E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     1.67850123E-05   # C11' nu_2 nu_2
 03051616 4137   00   2     8.86192318E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     1.67850112E-05   # C11' nu_3 nu_3
