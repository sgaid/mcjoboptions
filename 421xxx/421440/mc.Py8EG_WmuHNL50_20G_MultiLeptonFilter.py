## Pythia8 W->tau+HNL Dirac

evgenConfig.description = "W->tau+HNL production with the A14 NNPDF23LO tune"
evgenConfig.keywords = ["electroweak", "W", "exotic", "BSM"]
evgenConfig.contact = ["Jacob Bundgaard Nielsen, jacob.bundgaard.nielsen@cern.ch"]
evgenConfig.process = "W -> HNL tau-+"

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

testSeq.TestHepMC.MaxTransVtxDisp = 200000 #in mm
testSeq.TestHepMC.MaxTransVtxDispLoose = 300000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 500000 #in mm

genSeq.Pythia8.Commands += ["50:new = N2 N2 2 0 0 20.0 0.0 0.0 0.0 0.1 0 1 0 1 0",
                            "50:isResonance = false",
                            "50:addChannel = 1 1.0 23 15 -11 12", #decay in tau mu vmu
                            "50:mayDecay = on",
                            "WeakSingleBoson:ffbar2W = on", # create W bosons
                            "24:onMode = off", # switch off all W decays
             "24:addchannel = 1 1.0 103 -13 50",
             "ParticleDecays:limitTau0 = off", # switch off decaying lifetime limits
             "ParticleDecays:tau0Max = 600.0"]

# import generator filter for multi leptons
include ("GeneratorFilters/CreatexAODSlimContainers.py")
createxAODSlimmedContainer("TruthElectrons",prefiltSeq)
prefiltSeq.xAODCnv.AODContainerName = 'GEN_EVENT'

createxAODSlimmedContainer("TruthMuons",prefiltSeq)
prefiltSeq.xAODCnv.AODContainerName = 'GEN_EVENT'
                            
from GeneratorFilters.GeneratorFiltersConf import xAODMultiLeptonFilter

filtSeq += xAODMultiLeptonFilter("AllLeptons")
filtSeq.AllLeptons.Ptcut = 5000.
filtSeq.AllLeptons.Etacut = 2.8
filtSeq.AllLeptons.NLeptons = 2

filtSeq += xAODMultiLeptonFilter("LeadingLepton")
filtSeq.LeadingLepton.Ptcut = 15000.
filtSeq.LeadingLepton.Etacut = 2.8
filtSeq.LeadingLepton.NLeptons = 1

