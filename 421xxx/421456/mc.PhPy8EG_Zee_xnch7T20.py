#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults
#--------------------------------------------------------------
# based on 361106, old filter format 600700
include('PowhegControl/PowhegControl_Z_Common.py')
PowhegConfig.decay_mode = "z > e+ e-"

# Configure Powheg setup
PowhegConfig.ptsqmin = 4.0 # needed for AZNLO tune
PowhegConfig.running_width = 1
PowhegConfig.nEvents   *= 15 # increase number of generated events by 20%
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Photospp.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Z->ee production without lepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["oldrich.kepka@cern.ch"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2electron' ]
evgenConfig.nEventsPerJob = 5000

#--------------------------------------------------------------
# FILTERS
#--------------------------------------------------------------
include('GeneratorFilters/MultiLeptonFilter.py')
## Default cut params 
filtSeq.MultiLeptonFilter.Ptcut = 3500.
filtSeq.MultiLeptonFilter.Etacut = 2.7
filtSeq.MultiLeptonFilter.NLeptons = 2

include('GeneratorFilters/xAODChargedTracksWeightFilter_Common.py')
filtSeq.xAODChargedTracksWeightFilter.NchMin = 7
filtSeq.xAODChargedTracksWeightFilter.NchMax = 20
filtSeq.xAODChargedTracksWeightFilter.Ptcut = 500
filtSeq.xAODChargedTracksWeightFilter.Etacut= 2.5
filtSeq.xAODChargedTracksWeightFilter.SplineX =[ 0, 2, 5, 10, 20, 30, 35 ]
filtSeq.xAODChargedTracksWeightFilter.SplineY =[ 4, 4, 9, 13, 17, 18, 18 ]
