# taken from PowhegControl/share/example/processes/ZZ
# the same as 421413, only filter in xAOD format

#--------------------------------------------------------------
# Powheg ZZ setup starting from ATLAS defaults
#--------------------------------------------------------------
include("PowhegControl/PowhegControl_ZZ_Common.py")
PowhegConfig.nEvents     *= 5. # compensate filter efficiency 
PowhegConfig.generate()
#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 ZZ production with A14 NNPDF2.3 tune"
evgenConfig.keywords    = [ "SM", "diboson", "ZZ" ]
evgenConfig.contact     = [ "james.robinson@cern.ch" ]

#---------------------------------------------------------------------------------------------------
# Filter for 2 leptons (inc tau(had)) with pt cuts on e/mu and tau(had)
#---------------------------------------------------------------------------------------------------
include("GeneratorFilters/xAODMultiElecMuTauFilter_Common.py")
filtSeq.xAODMultiElecMuTauFilter.IncludeHadTaus = True
filtSeq.xAODMultiElecMuTauFilter.NLeptons = 2
filtSeq.xAODMultiElecMuTauFilter.MinPt = 13000.
filtSeq.xAODMultiElecMuTauFilter.MinVisPtHadTau = 15000.
filtSeq.xAODMultiElecMuTauFilter.MaxEta = 3.


