include('Superchic_i/Pythia8_Base_Common.py')


if genSeq.SuperChicConfig.diff != 'el':
    raise Exception("EL Pythia8 showr configuration can only be used with diff='el'")

genSeq.Pythia8.Commands += [
    "PartonLevel:ISR = off",
    "PartonLevel:MPI = off",
    "PartonLevel:Remnants = off",
    "Check:event = off",
    "LesHouches:matchInOut = off"
]


