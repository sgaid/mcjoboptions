# taken from PowhegControl/share/example/processes/ZZ

#--------------------------------------------------------------
# Powheg ZZ setup starting from ATLAS defaults
#--------------------------------------------------------------
include("PowhegControl/PowhegControl_ZZ_Common.py")
PowhegConfig.nEvents     *= 15. # compensate filter efficiency 
PowhegConfig.generate()
#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 ZZ production with A14 NNPDF2.3 tune"
evgenConfig.keywords    = [ "SM", "diboson", "ZZ" ]
evgenConfig.contact     = [ "james.robinson@cern.ch" ]

## HT filter setup for anti-kT R=0.4 truth jets
include("GeneratorFilters/xAODHTFilter_Common.py")

filtSeq.xAODHTFilter.MinJetPt = 20.*GeV # Min pT to consider jet in HT
filtSeq.xAODHTFilter.MaxJetEta = 999. # Max eta to consider jet in HT
filtSeq.xAODHTFilter.MinHT = 200.*GeV # Min HT to keep event
filtSeq.xAODHTFilter.MaxHT = 1000.*GeV # Max HT to keep event
filtSeq.xAODHTFilter.TruthJetContainer = "AntiKt4TruthWZJets" # Which jets to use for HT
filtSeq.xAODHTFilter.UseNeutrinosFromWZTau = False # Include neutrinos from W/Z/tau in the HT
filtSeq.xAODHTFilter.UseLeptonsFromWZTau = True # Include e/mu from W/Z/tau in the HT



