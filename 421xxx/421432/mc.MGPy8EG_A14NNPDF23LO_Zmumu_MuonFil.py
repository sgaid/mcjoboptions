#based on 950015
from MadGraphControl.MadGraphUtils import *

nevents=1000
mode=0

process = """
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > mu+ mu-
output -f"""

process_dir = new_process(process)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'pdlabel':"'nn23lo1'",
           'mmll':40,
           'use_syst':"False",
           'nevents' :int(nevents)}
    
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=2,saveProcDir=True) 

#### Shower 
evgenConfig.description = 'MadGraph_Zmumu'
evgenConfig.keywords+=['Z','jets']
evgenConfig.nEventsPerJob = 100

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

#--------------------------------------------------------------
# FILTERS
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import MuonFilter
MuonFilter = MuonFilter("MuonFilter")
filtSeq += MuonFilter
filtSeq.MuonFilter.Ptcut = 25000

