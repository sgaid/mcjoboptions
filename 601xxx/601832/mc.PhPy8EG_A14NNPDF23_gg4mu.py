#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 gg->4mu 'no higgs' showering with A14 NNPDF2.3 tune."
evgenConfig.keywords = ["SM", "Higgs"]
evgenConfig.contact = ["andrej.saibel@cern.ch", "guglielmo.frattari@cern.ch"]
evgenConfig.nEventsPerJob    = 10000
evgenConfig.inputFilesPerJob = 52
#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = -1' ]
