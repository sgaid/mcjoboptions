#--------------------------------------------------------------
# POWHEG+Pythia8 gg->H+Z->gamgamqqbar production
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ggF_HZ_Common.py')

PowhegConfig.runningscales = 1 # 
# not needed? PowhegConfig.hdecaymode = -1 #Pythia will handle h decays
# PowhegConfig.vdecaymode = 10 # Z->inc
# PowhegConfig.decay_mode = "z > all"
PowhegConfig.decay_mode_Z = "z > all"

PowhegConfig.mass_Z_low = 10

PowhegConfig.bornktmin = 0.26 # settings suggested for pTV reweighting
PowhegConfig.bornsuppfact = 0.00001
#PowhegConfig.ptVhigh = 200 # step-wise pTV reweighting
#PowhegConfig.ptVlow = 120
##PowhegConfig.Vstep = 10

PowhegConfig.withnegweights = 1 # allow neg. weighted events
PowhegConfig.doublefsr = 1

# PowhegConfig.storeinfo_rwgt = 1 # store info for PDF / scales variations reweighting
PowhegConfig.PDF = list(range(93300,93343)) + list(range( 90400, 90433 )) + list(range(260000, 260101)) + [27100] +  [14400] + [331700] #  PDF variations
PowhegConfig.mu_F = [ 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 2.0, 2.0, 2.0 ] # scale variations: first pair is the nominal setting
PowhegConfig.mu_R = [ 1.0, 0.5, 2.0, 1.0, 0.5, 2.0, 1.0, 0.5, 2.0 ]
PowhegConfig.kappa_ghz = [1.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0]
PowhegConfig.kappa_ght = [1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0]
PowhegConfig.kappa_ghb = [1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0]

PowhegConfig.generate(remove_oldStyle_rwt_comments=True)

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG gg->H+Z->all production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs" ]
evgenConfig.contact     = [ 'carlo.enrico.pandini@cern.ch', 'paolo.francavilla@cern.ch', 'r.d.schaffer@cern.ch' ]
evgenConfig.process     = "gg->ZH, H->all, Z->inc"
evgenConfig.generators  = [ 'Powheg' ]
evgenConfig.nEventsPerJob = 2000
