#--------------------------------------------------------------
# Pythia A14 tune
#--------------------------------------------------------------

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
#--------------------------------------------------------------
# Higgs at Pythia8 (H->yy,  removing Dalitz decays)
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',     # decay of Higgs
                             '25:onIfMatch = 22 22' ]

genSeq.Pythia8.Commands += ["TimeShower:QEDshowerByGamma = off"]
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, bbH 4FS, H->gamgam mh=125 GeV"
evgenConfig.keywords    = [ "Higgs", "SMHiggs", "diphoton" ]
evgenConfig.contact     = [ 'ana.cueto@cern.ch' ]
evgenConfig.generators  = [ 'Powheg','Pythia8' ]
evgenConfig.inputFilesPerJob = 1
evgenConfig.nEventsPerJob    = 10000
