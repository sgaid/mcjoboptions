evgenConfig.description = "Single pion equal mixture pi0/pi+/pi- with flat 2 < E < 50 GeV and eta distributions, |eta| < 3"
evgenConfig.keywords = ["singleParticle", "pi0"]
evgenConfig.contact = ["angerami@cern.ch"] 
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG
genSeq += PG.ParticleGun()
evgenConfig.generators += ['ParticleGun']
genSeq.ParticleGun.sampler.pid = (111,-211,211)
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=PG.UniformSampler(2000., 50000.), eta=[-3.0, 3.0])

include("EvtGen_i/EvtGen_Fragment.py")
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
evgenConfig.auxfiles+=['inclusive.pdt']
genSeq.EvtInclusiveDecay.allowAllKnownDecays=True
