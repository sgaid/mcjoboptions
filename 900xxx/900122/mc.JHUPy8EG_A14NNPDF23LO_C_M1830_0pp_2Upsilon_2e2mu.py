#--------------------------------------------------------------
#JHUgen+Pythia8 pp/gg ->  T -> Upsilon Upsilonn ->2mu+2e production
#--------------------------------------------------------------
evgenConfig.nEventsPerJob = 10000
evgenConfig.description = "JHUGen+Pythia8 pp/gg-> T -> Upsilon Upsilonn ->2mu+2e production"
evgenConfig.keywords = ["heavyFlavour","Upsilon","Muon"]
evgenConfig.contact = ["yue.xu@cern.ch"]
evgenConfig.process = "pp/gg -> T -> Upsilon Upsilonn ->2mu+2e production"
evgenConfig.generators += ['Lhef']
evgenConfig.inputFilesPerJob = 1

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Photospp.py")
include("Pythia8_i/Pythia8_LHEF.py")

### Set lepton filters
if not hasattr(filtSeq, "MultiMuonFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
   muonfilter1 = MultiMuonFilter("muonfilter1")
   muonfilter2 = MultiMuonFilter("muonfilter2")
   filtSeq += muonfilter1
   filtSeq += muonfilter2

if not hasattr(filtSeq, "MultiElectronFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
   elefilter1 = MultiElectronFilter("elefilter1")
   filtSeq += elefilter1

filtSeq.muonfilter1.Ptcut = 1500.0 #MeV
filtSeq.muonfilter1.Etacut = 2.7
filtSeq.muonfilter1.NMuons = 2 #minimum

filtSeq.muonfilter2.Ptcut = 2000.0 #MeV
filtSeq.muonfilter2.Etacut = 2.7
filtSeq.muonfilter2.NMuons = 1 #minimum

filtSeq.elefilter1.Ptcut = 4000.0 #MeV
filtSeq.elefilter1.Etacut = 2.7
filtSeq.elefilter1.NElectrons = 2 #minimum

