evgenConfig.description = "Bc+ -> J/psi(mumu) mu+ nu_mu sample with BCVEGPY"
evgenConfig.contact = ["Semen.Turchikhin@cern.ch"]
evgenConfig.keywords = ["exclusive","Jpsi","2muon"]
evgenConfig.nEventsPerJob = 5000

include("Pythia8_i/Pythia8_A14_CTEQ6L1_EvtGen_Common.py")
include("Pythia8_i/Pythia8_BCVEGPY.py")
include("Pythia8_PDG2020Masses.py")
include("Pythia8_BcStates_v2.py")

genSeq.EvtInclusiveDecay.whiteList+=[100541, 100543, -100541, -100543]
genSeq.EvtInclusiveDecay.userDecayFile = "Bc_JpsiMuMu_Mu_Nu.dec"
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8_BcPDG20.pdt"
evgenConfig.auxfiles += [genSeq.EvtInclusiveDecay.pdtFile,genSeq.EvtInclusiveDecay.userDecayFile]

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter     
filtSeq += MultiMuonFilter("ThreeMuonFilter")

ThreeMuonFilter = filtSeq.ThreeMuonFilter
ThreeMuonFilter.Ptcut = 3500.
ThreeMuonFilter.Etacut = 2.7
ThreeMuonFilter.NMuons = 3
