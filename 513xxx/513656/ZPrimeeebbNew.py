from MadGraphControl.MadGraphUtils import *
import os
import re
import numpy as np
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment


THIS_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
job_option_name = [ f for f in os.listdir(THIS_DIR) if (f.startswith('mc') and f.endswith('.py'))][0]
print job_option_name

matchesMass = re.search("M([0-9]+).*\.py", job_option_name)
if matchesMass is None:
    raise RuntimeError("Cannot find mass string in job option name: {:s}.".format(job_option_name))
else:
    zpmass = float(matchesMass.group(1))


matchesCoupling = re.search("G([0-9]+).*\.py", job_option_name)
if matchesCoupling is None:
    raise RuntimeError("Cannot find coupling string in job option name: {:s}.".format(job_option_name))
else:
   gZp = float(matchesCoupling.group(1))/10


#determine channel (mumu or ee final state)

matchesFinalState = re.search("Zp+([a-z]+)_", job_option_name)

if matchesFinalState is None:
    raise RuntimeError("Cannot find final state (mumu or ee) string in job option name: {:s}.".format(job_option_name))
else:
    channel = matchesFinalState.group(1)


if 'mumu' in channel:
    gZpmumu = gZp
    gZpee   = 0
    lepton     = "mu-"
    antilepton = "mu+"
elif 'ee' in channel:
    gZpmumu = 0
    gZpee   = gZp
    lepton     = "e-"
    antilepton = "e+"


#Compute width manually
def Compute_Width(mZp, gZp):
    m_top = 173.0
    V_cb  = 0.04
    gZpll = gZp
    gZpqq = gZp
    Gamma = (1/(24*np.pi))*(gZpll**2)*mZp + (1/(8*np.pi))*(gZpqq**2)*mZp + (1/(8*np.pi))*(gZpqq**2)*(mZp**2 - m_top**2)*np.sqrt(mZp**2 - 4*(m_top**2))/(mZp**2) + (1/(16*np.pi))*(gZpqq**2)*mZp*V_cb**2 + (1/(8*np.pi))*(gZpqq**2)*mZp*V_cb**4
    return Gamma


Gamma = Compute_Width(zpmass, gZp)

print ("This is zpmass:    ", zpmass)
print ("This is gZp:     ", gZp)
print ("This is final state:   ", channel)
print ("This is gZpmumu:    ", gZpmumu)
print ("This is gZpee:    ", gZpee)
print ("This is width:    ", Gamma)


# ecmEnergy given when running Gen_tf.py
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

mgproc = """
import model Zpll_LH_UFO_CrossGen
define p1 = g u d c s b u~ d~ c~ s~ b~ ## 5-flavour scheme
define j1 = g u d c s b u~ d~ c~ s~ b~ ## any jet

generate p1 p1 > zp NP=2 QCD=0 QED=0, zp > e+ e- @0
add process p1 p1 > zp j1 NP=2 QCD=1 QED=0, zp > e+ e- @1
add process p1 p1 > zp j1 j1 NP=2 QCD=2 QED=0, zp > e+ e- @2

output -f
"""

process_dir = new_process(process=mgproc)

settings = {
    'nevents'      : 20.0*runArgs.maxEvents,
    'lhe_version': '2.0',
    'cut_decays' : 'F',
    'ickkw' : 0,
    'xqcut': 0.,
    'drjj' : 0.0,
    'maxjetflavor': 5,
    'dparameter': 0.4,
    'ktdurham': 30.,
}
modify_run_card(process_dir=process_dir, runArgs=runArgs, settings=settings)

# Parameters
parameters = {
    'DECAY':{
        '9000005'  : str(Gamma)
    },
    'npcoupling':{
        '1': str(gZpmumu), #'0.5', #gZpmumu
        '2': str(gZpee), #'0',   #gZpee
        '3': str(gZp), #'0.5', #gZpbb
        '4': str(gZp), #'0.5', #gZpbs
        '5': str(gZp), #'0.5', #gZpss
        '6': '0',   #gZptt
    },
    'mass':{
        '9000005' : '{:e}'.format(zpmass)
        },
}
modify_param_card(process_dir=process_dir, params=parameters)

pdgfile = open("pdgid_extras.txt", "w+")
pdgfile.write("""9000005
-9000005
""")
pdgfile.close()


# Print cards on screen
print_cards(run_card='run_card.dat', param_card='param_card.dat')

generate(process_dir=process_dir, runArgs=runArgs)

arrange_output(process_dir=process_dir, lhe_version=3, saveProcDir=False, runArgs=runArgs)

# Metadata
evgenConfig.description = ('ZPrime+bbbar to lp+ lp-')
evgenConfig.keywords+=['BSM','exotic','zprime']
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.process = 'pp -> zp b b~'
evgenConfig.contact = ["Roy S. Brener <roy.brener@cern.ch>"]

PYTHIA8_nJetMax=2
PYTHIA8_Dparameter=0.4
PYTHIA8_Process="guess"
PYTHIA8_TMS=30.
PYTHIA8_nQuarksMerge=5

# Shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")

genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]
