# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  19:00
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.96322921E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.59450406E+03  # scale for input parameters
    1    1.98241713E+02  # M_1
    2    1.89050541E+02  # M_2
    3    4.86231742E+03  # M_3
   11    2.02105113E+03  # A_t
   12    1.21339684E+03  # A_b
   13   -1.15161522E+03  # A_tau
   23    1.39941691E+03  # mu
   25    1.87483306E+01  # tan(beta)
   26    3.20017456E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.67723550E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.59468998E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.94410896E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.59450406E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.59450406E+03  # (SUSY scale)
  1  1     8.39629023E-06   # Y_u(Q)^DRbar
  2  2     4.26531544E-03   # Y_c(Q)^DRbar
  3  3     1.01433930E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.59450406E+03  # (SUSY scale)
  1  1     3.16322652E-04   # Y_d(Q)^DRbar
  2  2     6.01013038E-03   # Y_s(Q)^DRbar
  3  3     3.13692949E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.59450406E+03  # (SUSY scale)
  1  1     5.52006158E-05   # Y_e(Q)^DRbar
  2  2     1.14137366E-02   # Y_mu(Q)^DRbar
  3  3     1.91960274E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.59450406E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     2.02105109E+03   # A_t(Q)^DRbar
Block Ad Q=  3.59450406E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.21339693E+03   # A_b(Q)^DRbar
Block Ae Q=  3.59450406E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.15161521E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.59450406E+03  # soft SUSY breaking masses at Q
   1    1.98241713E+02  # M_1
   2    1.89050541E+02  # M_2
   3    4.86231742E+03  # M_3
  21    8.19210372E+06  # M^2_(H,d)
  22   -1.73442081E+06  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.67723550E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.59468998E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.94410896E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.21994373E+02  # h0
        35     3.19985303E+03  # H0
        36     3.20017456E+03  # A0
        37     3.20403378E+03  # H+
   1000001     1.00870182E+04  # ~d_L
   2000001     1.00626637E+04  # ~d_R
   1000002     1.00867088E+04  # ~u_L
   2000002     1.00652779E+04  # ~u_R
   1000003     1.00870197E+04  # ~s_L
   2000003     1.00626652E+04  # ~s_R
   1000004     1.00867102E+04  # ~c_L
   2000004     1.00652788E+04  # ~c_R
   1000005     4.78593684E+03  # ~b_1
   2000005     5.05560754E+03  # ~b_2
   1000006     2.69846606E+03  # ~t_1
   2000006     4.78807557E+03  # ~t_2
   1000011     1.00212656E+04  # ~e_L-
   2000011     1.00092709E+04  # ~e_R-
   1000012     1.00204973E+04  # ~nu_eL
   1000013     1.00212717E+04  # ~mu_L-
   2000013     1.00092782E+04  # ~mu_R-
   1000014     1.00205019E+04  # ~nu_muL
   1000015     1.00112890E+04  # ~tau_1-
   2000015     1.00230965E+04  # ~tau_2-
   1000016     1.00218025E+04  # ~nu_tauL
   1000021     5.32531196E+03  # ~g
   1000022     1.99221293E+02  # ~chi_10
   1000023     2.09895415E+02  # ~chi_20
   1000025     1.42202163E+03  # ~chi_30
   1000035     1.42358934E+03  # ~chi_40
   1000024     2.10048716E+02  # ~chi_1+
   1000037     1.42420995E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -5.10066614E-02   # alpha
Block Hmix Q=  3.59450406E+03  # Higgs mixing parameters
   1    1.39941691E+03  # mu
   2    1.87483306E+01  # tan[beta](Q)
   3    2.43074589E+02  # v(Q)
   4    1.02411172E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -1.68949287E-02   # Re[R_st(1,1)]
   1  2     9.99857271E-01   # Re[R_st(1,2)]
   2  1    -9.99857271E-01   # Re[R_st(2,1)]
   2  2    -1.68949287E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99805762E-01   # Re[R_sb(1,1)]
   1  2     1.97088159E-02   # Re[R_sb(1,2)]
   2  1    -1.97088159E-02   # Re[R_sb(2,1)]
   2  2     9.99805762E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     2.13343190E-01   # Re[R_sta(1,1)]
   1  2     9.76977320E-01   # Re[R_sta(1,2)]
   2  1    -9.76977320E-01   # Re[R_sta(2,1)]
   2  2     2.13343190E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.97640182E-01   # Re[N(1,1)]
   1  2    -5.80290792E-02   # Re[N(1,2)]
   1  3     3.60375528E-02   # Re[N(1,3)]
   1  4    -6.92726887E-03   # Re[N(1,4)]
   2  1     6.00395674E-02   # Re[N(2,1)]
   2  2     9.96607611E-01   # Re[N(2,2)]
   2  3    -5.52385514E-02   # Re[N(2,3)]
   2  4     1.08268878E-02   # Re[N(2,4)]
   3  1    -1.86967486E-02   # Re[N(3,1)]
   3  2     3.25736044E-02   # Re[N(3,2)]
   3  3     7.05986073E-01   # Re[N(3,3)]
   3  4     7.07229140E-01   # Re[N(3,4)]
   4  1    -2.75635309E-02   # Re[N(4,1)]
   4  2     4.84237027E-02   # Re[N(4,2)]
   4  3     7.05147972E-01   # Re[N(4,3)]
   4  4    -7.06867551E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.96720093E-01   # Re[U(1,1)]
   1  2     8.09262380E-02   # Re[U(1,2)]
   2  1     8.09262380E-02   # Re[U(2,1)]
   2  2     9.96720093E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.99873839E-01   # Re[V(1,1)]
   1  2     1.58841765E-02   # Re[V(1,2)]
   2  1     1.58841765E-02   # Re[V(2,1)]
   2  2     9.99873839E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02508870E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.95329571E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     3.60459336E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     3.35885995E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     7.29937884E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44763053E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     6.86729215E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.22143328E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.49231962E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.32192070E-04    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.04870961E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     3.83136534E-03    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03007385E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.94344526E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     3.60259585E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     5.82321189E-04    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     9.75375774E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     4.91806902E-04    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.44788081E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     6.86613254E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.22088385E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.35046981E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     4.17764472E-04    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.04766772E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     3.83070558E-03    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     6.83841783E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     7.07334268E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     3.96997279E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     4.84410202E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     4.70850866E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     7.13865143E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     8.60533831E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.47811585E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.86133203E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.98698608E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     2.53994122E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.62238862E-02    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.59763946E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     1.13008282E-02    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44766978E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     1.06325743E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.83188840E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     5.29897513E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     1.16638418E-03    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.08641542E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     1.47593839E-04    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44791982E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.06307430E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.83140066E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     5.29806268E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     1.16618334E-03    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.08537888E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     3.18626537E-04    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.51840812E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.01385552E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.70031101E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.05282299E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.11220242E-03    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     5.80678727E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.62871352E-02    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.17289553E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.33888963E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.86548180E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.48679361E+02   # ~d_L
#    BR                NDA      ID1      ID2
     4.49120419E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     7.85870760E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.00218658E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     2.21097065E-04    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.60637538E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.01804168E-03    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.54944824E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.17303564E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.33884712E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.86515431E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.48689989E+02   # ~s_L
#    BR                NDA      ID1      ID2
     4.49113493E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     7.85856055E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.06524964E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     2.27385676E-04    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.60634445E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.02441807E-03    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.54930485E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.59247324E+02   # ~b_1
#    BR                NDA      ID1      ID2
     7.47567108E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.28399853E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.44315274E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.44933799E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.61535414E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     5.11355650E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     4.23085053E-02    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     1.97321228E+01   # ~b_2
#    BR                NDA      ID1      ID2
     1.41913171E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     8.89480269E-04    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.12807824E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.12639936E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     7.69022567E-04    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.26727642E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.80312031E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.64177977E-03    2     2000006       -24   # BR(~b_2 -> ~t_2 W^-)
     8.07105096E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.62372735E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     4.34408522E+02   # ~u_R
#    BR                NDA      ID1      ID2
     5.14584774E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.86357432E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     9.48300038E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.48666682E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.15702731E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     8.20322688E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.47181495E-04    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.61654469E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     7.54903319E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.34415584E+02   # ~c_R
#    BR                NDA      ID1      ID2
     5.14576438E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.86355400E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     9.48284768E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.48677267E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.15700543E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     8.20306953E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.50373890E-04    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.61651459E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     7.54888967E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     6.30898510E+01   # ~t_1
#    BR                NDA      ID1      ID2
     9.31676929E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     3.51877605E-04    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.25001150E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.25845851E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.55612414E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.61442091E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.94503069E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.31711260E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.51544450E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.51269945E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.60528010E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.79316783E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.09086896E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     3.41609358E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.64470381E-09   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.45561924E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.23062515E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.15183550E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.15129312E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.01062674E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.06280417E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     7.73774180E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     3.12599488E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.92093598E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.96637020E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.03092750E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.95959950E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     9.01739092E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.82524720E-12   # chi^0_2
#    BR                NDA      ID1      ID2
     5.13869163E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     7.03432522E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     5.83159474E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     9.07996253E-02    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     9.07186076E-02    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     9.48017645E-03    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     1.93476341E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     1.93195497E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     1.29654053E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.14840639E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     1.16438773E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.68312378E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.68312378E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     7.31349866E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.71818006E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     5.31274482E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.43056868E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.47772345E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     4.08948047E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     8.31632548E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     8.31632548E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.05718271E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.95345513E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.95345513E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.69525990E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     8.54588037E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     7.14298035E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.92377522E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.59431420E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     4.47281182E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     8.50545466E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     8.50545466E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.00091394E+02   # ~g
#    BR                NDA      ID1      ID2
     4.35820498E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     4.35820498E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.71937024E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     2.71937024E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     2.91118498E-02    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.91118498E-02    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     7.67892043E-03    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     7.67892043E-03    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.29204092E-03   # Gamma(h0)
     2.50206655E-03   2        22        22   # BR(h0 -> photon photon)
     1.33764825E-03   2        22        23   # BR(h0 -> photon Z)
     2.21454379E-02   2        23        23   # BR(h0 -> Z Z)
     1.94363299E-01   2       -24        24   # BR(h0 -> W W)
     7.97421156E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.37672538E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.39164601E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.89555735E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.53350556E-07   2        -2         2   # BR(h0 -> Up up)
     2.97590829E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.25030798E-07   2        -1         1   # BR(h0 -> Down down)
     2.26058559E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.00728770E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     4.12895575E+01   # Gamma(HH)
     3.36983941E-08   2        22        22   # BR(HH -> photon photon)
     8.12227959E-08   2        22        23   # BR(HH -> photon Z)
     1.97380602E-06   2        23        23   # BR(HH -> Z Z)
     5.48018873E-07   2       -24        24   # BR(HH -> W W)
     9.53182549E-07   2        21        21   # BR(HH -> gluon gluon)
     4.28832113E-09   2       -11        11   # BR(HH -> Electron electron)
     1.90939246E-04   2       -13        13   # BR(HH -> Muon muon)
     5.51461444E-02   2       -15        15   # BR(HH -> Tau tau)
     4.51777400E-13   2        -2         2   # BR(HH -> Up up)
     8.76416825E-08   2        -4         4   # BR(HH -> Charm charm)
     5.74171407E-03   2        -6         6   # BR(HH -> Top top)
     3.02814813E-07   2        -1         1   # BR(HH -> Down down)
     1.09526427E-04   2        -3         3   # BR(HH -> Strange strange)
     2.60336324E-01   2        -5         5   # BR(HH -> Bottom bottom)
     4.17779066E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.04537223E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     2.04537223E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     8.40041604E-06   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     2.34933913E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     9.97443754E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.25669172E-01   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     8.94112836E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.05867123E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     2.67087428E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.88712586E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     7.39634294E-05   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     8.81712510E-04   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     1.25853417E-04   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     1.60931602E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     4.08400513E+01   # Gamma(A0)
     9.53560517E-08   2        22        22   # BR(A0 -> photon photon)
     1.53085102E-07   2        22        23   # BR(A0 -> photon Z)
     1.18862941E-05   2        21        21   # BR(A0 -> gluon gluon)
     4.18929873E-09   2       -11        11   # BR(A0 -> Electron electron)
     1.86531964E-04   2       -13        13   # BR(A0 -> Muon muon)
     5.38733291E-02   2       -15        15   # BR(A0 -> Tau tau)
     4.26596868E-13   2        -2         2   # BR(A0 -> Up up)
     8.27536247E-08   2        -4         4   # BR(A0 -> Charm charm)
     5.47356131E-03   2        -6         6   # BR(A0 -> Top top)
     2.95822963E-07   2        -1         1   # BR(A0 -> Down down)
     1.06996924E-04   2        -3         3   # BR(A0 -> Strange strange)
     2.54325434E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     4.44675532E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.06653351E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     2.06653351E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.15850188E-04   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     2.50079076E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.06330554E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     9.10461662E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.26240965E-01   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.13027870E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.92165044E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     2.68299707E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     2.62394662E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.98989153E-04   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     6.76799296E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     3.40855856E-06   2        23        25   # BR(A0 -> Z h0)
     1.10831024E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     4.25944097E+01   # Gamma(Hp)
     4.79454237E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     2.04981661E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     5.79804749E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     2.95083803E-07   2        -1         2   # BR(Hp -> Down up)
     4.98447561E-06   2        -3         2   # BR(Hp -> Strange up)
     2.91541744E-06   2        -5         2   # BR(Hp -> Bottom up)
     1.83473606E-08   2        -1         4   # BR(Hp -> Down charm)
     1.06437811E-04   2        -3         4   # BR(Hp -> Strange charm)
     4.08262919E-04   2        -5         4   # BR(Hp -> Bottom charm)
     4.98913033E-07   2        -1         6   # BR(Hp -> Down top)
     1.10356498E-05   2        -3         6   # BR(Hp -> Strange top)
     2.82919410E-01   2        -5         6   # BR(Hp -> Bottom top)
     8.32906854E-06   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.87693452E-01   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     2.38062755E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     6.96000289E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.99211752E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     3.27913157E-04   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     1.98931706E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     2.03589437E-04   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     3.28184637E-06   2        24        25   # BR(Hp -> W h0)
     4.08243716E-11   2        24        35   # BR(Hp -> W HH)
     2.73598688E-11   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.16296716E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    3.51583604E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    3.51499900E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00023813E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    2.60681928E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    2.84495102E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.16296716E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    3.51583604E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    3.51499900E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99994797E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    5.20261269E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99994797E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    5.20261269E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.27074972E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    8.42264815E-04        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    9.61930217E-02        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    5.20261269E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99994797E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.25904631E-04   # BR(b -> s gamma)
    2    1.58922152E-06   # BR(b -> s mu+ mu-)
    3    3.52456208E-05   # BR(b -> s nu nu)
    4    2.64283054E-15   # BR(Bd -> e+ e-)
    5    1.12898724E-10   # BR(Bd -> mu+ mu-)
    6    2.36344748E-08   # BR(Bd -> tau+ tau-)
    7    8.89145316E-14   # BR(Bs -> e+ e-)
    8    3.79842617E-09   # BR(Bs -> mu+ mu-)
    9    8.05691645E-07   # BR(Bs -> tau+ tau-)
   10    9.65725849E-05   # BR(B_u -> tau nu)
   11    9.97557830E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42073193E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93674740E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15767042E-03   # epsilon_K
   17    2.28166190E-15   # Delta(M_K)
   18    2.47940286E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28909281E-11   # BR(K^+ -> pi^+ nu nu)
   20    6.38328283E-17   # Delta(g-2)_electron/2
   21    2.72905852E-12   # Delta(g-2)_muon/2
   22    7.72443971E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    4.46521607E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.94574073E-01   # C7
     0305 4322   00   2    -1.17807579E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.03042886E-01   # C8
     0305 6321   00   2    -1.56540897E-04   # C8'
 03051111 4133   00   0     1.62263432E+00   # C9 e+e-
 03051111 4133   00   2     1.62282065E+00   # C9 e+e-
 03051111 4233   00   2     5.86297154E-05   # C9' e+e-
 03051111 4137   00   0    -4.44532642E+00   # C10 e+e-
 03051111 4137   00   2    -4.44297964E+00   # C10 e+e-
 03051111 4237   00   2    -4.36279406E-04   # C10' e+e-
 03051313 4133   00   0     1.62263432E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62282061E+00   # C9 mu+mu-
 03051313 4233   00   2     5.86296975E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44532642E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44297967E+00   # C10 mu+mu-
 03051313 4237   00   2    -4.36279403E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50484329E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     9.44256172E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50484329E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     9.44256202E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50484329E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     9.44264665E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85811931E-07   # C7
     0305 4422   00   2     1.81784715E-06   # C7
     0305 4322   00   2     3.38297243E-08   # C7'
     0305 6421   00   0     3.30473434E-07   # C8
     0305 6421   00   2    -8.03783775E-07   # C8
     0305 6321   00   2    -2.47522493E-08   # C8'
 03051111 4133   00   2     5.43798371E-07   # C9 e+e-
 03051111 4233   00   2     1.20574329E-06   # C9' e+e-
 03051111 4137   00   2     1.86402071E-07   # C10 e+e-
 03051111 4237   00   2    -8.97334302E-06   # C10' e+e-
 03051313 4133   00   2     5.43797803E-07   # C9 mu+mu-
 03051313 4233   00   2     1.20574322E-06   # C9' mu+mu-
 03051313 4137   00   2     1.86402666E-07   # C10 mu+mu-
 03051313 4237   00   2    -8.97334323E-06   # C10' mu+mu-
 03051212 4137   00   2    -2.06712206E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     1.94213615E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -2.06711899E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     1.94213615E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -2.06625236E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     1.94213615E-06   # C11' nu_3 nu_3
