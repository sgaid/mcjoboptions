evgenConfig.description = 'aMcAtNlo Zvv+0,1,2,3j NLO FxFx HT2-biased, pT(vv)>70 GeV, BFilter Py8 ShowerWeights'
evgenConfig.contact = ["francesco.giuli@cern.ch","federico.sforza@cern.ch","jan.kretzschmar@cern.ch"]
evgenConfig.keywords += ['SM', 'Z', 'neutrino', 'jets', 'NLO', 'invisible', 'monojet']
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]

# FxFx match+merge eff: 33.4%
# BFilter - eff ~9.6%
# one LHE file contains 50000 events
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 7

include("MadGraphControl_Vjets_FxFx_shower.py")
