evgenConfig.description = 'aMcAtNlo Wtaunu+0,1,2,3j NLO FxFx HT2-biased CFilterBVeto Py8 ShowerWeights, forced leptonic tau decays'
evgenConfig.contact = ["francesco.giuli@cern.ch","federico.sforza@cern.ch","jan.kretzschmar@cern.ch"]
evgenConfig.keywords += ['SM', 'W', 'tau', 'jets', 'NLO']
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]

# FxFx match+merge eff: 30.2%
# CFilterBVeto - eff ~25.2%
# one LHE file contains 55000 events
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 3

# restore the resonances (24)->(-15)+(16) or (-24)->(15)+(-16) in LHE files, if not present
restoreResonances = [[-15, 16, 24], [15, -16, -24]]

include("MadGraphControl_Vjets_FxFx_shower.py")
