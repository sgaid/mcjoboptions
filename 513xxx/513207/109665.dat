# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  22:20
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.30921155E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.02814052E+03  # scale for input parameters
    1    1.25361702E+02  # M_1
    2    1.24033273E+02  # M_2
    3    1.14321731E+03  # M_3
   11    1.65378968E+03  # A_t
   12    9.85530720E+02  # A_b
   13   -1.09243946E+03  # A_tau
   23   -1.34426420E+03  # mu
   25    5.31466794E+01  # tan(beta)
   26    4.43451631E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.20770614E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.84892803E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.61479037E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.02814052E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.02814052E+03  # (SUSY scale)
  1  1     8.38585619E-06   # Y_u(Q)^DRbar
  2  2     4.26001494E-03   # Y_c(Q)^DRbar
  3  3     1.01307878E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.02814052E+03  # (SUSY scale)
  1  1     8.95578776E-04   # Y_d(Q)^DRbar
  2  2     1.70159967E-02   # Y_s(Q)^DRbar
  3  3     8.88133512E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.02814052E+03  # (SUSY scale)
  1  1     1.56285045E-04   # Y_e(Q)^DRbar
  2  2     3.23147907E-02   # Y_mu(Q)^DRbar
  3  3     5.43481619E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.02814052E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     1.65378971E+03   # A_t(Q)^DRbar
Block Ad Q=  3.02814052E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     9.85530223E+02   # A_b(Q)^DRbar
Block Ae Q=  3.02814052E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.09243945E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.02814052E+03  # soft SUSY breaking masses at Q
   1    1.25361702E+02  # M_1
   2    1.24033273E+02  # M_2
   3    1.14321731E+03  # M_3
  21    1.77430694E+07  # M^2_(H,d)
  22   -1.62639301E+06  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.20770614E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.84892803E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.61479037E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.21817471E+02  # h0
        35     4.42941753E+03  # H0
        36     4.43451631E+03  # A0
        37     4.43120446E+03  # H+
   1000001     1.01235970E+04  # ~d_L
   2000001     1.00981004E+04  # ~d_R
   1000002     1.01232295E+04  # ~u_L
   2000002     1.01012023E+04  # ~u_R
   1000003     1.01236101E+04  # ~s_L
   2000003     1.00981220E+04  # ~s_R
   1000004     1.01232417E+04  # ~c_L
   2000004     1.01012037E+04  # ~c_R
   1000005     3.21553631E+03  # ~b_1
   2000005     4.62589631E+03  # ~b_2
   1000006     2.84617317E+03  # ~t_1
   2000006     3.22174177E+03  # ~t_2
   1000011     1.00214642E+04  # ~e_L-
   2000011     1.00089824E+04  # ~e_R-
   1000012     1.00206908E+04  # ~nu_eL
   1000013     1.00215327E+04  # ~mu_L-
   2000013     1.00090824E+04  # ~mu_R-
   1000014     1.00207479E+04  # ~nu_muL
   1000015     1.00328475E+04  # ~tau_1-
   2000015     1.00456742E+04  # ~tau_2-
   1000016     1.00370403E+04  # ~nu_tauL
   1000021     1.45047848E+03  # ~g
   1000022     1.26769811E+02  # ~chi_10
   1000023     1.39807057E+02  # ~chi_20
   1000025     1.36832020E+03  # ~chi_30
   1000035     1.36881348E+03  # ~chi_40
   1000024     1.39993987E+02  # ~chi_1+
   1000037     1.37016366E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.88539138E-02   # alpha
Block Hmix Q=  3.02814052E+03  # Higgs mixing parameters
   1   -1.34426420E+03  # mu
   2    5.31466794E+01  # tan[beta](Q)
   3    2.43227903E+02  # v(Q)
   4    1.96649349E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.68413671E-02   # Re[R_st(1,1)]
   1  2     9.95299829E-01   # Re[R_st(1,2)]
   2  1    -9.95299829E-01   # Re[R_st(2,1)]
   2  2    -9.68413671E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99866262E-01   # Re[R_sb(1,1)]
   1  2     1.63541596E-02   # Re[R_sb(1,2)]
   2  1    -1.63541596E-02   # Re[R_sb(2,1)]
   2  2    -9.99866262E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -7.88904252E-01   # Re[R_sta(1,1)]
   1  2     6.14516136E-01   # Re[R_sta(1,2)]
   2  1    -6.14516136E-01   # Re[R_sta(2,1)]
   2  2    -7.88904252E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.99379901E-01   # Re[N(1,1)]
   1  2     9.87313828E-03   # Re[N(1,2)]
   1  3     3.37042476E-02   # Re[N(1,3)]
   1  4     2.52137846E-03   # Re[N(1,4)]
   2  1     1.18464367E-02   # Re[N(2,1)]
   2  2     9.98205849E-01   # Re[N(2,2)]
   2  3     5.85037863E-02   # Re[N(2,3)]
   2  4     4.69596281E-03   # Re[N(2,4)]
   3  1    -2.16379562E-02   # Re[N(3,1)]
   3  2     3.82912769E-02   # Re[N(3,2)]
   3  3    -7.05713377E-01   # Re[N(3,3)]
   3  4     7.07130969E-01   # Re[N(3,4)]
   4  1    -2.51251528E-02   # Re[N(4,1)]
   4  2     4.49597865E-02   # Re[N(4,2)]
   4  3    -7.05272969E-01   # Re[N(4,3)]
   4  4    -7.07062503E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.96536218E-01   # Re[U(1,1)]
   1  2    -8.31598850E-02   # Re[U(1,2)]
   2  1     8.31598850E-02   # Re[U(2,1)]
   2  2    -9.96536218E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.99977744E-01   # Re[V(1,1)]
   1  2     6.67167398E-03   # Re[V(1,2)]
   2  1     6.67167398E-03   # Re[V(2,1)]
   2  2     9.99977744E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02733247E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.98800353E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.40334298E-04    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     4.51073103E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     6.08146721E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44836954E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.35998412E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.07196010E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.09604223E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.91724740E-04    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.04646592E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     4.05622812E-03    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.06743521E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.90910600E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.53263795E-04    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.41538327E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     2.56870525E-03    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     3.92368171E-03    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.45038475E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.34858858E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.06776193E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     8.97781763E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     9.78910518E-04    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.03810607E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     4.05062206E-03    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.87864928E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.46514107E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.63930998E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.41761384E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.40104467E-01    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.22124263E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     8.55647801E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.78142098E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.97083266E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     7.95749958E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.68524326E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.69980000E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.54942010E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     2.29895401E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44841295E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     9.00094149E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.99471281E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     7.28790826E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     9.99559091E-04    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.08764817E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.45042635E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.98849815E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.99057278E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     7.27783622E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     9.98177680E-04    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.07933154E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     1.39862548E-03    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     2.01895104E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.46789570E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.15194094E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.23758680E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     7.18351245E-04    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     4.39466007E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     2.79418832E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.70161316E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.30877017E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.92682045E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     9.01628669E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.74314304E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     4.90170641E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.16480760E-04    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     9.81206356E-02    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     6.58733613E-04    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.50259256E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.70274911E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.30779351E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.92537952E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     9.01689496E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.74310095E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     4.90140417E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.15637957E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     1.47389996E-04    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     9.81141398E-02    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     6.62600134E-04    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.50203089E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.83265337E+02   # ~b_1
#    BR                NDA      ID1      ID2
     2.01684772E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     5.01617457E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     5.94797609E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     5.93956281E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     9.86194424E-02    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.54809409E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     5.68450656E-01    2     1000021         5   # BR(~b_1 -> ~g b)
     7.06651008E-03    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     4.25592842E+02   # ~b_2
#    BR                NDA      ID1      ID2
     6.18995932E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     4.25124489E-04    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     7.08187842E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     7.07362460E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     8.54443544E-04    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.40919831E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     6.97553632E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     5.69641608E-03    2     2000006       -24   # BR(~b_2 -> ~t_2 W^-)
     2.90171340E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     3.80889133E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     7.87329653E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.86063761E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.71359250E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     9.01617800E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.39587729E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     4.94319771E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     9.87971256E-02    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     8.50236052E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.87336768E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.86061205E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.71350579E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     9.01678509E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.39578496E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     4.94287067E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     9.87910389E-02    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     8.50179883E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.95359912E+02   # ~t_1
#    BR                NDA      ID1      ID2
     3.17957169E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     3.29448545E-04    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     4.82236449E-04    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     8.74374884E-02    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     8.88690797E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     9.74752467E-04    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.75261361E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     3.25135955E-03    2     1000021         4   # BR(~t_1 -> ~g c)
     6.11586617E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.84792167E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.65057388E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     4.89820007E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     7.64222289E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     7.57653159E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     9.96831025E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.18538721E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.69089330E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     3.58446109E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     6.28426661E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.36891464E-10   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.41696713E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.26567320E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.13776567E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.13740085E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.04218408E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     9.92811421E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     9.07967259E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     3.00078109E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.95043450E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.94441663E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     9.55795997E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.27189636E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     7.76085423E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     6.05033218E-12   # chi^0_2
#    BR                NDA      ID1      ID2
     3.62883894E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     8.81792758E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     7.77087174E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.13113715E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.13044067E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     3.41362313E-02    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     2.42299597E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     2.42072942E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     1.84484234E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.44048423E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     1.07436103E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.74237378E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.74237378E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     5.18067853E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.56843928E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     5.01709249E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.69557100E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.29632213E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     4.31256075E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     8.74145119E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     8.74145119E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     9.92852601E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.96820737E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.96820737E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     4.15037345E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.22915696E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     5.03063580E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.70006028E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.30361106E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     4.36989777E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     7.95714359E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     7.95714359E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.68842577E-03   # ~g
#    BR                NDA      ID1      ID2
     1.20042263E-04    2     1000022        21   # BR(~g -> chi^0_1 g)
     1.05839382E-04    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
     7.47554426E-04    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     7.47553842E-04    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     1.10073555E-01    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     2.26205240E-04    3     1000022         1        -1   # BR(~g -> chi^0_1 d d_bar)
     2.26208816E-04    3     1000022         3        -3   # BR(~g -> chi^0_1 s s_bar)
     1.10980697E-02    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.38530155E-03    3     1000023         2        -2   # BR(~g -> chi^0_2 u u_bar)
     1.38529482E-03    3     1000023         4        -4   # BR(~g -> chi^0_2 c c_bar)
     1.25775907E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.37336577E-03    3     1000023         1        -1   # BR(~g -> chi^0_2 d d_bar)
     1.37337455E-03    3     1000023         3        -3   # BR(~g -> chi^0_2 s s_bar)
     1.61404984E-01    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     2.75874408E-03    3     1000024         1        -2   # BR(~g -> chi^+_1 d u_bar)
     2.75874408E-03    3    -1000024        -1         2   # BR(~g -> chi^-_1 d_bar u)
     2.75874630E-03    3     1000024         3        -4   # BR(~g -> chi^+_1 s c_bar)
     2.75874630E-03    3    -1000024        -3         4   # BR(~g -> chi^-_1 s_bar c)
     2.86409086E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.86409086E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.12216025E-03   # Gamma(h0)
     2.61385825E-03   2        22        22   # BR(h0 -> photon photon)
     1.38172241E-03   2        22        23   # BR(h0 -> photon Z)
     2.28096556E-02   2        23        23   # BR(h0 -> Z Z)
     2.00910775E-01   2       -24        24   # BR(h0 -> W W)
     8.37513320E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.28516046E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.35091556E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.77809287E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.51118580E-07   2        -2         2   # BR(h0 -> Up up)
     2.93274635E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.14586471E-07   2        -1         1   # BR(h0 -> Down down)
     2.22281152E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.90966121E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     2.09943166E+02   # Gamma(HH)
     1.16964498E-08   2        22        22   # BR(HH -> photon photon)
     2.57180544E-08   2        22        23   # BR(HH -> photon Z)
     2.50599274E-08   2        23        23   # BR(HH -> Z Z)
     4.71595743E-09   2       -24        24   # BR(HH -> W W)
     5.67457322E-06   2        21        21   # BR(HH -> gluon gluon)
     7.69845829E-09   2       -11        11   # BR(HH -> Electron electron)
     3.42819692E-04   2       -13        13   # BR(HH -> Muon muon)
     9.90164920E-02   2       -15        15   # BR(HH -> Tau tau)
     1.95665735E-14   2        -2         2   # BR(HH -> Up up)
     3.79623084E-09   2        -4         4   # BR(HH -> Charm charm)
     3.79623251E-04   2        -6         6   # BR(HH -> Top top)
     5.57440488E-07   2        -1         1   # BR(HH -> Down down)
     2.01646253E-04   2        -3         3   # BR(HH -> Strange strange)
     6.66820129E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.23946598E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     7.03657804E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     7.03657804E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     5.23038768E-06   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     7.38594802E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     2.62280634E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     3.73502211E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     3.87141621E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     2.32844559E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     6.63183646E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     6.87079731E-03   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.06069171E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     4.06104009E-04   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     1.53293825E-04   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     7.83436216E-08   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.91753932E+02   # Gamma(A0)
     2.48281835E-09   2        22        22   # BR(A0 -> photon photon)
     7.81909100E-10   2        22        23   # BR(A0 -> photon Z)
     8.64060703E-06   2        21        21   # BR(A0 -> gluon gluon)
     7.47196928E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.32726216E-04   2       -13        13   # BR(A0 -> Muon muon)
     9.61012836E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.76679321E-14   2        -2         2   # BR(A0 -> Up up)
     3.42787130E-09   2        -4         4   # BR(A0 -> Charm charm)
     3.48329862E-04   2        -6         6   # BR(A0 -> Top top)
     5.40962457E-07   2        -1         1   # BR(A0 -> Down down)
     1.95685419E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.47240185E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.35538007E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     7.71691148E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     7.71691148E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     3.17339659E-06   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     8.07647693E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.86800608E-04   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     4.25655882E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     4.08529078E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     2.54611658E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     7.55434058E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     7.25376539E-03   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.99171447E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     2.82516962E-04   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     2.47545912E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     6.52833232E-08   2        23        25   # BR(A0 -> Z h0)
     1.97547921E-11   2        23        35   # BR(A0 -> Z HH)
     1.43035874E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.41175327E+02   # Gamma(Hp)
     8.56465039E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.66165554E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.03572480E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.34820428E-07   2        -1         2   # BR(Hp -> Down up)
     9.06770727E-06   2        -3         2   # BR(Hp -> Strange up)
     7.33003216E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.52314018E-08   2        -1         4   # BR(Hp -> Down charm)
     1.92769973E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.02646696E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.61786689E-08   2        -1         6   # BR(Hp -> Down top)
     6.40437610E-07   2        -3         6   # BR(Hp -> Strange top)
     6.91814745E-01   2        -5         6   # BR(Hp -> Bottom top)
     5.21025956E-06   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     5.55716788E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     6.12717844E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.38211361E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     6.13693817E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.55310344E-04   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     6.13411027E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.33160129E-04   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     5.18037575E-08   2        24        25   # BR(Hp -> W h0)
     1.02861498E-13   2        24        35   # BR(Hp -> W HH)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.00428638E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.82456524E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.82456953E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99998482E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.55553782E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.54036248E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.00428638E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.82456524E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.82456953E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999998E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.62270720E-09        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999998E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.62270720E-09        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26230507E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.28377439E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.38652575E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.62270720E-09        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999998E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.14754408E-04   # BR(b -> s gamma)
    2    1.58972148E-06   # BR(b -> s mu+ mu-)
    3    3.52221168E-05   # BR(b -> s nu nu)
    4    2.76303883E-15   # BR(Bd -> e+ e-)
    5    1.18032183E-10   # BR(Bd -> mu+ mu-)
    6    2.46113669E-08   # BR(Bd -> tau+ tau-)
    7    9.38300419E-14   # BR(Bs -> e+ e-)
    8    4.00835666E-09   # BR(Bs -> mu+ mu-)
    9    8.46734200E-07   # BR(Bs -> tau+ tau-)
   10    9.61280705E-05   # BR(B_u -> tau nu)
   11    9.92966166E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42176182E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93696316E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15811187E-03   # epsilon_K
   17    2.28166707E-15   # Delta(M_K)
   18    2.47791042E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28552207E-11   # BR(K^+ -> pi^+ nu nu)
   20   -2.50952019E-16   # Delta(g-2)_electron/2
   21   -1.07292212E-11   # Delta(g-2)_muon/2
   22   -3.05337126E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    8.36279990E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.82309689E-01   # C7
     0305 4322   00   2    -4.40444042E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -9.25460201E-02   # C8
     0305 6321   00   2    -4.23954542E-05   # C8'
 03051111 4133   00   0     1.61785554E+00   # C9 e+e-
 03051111 4133   00   2     1.61824339E+00   # C9 e+e-
 03051111 4233   00   2     6.34840706E-04   # C9' e+e-
 03051111 4137   00   0    -4.44054765E+00   # C10 e+e-
 03051111 4137   00   2    -4.43605470E+00   # C10 e+e-
 03051111 4237   00   2    -4.75739275E-03   # C10' e+e-
 03051313 4133   00   0     1.61785554E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61824308E+00   # C9 mu+mu-
 03051313 4233   00   2     6.34840139E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44054765E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43605501E+00   # C10 mu+mu-
 03051313 4237   00   2    -4.75739354E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50437855E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.03077319E-03   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50437855E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.03077324E-03   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50437856E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.03078688E-03   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85846392E-07   # C7
     0305 4422   00   2    -1.42859184E-05   # C7
     0305 4322   00   2     4.28854557E-07   # C7'
     0305 6421   00   0     3.30502952E-07   # C8
     0305 6421   00   2     3.01445585E-05   # C8
     0305 6321   00   2     8.55674077E-07   # C8'
 03051111 4133   00   2     2.04363552E-06   # C9 e+e-
 03051111 4233   00   2     1.20074606E-05   # C9' e+e-
 03051111 4137   00   2     4.23688768E-07   # C10 e+e-
 03051111 4237   00   2    -8.99858147E-05   # C10' e+e-
 03051313 4133   00   2     2.04362760E-06   # C9 mu+mu-
 03051313 4233   00   2     1.20074545E-05   # C9' mu+mu-
 03051313 4137   00   2     4.23697242E-07   # C10 mu+mu-
 03051313 4237   00   2    -8.99858329E-05   # C10' mu+mu-
 03051212 4137   00   2    -5.75942907E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     1.94970172E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -5.75938621E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     1.94970172E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -5.74747551E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     1.94970172E-05   # C11' nu_3 nu_3
