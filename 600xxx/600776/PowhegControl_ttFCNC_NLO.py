#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.contact     = [ 'andrea.helen.knue@cern.ch','ian.connelly@cern.ch']

include('PowhegControl/PowhegControl_tt_Common.py')
# Initial settings
if hasattr(PowhegConfig, "topdecaymode"):
    # Use PowhegControl-00-02-XY (and earlier) syntax
    PowhegConfig.topdecaymode = 22222 # inclusive top decays
else:
    # Use PowhegControl-00-03-XY (and later) syntax
    PowhegConfig.decay_mode      = "t t~ > undecayed"
    PowhegConfig.MadSpin_enabled = False

PowhegConfig.hdamp        = 258.75                                        # 1.5 * mtop
DoSingleWeight = False
if DoSingleWeight:
    PowhegConfig.mu_F         = 1.0
    PowhegConfig.mu_R         = 1.0 
    PowhegConfig.PDF          = 260000 

# For jobs with lepton filter, add more safety than the default 10% 
if any("HLepF" in JO for JO in runArgs.jobConfig):
    nevents=evgenConfig.nEventsPerJob
    if runArgs.maxEvents>0:
        nevents=runArgs.maxEvents
    PowhegConfig.nEvents=int(7.0*nevents)

PowhegConfig.generate()

#--------------------------------------------------------------
# Now preparing for MadSpin
#--------------------------------------------------------------

# The couplings. Only real part.
# The tqH relevant couplings (c,u) x (LH,RH) (scalar of course)
RCtphi  = 0.
RCuphi  = 0.
RCtcphi = 0.
RCctphi = 0.

# The tqZ,tqg relevant couplings (c,u) x (Vector,Tensor) x (LH,RH)
# vector for tqZ
RC1utR = 0.
RC1utL = 0.
RC3utL = 0.
RC1ctR = 0.
RC1ctL = 0.
RC3ctL = 0.

# tensor for tqZ and tqgamma
RCtW  = 0.
RCtB  = 0.
RCuW  = 0.
RCuB  = 0.
RCtcW = 0.
RCtcB = 0.
RCctW = 0.
RCctB = 0.

# Get the sample short description
# in release 21.6.49, get_physics_short is in MadGraphUtilsHelpers,
# it is in Generators/MCJobOptionUtils/python/JOsupport.py in more recent release
from MadGraphControl.MadGraphUtilsHelpers import *
shortPhys = get_physics_short()
istcH = shortPhys.find('Q2c') >= 0
isPy8 = shortPhys.split('_')[0].find('Py8') >= 0 or shortPhys.split('_')[0].find('Pythia8') >= 0
print 'name = ',shortPhys,' is charm = ',istcH,' isPy8 = ',isPy8

model            = 'TopFCNC-onlyh' #eventually, change for tqZ, tqphoton, tqgluon
madspin_card_rep = 'madspin_card.dat'
madspin_in       = 'import run.lhe'
madspin_rep      = 'set ms_dir MadSpin'
madspin_seed     = runArgs.randomSeed

mscard = open(madspin_card_rep,'w')
mscard.write("""
set Nevents_for_max_weigth 250 # number of events for the estimate of the max. weight (default: 75)
set max_weight_ps_point 1000  # number of PS to estimate the maximum for each event   (default: 400)
set seed %i
%s
%s
define l+ = l+ ta+
define l- = l- ta-
define All = l+ l- vl vl~ j
\n
"""%(madspin_seed,madspin_in,madspin_rep))

## The W decay
qT2 = 1
if shortPhys.find('Wmln') >= 0:
    wstr = ', w- > l- vl~'
    qT2 = -1
elif shortPhys.find('Wpln') >= 0:
    wstr = ', w+ > l+ vl'
elif shortPhys.find('Wmqq') >= 0:
    wstr = ', w- > j j'
    qT2 = -1
elif shortPhys.find('Wpqq') >= 0:
    wstr = ', w+ > j j'
elif shortPhys.find('Q2cbar') >= 0 or shortPhys.find('Q2ubar') >= 0:
    wstr = ', w+ > All All'
elif shortPhys.find('Q2c') >= 0 or shortPhys.find('Q2u') >= 0:
    wstr = ', w- > All All'
    qT2 = -1
else :
    raise RuntimeError("No W decays are generated please check the job option")

## The SM top
if qT2 == -1:
    t2str = 'decay t~ > w- b~'
else:
    t2str = 'decay t > w+ b'

# the tqH part
keyName = None
if shortPhys.find('Q2cH') >= 0 or shortPhys.find('Q2uH') >= 0 or shortPhys.find('Q2cbarH') >= 0 or shortPhys.find('Q2ubarH') >= 0:

    keyName = 'Higgs'

    # For the time being, in the ttbar, t-->qH search, only one helicity is possible
    if istcH:
        if qT2 == 1:
            t1str = 'decay t~ > c~ h'
        else:
            t1str = 'decay t > c h'
        RCtcphi = 1
    else:
        if qT2 == 1:
            t1str = 'decay t~ > u~ h'
        else:
            t1str = 'decay t > u h'
        RCtphi = 1
 
    mscard.write("""%s\n%s%s\nlaunch"""%(t1str,t2str,wstr))
else:
    raise RuntimeError("No good runNumber")

mscard.close()

coup = {
    'RCtphi'  : RCtphi ,
    'RCuphi'  : RCuphi ,
    'RCtcphi' : RCtcphi,
    'RCctphi' : RCctphi,

    'RC1utR'  : RC1utR,
    'RC1utL'  : RC1utL,
    'RC3utL'  : RC3utL,
    'RC1ctR'  : RC1ctR,
    'RC1ctL'  : RC1ctL,
    'RC3ctL'  : RC3ctL,
    'RCtW'    : RCtW,
    'RCtB'    : RCtB,
    'RCuW'    : RCuW,
    'RCuB'    : RCuB,
    'RCtcW'   : RCtcW,
    'RCtcB'   : RCtcB,
    'RCctW'   : RCctW,
    'RCctB'   : RCctB
    }

import shutil,os,subprocess

paramFileName  = 'MadGraph_param_card_ttFCNC_NLO_FixedFCNCBR.dat'
from MadGraphControl.MadGraphUtils import *

paramFileNameN = 'param_card.dat'
os.system('mkdir Cards')
modify_param_card(param_card_input=paramFileName, process_dir=".")

# --------------------------------------------------------------
#  param_card
# --------------------------------------------------------------


mparams = dict()
mparams['dim6'] = dict()
mparams['dim6']['1'] = 1e3
for i in [2,3,10,11,24,25,32,33]:
    mparams['dim6'][str(i)] = 0
mparams['dim6']['2']  = RCtphi
mparams['dim6']['10'] = RCuphi
mparams['dim6']['24'] = RCtcphi
mparams['dim6']['32'] = RCctphi
# not in only-h
#mparams['dim6']['4']  = RCtG 
#mparams['dim6']['12'] = RCuG 
#mparams['dim6']['26'] = RCtcG
#mparams['dim6']['34'] = RCctG
modify_param_card(process_dir='.',params=mparams)


##### Start LHE cooking for MadSpin
fin  = open('PowhegOTF._1.events','r')
fout = open('run.lhe','w')
line = fin.readline()
while line != "-->\n":
  fout.write(line)
  line = fin.readline()
fout.write(line)

# add the process
fout.write('<MG5ProcCard>\n')
fout.write('import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/%s\n'%(model)) 
fout.write('generate p p > t t~ [QCD]  \n')                                             
fout.write('</MG5ProcCard>\n')

# add run parameters
eline = str(PowhegConfig.nEvents)+' = nevents\n'
fout.write('<MGRunCard>\n')
fout.write('#0.01 = req_acc_FO\n') #Need because of a new check introduced in 2.5.0 : if not there, RunCardLO will be considered, which wants a line like that "x = nhel ! x = 0,1"
fout.write(eline)
fout.write('50.0 = bwcutoff\n')
fout.write('</MGRunCard>\n')

# add model parameters
fout.write('<slha>\n')
shutil.copyfileobj(open('Cards/'+paramFileNameN, 'r'), fout)
fout.write('</slha>\n')
fout.write('<montecarlomasses>\n')
fout.write('       1   0.330000E+00\n')
fout.write('       2   0.330000E+00\n')
fout.write('       3   0.500000E+00\n')
fout.write('       4   0.150000E+01\n')
fout.write('       5   0.480000E+01\n')
fout.write('      11   0.510999E-03\n')
fout.write('      13   0.105658E+00\n')
fout.write('      15   0.177682E+01\n')
fout.write('      21   0.000000E+00\n')
fout.write('</montecarlomasses>\n')

# add the events !
line = fin.readline()
while 'LesHouchesEvents' not in line:
  fout.write(line)
  line = fin.readline()
fout.write(line)
fout.close()

#### End of LHE cooking for MadSpin

# run MadSpin
os.system('$MADPATH/MadSpin/madspin < madspin_card.dat')

# This is a Powheg job, so expect lhe file to be called PowhegOTF._1.events
unzip = subprocess.Popen(['gunzip','run_decayed.lhe.gz'])
unzip.wait()
os.system('cp run_decayed.lhe PowhegOTF._1.events')

###################################################################
# Shower the events

evgenConfig.keywords    = [ 'top', 'ttbar', 'FCNC' ]
if keyName != None:
    evgenConfig.keywords += [keyName]
evgenConfig.generators += [ "Powheg", "EvtGen"]

if isPy8:
    #--------------------------------------------------------------
    # Pythia8 showering
    #--------------------------------------------------------------
    # Provide config information
    evgenConfig.generators += [ "Pythia8" ]
    evgenConfig.description = "POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, ME NNPDF30 NLO, A14 NNPDF23 LO, FCNC Top decays"

    include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
    include("Pythia8_i/Pythia8_Powheg_Main31.py")
    
    genSeq.Pythia8.Commands += [ 'POWHEG:pThard = 0' ]
    genSeq.Pythia8.Commands += [ 'POWHEG:nFinal = 2' ]
    genSeq.Pythia8.Commands += [ 'POWHEG:pTdef = 2' ]
    genSeq.Pythia8.Commands += [ 'POWHEG:vetoCount = 3' ]
    genSeq.Pythia8.Commands += [ 'POWHEG:pTemt  = 0' ]
    genSeq.Pythia8.Commands += [ 'POWHEG:emitted = 0' ]
    genSeq.Pythia8.Commands += [ 'POWHEG:MPIveto = 0' ]

else:
    #--------------------------------------------------------------
    # Herwig7 showering
    #--------------------------------------------------------------
    # Provide config information
    evgenConfig.generators += [ "Herwig7" ]
    evgenConfig.tune        = "H7.1-Default"
    evgenConfig.description = "PowhegBox+Herwig7 7.1 ttbar production with Powheg hdamp equal top mass, H7.1-Default tune, with EvtGen, FCNC Top decays"

    # initialize Herwig7 generator configuration for showering of LHE files
    include("Herwig7_i/Herwig7_LHEF.py")
    
    # configure Herwig7
    Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
    Herwig7Config.lhef_powhegbox_commands(lhe_filename='PowhegOTF._1.events', me_pdf_order="NLO")
    Herwig7Config.tune_commands()
    
    #JB probably not needed here ?
    #include("Herwig7_i/Herwig71_AngularShowerScaleVariations.py")

    # add EvtGenHerwig7_i
    include("Herwig7_i/Herwig71_EvtGen.py")

    
