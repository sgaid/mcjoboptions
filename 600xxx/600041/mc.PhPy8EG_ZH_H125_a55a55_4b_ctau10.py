#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

#evgenConfig.inputfilecheck = "TXT"
#evgenConfig.inputfilecheck = "merged_lhef"

import os, sys, glob
for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      25     1'):
            f2.write(line.replace('      25     1','      35     1'))
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )

nProcess = 2 # 0: WpH, 1: WmH, 2: ZH, 3: ggZH
ma = 55.       # 55 GeV
adecay = 5    # 2, 5, 15
ctau = 10.     # 10 mm
width=1.9732699E-13/ctau # width in GeV

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------

if nProcess==0:
    genSeq.Pythia8.Commands += [ 'POWHEG:NFinal = 3']
elif nProcess==1:
    genSeq.Pythia8.Commands += [ 'POWHEG:NFinal = 3']
elif nProcess==2:
    genSeq.Pythia8.Commands += [ 'POWHEG:NFinal = 3']
elif nProcess==3:
    genSeq.Pythia8.Commands += [ 'POWHEG:NFinal = 2']

#--------------------------------------------------------------
# Higgs->bbar at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on',

                            '35:m0 = 125',
                            '35:mWidth = 0.00407',
                            '35:doForceWidth = on',
                            '35:onMode = off',
                            '35:onIfMatch = 36 36', # h->aa

                            '36:oneChannel = 1 1.0 101 %d %d' % (adecay, -adecay), # hardcode decay table, only a>XX; meMode=101 -> do not recompute the width, use user input instead
                            '36:m0 = %.1f' % ma, #scalar mass',
                            '36:mMin = 0',
                            '36:mWidth= %.7g' % width, # width in GeV from nominal proper lifetime (in mm/c) as from above                            
                            ]

genSeq.Pythia8.Commands = [i for i in genSeq.Pythia8.Commands if (("limitTau0" not in i) and ("tau0Max" not in i))]
genSeq.Pythia8.Commands += [
                            'ParticleDecays:tau0Max = 100000.0',
                            'ParticleDecays:limitTau0 = off'
                           ]

testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
if nProcess==0:
    if adecay==5:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->l+vbbbarbbbar production"
        evgenConfig.process = "WpH, H->2a->4b, W->lv"
    elif adecay==15:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->l+vtau+tau-tau+tau- production"
        evgenConfig.process = "WpH, H->2a->4tau, W->lv"
    elif adecay==2:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->l+vuubaruubar production"
        evgenConfig.process = "WpH, H->2a->4u, W->lv"
elif nProcess==1:
    if adecay==5:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->l-vbbbarbbbar production"
        evgenConfig.process = "WmH, H->2a->4b, W->lv"
    elif adecay==15:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->l-vtau+tau-tau+tau- production"
        evgenConfig.process = "WmH, H->2a->4tau, W->lv"
    elif adecay==2:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+W+jet->l-vuubaruubar production"
        evgenConfig.process = "WmH, H->2a->4u, W->lv"
elif nProcess==2:
    if adecay==5:
        evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet->l+l-bbbarbbbar production"
        evgenConfig.process     = "ZH, H->aa->4b, Z->ll"
elif nProcess==3:
    if adecay==5:
        evgenConfig.description = "POWHEG+Pythia8 gg->H+Z->l+l-bbbarbbbar production"
        evgenConfig.process     = "gg->ZH, H->aa->4b, Z->ll"
evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs", "mH125"]
evgenConfig.contact     = [ 'manfredi.ronzani@cern.ch' ]

evgenConfig.nEventsPerJob    = 5000
evgenConfig.inputFilesPerJob = 100
