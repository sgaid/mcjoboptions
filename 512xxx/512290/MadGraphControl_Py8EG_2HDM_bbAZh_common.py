import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

#----------------------------------------------------------------------------
# Process definition
#----------------------------------------------------------------------------
# For the gluon-gluon fusion production use the 4FS
if Decay == "llbb":
    process='''
import model Pseudoscalar_2HDM
define p = g d u s c d~ u~ s~ c~
define j = g d u s c d~ u~ s~ c~
generate p p > h3 b b~, ( h3 > z h1, z > l+ l-, h1 > b b~)
output -f
'''
else:
    process='''
import model Pseudoscalar_2HDM
define p = g d u s c d~ u~ s~ c~
define j = g d u s c d~ u~ s~ c~
generate p p > h3 b b~, ( h3 > z h1, z > vl vl~, h1 > b b~)
output -f
'''


#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else:
  raise RuntimeError('No center of mass energy found.')

#---------------------------------------------------------------------------
# Number of events to generate
#---------------------------------------------------------------------------

nevents=evgenConfig.nEventsPerJob
multiplier=1.1
nevents = runArgs.maxEvents*multiplier if runArgs.maxEvents>0 else nevents*multiplier
nevents = int(nevents)

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = {
    'maxjetflavor'  : 4,
    'asrwgtflavor'  : 4,
    'lhe_version'	: '3.0',
    'cut_decays'	: 'F',
    'nevents'     	: nevents,
}

# Build run_card
process_dir = new_process(process)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------
# MG5 parameter Card
#---------------------------------------------------------------------------

# Build param_card.dat

params={}
## blocks might be modified
dict_blocks={
'mass': ['mt', 'mh2', 'mh3', 'mhc'],
'FRBlock': ['tanbeta', 'sinbma'],
'Higgs': ['lam3', 'laP1', 'laP2', 'sinp'],
}

for bl in dict_blocks.keys():
    for pa in dict_blocks[bl]:
        if pa in THDMparams.keys():
            if bl not in params: params[bl]={}
            params[bl][pa]=THDMparams[pa]

# decay width
THDMparams_decay={
'25': '0.0032',
'35': 'Auto',
'36': str(THDMparams['mh3'] * 0.10),
'37': 'Auto',
}

params['decay']=THDMparams_decay

print('Updating parameters:')
print(params)

modify_param_card(process_dir=process_dir,params=params)

# Build reweight_card.dat
if reweight:
    print(params)
    # Create reweighting card
    reweight_card_loc=process_dir+'/Cards/reweight_card.dat'
    rwcard = open(reweight_card_loc,'w')

    for rw_name in reweights:
        params_rwt = params.copy()

        param_name, value = rw_name.split('_')
        #params_rwt['FRBLOCK']['TANBETA'] = value
        params_rwt['DECAY']['36'] = str(float(value)/100.*THDMparams['mh3'])

        param_card_reweight = process_dir+'/Cards/param_card_reweight.dat'
        shutil.copy(process_dir+'/Cards/param_card.dat', param_card_reweight)
        param_card_rwt_new=process_dir+'/Cards/param_card_rwt_%s.dat' % rw_name
        modify_param_card(param_card_input=param_card_reweight, process_dir=process_dir,params=params_rwt, output_location=param_card_rwt_new)

        rwcard.write('launch --rwgt_info=%s\n' % rw_name)
        rwcard.write('%s\n' % param_card_rwt_new)
    rwcard.close()

print_cards()

#---------------------------------------------------------------------------
# Generate the events
#---------------------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

#---------------------------------------------------------------------------
# Metadata
#---------------------------------------------------------------------------

evgenConfig.process = 'p p > h3 b b~, h3 > z h1'

evgenConfig.description = "2HDM for bbA->Zh(->" + Decay + ") initiated process with mA = " + str(THDMparams['mh3'])

evgenConfig.keywords = ['BSMHiggs','ZHiggs']
evgenConfig.contact = ['tong.qiu@cern.ch']

#---------------------------------------------------------------------------
# Shower
#---------------------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_MadGraph.py')
