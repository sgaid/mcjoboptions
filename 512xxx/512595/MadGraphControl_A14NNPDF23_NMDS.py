evgenConfig.description = "Nonminimal dark sectors"
evgenConfig.process = "Nonminimal dark sectors"
evgenConfig.keywords = ["BSM","exotic","extraDimensions"]
evgenConfig.generators += ["MadGraph"]
evgenConfig.contact = ["Elena Villhauer <elena.michelle.villhauer@cern.ch>"]
evgenConfig.inputfilecheck = "TXT"
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_LHEF.py")
