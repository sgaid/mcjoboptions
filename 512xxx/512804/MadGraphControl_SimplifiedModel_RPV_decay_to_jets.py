from MadGraphControl.MadGraphUtilsHelpers import *
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

keepOutput = False

# mc.MGPy8EG_A14NNPDF23LO_GG_rpv_UDB_1000_squarks.py
# mc.MGPy8EG_A14NNPDF23LO_GG_rpv_UDB_1000_200_viaStop.py
# mc.MGPy8EG_A14NNPDF23LO_SS_rpv_UDB_1000_200_viaN1.py
# mc.MGPy8EG_A14NNPDF23LO_SS_rpv_UDB_1000_200_viaStop.py
# mc.MGPy8EG_A14NNPDF23LO_GG_rpv_UDB_1000_200_viaN1.py

# C1 chargino, N1 neutralino


#Read the filename job options.
tokens = get_physics_short().split('_')

#Recover the masses from the parser.
gen_type = str(tokens[2])
decay_type = str(tokens[4])
m1 = float(tokens[5]) # mass of gluino or squark  
decay_via = str(tokens[-1])
if decay_via != "squarks":
   m2 = float(tokens[6]) # mass of intermediate particles 

if m1 == 100:
  evt_multiplier = 10

#Configure the decays for the given model.
if decay_type == 'ALL':
  decay = """
  #       BR         NDA    ID1    ID2   ID3
    0.05555556        3      2      1     3
    0.05555556        3      2      1     5
    0.05555556        3      2      3     5
    0.05555556        3      4      1     3
    0.05555556        3      4      1     5
    0.05555556        3      4      3     5
    0.05555556        3      6      1     3
    0.05555556        3      6      1     5
    0.05555556        3      6      3     5
    0.05555556        3     -2     -1    -3
    0.05555556        3     -2     -1    -5
    0.05555556        3     -2     -3    -5
    0.05555556        3     -4     -1    -3
    0.05555556        3     -4     -1    -5
    0.05555556        3     -4     -3    -5
    0.05555556        3     -6     -1    -3
    0.05555556        3     -6     -1    -5
    0.05555556        3     -6     -3    -5
  """
elif decay_type == 'UDS':
  decay = """
  #       BR         NDA    ID1    ID2   ID3
    0.50000000E+00    3      2      1     3     # BR( -> u d s )
    0.50000000E+00    3     -2     -1    -3     # BR( -> ~u ~d ~s )
  """
elif decay_type == 'UDB':
  decay = """
  #       BR         NDA    ID1    ID2   ID3
    0.50000000E+00    3      2      1     5     # BR( -> u d b )
    0.50000000E+00    3     -2     -1    -5     # BR( -> ~u ~d ~b )
  """

# generating gluino events

if gen_type == 'GG' and decay_via == 'squarks':
  decays['1000021'] = """DECAY   1000021     1.00000000E+00   # gluino decays
  """ + decay
  masses['1000021'] = m1
# 2 * (2+1)

if gen_type == 'GG' and decay_via == 'viaStop':
  decays['1000021'] = """DECAY   1000021     1.00000000E+00   # gluino decays
  #       BR         NDA       ID1       ID2   
    0.50000000E+00    2      1000005     -5        # BR(go -> sbottom  ~b )
    0.50000000E+00    2     -1000005      5        # BR(go -> ~sbottom b )
  """
  decays['1000005'] = """DECAY   1000005     1.00000000E+00   # sbottom decays
  #       BR         NDA     ID1     ID2   
    1.00000000E+00    2       -2     -1       # BR(sbottom -> ~u ~d)
  """
  masses['1000021'] = m1
  masses['1000005'] = m2

# 2 * (2+5)
# go -> N1 + 4 light quarks (u,d,s,c)

if gen_type == 'GG' and decay_via == 'viaN1':
  decays['1000021'] = """DECAY   1000021     1.00000000E+00   # gluino decays
  #       BR         NDA       ID1       ID2    ID3   
    0.25000000E+00    3      1000022      1      -1     # BR(go -> N1 d ~d)
    0.25000000E+00    3      1000022      2      -2     # BR(go -> N1 u ~u)
    0.25000000E+00    3      1000022      3      -3     # BR(go -> N1 s ~s)
    0.25000000E+00    3      1000022      4      -4     # BR(go -> N1 c ~c)
  """
  
  decays['1000022'] = """DECAY   1000022     1.00000000E+00   # neutralino decays
  """ + decay
  
  masses['1000021'] = m1
  masses['1000022'] = m2

# generating squark events 

# 2 * (2+2)
if gen_type == 'SS' and decay_via == 'viaStop':
  squark_decay = """
  #       BR         NDA       ID1       ID2   
    1.00000000E+00    2      1000006     23        # BR(squark -> Z stop )
  """

  for pdgid in ('1000001','1000002','1000003','1000004'):
    decays[pdgid] = """DECAY   {pdgid}     1.00000000E+00   # squark decays
    """.format(pdgid=pdgid) + squark_decay
    masses[pdgid] = m1

  if decay_type == 'UDS':
    decays['1000006'] = """DECAY   1000006     1.00000000E+00   # stop decays
    #       BR         NDA     ID1     ID2   
      1.00000000E+00    2      -1      -3       # BR(stop -> d s)
    """
  if decay_type == 'UDB':
    decays['1000006'] = """DECAY   1000006     1.00000000E+00   # stop decays
    #       BR         NDA     ID1     ID2   
      1.00000000E+00    2      -1      -5       # BR(stop -> d b)
    """

  masses['1000006'] = m2

# 2 * (1+3)

if gen_type == 'SS' and decay_via == 'viaN1':

  for i in ('1','2','3','4'):
    pdgid = '100000'+i
    decays[pdgid] = """DECAY   {pdgid}     1.00000000E+00   # squark decays
    #       BR         NDA       ID1       ID2   
    1.00000000E+00      2      1000022     {i}      # BR(squark -> N1 quark)
    """.format(pdgid=pdgid, i=i)
    masses[pdgid] = m1

  decays['1000022'] = """DECAY   1000022     1.00000000E+00   # neutralino decays
  """ + decay

  masses['1000022'] = m2



#To perform the matching based on the number of jets in the process above.
njets = 2


#Define the process.
if gen_type == 'GG':
  process = '''
  import model RPVMSSM_UFO
  define susysq = ul ur dl dr cl cr sl sr t1 t2 b1 b2
  define susysq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
  generate p p > go go QED=0 RPV=0 / susysq susysq~ @1
  add process p p > go go j QED=0 RPV=0 / susysq susysq~ @2
  add process p p > go go j j QED=0 RPV=0 / susysq susysq~ @3
'''
elif gen_type == 'SS':
  process = '''
  import model RPVMSSM_UFO
  define lightsq = ul ur dl dr cl cr sl sr
  define lightsq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~
  define heavysq = t1 t2 b1 b2
  define heavysq~ = t1~ t2~ b1~ b2~
  generate p p > lightsq lightsq~ QED=0 RPV=0 / heavysq heavysq~ go @1
  add process p p > lightsq lightsq~ j QED=0 RPV=0 / heavysq heavysq~ go @2
  add process p p > lightsq lightsq~ j j QED=0 RPV=0 / heavysq heavysq~ go @3
  '''
else:
    raise RunTimeError("ERROR: did not recognize generation type")

if njets==1:
    process = '\n'.join([x for x in process.split('\n') if not "j j" in x])

gen_keywords = 'gluino'
decay_keywords = ''
if decay_via != 'squark':
  gen_keywords = ('gluino' if gen_type=='GG' else 'squark')
  decay_keywords = ('decay via stop' if decay_via == 'viaStop' else 'decay via Neutralino')

#Some useful configuration options.
evgenConfig.contact  = ["kbai@cern.ch"]
evgenConfig.keywords += [ 'SUSY', 'RPV', gen_keywords, 'simplifiedModel']
evgenConfig.description = '%s pair production and %s. m_%s = %s GeV'%(gen_keywords, decay_keywords,gen_keywords, m1)

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets > 0:
  genSeq.Pythia8.Commands += ["Merging:Process = guess"]
  genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]
