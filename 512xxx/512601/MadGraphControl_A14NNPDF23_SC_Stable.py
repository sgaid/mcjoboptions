evgenConfig.description = "Scalar Cascades Stable lightest phi"
evgenConfig.process = "Scalar Cascades"
evgenConfig.keywords = ["BSM","exotic","extraDimensions"]
evgenConfig.generators += ["MadGraph"]
evgenConfig.contact = ["Elena Villhauer <elena.michelle.villhauer@cern.ch>"]
evgenConfig.inputfilecheck = "TXT"
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands += [
                            "1000022:spinType = 0",
                            "1000022:mWidth = 0",
                            "1000022:onMode = off",
                            "1000022:mMax = 0",
                            "1000022:colType = 0",
                            "1000022:chargeType = 0",
                            "1000022:mMin = 0",
                            "1000022:mayDecay = off",
                            "1000022:isResonance = false",
                            ]

