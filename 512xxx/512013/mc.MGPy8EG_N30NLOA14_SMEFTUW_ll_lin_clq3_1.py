# definitions that will be used for process
definitions="""define l+ = e+ mu+ ta+ 
define l- = e- mu- ta-
"""

# process (EFTORDER: NP<=1)
processes={'ll' : 'generate p p > l+ l- SMHLOOP==0 EFTORDER',
           'ee' : 'generate p p > e+ e- SMHLOOP==0 EFTORDER',
           'mumu' : 'generate p p > mu+ mu- SMHLOOP==0 EFTORDER',
}

# cuts and other run card settings
settings={'dynamical_scale_choice':'-1',
          'mmll':'110', 
          'etal':'3', 
}

# param card settings 
params={}

# parameters used in reweighting (optional)
relevant_coeffs = 'clq1 clq3 ceu ced clu cld cqe cHDD cHWB cHl1 cHl3 cHe cHq1 cHq3 cHu cHd cll1'.split()

evgenConfig.description = 'high-mass DY production with SMEFTsim'
evgenConfig.keywords+=['SM','drellYan','electroweak']
evgenConfig.generators = ["MadGraph","Pythia8","EvtGen"]
evgenConfig.contact = ['Hannes Mildner <hannes.mildner@cern.ch>']

include("MadGraphControl_SMEFT_v1.py")
