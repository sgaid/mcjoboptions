model="LightVector" 
fs = "mumu" 
mDM1 = 5. 
mDM2 = 50. 
mZp = 20. 
mHD = 125. 
filteff = 1.029866e-01 

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Even S. Haaland <even.simonsen.haaland@cern.ch>"]

include("MadGraphControl_MGPy8EG_mono_zp_lep.py")