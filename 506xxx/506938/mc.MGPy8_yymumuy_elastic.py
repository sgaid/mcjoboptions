
from MadGraphControl.MadGraphUtils import *

import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':244800,
    'pdf_variations': None,
    'alternative_pdfs': None,
    'scale_variations': None,
    'alternative_dynamic_scales' : None,
}


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
nevents=5*runArgs.maxEvents if runArgs.maxEvents>0 else 5500
mode=0

#---------------------------------------------------------------------------------------------------
# Setting parameters for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters = {}

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'fixed_ren_scale':'True',
           'fixed_fac_scale':'True',
           'scale': '10',
           'dsqrt_q2fact1': '10',
           'dsqrt_q2fact2': '10',
        'lpp1'      : '2',
        'lpp2'      : '2',
        'pta'       : '1.5',
        'drll'      : '0.1',
        'dral'      : '0.1',
        'ptl1min'   : '3.5',
        'ptl'       : '-1.0',
        'etaa'       : '-1.0',
        'etal'       : '2.6',
           'nevents':int(nevents)
           }


#---------------------------------------------------------------------------------------------------
# Parameters are set above
#---------------------------------------------------------------------------------------------------
process="""
import model sm
define p = a g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate a a > mu+ mu- a
output -f"""

process_dir = new_process(process)

#---------------------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------------
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------------
modify_param_card(process_dir=process_dir,params=parameters)

#---------------------------------------------------------------------------------------------------
# Printing cards   
#---------------------------------------------------------------------------------------------------
print_cards()

#---------------------------------------------------------------------------------------------------
# Generate events                                                               
#---------------------------------------------------------------------------------------------------     
generate(process_dir=process_dir,runArgs=runArgs)

#---------------------------------------------------------------------------------------------------
# Move output files into the appropriate place, with the appropriate name
#---------------------------------------------------------------------------------------------------   
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=2,saveProcDir=True)

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.description = "elastic gamma gamma to mu mu gamma in pp collisions with loose cuts on muons and gamma"
evgenConfig.keywords = ["lepton"]
evgenConfig.contact = ['Mateusz.Dyndal@cern.ch']

include("Pythia8_i/Py8_NNPDF23_NNLO_as118_QED_EE_Common.py")
include("Pythia8_i/Pythia8_LHEF.py") 
genSeq.Pythia8.Commands += [ 'SpaceShower:QEDshowerByL = 1' ]


