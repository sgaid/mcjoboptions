evgenConfig.contact     = [ 'andrea.helen.knue@cern.ch','ian.connelly@cern.ch']

from MadGraphControl.MadGraphUtils import *
# in release 21.6.49, get_physics_short is in MadGraphUtilsHelpers,
# it is in Generators/MCJobOptionUtils/python/JOsupport.py in more recent release
from MadGraphControl.MadGraphUtilsHelpers import *

import fileinput
JOlog = Logging.logging.getLogger('FCNCJobOption')

nevents=evgenConfig.nEventsPerJob
if runArgs.maxEvents>0: nevents=runArgs.maxEvents
nevents=int(1.1*nevents)

mode=0

gridpack_dir  = 'madevent/'
gridpack_mode = False

JOlog.info(runArgs.jobConfig)

# Get the DSID and the sample short description
shortPhys = get_physics_short()
theOpe = shortPhys[shortPhys.rfind('_')+1:shortPhys.find('QH')]
isPy8  = shortPhys.split('_')[0].find('Py8') >= 0 or shortPhys.split('_')[0].find('Pythia8') >= 0
print 'name = ',shortPhys,' operator = ',theOpe,' isPy8 = ',isPy8

NLO = True
if NLO:
    proc    = 'generate p p > t h $$ t~ [QCD]\nadd process p p > t~ h $$ t [QCD]'
else:
    # this is just for quick checks
    proc    = 'define mt = t t~\ndefine mw = w+ w-\ndefine mb = b b~\ndefine lep = l+ l- ta+ ta- vl vl~\ngenerate p p > mt h'

fcard = open('proc_card_mg5.dat','w')
model   = 'TopFCNC-onlyh'
runName = 'madgraph.'+runArgs.jobConfig[0]+'.MadGraph_mctHWb_NLO5FS'
fcard.write("""import model %s\n%s\noutput -f"""%(model,proc))
fcard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

##Fetch default NLO run_card.dat and set parameters
##nnpdf3.0 nlo as 0.118 5fs
lhaid=260000

reweight_scale = '.true.'
reweight_PDF   = '.true.'

pdflabel='lhapdf'

parton_shower = 'PYTHIA8'
if not isPy8:
    parton_shower = 'HERWIGPP'

muR_over_ref  = 1.0
dyn_scale     = -1
# I think this is the choice of 1412.5594
fixed_ren_scale = '.true.'
fixed_fac_scale = '.true.'
muR_ref_fixed   = 172.5+125.
muF_ref_fixed   = 172.5+125.

lhe_version   = 3

# old way for pdf and scale and variations.
#extras = { 'lhaid'         : lhaid,
#           'pdlabel'       : "'"+pdflabel+"'",
#           'parton_shower' : parton_shower,
#           'reweight_scale': reweight_scale,
#           'reweight_PDF'  : reweight_PDF,
#           'muR_over_ref'  : muR_over_ref,
#           'dynamical_scale_choice' : dyn_scale,
#           'fixed_ren_scale' : fixed_ren_scale,
#           'fixed_fac_scale' : fixed_fac_scale,
#           'muR_ref_fixed' : muR_ref_fixed,  
#           'muF_ref_fixed' : muF_ref_fixed,           
#           'req_acc'       : 0.001
#       }
# new way ?
extras = { 'parton_shower' : parton_shower,
           'dynamical_scale_choice' : dyn_scale,
           'fixed_ren_scale' : fixed_ren_scale,
           'fixed_fac_scale' : fixed_fac_scale,
           'muR_ref_fixed' : muR_ref_fixed,  
           'muF_ref_fixed' : muF_ref_fixed,           
           'req_acc'       : 0.001,
           'nevents'       : nevents
       }
       
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
    
# The couplings. Only real part.
# The tH relevant couplings (c,u) x (LH,RH) x (tqH,tqg)
RCtphi  = 0.
RCuphi  = 0.
RCtcphi = 0.
RCctphi = 0.
RCtG    = 0.
RCuG    = 0.
RCtcG   = 0.
RCctG   = 0.

process = """import model %s\n%s\noutput -f"""%(model,proc)
process_dir = new_process(process)

## For MadSpin
madspin_card_loc = process_dir+'/Cards/madspin_card.dat'
madspin_card_rep = madspin_card_loc
madspin_rep      = 'set ms_dir MadSpin'
madspin_seed     = runArgs.randomSeed
madspin_in       = 'import '+process_dir+'/Events/run_01/events.lhe'

if not NLO:
    madspin_in       = 'import Events/run_01/unweighted_events.lhe'

if hasattr(runArgs, 'inputGenConfFile'):
    madspin_card_rep = gridpack_dir+'Cards/'+madspin_card_loc
    madspin_in       = 'import '+gridpack_dir+'Events/'+runName+'/events.lhe'
    madspin_rep      = 'set ms_dir '+gridpack_dir+'MadSpin'
    madspin_seed     = 10000000+int(runArgs.randomSeed)

mscard = open(madspin_card_rep,'w')
mscard.write("""
set Nevents_for_max_weigth 250 # number of events for the estimate of the max. weight (default: 75)
set max_weight_ps_point 1000  # number of PS to estimate the maximum for each event   (default: 400)
set seed %i
%s
%s
define l+ = l+ ta+
define l- = l- ta-
define All = l+ l- vl vl~ j
\n
"""%(madspin_seed,madspin_in,madspin_rep))

## The W decay
if shortPhys.find('Wln') >= 0:
    wstrm = ', w- > l- vl~'
    wstrp = ', w+ > l+ vl'
elif shortPhys.find('Wqq') >= 0:
    wstrm = ', w- > j j'
    wstrp = ', w+ > j j'
else :
    raise RuntimeError("No W decays are generated please check the job option")

## The top decay (SM)
tstrm = 'decay t~ > w- b~'
tstrp = 'decay t > w+ b'

mscard.write("""%s%s\n"""%(tstrm,wstrm))
mscard.write("""%s%s\nlaunch"""%(tstrp,wstrp))
mscard.close()

## The operator (only t-q-H for the time being, choose between c and u. No color dipole; expect to be small because of limit from pp --> t)
## it seems that both q flavours need to have non zero value ?? 
if  theOpe == 'tphi':
    RCtphi  = 1
    RCtcphi = 1e-5
elif theOpe == 'tcphi':
    RCtcphi = 1
    RCtphi  = 1e-5
elif theOpe == 'uphi':
    RCuphi = 1
    RCctphi  = 1e-5
elif theOpe == 'ctphi':
    RCctphi = 1
    RCuphi  = 1e-5
else :
    raise RuntimeError("Unknow operateur please check the DSID")

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

# --------------------------------------------------------------
#  param_card
# --------------------------------------------------------------
# Set ATLAS SM parameters
params = dict() 
params['mass'] = dict()
params['mass']['6'] = '1.725000e+02'
params['mass']['23'] = '9.118760e+01'
params['mass']['24'] = '8.039900e+01'
params['mass']['25'] = '1.250000e+02'
params['yukawa'] = dict()
params['yukawa']['6'] = '1.725000e+02'
params['DECAY'] = dict()
params['DECAY']['23'] = 'DECAY  23   2.495200e+00'
params['DECAY']['24'] = '''DECAY  24   2.085000e+00
   3.377000e-01   2   -1   2
   3.377000e-01   2   -3   4
   1.082000e-01   2  -11  12
   1.082000e-01   2  -13  14
   1.082000e-01   2  -15  16'''
params['DECAY']['25'] = 'DECAY  25   4.070000e-03' #JB changed the width
modify_param_card(process_dir=process_dir,params=params)

mparams = dict()
mparams['dim6'] = dict()
mparams['dim6']['1'] = 1e3
for i in [2,3,10,11,24,25,32,33]:
    mparams['dim6'][str(i)] = 0
mparams['dim6']['2']  = RCtphi
mparams['dim6']['10'] = RCuphi
mparams['dim6']['24'] = RCtcphi
mparams['dim6']['32'] = RCctphi
# not in only-h
#mparams['dim6']['4']  = RCtG 
#mparams['dim6']['12'] = RCuG 
#mparams['dim6']['26'] = RCtcG
#mparams['dim6']['34'] = RCctG
modify_param_card(process_dir=process_dir,params=mparams)

# --------------------------------------------------------------
# Generate
# --------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

###################################################################
# Shower the events

evgenConfig.keywords    = [ 'top', 'Higgs', 'FCNC' ]
evgenConfig.generators += [ "aMcAtNlo", "EvtGen"]

if isPy8:
    #--------------------------------------------------------------
    # Pythia8 showering
    #--------------------------------------------------------------
    # Provide config information
    evgenConfig.generators += [ "Pythia8" ]
    evgenConfig.description = "aMcAtNlo+Pythia8+EvtGen tH production for FCNC NLO model"

    include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
    include("Pythia8_i/Pythia8_aMcAtNlo.py")

else:
    #--------------------------------------------------------------
    # Herwig7 showering
    #--------------------------------------------------------------
    # Provide config information
    evgenConfig.generators += [ "Herwig7" ]
    evgenConfig.tune        = "H7.1-Default"
    evgenConfig.description = "aMcAtNlo+Herwig7+EvtGen tH production for FCNC NLO mode"

    # initialize Herwig7 generator configuration for showering of LHE files
    include("Herwig7_i/Herwig7_LHEF.py")
    
    # configure Herwig7
    Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
    Herwig7Config.lhef_mg5amc_commands(lhe_filename='tmp_LHE_events.events', me_pdf_order="NLO")
    Herwig7Config.tune_commands()
    
    #JB probably not needed here ?
    #include("Herwig7_i/Herwig71_AngularShowerScaleVariations.py")

    # add EvtGenHerwig7_i
    include("Herwig7_i/Herwig71_EvtGen.py")
