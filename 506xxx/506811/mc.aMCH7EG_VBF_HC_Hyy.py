
from tempfile import mkstemp
from shutil import move
from os import remove, close

def replace(file_path, pattern, subst):
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    close(fh)
    remove(file_path)
    move(abs_path, file_path)


def replaceNextLine(file_path, pattern, strNextLine):
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            found = False
            for line in old_file:
                if found:
                    new_file.write(strNextLine+"\n")
                    found = False
                else:
                    new_file.write(line)
                    if pattern in line:
                        found = True
    close(fh)
    remove(file_path)
    move(abs_path, file_path)


evgenConfig.keywords    = ['VBF', 'Higgs', 'mH125', 'gluonFusionHiggs', 'resonance']
evgenConfig.contact     = ['ana.cueto@cern.ch']
evgenConfig.generators  = ['aMcAtNlo', 'Herwig7', 'EvtGen']
evgenConfig.description = "MG+H7.1 VBF at NLO"
evgenConfig.nEventsPerJob = 10000
evgenConfig.tune = "H7.1-Default"

#import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# General settings
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

#Used for shell commands.
import subprocess

######################################################################
# Configure event generation.
######################################################################

gridpack_mode=True
if not is_gen_from_gridpack():
    process = """
    import model HC_NLO_X0_UFO-no_b_mass
    define p = g d d~ u u~ s s~ c c~ b b~
    define j = g d d~ u u~ s s~ c c~ b b~
    generate p p > x0 j j $$ w+ w- z / a [QCD]
    output -f
    """
    
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION


ptj=10
maxjetflavor=5


#Fetch default run_card.dat and set parameters
settings = {'lhe_version'   :'3.0',
          'reweight_scale':'True',
          'pdlabel'       :"lhapdf",
          'lhaid'         :260000,
          'parton_shower' :"HERWIGPP",
          'ptj'           :str(ptj),
          'maxjetflavor'  :str(maxjetflavor),
          'jetalgo'       :'-1',
          'jetradius'     :'0.4',
          'nevents'       : int(nevents)  
      }


modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)


masses={'25': '1.250000e+02'}
parameters={
        'Lambda': 1000.0,
        'cosa'  : 1.0,
        'kSM'   : 1.0,
        'kHtt'  : 0.0,
        'kAtt'  : 0.0,
        'kHll'  : 0.0,
        'kAll'  : 0.0,
        'kHaa'  : 1.0,
        'kAaa'  : 0.0,
        'kHza'  : 0.0,
        'kAza'  : 0.0,
        'kHzz'  : 0.0,
        'kAzz'  : 0.0,
        'kHww'  : 0.0,
        'kAww'  : 0.0,
        'kHda'  : 0.0,
        'kHdz'  : 0.0,
        'kHdwR' : 0.0,
        'kHdwI' : 0.0
        }


params={}
params['MASS']=masses
params['FRBLOCK']=parameters
modify_param_card(process_dir=process_dir,params=params)

# fix check_poles issue
replace(os.path.join(process_dir,"bin/internal/amcatnlo_run_interface.py"), "tests.append('check_poles')", "pass #tests.append('check_poles')")
replaceNextLine(os.path.join(process_dir,"Cards/FKS_params.dat"), "#IRPoleCheckThreshold", "-1")
replaceNextLine(os.path.join(process_dir,"Cards/FKS_params.dat"), "#PrecisionVirtualAtRunTime", "-1")


madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)

#NOTE: Can't set random seed in due to crashes when using "set spinmode none".
mscard = open(madspin_card,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
 set spinmode none
#
# specify the decay for the final state particles
decay x0 > a a
# running the actual code
launch""")
mscard.close()

print_cards()

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
#arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True,outputDS=runName+'._00001.events.tar.gz')
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)




print "Now performing parton showering ..."
   
######################################################################
# End of event generation, start configuring parton shower here.
######################################################################
'''
if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
else: opts.nprocs = 0
    print opts
'''

#--------------------------------------------------------------
# Herwig 7 showering setup
#--------------------------------------------------------------
include("Herwig7_i/Herwig7_LHEF.py")
# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename="tmp_LHE_events.events", me_pdf_order="NLO")
include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
Herwig7Config.run()

