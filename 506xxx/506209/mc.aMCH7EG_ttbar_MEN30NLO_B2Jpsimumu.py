#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
# Provide config information
evgenConfig.generators  += ["McAtNlo", "Herwig7"]
evgenConfig.tune         = "H7.1-Default"
evgenConfig.description  = 'MG5_aMC@NLO+Herwig7+EvtGen, H7p1 default tune, ME NNPDF 3.0 NLO, with at least one lepton filter, Jpsi->mumu filter from DSID 412121 LHE files'
evgenConfig.keywords     = ['SM', 'top', 'ttbar', 'lepton', 'Jpsi']
evgenConfig.contact      = ['dpanchal@utexas.edu']
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 3

# initialize Herwig7 generator configuration for showering of LHE files                                                                                               
include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7 
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

#include("Herwig7_i/Herwig71_AngularShowerScaleVariations.py")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

# Add custom command to turn off B-mixing (Verify with Herwig experts)
Herwig7Config.add_commands("""
set /Herwig/Particles/B0:DeltaM 0.0
set /Herwig/Particles/Bbar0:DeltaM 0.0
set /Herwig/Particles/B_s0:DeltaM 0.0
set /Herwig/Particles/B_sbar0:DeltaM 0.0
""")

# run Herwig7
Herwig7Config.run()

#--------------------------------------------------------------
# Special decay of B->Jpsi->mumu
#--------------------------------------------------------------
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
evgenConfig.auxfiles += ['B2Jpsimumu.DEC']
genSeq.EvtInclusiveDecay.userDecayFile = 'B2Jpsimumu.DEC'

## NonAllHad filter
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

# Jpsi->mumu filter
include('GeneratorFilters/TTbarWithJpsimumuFilter.py')
filtSeq.TTbarWithJpsimumuFilter.JpsipTMinCut = 5000.
