# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  15:05
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.44118264E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.16537242E+03  # scale for input parameters
    1   -6.08389249E+01  # M_1
    2   -1.21160781E+03  # M_2
    3    1.80220457E+03  # M_3
   11   -2.01708229E+02  # A_t
   12   -2.12569844E+02  # A_b
   13   -5.78172795E+02  # A_tau
   23   -4.62165999E+02  # mu
   25    2.33782995E+01  # tan(beta)
   26    4.70974190E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.02760909E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.26141229E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.81755224E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.16537242E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.16537242E+03  # (SUSY scale)
  1  1     8.39203897E-06   # Y_u(Q)^DRbar
  2  2     4.26315580E-03   # Y_c(Q)^DRbar
  3  3     1.01382571E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.16537242E+03  # (SUSY scale)
  1  1     3.94239974E-04   # Y_d(Q)^DRbar
  2  2     7.49055951E-03   # Y_s(Q)^DRbar
  3  3     3.90962517E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.16537242E+03  # (SUSY scale)
  1  1     6.87977584E-05   # Y_e(Q)^DRbar
  2  2     1.42251945E-02   # Y_mu(Q)^DRbar
  3  3     2.39244369E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.16537242E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -2.01708230E+02   # A_t(Q)^DRbar
Block Ad Q=  4.16537242E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -2.12569844E+02   # A_b(Q)^DRbar
Block Ae Q=  4.16537242E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -5.78172794E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  4.16537242E+03  # soft SUSY breaking masses at Q
   1   -6.08389249E+01  # M_1
   2   -1.21160781E+03  # M_2
   3    1.80220457E+03  # M_3
  21   -2.32745386E+04  # M^2_(H,d)
  22    1.77916632E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.02760909E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.26141229E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.81755224E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22010233E+02  # h0
        35     4.71081166E+02  # H0
        36     4.70974190E+02  # A0
        37     4.81117768E+02  # H+
   1000001     1.01189804E+04  # ~d_L
   2000001     1.00942991E+04  # ~d_R
   1000002     1.01186185E+04  # ~u_L
   2000002     1.00977525E+04  # ~u_R
   1000003     1.01189812E+04  # ~s_L
   2000003     1.00943003E+04  # ~s_R
   1000004     1.01186194E+04  # ~c_L
   2000004     1.00977530E+04  # ~c_R
   1000005     4.06509364E+03  # ~b_1
   2000005     4.86867225E+03  # ~b_2
   1000006     4.06687594E+03  # ~t_1
   2000006     4.26625441E+03  # ~t_2
   1000011     1.00210493E+04  # ~e_L-
   2000011     1.00087331E+04  # ~e_R-
   1000012     1.00202828E+04  # ~nu_eL
   1000013     1.00210528E+04  # ~mu_L-
   2000013     1.00087394E+04  # ~mu_R-
   1000014     1.00202861E+04  # ~nu_muL
   1000015     1.00105004E+04  # ~tau_1-
   2000015     1.00220662E+04  # ~tau_2-
   1000016     1.00212262E+04  # ~nu_tauL
   1000021     2.21356840E+03  # ~g
   1000022     6.01341634E+01  # ~chi_10
   1000023     4.71380237E+02  # ~chi_20
   1000025     4.76304954E+02  # ~chi_30
   1000035     1.29866673E+03  # ~chi_40
   1000024     4.70729540E+02  # ~chi_1+
   1000037     1.29772168E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -4.57967166E-02   # alpha
Block Hmix Q=  4.16537242E+03  # Higgs mixing parameters
   1   -4.62165999E+02  # mu
   2    2.33782995E+01  # tan[beta](Q)
   3    2.42899710E+02  # v(Q)
   4    2.21816688E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.99618615E-01   # Re[R_st(1,1)]
   1  2     2.76156512E-02   # Re[R_st(1,2)]
   2  1    -2.76156512E-02   # Re[R_st(2,1)]
   2  2     9.99618615E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99994620E-01   # Re[R_sb(1,1)]
   1  2     3.28038243E-03   # Re[R_sb(1,2)]
   2  1    -3.28038243E-03   # Re[R_sb(2,1)]
   2  2    -9.99994620E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -7.99513530E-02   # Re[R_sta(1,1)]
   1  2     9.96798767E-01   # Re[R_sta(1,2)]
   2  1    -9.96798767E-01   # Re[R_sta(2,1)]
   2  2    -7.99513530E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.95258466E-01   # Re[N(1,1)]
   1  2    -1.27070845E-03   # Re[N(1,2)]
   1  3    -9.58805161E-02   # Re[N(1,3)]
   1  4     1.63063756E-02   # Re[N(1,4)]
   2  1    -7.94266668E-02   # Re[N(2,1)]
   2  2    -7.14952978E-02   # Re[N(2,2)]
   2  3    -7.04130744E-01   # Re[N(2,3)]
   2  4     7.01982708E-01   # Re[N(2,4)]
   3  1     5.60780174E-02   # Re[N(3,1)]
   3  2    -3.08532417E-02   # Re[N(3,2)]
   3  3     7.02974949E-01   # Re[N(3,3)]
   3  4     7.08328705E-01   # Re[N(3,4)]
   4  1    -2.69193710E-03   # Re[N(4,1)]
   4  2     9.96962830E-01   # Re[N(4,2)]
   4  3    -2.88624777E-02   # Re[N(4,3)]
   4  4     7.22829557E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -4.08163555E-02   # Re[U(1,1)]
   1  2    -9.99166665E-01   # Re[U(1,2)]
   2  1    -9.99166665E-01   # Re[U(2,1)]
   2  2     4.08163555E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     1.02287855E-01   # Re[V(1,1)]
   1  2     9.94754841E-01   # Re[V(1,2)]
   2  1     9.94754841E-01   # Re[V(2,1)]
   2  2    -1.02287855E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02844998E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.90580729E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     6.28136472E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     3.13088214E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.40522578E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.83026433E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     4.05599426E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.00720404E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.04123792E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.05879467E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03647549E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.89010238E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     6.66623797E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     3.51944247E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     7.95154745E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.40562792E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.82800496E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.19650161E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.41440593E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.00634712E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.04094040E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.05706355E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     7.35026927E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     6.78447749E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     8.18596399E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     7.63518926E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.44682272E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.55959327E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     4.93456908E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.51385551E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.30425924E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     4.04844343E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     3.80974222E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.78105734E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.60186899E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.40526750E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.91413829E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.63942032E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     1.15538730E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02426846E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.53858402E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.00473857E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.40566958E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.91159144E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.63866622E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     1.15505720E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02340447E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.82193987E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.00302775E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.51901231E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.24741732E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.44201043E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.06897256E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.79809034E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     8.07161140E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.55687505E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.27623928E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.67135604E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.92255608E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.54789155E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.66006984E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.70659192E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     5.01453488E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.72861262E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.00642685E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.47121839E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.27646406E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.67126194E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.92225098E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.54804091E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.66016210E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.77161312E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     5.01444871E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.77074624E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.00640980E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.47107117E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.95265342E+02   # ~b_1
#    BR                NDA      ID1      ID2
     2.33165102E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.04041190E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.02086187E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.86286880E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.70204100E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.00419939E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     5.37802883E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     2.73650857E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.03113179E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.63756999E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.62746835E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     5.28553759E-02    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.00302840E-04    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     8.83767870E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     7.44833313E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.99866922E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.90162739E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     9.69728148E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.54777493E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.61309939E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.80476906E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.00477702E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.08563076E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.97534261E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.47097009E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.44840594E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.99864008E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.92565297E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     9.69718685E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.54792405E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.61307247E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.82571013E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.00469191E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.09870953E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.97517170E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.47082285E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     2.95096119E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.90392893E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.35025938E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.36711766E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     4.94597547E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.11220264E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     9.76727896E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     5.36754860E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
     1.34837359E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     3.57502468E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.64023694E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.17199915E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.19455627E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     7.09039398E-04    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.36346797E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.42552860E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     4.98347264E-01    2     1000021         6   # BR(~t_2 -> ~g t)
DECAY   1000024     3.22127928E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.96752960E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     3.17629412E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.92883262E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.72316142E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     1.53768659E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.50386265E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     1.42031878E-03    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     1.06160192E-01    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     9.21452019E-02    2          37   1000025   # BR(chi^+_2 -> H^+ chi^0_3)
     1.50179315E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     9.49388312E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     1.48169780E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     9.34003404E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     3.18835496E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.90291412E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     3.16325580E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.84565388E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     3.83715017E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     6.15391678E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     7.00233024E-04    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
DECAY   1000025     3.74480697E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     5.96742193E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     4.02481031E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.24306167E-04    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
DECAY   1000035     2.15370166E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.38280360E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.38280360E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     9.46673333E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     9.46673333E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     2.12468832E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.16265456E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     5.62346408E-04    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     1.33972911E-02    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     1.10768709E-02    2     1000025        36   # BR(chi^0_4 -> chi^0_3 A^0)
     1.12066966E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     9.45302060E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     7.30923170E-04    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     7.06258143E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     8.51554246E-02    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.20649276E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.08959136E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     2.99320216E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     2.99320216E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.78478384E-02   # ~g
#    BR                NDA      ID1      ID2
     6.63609680E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     7.32475941E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     5.74354483E-04    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     5.74354428E-04    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     2.05910631E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     1.69535320E-04    3     1000022         1        -1   # BR(~g -> chi^0_1 d d_bar)
     1.69542355E-04    3     1000022         3        -3   # BR(~g -> chi^0_1 s s_bar)
     5.20521704E-03    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     2.03533692E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     2.62206844E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     2.06906318E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     2.58248146E-02    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.14213415E-04    3     1000035         2        -2   # BR(~g -> chi^0_4 u u_bar)
     1.14210170E-04    3     1000035         4        -4   # BR(~g -> chi^0_4 c c_bar)
     3.45479960E-03    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.14416083E-04    3     1000035         1        -1   # BR(~g -> chi^0_4 d d_bar)
     1.14416096E-04    3     1000035         3        -3   # BR(~g -> chi^0_4 s s_bar)
     6.31809990E-03    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.38832382E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.38832382E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     3.97242104E-03    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     3.97242104E-03    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.77950215E-03   # Gamma(h0)
     2.17987080E-03   2        22        22   # BR(h0 -> photon photon)
     1.16382659E-03   2        22        23   # BR(h0 -> photon Z)
     1.93292612E-02   2        23        23   # BR(h0 -> Z Z)
     1.69592171E-01   2       -24        24   # BR(h0 -> W W)
     6.85789855E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.67368527E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.52373815E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.27632619E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.29609095E-07   2        -2         2   # BR(h0 -> Up up)
     2.51524710E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.60167601E-07   2        -1         1   # BR(h0 -> Down down)
     2.38768914E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.40554234E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     1.93979153E-04   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     4.52689147E+00   # Gamma(HH)
     2.09100006E-07   2        22        22   # BR(HH -> photon photon)
     9.52028684E-08   2        22        23   # BR(HH -> photon Z)
     7.36081617E-05   2        23        23   # BR(HH -> Z Z)
     1.57692962E-04   2       -24        24   # BR(HH -> W W)
     2.06346080E-04   2        21        21   # BR(HH -> gluon gluon)
     9.23956003E-09   2       -11        11   # BR(HH -> Electron electron)
     4.11156127E-04   2       -13        13   # BR(HH -> Muon muon)
     1.18704123E-01   2       -15        15   # BR(HH -> Tau tau)
     5.77830577E-13   2        -2         2   # BR(HH -> Up up)
     1.12030405E-07   2        -4         4   # BR(HH -> Charm charm)
     3.25115877E-03   2        -6         6   # BR(HH -> Top top)
     8.72838155E-07   2        -1         1   # BR(HH -> Down down)
     3.15706565E-04   2        -3         3   # BR(HH -> Strange strange)
     8.74144270E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.94989349E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     7.84746890E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     4.48953230E+00   # Gamma(A0)
     1.83406410E-07   2        22        22   # BR(A0 -> photon photon)
     3.87180644E-08   2        22        23   # BR(A0 -> photon Z)
     2.42642781E-04   2        21        21   # BR(A0 -> gluon gluon)
     9.22214408E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.10381209E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.18487290E-01   2       -15        15   # BR(A0 -> Tau tau)
     4.49615909E-13   2        -2         2   # BR(A0 -> Up up)
     8.72115519E-08   2        -4         4   # BR(A0 -> Charm charm)
     5.61841400E-03   2        -6         6   # BR(A0 -> Top top)
     8.71220630E-07   2        -1         1   # BR(A0 -> Down down)
     3.15121538E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.72632390E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.16573938E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.26831761E-04   2        23        25   # BR(A0 -> Z h0)
     1.06198253E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     3.75814644E+00   # Gamma(Hp)
     1.26165005E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.39394750E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.52567466E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.03961167E-06   2        -1         2   # BR(Hp -> Down up)
     1.71192630E-05   2        -3         2   # BR(Hp -> Strange up)
     1.13490066E-05   2        -5         2   # BR(Hp -> Bottom up)
     5.36517793E-08   2        -1         4   # BR(Hp -> Down charm)
     3.74799777E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.58924554E-03   2        -5         4   # BR(Hp -> Bottom charm)
     5.24833995E-07   2        -1         6   # BR(Hp -> Down top)
     1.18551554E-05   2        -3         6   # BR(Hp -> Strange top)
     8.44717779E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.69286621E-04   2        24        25   # BR(Hp -> W h0)
     3.60861751E-08   2        24        35   # BR(Hp -> W HH)
     3.80461308E-08   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.14758476E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    5.46397303E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    5.46544888E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99729968E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    2.09970815E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.82967588E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.14758476E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    5.46397303E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    5.46544888E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99990709E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    9.29067242E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99990709E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    9.29067242E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25679759E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.92506073E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.17098691E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    9.29067242E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99990709E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    4.17654954E-04   # BR(b -> s gamma)
    2    1.58977756E-06   # BR(b -> s mu+ mu-)
    3    3.52530463E-05   # BR(b -> s nu nu)
    4    2.47688021E-15   # BR(Bd -> e+ e-)
    5    1.05809542E-10   # BR(Bd -> mu+ mu-)
    6    2.21520357E-08   # BR(Bd -> tau+ tau-)
    7    8.38371603E-14   # BR(Bs -> e+ e-)
    8    3.58152179E-09   # BR(Bs -> mu+ mu-)
    9    7.59729920E-07   # BR(Bs -> tau+ tau-)
   10    8.18533168E-05   # BR(B_u -> tau nu)
   11    8.45513425E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42132748E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93120225E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15852656E-03   # epsilon_K
   17    2.28168889E-15   # Delta(M_K)
   18    2.47993499E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29034242E-11   # BR(K^+ -> pi^+ nu nu)
   20    2.40965136E-16   # Delta(g-2)_electron/2
   21    1.03020321E-11   # Delta(g-2)_muon/2
   22    2.91554307E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.43513975E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.83710989E-01   # C7
     0305 4322   00   2    -1.84855243E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.94627658E-01   # C8
     0305 6321   00   2    -1.94471569E-03   # C8'
 03051111 4133   00   0     1.62789476E+00   # C9 e+e-
 03051111 4133   00   2     1.62825864E+00   # C9 e+e-
 03051111 4233   00   2    -4.50631796E-05   # C9' e+e-
 03051111 4137   00   0    -4.45058687E+00   # C10 e+e-
 03051111 4137   00   2    -4.44893891E+00   # C10 e+e-
 03051111 4237   00   2     3.34231535E-04   # C10' e+e-
 03051313 4133   00   0     1.62789476E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62825859E+00   # C9 mu+mu-
 03051313 4233   00   2    -4.50845436E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.45058687E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44893896E+00   # C10 mu+mu-
 03051313 4237   00   2     3.34252855E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50499516E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -7.22544956E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50499516E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -7.22498762E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50499516E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -7.09481217E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85821131E-07   # C7
     0305 4422   00   2     4.99382358E-06   # C7
     0305 4322   00   2    -5.87327537E-08   # C7'
     0305 6421   00   0     3.30481314E-07   # C8
     0305 6421   00   2     1.04991642E-05   # C8
     0305 6321   00   2     1.41688791E-07   # C8'
 03051111 4133   00   2     3.27254038E-07   # C9 e+e-
 03051111 4233   00   2     2.79801850E-06   # C9' e+e-
 03051111 4137   00   2     3.32128535E-07   # C10 e+e-
 03051111 4237   00   2    -2.06623461E-05   # C10' e+e-
 03051313 4133   00   2     3.27253008E-07   # C9 mu+mu-
 03051313 4233   00   2     2.79801822E-06   # C9' mu+mu-
 03051313 4137   00   2     3.32129634E-07   # C10 mu+mu-
 03051313 4237   00   2    -2.06623469E-05   # C10' mu+mu-
 03051212 4137   00   2    -4.87870998E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     4.46675524E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -4.87870524E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     4.46675524E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -4.87737382E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     4.46675523E-06   # C11' nu_3 nu_3
