#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = "MG5_aMC@NLO+Pythia8 tHbj, H->inv production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125"  ]
evgenConfig.contact     = [ 'philipp.mogg@cern.ch']
evgenConfig.process = "tH125+jb, H->inv, t->all"
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 4
evgenConfig.generators       = [ "aMcAtNlo", "Pythia8", "EvtGen" ]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

#--------------------------------------------------------------
# H->ZZ->4v decay
#--------------------------------------------------------------

genSeq.Pythia8.Commands += ['25:onMode = off', # decay of Higgs
                          '25:onIfMatch = 23 23',
                          '23:onMode = off',
                          '23:mMin = 2.0',
                          '23:onIfAny = 12 14 16']

#--------------------------------------------------------------
# Missing Et filter
#--------------------------------------------------------------
include('GeneratorFilters/MissingEtFilter.py')
filtSeq.MissingEtFilter.METCut = 75*GeV
filtSeq.Expression = "MissingEtFilter"
