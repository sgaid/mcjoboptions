# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.03.2021,  13:32
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.66292272E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.63521878E+03  # scale for input parameters
    1    7.78941374E+02  # M_1
    2   -6.37085836E+02  # M_2
    3    3.73429926E+03  # M_3
   11   -2.00563416E+03  # A_t
   12    1.41423345E+03  # A_b
   13   -1.89694352E+03  # A_tau
   23    5.38640943E+02  # mu
   25    3.55790524E+01  # tan(beta)
   26    3.99101775E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.09121542E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.13494391E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.64145205E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.63521878E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.63521878E+03  # (SUSY scale)
  1  1     8.38768319E-06   # Y_u(Q)^DRbar
  2  2     4.26094306E-03   # Y_c(Q)^DRbar
  3  3     1.01329950E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.63521878E+03  # (SUSY scale)
  1  1     5.99675965E-04   # Y_d(Q)^DRbar
  2  2     1.13938433E-02   # Y_s(Q)^DRbar
  3  3     5.94690646E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.63521878E+03  # (SUSY scale)
  1  1     1.04647841E-04   # Y_e(Q)^DRbar
  2  2     2.16378545E-02   # Y_mu(Q)^DRbar
  3  3     3.63913117E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.63521878E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -2.00563499E+03   # A_t(Q)^DRbar
Block Ad Q=  2.63521878E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.41423292E+03   # A_b(Q)^DRbar
Block Ae Q=  2.63521878E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.89694326E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.63521878E+03  # soft SUSY breaking masses at Q
   1    7.78941374E+02  # M_1
   2   -6.37085836E+02  # M_2
   3    3.73429926E+03  # M_3
  21    1.53650536E+07  # M^2_(H,d)
  22   -1.59151789E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.09121542E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.13494391E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.64145205E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.21885311E+02  # h0
        35     3.98980008E+03  # H0
        36     3.99101775E+03  # A0
        37     3.99097459E+03  # H+
   1000001     1.00836880E+04  # ~d_L
   2000001     1.00596316E+04  # ~d_R
   1000002     1.00834055E+04  # ~u_L
   2000002     1.00624227E+04  # ~u_R
   1000003     1.00836964E+04  # ~s_L
   2000003     1.00596456E+04  # ~s_R
   1000004     1.00834138E+04  # ~c_L
   2000004     1.00624247E+04  # ~c_R
   1000005     2.17628458E+03  # ~b_1
   2000005     4.69816850E+03  # ~b_2
   1000006     2.17596874E+03  # ~t_1
   2000006     3.19139603E+03  # ~t_2
   1000011     1.00209678E+04  # ~e_L-
   2000011     1.00088403E+04  # ~e_R-
   1000012     1.00201994E+04  # ~nu_eL
   1000013     1.00210051E+04  # ~mu_L-
   2000013     1.00089107E+04  # ~mu_R-
   1000014     1.00202359E+04  # ~nu_muL
   1000015     1.00283598E+04  # ~tau_1-
   2000015     1.00326264E+04  # ~tau_2-
   1000016     1.00307758E+04  # ~nu_tauL
   1000021     4.14036121E+03  # ~g
   1000022     5.30399238E+02  # ~chi_10
   1000023     5.46519376E+02  # ~chi_20
   1000025     7.12379736E+02  # ~chi_30
   1000035     7.93933197E+02  # ~chi_40
   1000024     5.32078957E+02  # ~chi_1+
   1000037     7.12572985E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.73253942E-02   # alpha
Block Hmix Q=  2.63521878E+03  # Higgs mixing parameters
   1    5.38640943E+02  # mu
   2    3.55790524E+01  # tan[beta](Q)
   3    2.43408177E+02  # v(Q)
   4    1.59282227E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.98564180E-01   # Re[R_st(1,1)]
   1  2     5.35684491E-02   # Re[R_st(1,2)]
   2  1    -5.35684491E-02   # Re[R_st(2,1)]
   2  2     9.98564180E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99997532E-01   # Re[R_sb(1,1)]
   1  2     2.22152337E-03   # Re[R_sb(1,2)]
   2  1    -2.22152337E-03   # Re[R_sb(2,1)]
   2  2     9.99997532E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     5.09710764E-01   # Re[R_sta(1,1)]
   1  2     8.60345824E-01   # Re[R_sta(1,2)]
   2  1    -8.60345824E-01   # Re[R_sta(2,1)]
   2  2     5.09710764E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     2.12231192E-02   # Re[N(1,1)]
   1  2     3.24660272E-01   # Re[N(1,2)]
   1  3    -6.80525719E-01   # Re[N(1,3)]
   1  4    -6.56528775E-01   # Re[N(1,4)]
   2  1     1.30056204E-01   # Re[N(2,1)]
   2  2     4.72828182E-02   # Re[N(2,2)]
   2  3     7.01279442E-01   # Re[N(2,3)]
   2  4    -6.99326006E-01   # Re[N(2,4)]
   3  1    -7.54550548E-03   # Re[N(3,1)]
   3  2     9.44629277E-01   # Re[N(3,2)]
   3  3     1.98308833E-01   # Re[N(3,3)]
   3  4     2.61327765E-01   # Re[N(3,4)]
   4  1     9.91250739E-01   # Re[N(4,1)]
   4  2    -5.96420449E-03   # Re[N(4,2)]
   4  3    -7.59308620E-02   # Re[N(4,3)]
   4  4     1.07800297E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -2.83274278E-01   # Re[U(1,1)]
   1  2     9.59038937E-01   # Re[U(1,2)]
   2  1    -9.59038937E-01   # Re[U(2,1)]
   2  2    -2.83274278E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     3.73182867E-01   # Re[V(1,1)]
   1  2     9.27757807E-01   # Re[V(1,2)]
   2  1     9.27757807E-01   # Re[V(2,1)]
   2  2    -3.73182867E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     4.96656561E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     4.53552124E-04    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.70255501E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.82463784E-01    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.43476078E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     3.45120152E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     4.16625944E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.69240917E-01    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     8.32212347E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     4.90597561E-02    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     5.59799817E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     4.98513471E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.31306876E-03    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.78764554E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.29658311E-04    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     9.78821927E-01    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     1.71034133E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
     1.48548764E-04    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.43569403E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     3.47891028E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.48139794E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.69092241E-01    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     8.31711550E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     4.90280502E-02    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     5.59438053E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.19928145E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.65285540E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.18091538E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     4.58313233E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     3.18880492E-01    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.59817019E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     9.20940879E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.52277516E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     6.23525740E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     8.61725149E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     2.31405683E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.50447279E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     3.24314801E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.69297634E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.43481392E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     3.00075014E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.50536358E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     2.73844489E-01    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     8.70463025E-02    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     8.51343457E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.23816825E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.43574697E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     2.99881108E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.50439083E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     2.73667543E-01    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     8.69900585E-02    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     8.56738713E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.23529978E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.69905624E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     2.53676925E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.27260274E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     2.31504423E-01    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     7.35881492E-02    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.14232755E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.55179720E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.53353299E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.70725347E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.84875787E-03    2     1000035         1   # BR(~d_R -> chi^0_4 d)
     9.89975204E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.83450176E+02   # ~d_L
#    BR                NDA      ID1      ID2
     6.64967442E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.75343487E-02    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     2.13571800E-03    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.03642578E-02    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.18268711E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.05009670E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.53406201E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.93597566E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.84809738E-03    2     1000035         3   # BR(~s_R -> chi^0_4 s)
     9.89883070E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.83480557E+02   # ~s_L
#    BR                NDA      ID1      ID2
     6.66688298E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.75333215E-02    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     2.13584141E-03    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.03683603E-02    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.18264296E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.04975101E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     7.51477385E+01   # ~b_1
#    BR                NDA      ID1      ID2
     9.51674254E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     8.80447065E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     9.72812990E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.05649357E-03    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.54286529E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.61163547E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     8.56077653E+01   # ~b_2
#    BR                NDA      ID1      ID2
     1.74023224E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.85268481E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.47411163E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     3.05682700E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.44640514E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.99931827E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.16103210E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.96228894E-03    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     9.83242431E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.71465490E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     5.70276772E+02   # ~u_R
#    BR                NDA      ID1      ID2
     6.62574896E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     3.82368176E-02    2     1000035         2   # BR(~u_R -> chi^0_4 u)
     9.61080737E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.83443725E+02   # ~u_L
#    BR                NDA      ID1      ID2
     6.96645613E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.20346839E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.72068280E-02    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     1.86559055E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.79871124E-02    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.10677244E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.04976422E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.70284168E+02   # ~c_R
#    BR                NDA      ID1      ID2
     6.65663478E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     3.82364036E-02    2     1000035         4   # BR(~c_R -> chi^0_4 c)
     9.61068566E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.83474060E+02   # ~c_L
#    BR                NDA      ID1      ID2
     6.96844034E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.22924524E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.72046894E-02    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     1.86556994E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.80210866E-02    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.10675440E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.04941849E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     7.58317750E+01   # ~t_1
#    BR                NDA      ID1      ID2
     3.81246612E-03    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     2.30209267E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.74008640E-04    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     2.48794723E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.18594580E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     6.18383828E-03    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.83480801E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.97070478E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.16798390E-02    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.49860647E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.80689964E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.99574477E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.21140556E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     4.70040918E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.63337646E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.54363283E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     6.42104559E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     3.19480575E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     4.56849238E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     3.89026914E-11   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     5.91475212E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     1.80117215E-02    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.97169302E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.93343765E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
DECAY   1000037     3.03403433E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     4.15188781E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.10772988E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.23595142E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.50442698E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     5.37072893E-06   # chi^0_2
#    BR                NDA      ID1      ID2
     1.72248969E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     7.22495018E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     6.65437996E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     9.26378772E-02    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     9.26008773E-02    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     4.08193351E-02    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     2.09056322E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     2.08924609E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     1.75258104E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.23883461E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     4.75844731E-02    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     4.75844731E-02    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     4.58146640E-02    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     4.58146640E-02    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     1.58614252E-02    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     1.58614252E-02    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     1.58572711E-02    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     1.58572711E-02    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
     1.47283043E-02    3     1000024        15       -16   # BR(chi^0_2 -> chi^+_1 tau^- nu_bar_tau)
     1.47283043E-02    3    -1000024       -15        16   # BR(chi^0_2 -> chi^-_1 tau^+ nu_tau)
DECAY   1000025     4.90798964E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.69430543E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.69430543E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     3.83433848E-03    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.40380927E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     8.73309895E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.29548066E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.95478956E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     1.98862913E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.98862913E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.55932597E-01    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     7.29914847E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.70332302E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.68291550E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.73881293E-04    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.73881293E-04    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.68244619E+02   # ~g
#    BR                NDA      ID1      ID2
     6.03513952E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     6.03513952E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.88101270E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.88101270E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     6.05266116E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     6.05266116E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.90761133E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.90761133E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.02024371E-03   # Gamma(h0)
     2.70845129E-03   2        22        22   # BR(h0 -> photon photon)
     1.43736907E-03   2        22        23   # BR(h0 -> photon Z)
     2.37923950E-02   2        23        23   # BR(h0 -> Z Z)
     2.09279520E-01   2       -24        24   # BR(h0 -> W W)
     8.68039997E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.19592958E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.31122479E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.66366642E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.48554461E-07   2        -2         2   # BR(h0 -> Up up)
     2.88311434E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.04090951E-07   2        -1         1   # BR(h0 -> Down down)
     2.18484857E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.80060092E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     9.61853929E+01   # Gamma(HH)
     3.43731574E-08   2        22        22   # BR(HH -> photon photon)
     9.21011214E-08   2        22        23   # BR(HH -> photon Z)
     1.02261855E-07   2        23        23   # BR(HH -> Z Z)
     2.16637099E-08   2       -24        24   # BR(HH -> W W)
     3.41755964E-06   2        21        21   # BR(HH -> gluon gluon)
     6.90552745E-09   2       -11        11   # BR(HH -> Electron electron)
     3.07491495E-04   2       -13        13   # BR(HH -> Muon muon)
     8.88112443E-02   2       -15        15   # BR(HH -> Tau tau)
     6.50648731E-14   2        -2         2   # BR(HH -> Up up)
     1.26224865E-08   2        -4         4   # BR(HH -> Charm charm)
     7.72956601E-04   2        -6         6   # BR(HH -> Top top)
     4.79905948E-07   2        -1         1   # BR(HH -> Down down)
     1.73584157E-04   2        -3         3   # BR(HH -> Strange strange)
     4.02639260E-01   2        -5         5   # BR(HH -> Bottom bottom)
     5.69541257E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.10213368E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.10213368E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     3.20127250E-02   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     2.17424600E-02   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.29055775E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     4.01068436E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.80646126E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     5.32756078E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     6.23624994E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.89427751E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.65650542E-02   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     6.76281251E-03   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     3.91418633E-04   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     3.80032917E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     9.32944821E+01   # Gamma(A0)
     2.13204188E-09   2        22        22   # BR(A0 -> photon photon)
     3.50611952E-08   2        22        23   # BR(A0 -> photon Z)
     7.94676173E-06   2        21        21   # BR(A0 -> gluon gluon)
     6.68340270E-09   2       -11        11   # BR(A0 -> Electron electron)
     2.97605314E-04   2       -13        13   # BR(A0 -> Muon muon)
     8.59559487E-02   2       -15        15   # BR(A0 -> Tau tau)
     5.84298574E-14   2        -2         2   # BR(A0 -> Up up)
     1.13349742E-08   2        -4         4   # BR(A0 -> Charm charm)
     6.93012848E-04   2        -6         6   # BR(A0 -> Top top)
     4.64454346E-07   2        -1         1   # BR(A0 -> Down down)
     1.67996049E-04   2        -3         3   # BR(A0 -> Strange strange)
     3.89672019E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     5.79051005E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.14495799E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.14495799E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     3.22028572E-02   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     2.16466099E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.36809345E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     4.10387948E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.47302609E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     6.60733235E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     6.54141118E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     2.43736879E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.67648517E-02   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     5.83198985E-03   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     5.57858606E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     2.21894002E-07   2        23        25   # BR(A0 -> Z h0)
     3.15276502E-14   2        23        35   # BR(A0 -> Z HH)
     4.17519595E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.10811410E+02   # Gamma(Hp)
     7.99117533E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.41647704E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     9.66374277E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.82026262E-07   2        -1         2   # BR(Hp -> Down up)
     8.16270344E-06   2        -3         2   # BR(Hp -> Strange up)
     4.88382265E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.31949008E-08   2        -1         4   # BR(Hp -> Down charm)
     1.73736323E-04   2        -3         4   # BR(Hp -> Strange charm)
     6.83910031E-04   2        -5         4   # BR(Hp -> Bottom charm)
     6.71983361E-08   2        -1         6   # BR(Hp -> Down top)
     1.72393652E-06   2        -3         6   # BR(Hp -> Strange top)
     4.61991754E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.85517268E-04   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.40331666E-01   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     3.02825593E-02   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.03715831E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.28045500E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     2.44437163E-06   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     2.67688679E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.07236004E-02   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.86876638E-07   2        24        25   # BR(Hp -> W h0)
     2.74626746E-14   2        24        35   # BR(Hp -> W HH)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.45706720E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.26592326E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.26586897E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00004289E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    7.47081051E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    7.89971177E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.45706720E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.26592326E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.26586897E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999401E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    5.98509633E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999401E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    5.98509633E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26391503E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    4.59233807E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.09054249E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    5.98509633E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999401E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.11906927E-04   # BR(b -> s gamma)
    2    1.59067085E-06   # BR(b -> s mu+ mu-)
    3    3.52342120E-05   # BR(b -> s nu nu)
    4    2.67905319E-15   # BR(Bd -> e+ e-)
    5    1.14445729E-10   # BR(Bd -> mu+ mu-)
    6    2.39358063E-08   # BR(Bd -> tau+ tau-)
    7    8.99175163E-14   # BR(Bs -> e+ e-)
    8    3.84126120E-09   # BR(Bs -> mu+ mu-)
    9    8.14042499E-07   # BR(Bs -> tau+ tau-)
   10    9.63627378E-05   # BR(B_u -> tau nu)
   11    9.95390190E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42042770E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93648926E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15762564E-03   # epsilon_K
   17    2.28166169E-15   # Delta(M_K)
   18    2.47897587E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28799165E-11   # BR(K^+ -> pi^+ nu nu)
   20   -3.87445079E-16   # Delta(g-2)_electron/2
   21   -1.65646385E-11   # Delta(g-2)_muon/2
   22   -4.69775801E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.12349394E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.77867090E-01   # C7
     0305 4322   00   2    -4.02517023E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -9.39423065E-02   # C8
     0305 6321   00   2    -9.53030433E-05   # C8'
 03051111 4133   00   0     1.61220257E+00   # C9 e+e-
 03051111 4133   00   2     1.61260588E+00   # C9 e+e-
 03051111 4233   00   2     3.20766914E-04   # C9' e+e-
 03051111 4137   00   0    -4.43489467E+00   # C10 e+e-
 03051111 4137   00   2    -4.43150698E+00   # C10 e+e-
 03051111 4237   00   2    -2.42359212E-03   # C10' e+e-
 03051313 4133   00   0     1.61220257E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61260579E+00   # C9 mu+mu-
 03051313 4233   00   2     3.20766754E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.43489467E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43150708E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.42359230E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50461689E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     5.25783303E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50461689E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     5.25783318E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50461689E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     5.25787807E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85835887E-07   # C7
     0305 4422   00   2    -1.29350076E-05   # C7
     0305 4322   00   2     7.30608571E-07   # C7'
     0305 6421   00   0     3.30493954E-07   # C8
     0305 6421   00   2    -6.91047606E-06   # C8
     0305 6321   00   2     2.02757675E-07   # C8'
 03051111 4133   00   2     5.18699524E-07   # C9 e+e-
 03051111 4233   00   2     6.10155491E-06   # C9' e+e-
 03051111 4137   00   2     2.15484727E-06   # C10 e+e-
 03051111 4237   00   2    -4.61035033E-05   # C10' e+e-
 03051313 4133   00   2     5.18696507E-07   # C9 mu+mu-
 03051313 4233   00   2     6.10155339E-06   # C9' mu+mu-
 03051313 4137   00   2     2.15485058E-06   # C10 mu+mu-
 03051313 4237   00   2    -4.61035079E-05   # C10' mu+mu-
 03051212 4137   00   2    -4.36863608E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.00018700E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -4.36863451E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.00018700E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -4.36819030E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.00018700E-05   # C11' nu_3 nu_3
