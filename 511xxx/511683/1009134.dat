# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 16.03.2021,  05:18
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.90402563E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.89860652E+03  # scale for input parameters
    1   -5.17899390E+02  # M_1
    2   -1.76659640E+03  # M_2
    3    1.04839359E+03  # M_3
   11    1.93967321E+03  # A_t
   12   -1.54553647E+03  # A_b
   13    1.86788535E+02  # A_tau
   23   -1.65903977E+02  # mu
   25    2.79230392E+01  # tan(beta)
   26    7.11758584E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.28760464E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.60267161E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.46119193E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.89860652E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.89860652E+03  # (SUSY scale)
  1  1     8.38974710E-06   # Y_u(Q)^DRbar
  2  2     4.26199153E-03   # Y_c(Q)^DRbar
  3  3     1.01354884E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.89860652E+03  # (SUSY scale)
  1  1     4.70751598E-04   # Y_d(Q)^DRbar
  2  2     8.94428037E-03   # Y_s(Q)^DRbar
  3  3     4.66838073E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.89860652E+03  # (SUSY scale)
  1  1     8.21495963E-05   # Y_e(Q)^DRbar
  2  2     1.69859311E-02   # Y_mu(Q)^DRbar
  3  3     2.85675418E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.89860652E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     1.93967328E+03   # A_t(Q)^DRbar
Block Ad Q=  3.89860652E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.54553637E+03   # A_b(Q)^DRbar
Block Ae Q=  3.89860652E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.86788530E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.89860652E+03  # soft SUSY breaking masses at Q
   1   -5.17899390E+02  # M_1
   2   -1.76659640E+03  # M_2
   3    1.04839359E+03  # M_3
  21    4.05207121E+05  # M^2_(H,d)
  22    2.57816172E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.28760464E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.60267161E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.46119193E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22214584E+02  # h0
        35     7.11788829E+02  # H0
        36     7.11758584E+02  # A0
        37     7.19557565E+02  # H+
   1000001     1.01228413E+04  # ~d_L
   2000001     1.00986526E+04  # ~d_R
   1000002     1.01224783E+04  # ~u_L
   2000002     1.01021159E+04  # ~u_R
   1000003     1.01228430E+04  # ~s_L
   2000003     1.00986552E+04  # ~s_R
   1000004     1.01224799E+04  # ~c_L
   2000004     1.01021165E+04  # ~c_R
   1000005     3.30721547E+03  # ~b_1
   2000005     3.49148229E+03  # ~b_2
   1000006     3.30858849E+03  # ~t_1
   2000006     4.59384201E+03  # ~t_2
   1000011     1.00204706E+04  # ~e_L-
   2000011     1.00086898E+04  # ~e_R-
   1000012     1.00197029E+04  # ~nu_eL
   1000013     1.00204776E+04  # ~mu_L-
   2000013     1.00087033E+04  # ~mu_R-
   1000014     1.00197098E+04  # ~nu_muL
   1000015     1.00125151E+04  # ~tau_1-
   2000015     1.00224589E+04  # ~tau_2-
   1000016     1.00216733E+04  # ~nu_tauL
   1000021     1.34953012E+03  # ~g
   1000022     1.79218996E+02  # ~chi_10
   1000023     1.87019774E+02  # ~chi_20
   1000025     5.24820807E+02  # ~chi_30
   1000035     1.86813692E+03  # ~chi_40
   1000024     1.83832331E+02  # ~chi_1+
   1000037     1.86810144E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.60699753E-02   # alpha
Block Hmix Q=  3.89860652E+03  # Higgs mixing parameters
   1   -1.65903977E+02  # mu
   2    2.79230392E+01  # tan[beta](Q)
   3    2.42962463E+02  # v(Q)
   4    5.06600282E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.99682468E-01   # Re[R_st(1,1)]
   1  2     2.51984686E-02   # Re[R_st(1,2)]
   2  1    -2.51984686E-02   # Re[R_st(2,1)]
   2  2    -9.99682468E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99993030E-01   # Re[R_sb(1,1)]
   1  2     3.73349752E-03   # Re[R_sb(1,2)]
   2  1    -3.73349752E-03   # Re[R_sb(2,1)]
   2  2    -9.99993030E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -4.20084221E-02   # Re[R_sta(1,1)]
   1  2     9.99117257E-01   # Re[R_sta(1,2)]
   2  1    -9.99117257E-01   # Re[R_sta(2,1)]
   2  2    -4.20084221E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.31857067E-02   # Re[N(1,1)]
   1  2    -3.41480326E-02   # Re[N(1,2)]
   1  3    -7.12610500E-01   # Re[N(1,3)]
   1  4     6.94504580E-01   # Re[N(1,4)]
   2  1    -4.28530735E-02   # Re[N(2,1)]
   2  2     2.70379991E-02   # Re[N(2,2)]
   2  3    -7.00583760E-01   # Re[N(2,3)]
   2  4    -7.11768892E-01   # Re[N(2,4)]
   3  1     9.94725010E-01   # Re[N(3,1)]
   3  2     5.85106645E-03   # Re[N(3,2)]
   3  3     3.65675296E-02   # Re[N(3,3)]
   3  4    -9.56594773E-02   # Re[N(3,4)]
   4  1    -1.48086296E-03   # Re[N(4,1)]
   4  2     9.99033845E-01   # Re[N(4,2)]
   4  3    -5.61124390E-03   # Re[N(4,3)]
   4  4     4.35625699E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -7.93565656E-03   # Re[U(1,1)]
   1  2    -9.99968512E-01   # Re[U(1,2)]
   2  1    -9.99968512E-01   # Re[U(2,1)]
   2  2     7.93565656E-03   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     6.16164668E-02   # Re[V(1,1)]
   1  2     9.98099900E-01   # Re[V(1,2)]
   2  1     9.98099900E-01   # Re[V(2,1)]
   2  2    -6.16164668E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.00163935E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     8.72549532E-03    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.84515595E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.89427268E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.35806827E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.32271903E-02    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01519494E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.05127528E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.01312801E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.28707121E-03    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     2.40299145E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     9.87162650E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.14513703E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.35864411E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     2.94133725E-04    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.13212611E-04    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     9.31883065E-02    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01391952E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.04871536E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     8.26512750E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.06713266E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     9.66334360E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     5.97937070E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     7.38442778E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.96497407E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.48037884E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.51973285E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     5.35299909E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     5.30404046E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     8.41806991E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.69110682E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.40078905E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.35810541E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     2.28601232E-03    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     8.09025705E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     8.92008349E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02441285E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.46409483E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02798747E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.35868120E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     2.28504513E-03    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     8.08683413E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     8.91630954E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02313355E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     2.88602826E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.02543793E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.52100811E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     2.04157822E-03    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     7.22519857E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     7.96631007E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.70110037E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.09097260E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.38365504E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.74512179E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.16397682E-03    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.92759363E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.97008382E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.27837054E-04    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.44944096E-03    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     4.62808466E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     9.26862817E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.59389656E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.74544427E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.16370786E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.92718303E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.97028255E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.36923898E-04    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.44943289E-03    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     4.62798305E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     9.26842606E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.59370771E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.83020188E+02   # ~b_1
#    BR                NDA      ID1      ID2
     2.57322547E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.47562244E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.49114717E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.38129082E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.35125667E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     4.83148825E-02    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     6.40766916E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     2.31298266E+02   # ~b_2
#    BR                NDA      ID1      ID2
     3.31045368E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     3.19714371E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     8.05817494E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     6.47632600E-02    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     8.62089337E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     7.91626352E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.47308638E-04    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     2.80461140E-02    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.71654222E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.96997882E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.65421858E-03    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     4.62306572E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     3.76957971E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.23375077E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.59366668E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.91633665E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.49529483E-04    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.80458964E-02    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.71645269E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.97017712E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.65421956E-03    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     4.62296469E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     3.94846895E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.23354843E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.59347782E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     2.85324108E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.13393539E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.18903889E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     3.47319629E-03    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.30452045E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     5.09425046E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     4.69375782E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     4.31826481E-03    2     1000021         4   # BR(~t_1 -> ~g c)
     6.29423080E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
     9.56250760E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     5.09611076E+02   # ~t_2
#    BR                NDA      ID1      ID2
     8.82710419E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     9.26397420E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.10849054E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     3.89338961E-04    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.82477019E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     7.82867308E-04    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.92724709E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     9.13139833E-03    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     4.54272796E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     7.88120580E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     7.15453444E-09   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.96189003E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     2.75143925E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.32063549E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.31718943E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     6.48845802E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     2.88814667E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.37997331E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     1.45279912E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     4.16913166E-03    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     1.06018516E-01    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     9.91963599E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     3.50985268E-04    2          37   1000025   # BR(chi^+_2 -> H^+ chi^0_3)
     1.43336987E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.01532187E-01    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     1.42509535E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     1.01484895E-01    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     7.45034710E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     5.04183716E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     5.48262598E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     1.35703881E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.25819485E-07   # chi^0_2
#    BR                NDA      ID1      ID2
     4.04033973E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     8.83527718E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     6.18147897E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.13285913E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.13090564E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     2.55654065E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     2.54960246E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     1.15844974E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.51495903E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     1.20123341E-03    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     1.20123341E-03    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     5.56352005E-04    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     5.56352005E-04    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     4.00411228E-04    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     4.00411228E-04    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     3.98224786E-04    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     3.98224786E-04    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
DECAY   1000025     1.53253695E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.37508257E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.37508257E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     3.86380806E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.01760975E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     1.46645646E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.36514193E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.88827406E-04    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     6.88827406E-04    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     2.98569451E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.39298318E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.39298318E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.00735418E-01    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     1.00735418E-01    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     4.98790383E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     8.81060425E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     9.15514462E-04    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     4.22725463E-02    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     3.50742609E-02    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     8.33288491E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     7.65164366E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     2.96692869E-03    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     5.77522577E-02    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     6.50147021E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     1.90412840E-04    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     3.26529231E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     3.39654779E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     5.53072469E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     5.53072469E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.52513367E-03   # ~g
#    BR                NDA      ID1      ID2
     1.28103462E-02    2     1000022        21   # BR(~g -> chi^0_1 g)
     1.54439292E-02    2     1000023        21   # BR(~g -> chi^0_2 g)
     2.53649784E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     1.65289419E-01    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     6.99482456E-02    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.70119075E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     6.65229027E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.52172858E-04    3     1000025         2        -2   # BR(~g -> chi^0_3 u u_bar)
     1.52166517E-04    3     1000025         4        -4   # BR(~g -> chi^0_3 c c_bar)
     2.48862061E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     3.78033920E-03    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     2.46464077E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.46464077E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.37538552E-03   # Gamma(h0)
     2.45744793E-03   2        22        22   # BR(h0 -> photon photon)
     1.32927685E-03   2        22        23   # BR(h0 -> photon Z)
     2.22353673E-02   2        23        23   # BR(h0 -> Z Z)
     1.94284047E-01   2       -24        24   # BR(h0 -> W W)
     7.77741831E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.39219410E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.39852799E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.91537924E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.40547688E-07   2        -2         2   # BR(h0 -> Up up)
     2.72761436E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.26748296E-07   2        -1         1   # BR(h0 -> Down down)
     2.26680900E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.05022435E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     8.69357174E+00   # Gamma(HH)
     1.12804255E-07   2        22        22   # BR(HH -> photon photon)
     7.27128055E-08   2        22        23   # BR(HH -> photon Z)
     1.96775602E-05   2        23        23   # BR(HH -> Z Z)
     3.59354478E-05   2       -24        24   # BR(HH -> W W)
     9.76586066E-05   2        21        21   # BR(HH -> gluon gluon)
     9.76846432E-09   2       -11        11   # BR(HH -> Electron electron)
     4.34746382E-04   2       -13        13   # BR(HH -> Muon muon)
     1.25528333E-01   2       -15        15   # BR(HH -> Tau tau)
     2.65382694E-13   2        -2         2   # BR(HH -> Up up)
     5.14609371E-08   2        -4         4   # BR(HH -> Charm charm)
     2.82717415E-03   2        -6         6   # BR(HH -> Top top)
     8.66306661E-07   2        -1         1   # BR(HH -> Down down)
     3.13347696E-04   2        -3         3   # BR(HH -> Strange strange)
     8.57647283E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.94285145E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.52720148E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.73131169E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     8.39853005E-04   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     6.42055419E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     7.80853846E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.61100827E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     8.56813806E+00   # Gamma(A0)
     2.81129974E-07   2        22        22   # BR(A0 -> photon photon)
     1.25498955E-07   2        22        23   # BR(A0 -> photon Z)
     1.55181699E-04   2        21        21   # BR(A0 -> gluon gluon)
     9.74057781E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.33505363E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.25173236E-01   2       -15        15   # BR(A0 -> Tau tau)
     2.40113434E-13   2        -2         2   # BR(A0 -> Up up)
     4.65621513E-08   2        -4         4   # BR(A0 -> Charm charm)
     3.38871238E-03   2        -6         6   # BR(A0 -> Top top)
     8.63838729E-07   2        -1         1   # BR(A0 -> Down down)
     3.12455080E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.55252189E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.55931050E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.23553545E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     7.23279422E-05   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     9.12232491E-03   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     7.28029913E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     5.33369103E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     3.24963772E-05   2        23        25   # BR(A0 -> Z h0)
     1.79115203E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     8.64366399E+00   # Gamma(Hp)
     1.16099199E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.96360315E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.40397314E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.96759899E-07   2        -1         2   # BR(Hp -> Down up)
     1.48590852E-05   2        -3         2   # BR(Hp -> Strange up)
     9.79937269E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.38528670E-08   2        -1         4   # BR(Hp -> Down charm)
     3.23245132E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.37225480E-03   2        -5         4   # BR(Hp -> Bottom charm)
     2.68208952E-07   2        -1         6   # BR(Hp -> Down top)
     6.26730113E-06   2        -3         6   # BR(Hp -> Strange top)
     8.45896641E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.01096526E-04   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     5.33017739E-05   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.12938398E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     3.37918387E-05   2        24        25   # BR(Hp -> W h0)
     4.41065708E-09   2        24        35   # BR(Hp -> W HH)
     4.49710050E-09   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.01527889E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    7.79680839E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    7.79696118E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99980404E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.30214691E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.28255095E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.01527889E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    7.79680839E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    7.79696118E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999926E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    7.42854179E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999926E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    7.42854179E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26440896E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.60579460E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.28893468E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    7.42854179E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999926E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.72840557E-04   # BR(b -> s gamma)
    2    1.58731897E-06   # BR(b -> s mu+ mu-)
    3    3.52548891E-05   # BR(b -> s nu nu)
    4    2.92093404E-15   # BR(Bd -> e+ e-)
    5    1.24778378E-10   # BR(Bd -> mu+ mu-)
    6    2.60855937E-08   # BR(Bd -> tau+ tau-)
    7    9.95984919E-14   # BR(Bs -> e+ e-)
    8    4.25482210E-09   # BR(Bs -> mu+ mu-)
    9    9.01164126E-07   # BR(Bs -> tau+ tau-)
   10    8.74611457E-05   # BR(B_u -> tau nu)
   11    9.03440151E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42001289E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93250838E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15775809E-03   # epsilon_K
   17    2.28167406E-15   # Delta(M_K)
   18    2.47993099E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29038230E-11   # BR(K^+ -> pi^+ nu nu)
   20    2.94625637E-16   # Delta(g-2)_electron/2
   21    1.25961890E-11   # Delta(g-2)_muon/2
   22    3.56463701E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.26425471E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.40669930E-01   # C7
     0305 4322   00   2    -1.09002932E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.54882661E-01   # C8
     0305 6321   00   2    -1.21333527E-03   # C8'
 03051111 4133   00   0     1.62571837E+00   # C9 e+e-
 03051111 4133   00   2     1.62615629E+00   # C9 e+e-
 03051111 4233   00   2     8.80541843E-05   # C9' e+e-
 03051111 4137   00   0    -4.44841047E+00   # C10 e+e-
 03051111 4137   00   2    -4.44698470E+00   # C10 e+e-
 03051111 4237   00   2    -6.50414587E-04   # C10' e+e-
 03051313 4133   00   0     1.62571837E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62615623E+00   # C9 mu+mu-
 03051313 4233   00   2     8.80424991E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44841047E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44698475E+00   # C10 mu+mu-
 03051313 4237   00   2    -6.50403052E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50504300E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.40673941E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50504300E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.40676461E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50504300E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.41386612E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85822859E-07   # C7
     0305 4422   00   2     5.59991933E-06   # C7
     0305 4322   00   2     4.33830736E-07   # C7'
     0305 6421   00   0     3.30482794E-07   # C8
     0305 6421   00   2     3.48478028E-05   # C8
     0305 6321   00   2     7.26225775E-07   # C8'
 03051111 4133   00   2     5.28681964E-07   # C9 e+e-
 03051111 4233   00   2     4.68316735E-06   # C9' e+e-
 03051111 4137   00   2    -6.68701643E-07   # C10 e+e-
 03051111 4237   00   2    -3.46809888E-05   # C10' e+e-
 03051313 4133   00   2     5.28680510E-07   # C9 mu+mu-
 03051313 4233   00   2     4.68316665E-06   # C9' mu+mu-
 03051313 4137   00   2    -6.68700178E-07   # C10 mu+mu-
 03051313 4237   00   2    -3.46809909E-05   # C10' mu+mu-
 03051212 4137   00   2     1.66856229E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     7.50095624E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     1.66856278E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     7.50095624E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     1.66869932E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     7.50095622E-06   # C11' nu_3 nu_3
