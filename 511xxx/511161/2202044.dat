# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 08.09.2021,  05:05
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.73685226E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.11352246E+03  # scale for input parameters
    1    4.79331787E+01  # M_1
    2   -1.07336424E+03  # M_2
    3    2.42273494E+03  # M_3
   11   -6.91629197E+03  # A_t
   12   -1.49441197E+03  # A_b
   13    6.21340860E+02  # A_tau
   23    1.14988469E+02  # mu
   25    4.66468680E+01  # tan(beta)
   26    4.81530582E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.00470615E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.33703656E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.64539508E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.11352246E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.11352246E+03  # (SUSY scale)
  1  1     8.38629853E-06   # Y_u(Q)^DRbar
  2  2     4.26023965E-03   # Y_c(Q)^DRbar
  3  3     1.01313222E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.11352246E+03  # (SUSY scale)
  1  1     7.86091418E-04   # Y_d(Q)^DRbar
  2  2     1.49357369E-02   # Y_s(Q)^DRbar
  3  3     7.79556361E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.11352246E+03  # (SUSY scale)
  1  1     1.37178701E-04   # Y_e(Q)^DRbar
  2  2     2.83642046E-02   # Y_mu(Q)^DRbar
  3  3     4.77039260E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.11352246E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -6.91629220E+03   # A_t(Q)^DRbar
Block Ad Q=  3.11352246E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.49441192E+03   # A_b(Q)^DRbar
Block Ae Q=  3.11352246E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     6.21340863E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.11352246E+03  # soft SUSY breaking masses at Q
   1    4.79331787E+01  # M_1
   2   -1.07336424E+03  # M_2
   3    2.42273494E+03  # M_3
  21    2.30596381E+07  # M^2_(H,d)
  22    1.88183862E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.00470615E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.33703656E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.64539508E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.28414867E+02  # h0
        35     4.81334510E+03  # H0
        36     4.81530582E+03  # A0
        37     4.81344197E+03  # H+
   1000001     1.01104816E+04  # ~d_L
   2000001     1.00860452E+04  # ~d_R
   1000002     1.01101227E+04  # ~u_L
   2000002     1.00892329E+04  # ~u_R
   1000003     1.01104901E+04  # ~s_L
   2000003     1.00860606E+04  # ~s_R
   1000004     1.01101312E+04  # ~c_L
   2000004     1.00892343E+04  # ~c_R
   1000005     3.01572697E+03  # ~b_1
   2000005     4.68370144E+03  # ~b_2
   1000006     2.93861979E+03  # ~t_1
   2000006     3.29883510E+03  # ~t_2
   1000011     1.00208239E+04  # ~e_L-
   2000011     1.00089163E+04  # ~e_R-
   1000012     1.00200624E+04  # ~nu_eL
   1000013     1.00208646E+04  # ~mu_L-
   2000013     1.00089955E+04  # ~mu_R-
   1000014     1.00201029E+04  # ~nu_muL
   1000015     1.00312310E+04  # ~tau_1-
   2000015     1.00324921E+04  # ~tau_2-
   1000016     1.00315852E+04  # ~nu_tauL
   1000021     2.85241437E+03  # ~g
   1000022     4.23261846E+01  # ~chi_10
   1000023     1.33674121E+02  # ~chi_20
   1000025     1.44943674E+02  # ~chi_30
   1000035     1.16096255E+03  # ~chi_40
   1000024     1.31078577E+02  # ~chi_1+
   1000037     1.16000345E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.11289264E-02   # alpha
Block Hmix Q=  3.11352246E+03  # Higgs mixing parameters
   1    1.14988469E+02  # mu
   2    4.66468680E+01  # tan[beta](Q)
   3    2.43156654E+02  # v(Q)
   4    2.31871701E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     8.99509205E-01   # Re[R_st(1,1)]
   1  2     4.36901808E-01   # Re[R_st(1,2)]
   2  1    -4.36901808E-01   # Re[R_st(2,1)]
   2  2     8.99509205E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99999574E-01   # Re[R_sb(1,1)]
   1  2     9.22768220E-04   # Re[R_sb(1,2)]
   2  1    -9.22768220E-04   # Re[R_sb(2,1)]
   2  2     9.99999574E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     3.44004778E-01   # Re[R_sta(1,1)]
   1  2     9.38967898E-01   # Re[R_sta(1,2)]
   2  1    -9.38967898E-01   # Re[R_sta(2,1)]
   2  2     3.44004778E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.29907817E-01   # Re[N(1,1)]
   1  2    -8.71117944E-03   # Re[N(1,2)]
   1  3    -3.47972207E-01   # Re[N(1,3)]
   1  4     1.18789355E-01   # Re[N(1,4)]
   2  1    -1.67778533E-01   # Re[N(2,1)]
   2  2    -5.40549952E-02   # Re[N(2,2)]
   2  3     6.89528294E-01   # Re[N(2,3)]
   2  4     7.02480714E-01   # Re[N(2,4)]
   3  1     3.27283976E-01   # Re[N(3,1)]
   3  2    -4.43888681E-02   # Re[N(3,2)]
   3  3    -6.35160865E-01   # Re[N(3,3)]
   3  4     6.98201620E-01   # Re[N(3,4)]
   4  1    -2.64868415E-03   # Re[N(4,1)]
   4  2     9.97512808E-01   # Re[N(4,2)]
   4  3     6.06220626E-03   # Re[N(4,3)]
   4  4     7.01743036E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -8.59220465E-03   # Re[U(1,1)]
   1  2     9.99963086E-01   # Re[U(1,2)]
   2  1    -9.99963086E-01   # Re[U(2,1)]
   2  2    -8.59220465E-03   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.92830093E-02   # Re[V(1,1)]
   1  2     9.95059236E-01   # Re[V(1,2)]
   2  1     9.95059236E-01   # Re[V(2,1)]
   2  2    -9.92830093E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02868269E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     8.64771938E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     2.81420486E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     1.07079110E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.41392337E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     7.97049805E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     6.43805090E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     5.30649712E-03    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01263989E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.07240439E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.06075069E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     8.59682288E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     2.94684270E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.07677945E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     3.16421415E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.41553270E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     7.97518762E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     6.96926939E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     5.75739633E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.00922808E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.06552658E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.46625895E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     3.42180366E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.51633902E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.30497411E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     3.32666897E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.75364228E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     6.70574032E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.81539334E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.64384317E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.28171113E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.30242387E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.08075702E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.76913183E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.19381049E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.41396860E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     7.42973391E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.94627768E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     1.49849363E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02944264E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.14705222E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.01231781E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.41557778E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     7.42131803E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.94180762E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     1.49679624E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02601177E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     7.27254047E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.00550960E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.86967765E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     5.62529707E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.98785944E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.13456004E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.29383427E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.47461214E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.55258002E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.79499833E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.16571251E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     2.33278783E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     8.87359592E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.91713410E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.07495826E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.35632913E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.80744894E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.34507040E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.07330453E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.37241356E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.79590473E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.17272056E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     2.64466097E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.13730304E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.91583181E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.07545086E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.36295650E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     6.03054731E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.34474929E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.07324045E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.37191231E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.29205196E+02   # ~b_1
#    BR                NDA      ID1      ID2
     3.67349820E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.33689822E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.14354144E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     7.42473598E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.67018378E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.53504766E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     2.04505485E-02    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     2.61884956E+02   # ~b_2
#    BR                NDA      ID1      ID2
     3.47684353E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.02923028E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     8.81659902E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.15275872E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     5.58710674E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     6.96685196E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.79645639E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.10047049E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     3.46269105E-03    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.67662477E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.07483369E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.67425393E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.88086387E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.33485337E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.08622893E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.06277996E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.37214270E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.96692576E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.79643442E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.12616608E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     3.46520345E-03    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.67652364E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.07532594E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.67421702E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.90293286E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.33453463E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.14154959E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.06271616E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.37164145E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.22856343E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.17097663E-03    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     2.84284075E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.70503557E-04    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     2.42714777E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.08409843E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     6.91448071E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     3.01817766E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.38622935E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     2.31059238E-03    2     1000021         4   # BR(~t_1 -> ~g c)
#    BR                NDA      ID1      ID2       ID3
     7.10939140E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.97180080E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.39897850E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.65442510E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.87884313E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     5.17969115E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.28977121E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.00950521E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.46871646E-02    2     1000021         6   # BR(~t_2 -> ~g t)
     9.23738072E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     7.62072337E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     5.51633224E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.69166037E-02   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99998814E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     1.01844482E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.21821843E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.51227811E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.33690475E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.46463101E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.39625467E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     5.72496943E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.65003911E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     5.67111929E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     5.13184287E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     6.86763500E-03   # chi^0_2
#    BR                NDA      ID1      ID2
     9.99615772E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     3.78391075E-04    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     1.19184134E-02   # chi^0_3
#    BR                NDA      ID1      ID2
     9.99830587E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.08428741E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.34866011E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.34866011E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     5.34197517E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     9.64169961E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.31404065E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     6.33186552E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.34378685E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.38736698E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.33678836E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     2.61009685E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     2.45547095E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     6.19698165E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     6.19698165E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     7.52158053E-01   # ~g
#    BR                NDA      ID1      ID2
     1.67496962E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.38418236E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     1.36846864E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     1.68814854E-02    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.58131005E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     6.11689354E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.56048625E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     5.22445127E-02    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.66041564E-02    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     2.05122664E-02    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.29291787E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.29291787E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     2.12999325E-02    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     2.12999325E-02    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     6.48582245E-03   # Gamma(h0)
     1.57137494E-03   2        22        22   # BR(h0 -> photon photon)
     1.20013420E-03   2        22        23   # BR(h0 -> photon Z)
     2.50297485E-02   2        23        23   # BR(h0 -> Z Z)
     1.93909005E-01   2       -24        24   # BR(h0 -> W W)
     4.68870977E-02   2        21        21   # BR(h0 -> gluon gluon)
     2.63852310E-09   2       -11        11   # BR(h0 -> Electron electron)
     1.17367188E-04   2       -13        13   # BR(h0 -> Muon muon)
     3.38434236E-02   2       -15        15   # BR(h0 -> Tau tau)
     7.47747916E-08   2        -2         2   # BR(h0 -> Up up)
     1.45138066E-02   2        -4         4   # BR(h0 -> Charm charm)
     3.03927006E-07   2        -1         1   # BR(h0 -> Down down)
     1.09923296E-04   2        -3         3   # BR(h0 -> Strange strange)
     2.92096666E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     3.90721072E-01   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     1.66579390E+02   # Gamma(HH)
     1.14360277E-08   2        22        22   # BR(HH -> photon photon)
     1.89323873E-08   2        22        23   # BR(HH -> photon Z)
     3.66570222E-08   2        23        23   # BR(HH -> Z Z)
     6.30800354E-09   2       -24        24   # BR(HH -> W W)
     3.73076885E-06   2        21        21   # BR(HH -> gluon gluon)
     8.33462303E-09   2       -11        11   # BR(HH -> Electron electron)
     3.71157703E-04   2       -13        13   # BR(HH -> Muon muon)
     1.07202756E-01   2       -15        15   # BR(HH -> Tau tau)
     2.79023313E-14   2        -2         2   # BR(HH -> Up up)
     5.41338736E-09   2        -4         4   # BR(HH -> Charm charm)
     3.93238780E-04   2        -6         6   # BR(HH -> Top top)
     5.76951338E-07   2        -1         1   # BR(HH -> Down down)
     2.08689927E-04   2        -3         3   # BR(HH -> Strange strange)
     5.38258769E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.66948981E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.05252589E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.05252589E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     2.15647002E-05   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     7.83869210E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.21552115E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     4.41466015E-03   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.52247269E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.69395667E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     4.49428123E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     5.18498866E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     5.10795726E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     3.90988782E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     1.08012625E-05   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     2.69771856E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.57664618E+02   # Gamma(A0)
     1.07363074E-09   2        22        22   # BR(A0 -> photon photon)
     1.52026449E-09   2        22        23   # BR(A0 -> photon Z)
     6.60713811E-06   2        21        21   # BR(A0 -> gluon gluon)
     8.07516386E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.59597756E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.03863928E-01   2       -15        15   # BR(A0 -> Tau tau)
     2.54666759E-14   2        -2         2   # BR(A0 -> Up up)
     4.94078279E-09   2        -4         4   # BR(A0 -> Charm charm)
     3.60100168E-04   2        -6         6   # BR(A0 -> Top top)
     5.58950391E-07   2        -1         1   # BR(A0 -> Down down)
     2.02179352E-04   2        -3         3   # BR(A0 -> Strange strange)
     5.21472584E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.80770127E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.11272393E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.11272393E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.03724086E-05   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     8.55298594E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.17127590E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     5.29445782E-03   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.66120556E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.64656922E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     4.44434547E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     5.14374107E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     5.95622831E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     4.41913493E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     5.21869526E-06   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     1.02117397E-07   2        23        25   # BR(A0 -> Z h0)
     2.01952057E-13   2        23        35   # BR(A0 -> Z HH)
     4.19348112E-41   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.92062025E+02   # Gamma(Hp)
     9.29939258E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.97578078E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.12457736E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.58633452E-07   2        -1         2   # BR(Hp -> Down up)
     9.47897778E-06   2        -3         2   # BR(Hp -> Strange up)
     6.14099233E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.64740156E-08   2        -1         4   # BR(Hp -> Down charm)
     2.01338576E-04   2        -3         4   # BR(Hp -> Strange charm)
     8.59958836E-04   2        -5         4   # BR(Hp -> Bottom charm)
     2.74739129E-08   2        -1         6   # BR(Hp -> Down top)
     8.99899870E-07   2        -3         6   # BR(Hp -> Strange top)
     5.79566689E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.60400466E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.68836718E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     2.14256862E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     8.75867733E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     2.08526737E-03   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     7.02138765E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     9.15472689E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     2.39691669E-11   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     8.37512842E-08   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.71705711E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.17595859E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.17593029E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00001300E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    4.46570239E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    4.59573545E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.71705711E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.17595859E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.17593029E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999907E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    9.33039488E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999907E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    9.33039488E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25715100E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    8.18038733E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.24396213E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    9.33039488E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999907E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    2.89520556E-04   # BR(b -> s gamma)
    2    1.59626835E-06   # BR(b -> s mu+ mu-)
    3    3.52596634E-05   # BR(b -> s nu nu)
    4    4.09610134E-15   # BR(Bd -> e+ e-)
    5    1.74976063E-10   # BR(Bd -> mu+ mu-)
    6    3.63563317E-08   # BR(Bd -> tau+ tau-)
    7    1.37178112E-13   # BR(Bs -> e+ e-)
    8    5.86008928E-09   # BR(Bs -> mu+ mu-)
    9    1.23390021E-06   # BR(Bs -> tau+ tau-)
   10    9.63049232E-05   # BR(B_u -> tau nu)
   11    9.94792986E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41997254E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93590180E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15756456E-03   # epsilon_K
   17    2.28166118E-15   # Delta(M_K)
   18    2.48152934E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29383655E-11   # BR(K^+ -> pi^+ nu nu)
   20   -4.51274970E-16   # Delta(g-2)_electron/2
   21   -1.92936671E-11   # Delta(g-2)_muon/2
   22   -5.47673987E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    8.44699658E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.51003260E-01   # C7
     0305 4322   00   2     1.06578834E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -7.63835215E-02   # C8
     0305 6321   00   2    -2.19627884E-05   # C8'
 03051111 4133   00   0     1.61853753E+00   # C9 e+e-
 03051111 4133   00   2     1.61896166E+00   # C9 e+e-
 03051111 4233   00   2     6.81450664E-04   # C9' e+e-
 03051111 4137   00   0    -4.44122963E+00   # C10 e+e-
 03051111 4137   00   2    -4.44044970E+00   # C10 e+e-
 03051111 4237   00   2    -5.10338948E-03   # C10' e+e-
 03051313 4133   00   0     1.61853753E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61896140E+00   # C9 mu+mu-
 03051313 4233   00   2     6.81450325E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44122963E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44044996E+00   # C10 mu+mu-
 03051313 4237   00   2    -5.10339009E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50518309E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.10556980E-03   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50518309E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.10556982E-03   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50518309E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.10557611E-03   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85830000E-07   # C7
     0305 4422   00   2    -1.22833004E-05   # C7
     0305 4322   00   2     2.96392891E-06   # C7'
     0305 6421   00   0     3.30488911E-07   # C8
     0305 6421   00   2    -3.00422366E-05   # C8
     0305 6321   00   2     1.02318829E-06   # C8'
 03051111 4133   00   2    -6.08463361E-07   # C9 e+e-
 03051111 4233   00   2     1.26275263E-05   # C9' e+e-
 03051111 4137   00   2     1.02043970E-05   # C10 e+e-
 03051111 4237   00   2    -9.45692106E-05   # C10' e+e-
 03051313 4133   00   2    -6.08469035E-07   # C9 mu+mu-
 03051313 4233   00   2     1.26275222E-05   # C9' mu+mu-
 03051313 4137   00   2     1.02044043E-05   # C10 mu+mu-
 03051313 4237   00   2    -9.45692233E-05   # C10' mu+mu-
 03051212 4137   00   2    -2.17590348E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     2.04869461E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -2.17590320E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     2.04869461E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -2.17582390E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     2.04869461E-05   # C11' nu_3 nu_3
