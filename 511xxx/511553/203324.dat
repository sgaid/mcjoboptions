# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.03.2021,  11:39
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.38836651E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.16059983E+03  # scale for input parameters
    1   -5.49488712E+02  # M_1
    2   -1.97121877E+03  # M_2
    3    3.04402834E+03  # M_3
   11    2.67797969E+03  # A_t
   12    8.03483939E+02  # A_b
   13   -1.05080004E+03  # A_tau
   23   -3.06354230E+02  # mu
   25    3.28158643E+01  # tan(beta)
   26    8.28491514E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.46500168E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.83788882E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.73817128E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.16059983E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.16059983E+03  # (SUSY scale)
  1  1     8.38826413E-06   # Y_u(Q)^DRbar
  2  2     4.26123818E-03   # Y_c(Q)^DRbar
  3  3     1.01336968E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.16059983E+03  # (SUSY scale)
  1  1     5.53141434E-04   # Y_d(Q)^DRbar
  2  2     1.05096873E-02   # Y_s(Q)^DRbar
  3  3     5.48542973E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.16059983E+03  # (SUSY scale)
  1  1     9.65272252E-05   # Y_e(Q)^DRbar
  2  2     1.99587687E-02   # Y_mu(Q)^DRbar
  3  3     3.35673656E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.16059983E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     2.67797932E+03   # A_t(Q)^DRbar
Block Ad Q=  3.16059983E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     8.03483896E+02   # A_b(Q)^DRbar
Block Ae Q=  3.16059983E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.05080009E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.16059983E+03  # soft SUSY breaking masses at Q
   1   -5.49488712E+02  # M_1
   2   -1.97121877E+03  # M_2
   3    3.04402834E+03  # M_3
  21    3.93289857E+05  # M^2_(H,d)
  22    5.40334138E+04  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.46500168E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.83788882E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.73817128E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22237157E+02  # h0
        35     8.28539459E+02  # H0
        36     8.28491514E+02  # A0
        37     8.35257166E+02  # H+
   1000001     1.01005674E+04  # ~d_L
   2000001     1.00779297E+04  # ~d_R
   1000002     1.01002001E+04  # ~u_L
   2000002     1.00810927E+04  # ~u_R
   1000003     1.01005725E+04  # ~s_L
   2000003     1.00779382E+04  # ~s_R
   1000004     1.01002052E+04  # ~c_L
   2000004     1.00810942E+04  # ~c_R
   1000005     2.58132559E+03  # ~b_1
   2000005     4.79483066E+03  # ~b_2
   1000006     2.58132293E+03  # ~t_1
   2000006     3.86987276E+03  # ~t_2
   1000011     1.00193587E+04  # ~e_L-
   2000011     1.00087702E+04  # ~e_R-
   1000012     1.00185958E+04  # ~nu_eL
   1000013     1.00193812E+04  # ~mu_L-
   2000013     1.00088135E+04  # ~mu_R-
   1000014     1.00186180E+04  # ~nu_muL
   1000015     1.00210309E+04  # ~tau_1-
   2000015     1.00258519E+04  # ~tau_2-
   1000016     1.00249465E+04  # ~nu_tauL
   1000021     3.48133120E+03  # ~g
   1000022     3.06945225E+02  # ~chi_10
   1000023     3.15826362E+02  # ~chi_20
   1000025     5.59393706E+02  # ~chi_30
   1000035     2.07488527E+03  # ~chi_40
   1000024     3.12718743E+02  # ~chi_1+
   1000037     2.07485098E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.05642745E-02   # alpha
Block Hmix Q=  3.16059983E+03  # Higgs mixing parameters
   1   -3.06354230E+02  # mu
   2    3.28158643E+01  # tan[beta](Q)
   3    2.43206567E+02  # v(Q)
   4    6.86398189E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.99107223E-01   # Re[R_st(1,1)]
   1  2     4.22463847E-02   # Re[R_st(1,2)]
   2  1    -4.22463847E-02   # Re[R_st(2,1)]
   2  2    -9.99107223E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99998851E-01   # Re[R_sb(1,1)]
   1  2     1.51566117E-03   # Re[R_sb(1,2)]
   2  1    -1.51566117E-03   # Re[R_sb(2,1)]
   2  2    -9.99998851E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.71308118E-01   # Re[R_sta(1,1)]
   1  2     9.85217503E-01   # Re[R_sta(1,2)]
   2  1    -9.85217503E-01   # Re[R_sta(2,1)]
   2  2    -1.71308118E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     1.27904039E-01   # Re[N(1,1)]
   1  2    -3.27308218E-02   # Re[N(1,2)]
   1  3    -7.07318361E-01   # Re[N(1,3)]
   1  4     6.94456612E-01   # Re[N(1,4)]
   2  1    -3.48171775E-02   # Re[N(2,1)]
   2  2     2.33216666E-02   # Re[N(2,2)]
   2  3    -7.03719458E-01   # Re[N(2,3)]
   2  4    -7.09240995E-01   # Re[N(2,4)]
   3  1    -9.91174359E-01   # Re[N(3,1)]
   3  2    -6.36188452E-03   # Re[N(3,2)]
   3  3    -6.65452753E-02   # Re[N(3,3)]
   3  4     1.14475516E-01   # Re[N(3,4)]
   4  1     1.30842150E-03   # Re[N(4,1)]
   4  2    -9.99171817E-01   # Re[N(4,2)]
   4  3     7.16849083E-03   # Re[N(4,3)]
   4  4    -4.00322518E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.01565214E-02   # Re[U(1,1)]
   1  2    -9.99948421E-01   # Re[U(1,2)]
   2  1    -9.99948421E-01   # Re[U(2,1)]
   2  2     1.01565214E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     5.66427081E-02   # Re[V(1,1)]
   1  2     9.98394513E-01   # Re[V(1,2)]
   2  1     9.98394513E-01   # Re[V(2,1)]
   2  2    -5.66427081E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     4.99805585E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.64299341E-02    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.21734164E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.82351110E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.33716383E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     4.17162392E-04    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     9.41156580E-02    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01202765E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.04189160E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.01391019E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.71680191E-02    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.99535334E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     9.79256091E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.57873975E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.33795978E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     7.13261080E-04    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.00646429E-04    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     9.40624949E-02    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01024402E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.03831326E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     9.67003284E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.32111882E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.12188351E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     4.94487429E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.15154866E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.26598427E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     2.30984251E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.54427785E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     6.82689315E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     7.39720724E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     9.15232705E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.53804715E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     3.33247699E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.09098533E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.33719458E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     3.36168171E-03    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     5.78402325E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     8.96903324E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02012722E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.11197897E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02244882E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.33799043E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     3.35968962E-03    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     5.78059573E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     8.96371850E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.01833872E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     2.70290198E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01888292E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.56246409E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     2.87883912E-03    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     4.95325739E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     7.68084225E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.58663000E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.45339440E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.15814973E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.22554636E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.47626064E-04    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     8.82420762E-03    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.91017071E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.42860480E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.84091836E-04    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.72181566E-03    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.48372425E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.09794148E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.33398281E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.22599526E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.65328668E-04    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     8.82373484E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.90946907E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.42886766E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.98963943E-04    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.72188666E-03    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.48353359E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.09790343E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.33369440E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     7.01154627E+01   # ~b_1
#    BR                NDA      ID1      ID2
     1.07692466E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.06012801E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     5.12591001E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.02679307E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     7.20988574E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     3.99123193E-02    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.45231568E+02   # ~b_2
#    BR                NDA      ID1      ID2
     9.83315828E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     9.70621917E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.84857780E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.95419720E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     5.82636696E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     4.23262248E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     3.46845628E-03    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     2.11877724E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.77223105E-03    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     3.33589183E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
     1.77202238E-03    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   2000002     6.39636919E+02   # ~u_R
#    BR                NDA      ID1      ID2
     5.74723536E-04    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     3.43650994E-02    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.65017539E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.42845316E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.98894099E-03    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     5.47848917E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     3.83365723E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.09450728E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.33368644E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.39644300E+02   # ~c_R
#    BR                NDA      ID1      ID2
     5.77458296E-04    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     3.43647793E-02    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.65006577E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.42871568E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.98893596E-03    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     5.47829907E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     4.13085407E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.09446929E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.33339801E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     7.21184030E+01   # ~t_1
#    BR                NDA      ID1      ID2
     3.42076129E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     3.55088826E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.22932006E-02    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.72537189E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.12068615E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     3.89892254E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     2.22300222E-02    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     2.02247507E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.85083387E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.92768148E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     4.54103371E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     6.96574789E-04    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.82895345E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.40974485E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.03910903E-02    2     1000021         6   # BR(~t_2 -> ~g t)
     5.91493205E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     5.50974859E-04    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     2.94707118E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.77684313E-04    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     5.16215261E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     2.75154848E-04    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     2.33311255E-08   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.74530570E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     2.96510914E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.24843557E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.24635570E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     7.94793884E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     3.18629195E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.40740466E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     1.47727285E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     5.74874796E-03    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     1.01491844E-01    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     9.63022498E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     1.01737978E-03    2          37   1000025   # BR(chi^+_2 -> H^+ chi^0_3)
     1.46838948E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     9.76588267E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     1.44250490E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     9.75810625E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     8.58995008E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.00065262E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     5.62806312E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     6.12613157E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     1.98378313E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.54614670E-07   # chi^0_2
#    BR                NDA      ID1      ID2
     4.21858034E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     8.48903723E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     6.45345976E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.08846427E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.08701739E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     7.55940452E-04    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     2.45635395E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     2.45121333E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     1.34806597E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.45559031E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     5.28710540E-04    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     5.28710540E-04    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     2.34944253E-04    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     2.34944253E-04    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     1.76236876E-04    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     1.76236876E-04    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     1.75224911E-04    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     1.75224911E-04    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
DECAY   1000025     1.59227445E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.38434222E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.38434222E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     1.55278804E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.26333621E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     1.44726268E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.36246388E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.31492185E-04    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     1.31492185E-04    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     3.33849866E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.40874059E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.40874059E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     9.66414809E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     9.66414809E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     4.40877650E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     9.50340458E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.22796255E-03    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     3.31994631E-02    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     2.87658382E-02    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     2.08615266E-04    2     1000025        36   # BR(chi^0_4 -> chi^0_3 A^0)
     8.87033214E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     8.28559317E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     3.83960793E-03    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     6.02348452E-02    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     6.79229183E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     6.40069070E-04    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     3.34687246E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     3.43062493E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.26398551E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     5.61392145E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     5.61392145E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     4.21686944E+01   # ~g
#    BR                NDA      ID1      ID2
     1.08150056E-03    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     1.08150056E-03    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     2.48798592E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.48798592E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.47659823E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.47659823E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
     1.00201768E-03    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     1.03421721E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     2.51658503E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.12639228E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.12639228E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.24921376E-03   # Gamma(h0)
     2.55209298E-03   2        22        22   # BR(h0 -> photon photon)
     1.38342395E-03   2        22        23   # BR(h0 -> photon Z)
     2.31675791E-02   2        23        23   # BR(h0 -> Z Z)
     2.02336775E-01   2       -24        24   # BR(h0 -> W W)
     8.09822659E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.28287984E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.34990352E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.77518651E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.40939288E-07   2        -2         2   # BR(h0 -> Up up)
     2.73529874E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.14161036E-07   2        -1         1   # BR(h0 -> Down down)
     2.22128489E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.94015131E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.36694718E+01   # Gamma(HH)
     9.80006143E-08   2        22        22   # BR(HH -> photon photon)
     6.99879099E-08   2        22        23   # BR(HH -> photon Z)
     8.90718549E-06   2        23        23   # BR(HH -> Z Z)
     1.44424524E-05   2       -24        24   # BR(HH -> W W)
     8.27854258E-05   2        21        21   # BR(HH -> gluon gluon)
     9.36825349E-09   2       -11        11   # BR(HH -> Electron electron)
     4.16954243E-04   2       -13        13   # BR(HH -> Muon muon)
     1.20394902E-01   2       -15        15   # BR(HH -> Tau tau)
     1.36744585E-13   2        -2         2   # BR(HH -> Up up)
     2.65185022E-08   2        -4         4   # BR(HH -> Charm charm)
     1.72972685E-03   2        -6         6   # BR(HH -> Top top)
     8.20219810E-07   2        -1         1   # BR(HH -> Down down)
     2.96691593E-04   2        -3         3   # BR(HH -> Strange strange)
     8.75237974E-01   2        -5         5   # BR(HH -> Bottom bottom)
     5.28865788E-04   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     7.72181962E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     3.19835303E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.41348120E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     5.43610684E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.34000722E+01   # Gamma(A0)
     1.61428160E-07   2        22        22   # BR(A0 -> photon photon)
     6.88000369E-08   2        22        23   # BR(A0 -> photon Z)
     1.27900715E-04   2        21        21   # BR(A0 -> gluon gluon)
     9.34978727E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.16132434E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.20159913E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.25748480E-13   2        -2         2   # BR(A0 -> Up up)
     2.43868787E-08   2        -4         4   # BR(A0 -> Charm charm)
     1.95235569E-03   2        -6         6   # BR(A0 -> Top top)
     8.18609196E-07   2        -1         1   # BR(A0 -> Down down)
     2.96109082E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.73554361E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.21021857E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.86951485E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.11075283E-04   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.87247978E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.40890596E-05   2        23        25   # BR(A0 -> Z h0)
     7.99177723E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.45922480E+01   # Gamma(Hp)
     1.08677449E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.64630022E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.31422698E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.30642461E-07   2        -1         2   # BR(Hp -> Down up)
     1.37941692E-05   2        -3         2   # BR(Hp -> Strange up)
     9.76663780E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.95215505E-08   2        -1         4   # BR(Hp -> Down charm)
     2.99400698E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.36767293E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.37231659E-07   2        -1         6   # BR(Hp -> Down top)
     3.39404314E-06   2        -3         6   # BR(Hp -> Strange top)
     8.66143045E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.26739776E-04   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     3.44431837E-05   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.33956082E-05   2        24        25   # BR(Hp -> W h0)
     1.26663124E-09   2        24        35   # BR(Hp -> W HH)
     1.31244427E-09   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.00661579E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.07687433E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.07688095E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99993857E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    9.34751230E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    9.28607754E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.00661579E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.07687433E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.07688095E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999990E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.01275472E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999990E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.01275472E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26259871E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.74178974E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.40003139E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.01275472E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999990E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.60079117E-04   # BR(b -> s gamma)
    2    1.58732203E-06   # BR(b -> s mu+ mu-)
    3    3.52525196E-05   # BR(b -> s nu nu)
    4    3.47318778E-15   # BR(Bd -> e+ e-)
    5    1.48366135E-10   # BR(Bd -> mu+ mu-)
    6    3.07965185E-08   # BR(Bd -> tau+ tau-)
    7    1.19767285E-13   # BR(Bs -> e+ e-)
    8    5.11628904E-09   # BR(Bs -> mu+ mu-)
    9    1.07543271E-06   # BR(Bs -> tau+ tau-)
   10    8.78382560E-05   # BR(B_u -> tau nu)
   11    9.07335556E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41982669E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93278998E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15769950E-03   # epsilon_K
   17    2.28167324E-15   # Delta(M_K)
   18    2.47974875E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28995296E-11   # BR(K^+ -> pi^+ nu nu)
   20    3.46756427E-16   # Delta(g-2)_electron/2
   21    1.48249535E-11   # Delta(g-2)_muon/2
   22    4.19578647E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.72770770E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.27580765E-01   # C7
     0305 4322   00   2    -8.65206768E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.44066993E-01   # C8
     0305 6321   00   2    -1.01162515E-03   # C8'
 03051111 4133   00   0     1.61851812E+00   # C9 e+e-
 03051111 4133   00   2     1.61905685E+00   # C9 e+e-
 03051111 4233   00   2     1.35323521E-04   # C9' e+e-
 03051111 4137   00   0    -4.44121022E+00   # C10 e+e-
 03051111 4137   00   2    -4.43956860E+00   # C10 e+e-
 03051111 4237   00   2    -1.01171554E-03   # C10' e+e-
 03051313 4133   00   0     1.61851812E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61905678E+00   # C9 mu+mu-
 03051313 4233   00   2     1.35310412E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44121022E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43956867E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.01170271E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50499555E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     2.19172999E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50499555E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     2.19175825E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50499555E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     2.19972074E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85828810E-07   # C7
     0305 4422   00   2     9.53026696E-06   # C7
     0305 4322   00   2     5.83784222E-07   # C7'
     0305 6421   00   0     3.30487892E-07   # C8
     0305 6421   00   2     1.06315404E-05   # C8
     0305 6321   00   2     3.37538239E-07   # C8'
 03051111 4133   00   2     3.96580784E-07   # C9 e+e-
 03051111 4233   00   2     5.84591060E-06   # C9' e+e-
 03051111 4137   00   2    -5.93243182E-07   # C10 e+e-
 03051111 4237   00   2    -4.37627433E-05   # C10' e+e-
 03051313 4133   00   2     3.96579203E-07   # C9 mu+mu-
 03051313 4233   00   2     5.84590934E-06   # C9' mu+mu-
 03051313 4137   00   2    -5.93241614E-07   # C10 mu+mu-
 03051313 4237   00   2    -4.37627471E-05   # C10' mu+mu-
 03051212 4137   00   2     1.47388607E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     9.48055419E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     1.47388653E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     9.48055419E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     1.47401569E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     9.48055418E-06   # C11' nu_3 nu_3
