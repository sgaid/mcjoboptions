# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.03.2021,  09:57
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.94110266E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.12383607E+03  # scale for input parameters
    1    2.73139088E+02  # M_1
    2    9.47742477E+02  # M_2
    3    1.05429948E+03  # M_3
   11    3.58943664E+03  # A_t
   12    8.36645405E+02  # A_b
   13   -1.56718607E+03  # A_tau
   23   -9.80839092E+01  # mu
   25    1.85143411E+01  # tan(beta)
   26    3.89585679E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.90111265E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.48752921E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.24573426E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.12383607E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.12383607E+03  # (SUSY scale)
  1  1     8.39659316E-06   # Y_u(Q)^DRbar
  2  2     4.26546932E-03   # Y_c(Q)^DRbar
  3  3     1.01437590E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.12383607E+03  # (SUSY scale)
  1  1     3.12386041E-04   # Y_d(Q)^DRbar
  2  2     5.93533478E-03   # Y_s(Q)^DRbar
  3  3     3.09789065E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.12383607E+03  # (SUSY scale)
  1  1     5.45136485E-05   # Y_e(Q)^DRbar
  2  2     1.12716936E-02   # Y_mu(Q)^DRbar
  3  3     1.89571343E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.12383607E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     3.58943662E+03   # A_t(Q)^DRbar
Block Ad Q=  4.12383607E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     8.36645345E+02   # A_b(Q)^DRbar
Block Ae Q=  4.12383607E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.56718601E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  4.12383607E+03  # soft SUSY breaking masses at Q
   1    2.73139088E+02  # M_1
   2    9.47742477E+02  # M_2
   3    1.05429948E+03  # M_3
  21    1.51298672E+07  # M^2_(H,d)
  22    3.79084560E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.90111265E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.48752921E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.24573426E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.24271076E+02  # h0
        35     3.89529227E+03  # H0
        36     3.89585679E+03  # A0
        37     3.89646414E+03  # H+
   1000001     1.01236577E+04  # ~d_L
   2000001     1.00986564E+04  # ~d_R
   1000002     1.01232965E+04  # ~u_L
   2000002     1.01014954E+04  # ~u_R
   1000003     1.01236582E+04  # ~s_L
   2000003     1.00986570E+04  # ~s_R
   1000004     1.01232970E+04  # ~c_L
   2000004     1.01014958E+04  # ~c_R
   1000005     4.28779173E+03  # ~b_1
   2000005     4.91981312E+03  # ~b_2
   1000006     3.45410337E+03  # ~t_1
   2000006     4.92342645E+03  # ~t_2
   1000011     1.00210064E+04  # ~e_L-
   2000011     1.00093127E+04  # ~e_R-
   1000012     1.00202380E+04  # ~nu_eL
   1000013     1.00210079E+04  # ~mu_L-
   2000013     1.00093156E+04  # ~mu_R-
   1000014     1.00202395E+04  # ~nu_muL
   1000015     1.00101603E+04  # ~tau_1-
   2000015     1.00214421E+04  # ~tau_2-
   1000016     1.00206727E+04  # ~nu_tauL
   1000021     1.36253013E+03  # ~g
   1000022     1.12987816E+02  # ~chi_10
   1000023     1.27070999E+02  # ~chi_20
   1000025     2.82757845E+02  # ~chi_30
   1000035     1.03038706E+03  # ~chi_40
   1000024     1.21324790E+02  # ~chi_1+
   1000037     1.02987583E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -5.15477505E-02   # alpha
Block Hmix Q=  4.12383607E+03  # Higgs mixing parameters
   1   -9.80839092E+01  # mu
   2    1.85143411E+01  # tan[beta](Q)
   3    2.42894739E+02  # v(Q)
   4    1.51777001E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -3.94861471E-02   # Re[R_st(1,1)]
   1  2     9.99220118E-01   # Re[R_st(1,2)]
   2  1    -9.99220118E-01   # Re[R_st(2,1)]
   2  2    -3.94861471E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -7.05950136E-04   # Re[R_sb(1,1)]
   1  2     9.99999751E-01   # Re[R_sb(1,2)]
   2  1    -9.99999751E-01   # Re[R_sb(2,1)]
   2  2    -7.05950136E-04   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -6.83122765E-04   # Re[R_sta(1,1)]
   1  2     9.99999767E-01   # Re[R_sta(1,2)]
   2  1    -9.99999767E-01   # Re[R_sta(2,1)]
   2  2    -6.83122765E-04   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -1.73831631E-01   # Re[N(1,1)]
   1  2     5.48505805E-02   # Re[N(1,2)]
   1  3     7.21792594E-01   # Re[N(1,3)]
   1  4     6.67674644E-01   # Re[N(1,4)]
   2  1    -8.38768427E-02   # Re[N(2,1)]
   2  2     5.28855346E-02   # Re[N(2,2)]
   2  3    -6.88648115E-01   # Re[N(2,3)]
   2  4     7.18283767E-01   # Re[N(2,4)]
   3  1    -9.81185418E-01   # Re[N(3,1)]
   3  2    -1.90487719E-02   # Re[N(3,2)]
   3  3    -6.89853844E-02   # Re[N(3,3)]
   3  4    -1.79313513E-01   # Re[N(3,4)]
   4  1     4.73436380E-03   # Re[N(4,1)]
   4  2    -9.96911068E-01   # Re[N(4,2)]
   4  3     4.49920366E-03   # Re[N(4,3)]
   4  4     7.82666253E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -6.41386682E-03   # Re[U(1,1)]
   1  2    -9.99979431E-01   # Re[U(1,2)]
   2  1     9.99979431E-01   # Re[U(2,1)]
   2  2    -6.41386682E-03   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -1.10723506E-01   # Re[V(1,1)]
   1  2     9.93851249E-01   # Re[V(1,2)]
   2  1     9.93851249E-01   # Re[V(2,1)]
   2  2     1.10723506E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02153680E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     3.02565296E-02    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     7.04395534E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.62677528E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.42119484E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     4.48370099E-04    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     9.14361256E-02    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.00424412E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.07645323E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02659655E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     3.04882294E-02    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     7.27547339E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     9.61711168E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     5.03145223E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.42144827E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     5.41099769E-04    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.04713893E-04    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     9.14206840E-02    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.00370899E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.07537080E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     6.45289728E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     8.13539607E-02    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     5.80360148E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     7.49716379E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.10873940E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     1.49288584E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     2.54068164E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.27829701E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     8.72828907E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.86011947E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.78492526E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.42123579E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     6.76945921E-03    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.96083090E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     7.90521229E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.03455677E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     7.60672309E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.00155187E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.42148920E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     6.76825342E-03    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.96030350E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     7.90380420E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.03401626E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     7.78347849E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.00048296E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.49292161E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.44468888E-03    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.81878263E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     7.52595415E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.88897672E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     5.52141981E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.71365117E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.73992797E+02   # ~d_R
#    BR                NDA      ID1      ID2
     2.20071384E-04    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     7.00179871E-03    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.92726701E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     9.02746993E+02   # ~d_L
#    BR                NDA      ID1      ID2
     3.63336576E-04    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     2.26984312E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.19434869E-03    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     4.81256738E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     9.66831744E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.53402422E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.74006957E+02   # ~s_R
#    BR                NDA      ID1      ID2
     2.24816934E-04    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     7.00171431E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.92708602E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     9.02757776E+02   # ~s_L
#    BR                NDA      ID1      ID2
     3.67414534E-04    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     2.30697273E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.19437170E-03    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     4.81251015E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     9.66820734E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.53392273E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.92241664E+02   # ~b_1
#    BR                NDA      ID1      ID2
     1.48276295E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.33146428E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     7.94733209E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.78755389E-02    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     9.36033801E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     5.17244727E+02   # ~b_2
#    BR                NDA      ID1      ID2
     9.75496783E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     8.79542375E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.09503817E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     3.81018660E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     1.91608779E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     7.89176116E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     6.40026924E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     3.16971063E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     7.91119457E+02   # ~u_R
#    BR                NDA      ID1      ID2
     8.61419144E-04    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     2.00545209E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     2.74086040E-02    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.71528806E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     9.02736952E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.85040920E-03    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     4.79619042E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.20991984E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.54993092E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.53379219E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.91126769E+02   # ~c_R
#    BR                NDA      ID1      ID2
     8.63470710E-04    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.02926929E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     2.74085001E-02    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.71519829E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     9.02747705E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.85051724E-03    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     4.79613598E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.21774040E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.54981769E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.53369066E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     3.40259535E+02   # ~t_1
#    BR                NDA      ID1      ID2
     9.18789278E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.06012501E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.88239641E-02    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.60321166E-03    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.03280941E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     3.20799056E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     5.65181085E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     5.22737744E+02   # ~t_2
#    BR                NDA      ID1      ID2
     8.63479306E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     9.96233991E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     7.05617120E-03    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     3.82142239E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.03390743E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     7.37684848E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     6.33133786E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     1.57387947E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.57770725E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     4.75069049E-08   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.53697212E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.15864431E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.17899158E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.17804675E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     9.47345244E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     8.77117919E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.17667481E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.55064388E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.49679977E-02    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.46615980E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.41635623E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.42994961E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     4.09653175E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     5.11722493E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     4.03815648E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.64157520E-06   # chi^0_2
#    BR                NDA      ID1      ID2
     1.27630603E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.14230226E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.02526303E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.46465825E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.46388848E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     4.77371498E-02    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.30531442E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.30257658E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     2.62064643E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.95867390E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     5.03655709E-03    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     5.03655709E-03    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     3.97852325E-03    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     3.97852325E-03    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     1.67885313E-03    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     1.67885313E-03    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     1.67602931E-03    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     1.67602931E-03    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
     1.06417801E-03    3     1000024        15       -16   # BR(chi^0_2 -> chi^+_1 tau^- nu_bar_tau)
     1.06417801E-03    3    -1000024       -15        16   # BR(chi^0_2 -> chi^-_1 tau^+ nu_tau)
DECAY   1000025     5.48403305E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.22934358E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.22934358E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     4.98805866E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.80580978E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     9.29188495E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.30740250E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     9.38794103E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.33821316E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.33821316E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     9.22881718E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.33975897E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     5.99026131E-03    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.09135328E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.62072013E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.62816201E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.74547257E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     2.05452349E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.35921847E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     4.33525981E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     4.33525981E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.79876298E-03   # ~g
#    BR                NDA      ID1      ID2
     1.63278698E-02    2     1000022        21   # BR(~g -> chi^0_1 g)
     1.79471977E-02    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.44677201E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     1.83830414E-01    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     1.47370857E-02    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     2.15286528E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.30200605E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     4.36310113E-04    3     1000025         2        -2   # BR(~g -> chi^0_3 u u_bar)
     4.36315036E-04    3     1000025         4        -4   # BR(~g -> chi^0_3 c c_bar)
     4.83011066E-02    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.21406498E-04    3     1000025         1        -1   # BR(~g -> chi^0_3 d d_bar)
     1.21407913E-04    3     1000025         3        -3   # BR(~g -> chi^0_3 s s_bar)
     3.81247036E-03    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.90902996E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.41712551E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.41712551E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.53493197E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.53493197E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.55919695E-03   # Gamma(h0)
     2.47849136E-03   2        22        22   # BR(h0 -> photon photon)
     1.52344490E-03   2        22        23   # BR(h0 -> photon Z)
     2.75033790E-02   2        23        23   # BR(h0 -> Z Z)
     2.30525488E-01   2       -24        24   # BR(h0 -> W W)
     7.77888441E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.08362248E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.26128225E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.52000462E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.44730272E-07   2        -2         2   # BR(h0 -> Up up)
     2.80874633E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.89050235E-07   2        -1         1   # BR(h0 -> Down down)
     2.13045558E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.66452930E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     6.41351625E+01   # Gamma(HH)
     5.55973955E-09   2        22        22   # BR(HH -> photon photon)
     2.25927787E-08   2        22        23   # BR(HH -> photon Z)
     8.51589042E-07   2        23        23   # BR(HH -> Z Z)
     1.85540826E-07   2       -24        24   # BR(HH -> W W)
     5.51428650E-07   2        21        21   # BR(HH -> gluon gluon)
     3.30974504E-09   2       -11        11   # BR(HH -> Electron electron)
     1.47377219E-04   2       -13        13   # BR(HH -> Muon muon)
     4.25660891E-02   2       -15        15   # BR(HH -> Tau tau)
     3.80788476E-13   2        -2         2   # BR(HH -> Up up)
     7.38741586E-08   2        -4         4   # BR(HH -> Charm charm)
     5.52948719E-03   2        -6         6   # BR(HH -> Top top)
     2.36293395E-07   2        -1         1   # BR(HH -> Down down)
     8.54682412E-05   2        -3         3   # BR(HH -> Strange strange)
     2.23347060E-01   2        -5         5   # BR(HH -> Bottom bottom)
     7.09895412E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.15816615E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     2.15816615E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     4.99033813E-05   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     5.49124450E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     2.45022607E-05   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     3.52110464E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.25117285E-01   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     2.23138393E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     2.75165117E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     9.26905181E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     4.17465649E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     8.10286221E-04   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     2.48786985E-05   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     5.37731722E-06   2        25        25   # BR(HH -> h0 h0)
DECAY        36     6.33527646E+01   # Gamma(A0)
     5.54437219E-10   2        22        22   # BR(A0 -> photon photon)
     1.01269768E-08   2        22        23   # BR(A0 -> photon Z)
     7.46299454E-06   2        21        21   # BR(A0 -> gluon gluon)
     3.20127159E-09   2       -11        11   # BR(A0 -> Electron electron)
     1.42549039E-04   2       -13        13   # BR(A0 -> Muon muon)
     4.11716338E-02   2       -15        15   # BR(A0 -> Tau tau)
     3.49299449E-13   2        -2         2   # BR(A0 -> Up up)
     6.77632450E-08   2        -4         4   # BR(A0 -> Charm charm)
     5.11226675E-03   2        -6         6   # BR(A0 -> Top top)
     2.28548275E-07   2        -1         1   # BR(A0 -> Down down)
     8.26666803E-05   2        -3         3   # BR(A0 -> Strange strange)
     2.16026975E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     7.12986230E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.18580972E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     2.18580972E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     4.63015487E-13   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     4.61312129E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.87670223E-04   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.99917202E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.07800994E-01   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     2.81842885E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.39393265E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.13208632E-01   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     2.17642677E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     3.85026044E-04   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     9.24934801E-14   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     1.76745830E-06   2        23        25   # BR(A0 -> Z h0)
     2.32814089E-40   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     6.55461372E+01   # Gamma(Hp)
     3.70408766E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     1.58361316E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     4.47935981E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     2.29365724E-07   2        -1         2   # BR(Hp -> Down up)
     3.88296259E-06   2        -3         2   # BR(Hp -> Strange up)
     2.50232317E-06   2        -5         2   # BR(Hp -> Bottom up)
     1.43498529E-08   2        -1         4   # BR(Hp -> Down charm)
     8.27378412E-05   2        -3         4   # BR(Hp -> Strange charm)
     3.50414943E-04   2        -5         4   # BR(Hp -> Bottom charm)
     3.95706109E-07   2        -1         6   # BR(Hp -> Down top)
     8.75078397E-06   2        -3         6   # BR(Hp -> Strange top)
     2.42259025E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.85093052E-04   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.27326907E-01   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     8.26878572E-05   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.00191141E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     7.17647017E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.17557172E-03   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     2.11612272E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     2.60939733E-11   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.70974365E-06   2        24        25   # BR(Hp -> W h0)
     4.59080096E-14   2        24        35   # BR(Hp -> W HH)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.12675490E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    3.42868151E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    3.42780826E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00025475E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    2.66256284E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    2.91731603E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.12675490E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    3.42868151E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    3.42780826E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99994182E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    5.81774145E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99994182E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    5.81774145E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.27137372E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    7.21524986E-04        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    9.47370971E-02        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    5.81774145E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99994182E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.20802264E-04   # BR(b -> s gamma)
    2    1.58951292E-06   # BR(b -> s mu+ mu-)
    3    3.52381445E-05   # BR(b -> s nu nu)
    4    2.71949503E-15   # BR(Bd -> e+ e-)
    5    1.16173678E-10   # BR(Bd -> mu+ mu-)
    6    2.43161563E-08   # BR(Bd -> tau+ tau-)
    7    9.18228029E-14   # BR(Bs -> e+ e-)
    8    3.92266509E-09   # BR(Bs -> mu+ mu-)
    9    8.31904868E-07   # BR(Bs -> tau+ tau-)
   10    9.66580861E-05   # BR(B_u -> tau nu)
   11    9.98441024E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42237957E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93738304E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15828937E-03   # epsilon_K
   17    2.28166813E-15   # Delta(M_K)
   18    2.47905197E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28816913E-11   # BR(K^+ -> pi^+ nu nu)
   20   -2.06139242E-16   # Delta(g-2)_electron/2
   21   -8.81311718E-12   # Delta(g-2)_muon/2
   22   -2.49450234E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    4.23582181E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.88968252E-01   # C7
     0305 4322   00   2    -6.29276184E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -9.83442682E-02   # C8
     0305 6321   00   2    -9.26090359E-05   # C8'
 03051111 4133   00   0     1.62742385E+00   # C9 e+e-
 03051111 4133   00   2     1.62767551E+00   # C9 e+e-
 03051111 4233   00   2     8.13129658E-05   # C9' e+e-
 03051111 4137   00   0    -4.45011595E+00   # C10 e+e-
 03051111 4137   00   2    -4.44703432E+00   # C10 e+e-
 03051111 4237   00   2    -6.00378025E-04   # C10' e+e-
 03051313 4133   00   0     1.62742385E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62767547E+00   # C9 mu+mu-
 03051313 4233   00   2     8.13129538E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.45011595E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44703436E+00   # C10 mu+mu-
 03051313 4237   00   2    -6.00378035E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50468509E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.29802126E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50468509E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.29802128E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50468509E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.29802529E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85823098E-07   # C7
     0305 4422   00   2    -4.51299772E-06   # C7
     0305 4322   00   2     4.35323416E-07   # C7'
     0305 6421   00   0     3.30482999E-07   # C8
     0305 6421   00   2     9.77573562E-06   # C8
     0305 6321   00   2     3.96547976E-07   # C8'
 03051111 4133   00   2     1.01805533E-07   # C9 e+e-
 03051111 4233   00   2     1.56605583E-06   # C9' e+e-
 03051111 4137   00   2     1.86878846E-06   # C10 e+e-
 03051111 4237   00   2    -1.15645436E-05   # C10' e+e-
 03051313 4133   00   2     1.01804901E-07   # C9 mu+mu-
 03051313 4233   00   2     1.56605573E-06   # C9' mu+mu-
 03051313 4137   00   2     1.86878920E-06   # C10 mu+mu-
 03051313 4237   00   2    -1.15645439E-05   # C10' mu+mu-
 03051212 4137   00   2    -3.82047467E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     2.50026305E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -3.82047435E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     2.50026305E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -3.82038276E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     2.50026305E-06   # C11' nu_3 nu_3
