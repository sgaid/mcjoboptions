# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  21:06
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.47075178E+00  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.40414693E+03  # scale for input parameters
    1    2.48032172E+02  # M_1
    2    2.39928705E+02  # M_2
    3    2.67592642E+03  # M_3
   11   -6.40138778E+03  # A_t
   12    1.72106293E+03  # A_b
   13    1.96225988E+02  # A_tau
   23   -1.89208877E+03  # mu
   25    5.22483498E+00  # tan(beta)
   26    8.92018932E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.16854791E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.72408228E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.76941682E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.40414693E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.40414693E+03  # (SUSY scale)
  1  1     8.53655709E-06   # Y_u(Q)^DRbar
  2  2     4.33657100E-03   # Y_c(Q)^DRbar
  3  3     1.03128466E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.40414693E+03  # (SUSY scale)
  1  1     8.96263221E-05   # Y_d(Q)^DRbar
  2  2     1.70290012E-03   # Y_s(Q)^DRbar
  3  3     8.88812267E-02   # Y_b(Q)^DRbar
Block Ye Q=  2.40414693E+03  # (SUSY scale)
  1  1     1.56404486E-05   # Y_e(Q)^DRbar
  2  2     3.23394872E-03   # Y_mu(Q)^DRbar
  3  3     5.43896974E-02   # Y_tau(Q)^DRbar
Block Au Q=  2.40414693E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -6.40138770E+03   # A_t(Q)^DRbar
Block Ad Q=  2.40414693E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.72106292E+03   # A_b(Q)^DRbar
Block Ae Q=  2.40414693E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.96225988E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  2.40414693E+03  # soft SUSY breaking masses at Q
   1    2.48032172E+02  # M_1
   2    2.39928705E+02  # M_2
   3    2.67592642E+03  # M_3
  21   -2.87192388E+06  # M^2_(H,d)
  22   -3.47658676E+06  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.16854791E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.72408228E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.76941682E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22708286E+02  # h0
        35     8.92383580E+02  # H0
        36     8.92018932E+02  # A0
        37     8.99669202E+02  # H+
   1000001     1.01021668E+04  # ~d_L
   2000001     1.00771238E+04  # ~d_R
   1000002     1.01018301E+04  # ~u_L
   2000002     1.00805276E+04  # ~u_R
   1000003     1.01021681E+04  # ~s_L
   2000003     1.00771241E+04  # ~s_R
   1000004     1.01018315E+04  # ~c_L
   2000004     1.00805298E+04  # ~c_R
   1000005     2.21975372E+03  # ~b_1
   2000005     2.85476595E+03  # ~b_2
   1000006     2.15635243E+03  # ~t_1
   2000006     2.68041641E+03  # ~t_2
   1000011     1.00215048E+04  # ~e_L-
   2000011     1.00085549E+04  # ~e_R-
   1000012     1.00207691E+04  # ~nu_eL
   1000013     1.00215058E+04  # ~mu_L-
   2000013     1.00085564E+04  # ~mu_R-
   1000014     1.00207700E+04  # ~nu_muL
   1000015     1.00089656E+04  # ~tau_1-
   2000015     1.00218069E+04  # ~tau_2-
   1000016     1.00210104E+04  # ~nu_tauL
   1000021     3.05507704E+03  # ~g
   1000022     2.51962798E+02  # ~chi_10
   1000023     2.67855410E+02  # ~chi_20
   1000025     1.89835844E+03  # ~chi_30
   1000035     1.90008294E+03  # ~chi_40
   1000024     2.68027594E+02  # ~chi_1+
   1000037     1.90090388E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.85631804E-01   # alpha
Block Hmix Q=  2.40414693E+03  # Higgs mixing parameters
   1   -1.89208877E+03  # mu
   2    5.22483498E+00  # tan[beta](Q)
   3    2.43464562E+02  # v(Q)
   4    7.95697775E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.45528863E-01   # Re[R_st(1,1)]
   1  2     3.25538277E-01   # Re[R_st(1,2)]
   2  1    -3.25538277E-01   # Re[R_st(2,1)]
   2  2     9.45528863E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99970040E-01   # Re[R_sb(1,1)]
   1  2     7.74076943E-03   # Re[R_sb(1,2)]
   2  1    -7.74076943E-03   # Re[R_sb(2,1)]
   2  2    -9.99970040E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -6.94255432E-02   # Re[R_sta(1,1)]
   1  2     9.97587136E-01   # Re[R_sta(1,2)]
   2  1    -9.97587136E-01   # Re[R_sta(2,1)]
   2  2    -6.94255432E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.99442402E-01   # Re[N(1,1)]
   1  2     2.51424360E-02   # Re[N(1,2)]
   1  3    -2.19417917E-02   # Re[N(1,3)]
   1  4     1.14013722E-03   # Re[N(1,4)]
   2  1     2.42159635E-02   # Re[N(2,1)]
   2  2    -9.98846565E-01   # Re[N(2,2)]
   2  3    -4.14162527E-02   # Re[N(2,3)]
   2  4     1.95447651E-03   # Re[N(2,4)]
   3  1    -1.53931989E-02   # Re[N(3,1)]
   3  2     2.75313951E-02   # Re[N(3,2)]
   3  3    -7.06353629E-01   # Re[N(3,3)]
   3  4    -7.07156010E-01   # Re[N(3,4)]
   4  1    -1.70739792E-02   # Re[N(4,1)]
   4  2     3.02558928E-02   # Re[N(4,2)]
   4  3    -7.06305743E-01   # Re[N(4,3)]
   4  4     7.07053929E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1     9.98331431E-01   # Re[U(1,1)]
   1  2     5.77438666E-02   # Re[U(1,2)]
   2  1    -5.77438666E-02   # Re[U(2,1)]
   2  2     9.98331431E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.99996239E-01   # Re[V(1,1)]
   1  2     2.74257263E-03   # Re[V(1,2)]
   2  1     2.74257263E-03   # Re[V(2,1)]
   2  2    -9.99996239E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02237583E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.98921925E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     5.86337714E-04    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.20496115E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     2.71239909E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44690566E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.51637562E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.95903160E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.05640833E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     1.26556924E-04    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.06811546E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     1.88934031E-03    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02276366E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.98844965E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     5.86363549E-04    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.39702197E-04    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     2.90436808E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
DECAY   1000013     1.44692519E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.51624883E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.95899221E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.12322283E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     1.33236262E-04    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.06803418E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     1.88931503E-03    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.17791125E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.65722519E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     5.13617504E-03    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     5.16930082E-03    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     5.18478673E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     9.30978051E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     9.47743833E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.44784282E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     9.62667740E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.94098986E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     2.10918144E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.14113162E-03    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     6.03116934E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     2.26699296E-03    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44693716E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     7.88071937E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     3.11608057E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.62223711E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     4.39277811E-04    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     6.08778986E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44695667E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     7.88061381E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     3.11603883E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.62218864E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     4.39271933E-04    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.08770880E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.45245432E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.85097391E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     3.10431908E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.60857785E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     4.37621320E-04    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     6.06494715E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     3.76515902E-03    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.61542156E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.48424906E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.91506586E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.92801755E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.30839890E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.63613776E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.11640285E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     3.48002921E-04    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.30236244E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.61543262E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.48423555E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.91504969E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.92805948E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.30839250E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.63610880E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.11639704E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     3.52441011E-04    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.30232002E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     3.13201129E+01   # ~b_1
#    BR                NDA      ID1      ID2
     7.11710087E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     3.04885428E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     4.17731531E-04    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.17432514E-04    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     5.96403082E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     9.07592246E-02    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.99370698E+00   # ~b_2
#    BR                NDA      ID1      ID2
     7.85994801E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     4.54115923E-04    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     3.51265030E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     3.50476395E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     7.06263624E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     3.30056946E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.42447039E-02    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     2.54417110E-02    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     6.78717250E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.30894138E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.66874858E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.92788803E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.31098120E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     5.53954869E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.12011022E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     8.30210994E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.78724441E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.30890692E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.66864865E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.92792975E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.31096927E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     5.53952010E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.12010450E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     8.30206747E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     2.65081069E+01   # ~t_1
#    BR                NDA      ID1      ID2
     3.14551345E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     3.01924857E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.89968534E-02    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.82118399E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     6.18798986E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     9.79183716E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     8.20492099E-04    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.33368078E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.93946766E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     9.27782272E-03    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     5.77462692E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     5.56927870E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.88339740E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     9.38700756E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.08124413E-01    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.95070429E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.21989552E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     3.39107291E-09   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.38969149E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.28744430E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.12977960E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.12975591E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.06332837E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     2.18170866E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     5.84601443E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     1.63546657E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     3.96075675E-02    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     1.25242159E-01    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     1.67975576E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.29225430E-01    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     1.70328214E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     1.28957835E-01    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     9.11981626E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.16221183E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     5.34809154E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     3.38975957E-12   # chi^0_2
#    BR                NDA      ID1      ID2
     2.48953775E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     9.84828645E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     9.07164914E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.25202595E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.25152938E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     8.69937054E-02    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     2.54191922E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     2.54054345E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     2.17969227E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.51876082E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     2.16487577E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     1.69482335E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     1.69482335E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     1.28701395E-01    2          37  -1000024   # BR(chi^0_3 -> H^+ chi^-_1)
     1.28701395E-01    2         -37   1000024   # BR(chi^0_3 -> H^- chi^+_1)
     2.71478896E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     9.51904001E-02    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     6.23845982E-03    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     2.38896193E-02    2     1000023        36   # BR(chi^0_3 -> chi^0_2 A^0)
     1.99468086E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     7.98615074E-02    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     2.70081341E-02    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
     1.08210903E-01    2     1000023        35   # BR(chi^0_3 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.13044303E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     3.77479077E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     5.61199846E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     5.61199846E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     2.17032500E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.69280705E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.69280705E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     9.88080668E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     9.88080668E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     2.20475805E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     7.88026578E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.35741266E-02    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     5.19808116E-02    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     4.24399318E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.69943225E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.25770570E-02    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     5.03967510E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.37139549E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     4.62635805E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     8.02555491E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     8.02555491E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     4.65863549E+01   # ~g
#    BR                NDA      ID1      ID2
     2.07882275E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.07882275E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     6.00591983E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     6.00591983E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     2.16320598E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.16320598E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.56050443E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.56050443E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
     1.18330334E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
     1.15529947E-04    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.19499790E-03   # Gamma(h0)
     2.63014330E-03   2        22        22   # BR(h0 -> photon photon)
     1.46651722E-03   2        22        23   # BR(h0 -> photon Z)
     2.50605520E-02   2        23        23   # BR(h0 -> Z Z)
     2.16788378E-01   2       -24        24   # BR(h0 -> W W)
     8.26687692E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.16540404E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.29765127E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.62464858E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.40087159E-07   2        -2         2   # BR(h0 -> Up up)
     2.71884959E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.00006372E-07   2        -1         1   # BR(h0 -> Down down)
     2.17007668E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.77503141E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.28503249E+00   # Gamma(HH)
     2.37060301E-06   2        22        22   # BR(HH -> photon photon)
     1.54597876E-07   2        22        23   # BR(HH -> photon Z)
     2.56611598E-03   2        23        23   # BR(HH -> Z Z)
     3.88161033E-03   2       -24        24   # BR(HH -> W W)
     9.17339764E-04   2        21        21   # BR(HH -> gluon gluon)
     2.79561256E-09   2       -11        11   # BR(HH -> Electron electron)
     1.24427505E-04   2       -13        13   # BR(HH -> Muon muon)
     3.59287942E-02   2       -15        15   # BR(HH -> Tau tau)
     5.20656441E-11   2        -2         2   # BR(HH -> Up up)
     1.00967760E-05   2        -4         4   # BR(HH -> Charm charm)
     6.82831156E-01   2        -6         6   # BR(HH -> Top top)
     2.42147452E-07   2        -1         1   # BR(HH -> Down down)
     8.75764695E-05   2        -3         3   # BR(HH -> Strange strange)
     2.33916261E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.10045075E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     4.84715665E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     3.52585460E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     6.40859613E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.97154729E-02   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.41150725E+00   # Gamma(A0)
     3.06738944E-06   2        22        22   # BR(A0 -> photon photon)
     2.78373256E-07   2        22        23   # BR(A0 -> photon Z)
     1.38938417E-03   2        21        21   # BR(A0 -> gluon gluon)
     2.50518602E-09   2       -11        11   # BR(A0 -> Electron electron)
     1.11501166E-04   2       -13        13   # BR(A0 -> Muon muon)
     3.21968060E-02   2       -15        15   # BR(A0 -> Tau tau)
     4.66983279E-11   2        -2         2   # BR(A0 -> Up up)
     9.05274612E-06   2        -4         4   # BR(A0 -> Charm charm)
     7.26082782E-01   2        -6         6   # BR(A0 -> Top top)
     2.17004284E-07   2        -1         1   # BR(A0 -> Down down)
     7.84830771E-05   2        -3         3   # BR(A0 -> Strange strange)
     2.09678247E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.47605055E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     6.50001065E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     4.78300270E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     8.79909266E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.52675105E-03   2        23        25   # BR(A0 -> Z h0)
     1.70639286E-34   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.68349451E+00   # Gamma(Hp)
     2.64500097E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     1.13082049E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     3.19858076E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.98731711E-07   2        -1         2   # BR(Hp -> Down up)
     3.30235207E-06   2        -3         2   # BR(Hp -> Strange up)
     2.11993059E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.91755007E-07   2        -1         4   # BR(Hp -> Down charm)
     8.20698780E-05   2        -3         4   # BR(Hp -> Strange charm)
     2.96881008E-04   2        -5         4   # BR(Hp -> Bottom charm)
     4.92870122E-05   2        -1         6   # BR(Hp -> Down top)
     1.07471679E-03   2        -3         6   # BR(Hp -> Strange top)
     9.57350871E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.86461755E-05   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     5.96159896E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     3.06088672E-03   2        24        25   # BR(Hp -> W h0)
     1.64835046E-08   2        24        35   # BR(Hp -> W HH)
     2.10404639E-08   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.64006841E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.73348937E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.72989006E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00131848E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.53130280E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.66315119E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.64006841E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.73348937E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.72989006E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99987925E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.20745086E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99987925E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.20745086E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25020655E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    3.24357846E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    2.02101860E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.20745086E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99987925E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.66475383E-04   # BR(b -> s gamma)
    2    1.59220312E-06   # BR(b -> s mu+ mu-)
    3    3.53557041E-05   # BR(b -> s nu nu)
    4    2.53820965E-15   # BR(Bd -> e+ e-)
    5    1.08429440E-10   # BR(Bd -> mu+ mu-)
    6    2.26989345E-08   # BR(Bd -> tau+ tau-)
    7    8.56434890E-14   # BR(Bs -> e+ e-)
    8    3.65868728E-09   # BR(Bs -> mu+ mu-)
    9    7.76048929E-07   # BR(Bs -> tau+ tau-)
   10    9.65676028E-05   # BR(B_u -> tau nu)
   11    9.97506367E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.44731210E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.94626971E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16970679E-03   # epsilon_K
   17    2.28178687E-15   # Delta(M_K)
   18    2.48694157E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.30704441E-11   # BR(K^+ -> pi^+ nu nu)
   20   -3.99137707E-17   # Delta(g-2)_electron/2
   21   -1.70643883E-12   # Delta(g-2)_muon/2
   22   -4.82726332E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -2.14201605E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.35259252E-01   # C7
     0305 4322   00   2    -8.25084126E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.46041049E-01   # C8
     0305 6321   00   2    -9.34269877E-04   # C8'
 03051111 4133   00   0     1.60839997E+00   # C9 e+e-
 03051111 4133   00   2     1.60957243E+00   # C9 e+e-
 03051111 4233   00   2     4.82381319E-06   # C9' e+e-
 03051111 4137   00   0    -4.43109207E+00   # C10 e+e-
 03051111 4137   00   2    -4.43954409E+00   # C10 e+e-
 03051111 4237   00   2    -3.56507029E-05   # C10' e+e-
 03051313 4133   00   0     1.60839997E+00   # C9 mu+mu-
 03051313 4133   00   2     1.60957243E+00   # C9 mu+mu-
 03051313 4233   00   2     4.82380613E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.43109207E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43954409E+00   # C10 mu+mu-
 03051313 4237   00   2    -3.56506960E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50718804E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     7.73989002E-06   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50718804E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     7.73989155E-06   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50718804E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     7.74032126E-06   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85802378E-07   # C7
     0305 4422   00   2    -3.28864480E-06   # C7
     0305 4322   00   2    -4.42243936E-07   # C7'
     0305 6421   00   0     3.30465251E-07   # C8
     0305 6421   00   2     5.50108919E-06   # C8
     0305 6321   00   2    -1.13627737E-07   # C8'
 03051111 4133   00   2     1.62188505E-06   # C9 e+e-
 03051111 4233   00   2     1.66381603E-07   # C9' e+e-
 03051111 4137   00   2     8.98561266E-06   # C10 e+e-
 03051111 4237   00   2    -1.24693499E-06   # C10' e+e-
 03051313 4133   00   2     1.62188494E-06   # C9 mu+mu-
 03051313 4233   00   2     1.66381602E-07   # C9' mu+mu-
 03051313 4137   00   2     8.98561277E-06   # C10 mu+mu-
 03051313 4237   00   2    -1.24693500E-06   # C10' mu+mu-
 03051212 4137   00   2    -1.90186370E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     2.70730190E-07   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.90186370E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     2.70730190E-07   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.90186214E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     2.70730189E-07   # C11' nu_3 nu_3
