# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.03.2021,  09:27
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.71024436E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.41020155E+03  # scale for input parameters
    1    1.66523699E+03  # M_1
    2    2.89051563E+02  # M_2
    3    2.63804258E+03  # M_3
   11   -1.53669979E+01  # A_t
   12    1.88616490E+03  # A_b
   13    1.29278751E+03  # A_tau
   23   -1.00342078E+02  # mu
   25    5.71552862E+01  # tan(beta)
   26    2.15015733E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.09878231E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.61287870E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.28577846E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.41020155E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.41020155E+03  # (SUSY scale)
  1  1     8.38565533E-06   # Y_u(Q)^DRbar
  2  2     4.25991291E-03   # Y_c(Q)^DRbar
  3  3     1.01305452E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.41020155E+03  # (SUSY scale)
  1  1     9.63105049E-04   # Y_d(Q)^DRbar
  2  2     1.82989959E-02   # Y_s(Q)^DRbar
  3  3     9.55098415E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.41020155E+03  # (SUSY scale)
  1  1     1.68068874E-04   # Y_e(Q)^DRbar
  2  2     3.47513127E-02   # Y_mu(Q)^DRbar
  3  3     5.84459910E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.41020155E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -1.53669980E+01   # A_t(Q)^DRbar
Block Ad Q=  2.41020155E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.88616610E+03   # A_b(Q)^DRbar
Block Ae Q=  2.41020155E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.29278734E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.41020155E+03  # soft SUSY breaking masses at Q
   1    1.66523699E+03  # M_1
   2    2.89051563E+02  # M_2
   3    2.63804258E+03  # M_3
  21    4.04899020E+06  # M^2_(H,d)
  22    1.15199961E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.09878231E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.61287870E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.28577846E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.20657541E+02  # h0
        35     2.14752742E+03  # H0
        36     2.15015733E+03  # A0
        37     2.14999908E+03  # H+
   1000001     1.01022270E+04  # ~d_L
   2000001     1.00769161E+04  # ~d_R
   1000002     1.01018502E+04  # ~u_L
   2000002     1.00798455E+04  # ~u_R
   1000003     1.01022497E+04  # ~s_L
   2000003     1.00769585E+04  # ~s_R
   1000004     1.01018730E+04  # ~c_L
   2000004     1.00798479E+04  # ~c_R
   1000005     2.16780212E+03  # ~b_1
   2000005     2.34186833E+03  # ~b_2
   1000006     2.17173689E+03  # ~t_1
   2000006     2.67485051E+03  # ~t_2
   1000011     1.00213711E+04  # ~e_L-
   2000011     1.00078801E+04  # ~e_R-
   1000012     1.00205938E+04  # ~nu_eL
   1000013     1.00214775E+04  # ~mu_L-
   2000013     1.00080876E+04  # ~mu_R-
   1000014     1.00207001E+04  # ~nu_muL
   1000015     1.00519000E+04  # ~tau_1-
   2000015     1.00673488E+04  # ~tau_2-
   1000016     1.00511940E+04  # ~nu_tauL
   1000021     3.00565845E+03  # ~g
   1000022     9.84604578E+01  # ~chi_10
   1000023     1.20947926E+02  # ~chi_20
   1000025     3.41173443E+02  # ~chi_30
   1000035     1.68961388E+03  # ~chi_40
   1000024     1.06059067E+02  # ~chi_1+
   1000037     3.41479269E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.75879963E-02   # alpha
Block Hmix Q=  2.41020155E+03  # Higgs mixing parameters
   1   -1.00342078E+02  # mu
   2    5.71552862E+01  # tan[beta](Q)
   3    2.43515394E+02  # v(Q)
   4    4.62317654E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.99968310E-01   # Re[R_st(1,1)]
   1  2     7.96114281E-03   # Re[R_st(1,2)]
   2  1    -7.96114281E-03   # Re[R_st(2,1)]
   2  2     9.99968310E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99822941E-01   # Re[R_sb(1,1)]
   1  2     1.88171836E-02   # Re[R_sb(1,2)]
   2  1    -1.88171836E-02   # Re[R_sb(2,1)]
   2  2    -9.99822941E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -9.99262808E-01   # Re[R_sta(1,1)]
   1  2     3.83906367E-02   # Re[R_sta(1,2)]
   2  1    -3.83906367E-02   # Re[R_sta(2,1)]
   2  2    -9.99262808E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -1.75371766E-02   # Re[N(1,1)]
   1  2     2.30584633E-01   # Re[N(1,2)]
   1  3     7.33374246E-01   # Re[N(1,3)]
   1  4     6.39285062E-01   # Re[N(1,4)]
   2  1    -1.80830134E-02   # Re[N(2,1)]
   2  2     1.37123094E-01   # Re[N(2,2)]
   2  3    -6.75167207E-01   # Re[N(2,3)]
   2  4     7.24582296E-01   # Re[N(2,4)]
   3  1     8.40425057E-03   # Re[N(3,1)]
   3  2     9.63340806E-01   # Re[N(3,2)]
   3  3    -7.94337133E-02   # Re[N(3,3)]
   3  4    -2.56113539E-01   # Re[N(3,4)]
   4  1     9.99647348E-01   # Re[N(4,1)]
   4  2    -1.57331019E-03   # Re[N(4,2)]
   4  3     1.32030245E-03   # Re[N(4,3)]
   4  4     2.64756655E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.16541217E-01   # Re[U(1,1)]
   1  2    -9.93185856E-01   # Re[U(1,2)]
   2  1     9.93185856E-01   # Re[U(2,1)]
   2  2    -1.16541217E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -3.74414928E-01   # Re[V(1,1)]
   1  2     9.27261269E-01   # Re[V(1,2)]
   2  1     9.27261269E-01   # Re[V(2,1)]
   2  2     3.74414928E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     4.74621815E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     3.25851031E-04    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     3.46402755E-04    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.99252967E-01    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.43916934E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     1.49957394E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     4.97806049E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.86424216E-01    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     8.20242266E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     8.32358092E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.03254176E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     4.79440470E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     3.01936647E-03    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     2.62839320E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.05482068E-04    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     9.89232820E-01    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     4.94598143E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.44159188E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     1.58687891E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     5.73085786E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.85956453E-01    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     8.18873622E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     8.30968175E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.02246856E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     2.12615186E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.86424495E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.47825965E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.94698789E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     5.59452648E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     9.35664953E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     4.05748838E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     1.84613720E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.95353193E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.71246007E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     4.42168291E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.58332279E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     3.61199168E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     9.44767135E-03    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.43922357E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     1.76416070E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     6.60078778E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     2.81101533E-01    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     8.29862342E-02    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     8.59028452E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.25766992E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44164591E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.76121515E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     6.58976671E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     2.80632202E-01    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     8.28477778E-02    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     8.74063158E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.24911786E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     2.12656391E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.19760245E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     4.48095478E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.90828557E-01    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     5.63552775E-02    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     3.75085565E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     3.61273622E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
DECAY   2000001     6.65486841E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.97948049E-03    2     1000035         1   # BR(~d_R -> chi^0_4 d)
     9.92014002E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.96909110E+02   # ~d_L
#    BR                NDA      ID1      ID2
     3.04713964E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.09878284E-03    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     5.15026711E-02    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     1.70021419E-03    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.51532172E-03    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.09827101E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.31308769E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.65624582E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.97786685E-03    2     1000035         3   # BR(~s_R -> chi^0_4 s)
     9.91814522E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.96982220E+02   # ~s_L
#    BR                NDA      ID1      ID2
     3.09214852E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.13705922E-03    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     5.14985924E-02    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     1.70006261E-03    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.51911937E-03    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.09817915E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.31235103E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.09866497E+02   # ~b_1
#    BR                NDA      ID1      ID2
     1.97691335E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.63255119E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     7.80396058E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.31475826E-04    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.43150968E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.17431496E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     8.44771079E+01   # ~b_2
#    BR                NDA      ID1      ID2
     2.67996892E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.29035617E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     3.76063059E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     3.55543656E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.87422980E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     8.04148952E-03    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
DECAY   2000002     6.81702848E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.11687802E-02    2     1000035         2   # BR(~u_R -> chi^0_4 u)
     9.68807945E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.96895203E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.88627038E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.00013561E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.18226719E-02    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     1.64111522E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.56404713E-02    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.57291118E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.31280224E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.81710316E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.11684503E-02    2     1000035         4   # BR(~c_R -> chi^0_4 c)
     9.68797607E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.96968291E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.88788150E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.00244761E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.18183367E-02    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     1.64097160E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.57221170E-02    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.57216898E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.31206556E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.13812832E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.88724300E-03    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     1.60855916E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     6.52650544E-04    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     2.02119633E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     9.88591161E-02    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     3.37421341E-04    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     3.64835484E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.40465360E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     2.99871761E-02    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.10246783E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.99664433E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.56468597E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     3.20417411E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.96512555E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     4.22794243E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     6.89457787E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.91016136E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.40835515E-04    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
DECAY   1000024     3.31463121E-08   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.57709831E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.12247716E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.19236645E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.19121395E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     9.16844128E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     2.41061634E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.46238124E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     3.29653836E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.46946203E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.77053761E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.94455492E-05   # chi^0_2
#    BR                NDA      ID1      ID2
     4.10262788E-02    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     8.80686469E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     8.44577256E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.12920582E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.12897719E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     7.62552450E-02    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     2.54829865E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     2.54748519E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     2.33029422E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.51008647E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     4.40196015E-02    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     4.40196015E-02    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     4.24869116E-02    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     4.24869116E-02    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     1.46732069E-02    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     1.46732069E-02    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     1.46696122E-02    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     1.46696122E-02    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
     1.37028556E-02    3     1000024        15       -16   # BR(chi^0_2 -> chi^+_1 tau^- nu_bar_tau)
     1.37028556E-02    3    -1000024       -15        16   # BR(chi^0_2 -> chi^-_1 tau^+ nu_tau)
DECAY   1000025     2.86971728E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.58504624E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.58504624E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     4.48874831E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.07357480E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     8.94032010E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.41207400E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     4.73488576E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     1.93006427E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.93006427E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     4.55174185E-02    2     1000037       -24   # BR(chi^0_4 -> chi^+_2 W^-)
     4.55174185E-02    2    -1000037        24   # BR(chi^0_4 -> chi^-_2 W^+)
     8.74390060E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.37706083E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.36841367E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     9.40140700E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.35501361E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     2.82132903E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.82080464E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     5.06623497E-04    3     1000022         5        -5   # BR(chi^0_4 -> chi^0_1 b b_bar)
     4.94989770E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     4.16961333E-04    3     1000023         5        -5   # BR(chi^0_4 -> chi^0_2 b b_bar)
     7.73614796E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     6.52296073E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     6.52296073E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
     1.37068157E-03    3     1000037         5        -6   # BR(chi^0_4 -> chi^+_2 b t_bar)
     1.37068157E-03    3    -1000037        -5         6   # BR(chi^0_4 -> chi^-_2 b_bar t)
DECAY   1000021     6.63850391E+01   # ~g
#    BR                NDA      ID1      ID2
     6.67459479E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     6.67459479E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.50098181E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.50098181E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.53023973E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     2.53023973E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.54299649E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.54299649E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.03423164E-01    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.03423164E-01    2    -2000005         5   # BR(~g -> ~b^*_2 b)
     1.08452448E-04    2     1000022        21   # BR(~g -> chi^0_1 g)
     1.14169901E-04    2     1000023        21   # BR(~g -> chi^0_2 g)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     2.91383659E-03   # Gamma(h0)
     2.69367058E-03   2        22        22   # BR(h0 -> photon photon)
     1.31811256E-03   2        22        23   # BR(h0 -> photon Z)
     2.09187736E-02   2        23        23   # BR(h0 -> Z Z)
     1.88602339E-01   2       -24        24   # BR(h0 -> W W)
     8.74567221E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.36602520E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.38687841E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.88158489E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.52634458E-07   2        -2         2   # BR(h0 -> Up up)
     2.96222149E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.25038213E-07   2        -1         1   # BR(h0 -> Down down)
     2.26060696E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.00106787E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.03735554E+02   # Gamma(HH)
     8.10176056E-08   2        22        22   # BR(HH -> photon photon)
     1.55527840E-07   2        22        23   # BR(HH -> photon Z)
     1.34540874E-07   2        23        23   # BR(HH -> Z Z)
     6.44210805E-08   2       -24        24   # BR(HH -> W W)
     1.68611364E-05   2        21        21   # BR(HH -> gluon gluon)
     8.49320732E-09   2       -11        11   # BR(HH -> Electron electron)
     3.78119784E-04   2       -13        13   # BR(HH -> Muon muon)
     1.09199690E-01   2       -15        15   # BR(HH -> Tau tau)
     1.49942621E-14   2        -2         2   # BR(HH -> Up up)
     2.90853607E-09   2        -4         4   # BR(HH -> Charm charm)
     2.25400951E-04   2        -6         6   # BR(HH -> Top top)
     6.53681418E-07   2        -1         1   # BR(HH -> Down down)
     2.36433695E-04   2        -3         3   # BR(HH -> Strange strange)
     6.42706074E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.66572410E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     6.68918455E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     6.68918455E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     2.10936379E-03   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     6.00253254E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.80774094E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     3.83954134E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.79365884E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.80804920E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     3.35116445E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.93222870E-03   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.05177194E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     9.26099710E-06   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     6.90644609E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     9.66404433E+01   # Gamma(A0)
     1.12485026E-09   2        22        22   # BR(A0 -> photon photon)
     4.13128376E-08   2        22        23   # BR(A0 -> photon Z)
     2.45106385E-05   2        21        21   # BR(A0 -> gluon gluon)
     8.28459187E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.68831605E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.06517635E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.42662151E-14   2        -2         2   # BR(A0 -> Up up)
     2.76713362E-09   2        -4         4   # BR(A0 -> Charm charm)
     2.21116893E-04   2        -6         6   # BR(A0 -> Top top)
     6.37526680E-07   2        -1         1   # BR(A0 -> Down down)
     2.30590416E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.26863479E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.83009227E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     7.22422871E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     7.22422871E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.93229509E-03   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     6.11648140E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.84416897E-04   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     4.00200183E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     2.50906201E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     2.11976094E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.74876220E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.50578253E-03   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     9.81301270E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     3.07091642E-05   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     1.99228191E-07   2        23        25   # BR(A0 -> Z h0)
     1.42892484E-12   2        23        35   # BR(A0 -> Z HH)
     9.80075497E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.21142476E+02   # Gamma(Hp)
     9.56999874E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     4.09147335E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.15730055E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     6.38834449E-07   2        -1         2   # BR(Hp -> Down up)
     1.07412020E-05   2        -3         2   # BR(Hp -> Strange up)
     7.12505726E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.98506395E-08   2        -1         4   # BR(Hp -> Down charm)
     2.30228483E-04   2        -3         4   # BR(Hp -> Strange charm)
     9.97762602E-04   2        -5         4   # BR(Hp -> Bottom charm)
     1.44781869E-08   2        -1         6   # BR(Hp -> Down top)
     6.53073892E-07   2        -3         6   # BR(Hp -> Strange top)
     6.71268851E-01   2        -5         6   # BR(Hp -> Bottom top)
     7.87366630E-04   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     7.87567618E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     4.65614161E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     5.87152221E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     6.52400965E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     6.48125310E-08   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     3.15428406E-03   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     3.46460940E-05   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.59090642E-07   2        24        25   # BR(Hp -> W h0)
     1.03588645E-12   2        24        35   # BR(Hp -> W HH)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.01072657E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    3.26671601E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    3.26672674E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99996716E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.09400402E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.06116819E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.01072657E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    3.26671601E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    3.26672674E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999991E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    8.75849590E-09        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999991E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    8.75849590E-09        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26127657E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.33311291E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.71420151E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    8.75849590E-09        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999991E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.43756071E-04   # BR(b -> s gamma)
    2    1.58590187E-06   # BR(b -> s mu+ mu-)
    3    3.52012393E-05   # BR(b -> s nu nu)
    4    1.30418065E-15   # BR(Bd -> e+ e-)
    5    5.56973811E-11   # BR(Bd -> mu+ mu-)
    6    1.07633510E-08   # BR(Bd -> tau+ tau-)
    7    4.55965498E-14   # BR(Bs -> e+ e-)
    8    1.94745470E-09   # BR(Bs -> mu+ mu-)
    9    3.88349551E-07   # BR(Bs -> tau+ tau-)
   10    9.31751207E-05   # BR(B_u -> tau nu)
   11    9.62463326E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42407789E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93511457E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15940499E-03   # epsilon_K
   17    2.28168389E-15   # Delta(M_K)
   18    2.47629381E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28170099E-11   # BR(K^+ -> pi^+ nu nu)
   20   -5.33977707E-16   # Delta(g-2)_electron/2
   21   -2.28296060E-11   # Delta(g-2)_muon/2
   22   -6.48754916E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    9.66816927E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.15516235E-01   # C7
     0305 4322   00   2    -2.95576060E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.13604343E-01   # C8
     0305 6321   00   2    -2.73615458E-04   # C8'
 03051111 4133   00   0     1.60991979E+00   # C9 e+e-
 03051111 4133   00   2     1.61073742E+00   # C9 e+e-
 03051111 4233   00   2     6.77935253E-04   # C9' e+e-
 03051111 4137   00   0    -4.43261189E+00   # C10 e+e-
 03051111 4137   00   2    -4.42608751E+00   # C10 e+e-
 03051111 4237   00   2    -5.13422179E-03   # C10' e+e-
 03051313 4133   00   0     1.60991979E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61073721E+00   # C9 mu+mu-
 03051313 4233   00   2     6.77930772E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.43261189E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.42608772E+00   # C10 mu+mu-
 03051313 4237   00   2    -5.13421961E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50393594E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.11441172E-03   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50393594E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.11441257E-03   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50393594E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.11465126E-03   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85817196E-07   # C7
     0305 4422   00   2    -3.52127951E-05   # C7
     0305 4322   00   2    -1.39242708E-06   # C7'
     0305 6421   00   0     3.30477944E-07   # C8
     0305 6421   00   2     1.23874363E-05   # C8
     0305 6321   00   2     1.67225322E-08   # C8'
 03051111 4133   00   2     1.78319986E-06   # C9 e+e-
 03051111 4233   00   2     1.45082237E-05   # C9' e+e-
 03051111 4137   00   2    -1.54330274E-07   # C10 e+e-
 03051111 4237   00   2    -1.09910860E-04   # C10' e+e-
 03051313 4133   00   2     1.78319117E-06   # C9 mu+mu-
 03051313 4233   00   2     1.45082135E-05   # C9' mu+mu-
 03051313 4137   00   2    -1.54321266E-07   # C10 mu+mu-
 03051313 4237   00   2    -1.09910891E-04   # C10' mu+mu-
 03051212 4137   00   2     6.87348312E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     2.38567718E-05   # C11' nu_1 nu_1
 03051414 4137   00   2     6.87352438E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     2.38567718E-05   # C11' nu_2 nu_2
 03051616 4137   00   2     6.88496555E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     2.38567718E-05   # C11' nu_3 nu_3
