from MadGraphControl import MadGraphUtils
from MadGraphControl.MadGraphUtils import *
import subprocess
import os

gridpack_mode=True

if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
if hasattr(runArgs,'maxEvents'):
    nEvents = int(runArgs.maxEvents)
else:
    beamEnergy = 6500.
    nEvents = 10000
safe_factor = 7
nEvents = int(nEvents*safe_factor)

# MadGraph info
process = {
    2:  'generate p p > j j h $$ w+ w- z QCD=0, (h > chi2 chi2, (chi2 > chi1 zp, zp > f- f+), (chi2 > chi1 zp, zp > f- f+))' ,
    4:  'generate p p > j j h $$ w+ w- z QCD=0, (h > chi2 chi2, (chi2 > chi1 hs, (hs > zp zp, zp > f- f+)), (chi2 > chi1 hs, (hs > zp zp, zp > f- f+)))' ,
    }
modelcode = 'iDM'
rname = 'GridRun_' + str(runArgs.randomSeed)


pcard_params = {"HIDDEN" : {"mZDinput" : params["mZDinput"],
                            "MHSinput" : params["MHSinput"],
                            "epsilon"  : params["epsilon"],
                            "gXmu" : params["gXmu"],
                            "gXe"  : params["gXe"],
                            "gXpi" : params["gXpi"] },
                "HIGGS" : {"MHinput" : params["mH"]},
                "MASS" : { "MChi1" : params["MChi1"],
                           "MChi2" : params["MChi2"]},
                "DECAY" : {"WZp" : params["WZp"],
                           "WHS" : "DECAY 3000005 1.000000e-08 # WHS"}
            }



rcard_params = {'auto_ptj_mjj': '   True  ', 'ptl': ' 10.0  ', 'ptj': ' 10.0  ', 'drll': ' 0.4 ', 'ptj4max': ' -1.0  ', 'ptb': ' 0.0  ', 'pta': ' 10.0  ', 'mmnl': ' 0.0   ', 'elmax': ' -1.0   ', 'ht3min': ' 0.0   ', 'draamax': ' -1.0  ', 'alpsfact': ' 1.0 ', 'isoem': ' True ', 'clusinfo': ' True ', 'ptl1max': ' -1.0  ', 'ptj4min': ' 0.0   ', 'mmaa': ' 0.0   ', 'epsgamma': ' 1.0 ', 'xetamin': ' 0.0   ', 'r0gamma': ' 0.4 ', 'etaa': ' 2.5  ', 'etab': '  -1.0  ', 'drjjmax': ' -1.0  ', 'ptl3max': ' -1.0  ', 'etajmin': ' 0.0  ', 'ptlmax': ' -1.0  ', 'ptl1min': ' 0.0   ', 'etal': ' 2.5  ', 'dsqrt_q2fact1': ' 91.188  ', 'sys_alpsfact  # \\alpha_s emission scale factors': '0.5 1 2 ', 'ptheavy': ' 0.0  ', 'bwcutoff': '  15.0  ', 'cut_decays': '   False  ', 'ptl2min': ' 0.0   ', 'lhe_version': ' 3.0 ', 'drabmax': ' -1.0  ', 'dparameter': ' 0.4  ', 'ht4min': ' 0.0   ', 'etaj': '  5.0 ', 'ptl3min': ' 0.0   ', 'drblmax': ' -1.0  ', 'mmjjmax': ' -1.0  ', 'drbl': ' 0.0   ', 'el': '  0.0  ', 'drbj': ' 0.0   ', 'ej': '  0.0  ', 'iseed': '  0   ', 'ea': '  0.0  ', 'drbb': ' 0.0   ', 'eb': '  0.0  ', 'htjmin': ' 0.0   ', 'highestmult': ' 1 ', 'mmll': ' 0.0   ', 'ptllmin': ' 0.0   ', 'eamax': ' -1.0   ', 'htjmax': ' -1.0  ', 'scale': ' 91.188  ', 'ptj1min': ' 0.0   ', 'ickkw': ' 0 ', 'misset': ' 0.0  ', 'pdfwgt': ' True ', 'drjl': ' 0.4 ', 'drjj': ' 0.4 ', 'drajmax': ' -1.0  ', 'ihtmax': ' -1.0  ', 'ihtmin': ' 0.0   ', 'ktscheme': ' 1 ', 'xpta': ' 0.0  ', 'xptb': ' 0.0  ', 'sys_matchscale # variation of merging scale': '30 50 ', 'xqcut': ' 0.0   ', 'use_syst': '   True  ', 'xptj': ' 0.0  ', 'mmbbmax': ' -1.0  ', 'xptl': ' 0.0  ', 'ktdurham': ' -1.0    ', 'etaamin': ' 0.0  ', 'gridpack': '  False     ', 'ptjmax': ' -1.0  ', 'lpp1': '     1        ', 'lpp2': '     1        ', 'ht3max': ' -1.0  ', 'run_tag': '  tag_1     ', 'mmaamax': ' -1.0  ', 'sys_scalefact  # factorization/renormalization scale factor': '0.5 1 2 ', 'xn': ' 1.0 ', 'drjlmax': ' -1.0  ', 'mmjj': ' 0.0   ', 'drbbmax': ' -1.0  ', 'dynamical_scale_choice': ' -1 ', 'ht4max': ' -1.0  ', 'ptbmax': ' -1.0  ', 'drllmax': ' -1.0  ', 'time_of_flight': ' -1.0 ', 'ht2max': ' -1.0  ', 'ebmax': ' -1.0   ', 'ptj2min': ' 0.0   ', 'ht2min': ' 0.0   ', 'scalefact': ' 1.0  ', 'deltaeta': ' 0.0   ', 'etalmin': ' 0.0  ', 'drbjmax': ' -1.0  ', 'mmnlmax': ' -1.0  ', 'ptj2max': ' -1.0  ', 'ptllmax': ' -1.0  ', 'lhaid': '     230000    ', 'polbeam1': '     0.0     ', 'polbeam2': '     0.0     ', 'ptl4min': ' 0.0   ', 'ptl2max': ' -1.0  ', 'fixed_ren_scale': ' False ', 'ptj3max': ' -1.0  ', 'ptj3min': ' 0.0   ', 'nhel': '   0  ', 'fixed_fac_scale': ' False        ', 'ejmax': '  -1.0   ', 'ptl4max': ' -1.0  ', 'cutuse': ' 0   ', 'ptj1max': ' -1.0  ', 'ptamax': ' -1.0  ', 'missetmax': ' -1.0  ', 'maxjetflavor': ' 4 ', 'asrwgtflavor': ' 4 ', 'pdlabel': 'nn23lo1', 'ptgmin': ' 0.0 ', 'chcluster': ' False ', 'draa': ' 0.4 ', 'drab': ' 0.0   ', 'dral': ' 0.4 ', 'draj': ' 0.4 ', 'etabmin': ' 0.0  ', 'dsqrt_q2fact2': ' 91.188  ', 'mmllmax': ' -1.0  ', 'dralmax': ' -1.0  ', 'mmbb': ' 0.0   ', 'sys_pdf # matching scales': 'Ct10nlo.LHgrid ', 'ebeam2': '     6500.0     ', 'ebeam1': '     6500.0     ', "nevents" : nEvents}


runArgs.inputGeneratorFile=rname
runArgs.inputfilecheck=rname

# initialise random number generator/sequence
import random
random.seed(runArgs.randomSeed)

# lifetime function
def lifetime(avgtau = 21):
    import math
    t = random.random()
    return -1.0 * avgtau * math.log(t)

# do not run MadGraph if config only is requested
if not opts.config_only:
    proc = ""
    if params["decayMode"] == "normal":
        proc+=("""
import model --modelname %s
define v = ve vm vt
define v~ = ve~ vm~ vt~
define j = g u c d s u~ c~ d~ s~
define f- = e- m- tt- d s u~ c~ b
define f+ = e+ m+ tt+ d~ s~ u c b~
%s
output -f
""" % (modelcode, process[params["nGamma"]]))
    elif params["decayMode"] == "quarks":
        proc += ("""
import model --modelname %s
define v = ve vm vt
define v~ = ve~ vm~ vt~
define j = g u c d s u~ c~ d~ s~
define f- = d s u~ c~
define f+ = d~ s~ u c
%s
output -f
""" % (modelcode, process[params["nGamma"]]))
    elif params["decayMode"] == "electrons":
        proc += ("""
import model --modelname %s
define v = ve vm vt
define v~ = ve~ vm~ vt~
define j = g u c d s u~ c~ d~ s~
define f- = e-
define f+ = e+
%s
output -f
""" % (modelcode, process[params["nGamma"]]))


# generating events in MG
if not is_gen_from_gridpack():
  process_dir = new_process(proc)
else:
  process_dir = MADGRAPH_GRIDPACK_LOCATION


modify_run_card(process_dir=process_dir,settings=rcard_params) ### creates the run card setting the params from the "settings" dictionary

modify_param_card(process_dir=process_dir,params=pcard_params)

#---------------------------------------------------------------------------
# MG5 + Pythia8 setup and process (lhe) generation
#---------------------------------------------------------------------------
try:
    generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
except RuntimeError as rte:
    for an_arg in rte.args:
        if 'Gridpack sucessfully created' in an_arg:
            print 'Handling exception and exiting'
            theApp.finalize()
            theApp.exit()
    print 'Unhandled exception - re-raising'
    raise rte


# hacking LHE file

unzip1 = subprocess.Popen(['gunzip',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz'])
unzip1.wait()
oldlhe = open(process_dir+'/Events/'+rname+'/unweighted_events.lhe','r')
newlhe = open(process_dir+'/Events/'+rname+'/unweighted_events2.lhe','w')

init = True

for line in oldlhe:
    if init==True:
        if '30000016' in line:
            line = line.replace('30000016','3000016')
        elif '30000015' in line:
            line = line.replace('30000015','1000022')
        newlhe.write(line)
        if '</init>' in line:
            init = False
    else:
        newline=line.rstrip('\n')
        columns=newline.split()
        pdgid=columns[0]

        if pdgid == '3000001' and params["avgtau"]>0:
            part1 = line[:-22]
            part2 = "%.5e" % (lifetime(params["avgtau"]))
            part3 = line[-12:]
            newlhe.write(part1+part2+part3)

        elif (pdgid == '-30000016') :
            part1 = ' -3000016'
            part2 = line[10:]
            newlhe.write(part1+part2)
        elif (pdgid == '30000016') :
            part1 = '  3000016'
            part2 = line[10:]
            newlhe.write(part1+part2)
        elif (pdgid == '-30000015') :
            part1 = ' -1000022'
            part2 = line[10:]
            newlhe.write(part1+part2)
        elif (pdgid == '30000015') :
            part1 = '  1000022'
            part2 = line[10:]
            newlhe.write(part1+part2)
        else:
            newlhe.write(line)
    
oldlhe.close()
newlhe.close()

# re-zipping hacked LHE
zip1 = subprocess.Popen(['gzip',process_dir+'/Events/'+rname+'/unweighted_events2.lhe'])
zip1.wait()
shutil.move(process_dir+'/Events/'+rname+'/unweighted_events2.lhe.gz',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz')
os.remove(process_dir+'/Events/'+rname+'/unweighted_events.lhe')

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)                                                                         

#if 'ATHENA_PROC_NUMBER' in os.environ:
#    opts.nprocs = 0

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")



genSeq.Pythia8.Commands += ["Main:timesAllowErrors = 60000"]

#relax the cuts on displaced vertices and non G4 particles
testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxNonG4Energy = 100000000 #in MeV

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.description = 'displaced FRVZ VBF - H -> %sgamma_d + X, mZd=%s, mH=%s' % (params["nGamma"], params["mZDinput"]*1000.,  params["mH"])
evgenConfig.contact  = ['iacopo.longarini@cern.ch' , 'cristiano.sebastiani@cern.ch']
evgenConfig.process="LJ_FRVZdisplaced_VBF"
evgenConfig.nEventsPerJob = 10000




#--------------------------------------------------------------
# VBFForwardJetsFilter
#--------------------------------------------------------------
# https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/GeneratorFilters/share/common/VBFForwardJetsFilter.py
# hard copied for 21.2.23
include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.4)

if not hasattr( filtSeq, "VBFForwardJetsFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import VBFForwardJetsFilter
    filtSeq += VBFForwardJetsFilter()
    pass
VBFForwardJetsFilter = filtSeq.VBFForwardJetsFilter
VBFForwardJetsFilter.JetMinPt = 30.*GeV
VBFForwardJetsFilter.JetMaxEta = 5.
VBFForwardJetsFilter.NJets = 2
VBFForwardJetsFilter.Jet1MinPt = 30.*GeV
VBFForwardJetsFilter.Jet1MaxEta = 5.
VBFForwardJetsFilter.Jet2MinPt = 30.*GeV
VBFForwardJetsFilter.Jet2MaxEta = 5.
VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2 = False
VBFForwardJetsFilter.MassJJ = 750.*GeV
VBFForwardJetsFilter.DeltaEtaJJ = 2.0
VBFForwardJetsFilter.UseLeadingJJ = True
VBFForwardJetsFilter.TruthJetContainer = "AntiKt4TruthJets"
VBFForwardJetsFilter.LGMinPt = 30.*GeV
VBFForwardJetsFilter.LGMaxEta = 2.5
VBFForwardJetsFilter.DeltaRJLG = 0.05
VBFForwardJetsFilter.RatioPtJLG = 0.3

