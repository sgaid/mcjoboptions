from MadGraphControl.MadGraphUtils import *
import math
#-----------------------------------------------------------------
# MadgGaph process
#-----------------------------------------------------------------
if mumu:
    # For dimuon
    process="""
    import model DMsimp_s_spin1 -modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > Y1 > mu+ mu-
    output -f
    """
else:
    process="""
    import model DMsimp_s_spin1 -modelname
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > Y1 > e+ e-
    output -f
    """

#-----------------------------------------------------------------
# Beam energy
#-----------------------------------------------------------------
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#-----------------------------------------------------------------
# Number of events to generate
#-----------------------------------------------------------------
nevents=evgenConfig.nEventsPerJob

if runArgs.maxEvents>0:
    nevents = runArgs.maxEvents * multiplier
else:
    nevents *= multiplier
nevents = int(nevents)

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
    
#-----------------------------------------------------------------
# modify run card
#-----------------------------------------------------------------   
extras = {'lhe_version':'3.0',
          'cut_decays' :'F',
          'pdlabel'    : "'lhapdf'",
          'lhaid'      : 263000,
          'nevents'    : nevents,
          'ptj'        : 1,
          'pta'        : 0,
          'ptl'        : 1,
          'ptl1min'    : 0,
          'ptl2min'    : 0,
          'etaj'       : -1,
          'etal'       : 2.5,
          'etaa'       :-1,
#          'drll'       :0.0,
#          'drjj'       :0.0,
#          'draa'       :0.0,
#          'draj'       :0.0,
#          'drja'       :0.0,
#          'dral'       :0.0,
          'ickkw'      : 1}

process_dir = new_process(process)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)
#-----------------------------------------------------------------
# modify parameters card
#-----------------------------------------------------------------
params = {
    "MASS": {
                    'MXd' : MXd,
                    'MY1' : MY1,
                    'MXc' : MXc
            },
    "DMINPUTS": {
                    'gVXd' : gVXd,
                    'gAXd' : gAXd,
                    'gVd11' : gVd11,
                    'gVu11' : gVu11,
                    'gVd22' : gVd22,
                    'gVu22' : gVu22,
                    'gVd33' : gVd33,
                    'gVu33' : gVu33,
                    'gVl11' : gVl11,
                    'gVl22' : gVl22,
                    'gVl33' : gVl33,
                    'gAd11' : gAd11,
                    'gAu11' : gAu11,
                    'gAd22' : gAd22,
                    'gAu22' : gAu22,
                    'gAd33' : gAd33,
                    'gAu33' : gAu33,
                    'gAl11' : gAl11,
                    'gAl22' : gAl22,
                    'gAl33' : gAl33
                },
    'DECAY':    {
                    '5000001':'DECAY 5000001 auto # Y1'
                }
}
print("Updating parameters:")
print(params)
modify_param_card(process_dir=process_dir,params=params)
print_cards()
#-----------------------------------------------------------------
# Generate the events    
#-----------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True) 
#-----------------------------------------------------------------
# Shower
#-----------------------------------------------------------------


evgenConfig.description = "Wimp dmA mediator from DMSimp"
evgenConfig.keywords = ["exotic","BSM"]
evgenConfig.contact = ["Sanae Ezzarqtouni <sanae.ezzarqtouni@cern.ch> , Ng Yvonne Ying Wun <yvonne.ng@desy.de>"]
#evgenConfig.minevents= 10000

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

#particle data = name antiname spin=2s+1 3xcharge colour mass width (left out, so set to 0: mMin mMax tau0)
genSeq.Pythia8.Commands += ["1000022:all = xd xd~ 2 0 0 %d 0" %(MXd),
                            "1000022:isVisible = false"]
