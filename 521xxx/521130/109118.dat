# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 20.12.2022,  21:26
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.34713444E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.62841422E+03  # scale for input parameters
    1   -1.52043580E+02  # M_1
    2   -1.61364979E+03  # M_2
    3    1.96256569E+03  # M_3
   11   -1.87399829E+03  # A_t
   12    2.95180947E+02  # A_b
   13    1.55006878E+03  # A_tau
   23   -5.70771326E+02  # mu
   25    2.25653123E+01  # tan(beta)
   26    8.57945535E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.27658197E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.03010536E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.60531186E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.62841422E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.62841422E+03  # (SUSY scale)
  1  1     8.39260109E-06   # Y_u(Q)^DRbar
  2  2     4.26344135E-03   # Y_c(Q)^DRbar
  3  3     1.01389362E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.62841422E+03  # (SUSY scale)
  1  1     3.80555653E-04   # Y_d(Q)^DRbar
  2  2     7.23055740E-03   # Y_s(Q)^DRbar
  3  3     3.77391958E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.62841422E+03  # (SUSY scale)
  1  1     6.64097442E-05   # Y_e(Q)^DRbar
  2  2     1.37314289E-02   # Y_mu(Q)^DRbar
  3  3     2.30940044E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.62841422E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -1.87399890E+03   # A_t(Q)^DRbar
Block Ad Q=  2.62841422E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     2.95180977E+02   # A_b(Q)^DRbar
Block Ae Q=  2.62841422E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.55006862E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.62841422E+03  # soft SUSY breaking masses at Q
   1   -1.52043580E+02  # M_1
   2   -1.61364979E+03  # M_2
   3    1.96256569E+03  # M_3
  21    2.56024879E+05  # M^2_(H,d)
  22   -2.21618988E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.27658197E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.03010536E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.60531186E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.19945358E+02  # h0
        35     8.58004136E+02  # H0
        36     8.57945535E+02  # A0
        37     8.63512953E+02  # H+
   1000001     1.01118603E+04  # ~d_L
   2000001     1.00887899E+04  # ~d_R
   1000002     1.01114984E+04  # ~u_L
   2000002     1.00918123E+04  # ~u_R
   1000003     1.01118643E+04  # ~s_L
   2000003     1.00887957E+04  # ~s_R
   1000004     1.01115024E+04  # ~c_L
   2000004     1.00918143E+04  # ~c_R
   1000005     3.31707504E+03  # ~b_1
   2000005     4.65026673E+03  # ~b_2
   1000006     2.08010781E+03  # ~t_1
   2000006     3.32125155E+03  # ~t_2
   1000011     1.00194717E+04  # ~e_L-
   2000011     1.00090094E+04  # ~e_R-
   1000012     1.00187127E+04  # ~nu_eL
   1000013     1.00194870E+04  # ~mu_L-
   2000013     1.00090381E+04  # ~mu_R-
   1000014     1.00187276E+04  # ~nu_muL
   1000015     1.00170880E+04  # ~tau_1-
   2000015     1.00239876E+04  # ~tau_2-
   1000016     1.00229779E+04  # ~nu_tauL
   1000021     2.34417277E+03  # ~g
   1000022     1.52335680E+02  # ~chi_10
   1000023     5.80890517E+02  # ~chi_20
   1000025     5.83580357E+02  # ~chi_30
   1000035     1.71428778E+03  # ~chi_40
   1000024     5.79360210E+02  # ~chi_1+
   1000037     1.71425187E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -4.40651100E-02   # alpha
Block Hmix Q=  2.62841422E+03  # Higgs mixing parameters
   1   -5.70771326E+02  # mu
   2    2.25653123E+01  # tan[beta](Q)
   3    2.43404601E+02  # v(Q)
   4    7.36070541E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     4.14382391E-02   # Re[R_st(1,1)]
   1  2     9.99141067E-01   # Re[R_st(1,2)]
   2  1    -9.99141067E-01   # Re[R_st(2,1)]
   2  2     4.14382391E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99996021E-01   # Re[R_sb(1,1)]
   1  2     2.82087119E-03   # Re[R_sb(1,2)]
   2  1    -2.82087119E-03   # Re[R_sb(2,1)]
   2  2    -9.99996021E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.92056275E-01   # Re[R_sta(1,1)]
   1  2     9.81383914E-01   # Re[R_sta(1,2)]
   2  1    -9.81383914E-01   # Re[R_sta(2,1)]
   2  2    -1.92056275E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.96095200E-01   # Re[N(1,1)]
   1  2    -1.45898692E-03   # Re[N(1,2)]
   1  3    -8.44548980E-02   # Re[N(1,3)]
   1  4     2.56825732E-02   # Re[N(1,4)]
   2  1    -7.79277763E-02   # Re[N(2,1)]
   2  2    -5.17845451E-02   # Re[N(2,2)]
   2  3    -7.04185529E-01   # Re[N(2,3)]
   2  4     7.03824100E-01   # Re[N(2,4)]
   3  1     4.14611875E-02   # Re[N(3,1)]
   3  2    -2.35666113E-02   # Re[N(3,2)]
   3  3     7.04691092E-01   # Re[N(3,3)]
   3  4     7.07909634E-01   # Re[N(3,4)]
   4  1    -1.60767075E-03   # Re[N(4,1)]
   4  2     9.98379110E-01   # Re[N(4,2)]
   4  3    -2.00144060E-02   # Re[N(4,3)]
   4  4     5.32540314E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -2.83100002E-02   # Re[U(1,1)]
   1  2    -9.99599192E-01   # Re[U(1,2)]
   2  1    -9.99599192E-01   # Re[U(2,1)]
   2  2     2.83100002E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     7.53625484E-02   # Re[V(1,1)]
   1  2     9.97156200E-01   # Re[V(1,2)]
   2  1     9.97156200E-01   # Re[V(2,1)]
   2  2    -7.53625484E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02658545E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.92254302E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     6.03499803E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     1.70824579E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.37264388E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.04373029E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.78484474E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.01250056E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     5.11553206E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.05015599E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03405816E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.90789530E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     6.39339416E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.07355841E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     7.40239745E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.37301951E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.04146502E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.91891353E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.35653934E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01168256E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     5.11414045E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.04851112E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     7.42645498E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     6.59489415E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     7.53989278E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     6.81871615E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.85175454E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.41209818E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     3.71971329E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.45051010E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     9.31523886E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     3.83329247E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     3.79892276E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.75790079E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     9.12884481E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.53822496E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.37267940E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     9.14252959E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     6.66854319E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02254954E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.62475284E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.01995244E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.37305494E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.14004261E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     6.66672932E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02172787E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.89541991E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01831803E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.47896083E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.48914279E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     6.19199758E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.80668073E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     7.47347172E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.59056034E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.18306110E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.77669663E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.92162511E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.42056169E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.68970843E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     4.97282376E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     9.96428020E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.48729304E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.18327405E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.77657444E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.92133797E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.42070622E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.68976891E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     4.97274105E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     9.96411597E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.48715148E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.72333908E+02   # ~b_1
#    BR                NDA      ID1      ID2
     3.10304300E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.55294283E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.54957022E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.53910879E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.65620502E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     9.27326641E-02    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     3.80690196E-01    2     1000021         5   # BR(~b_1 -> ~g b)
     5.49036021E-02    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
     6.53377367E-03    2     1000006       -37   # BR(~b_1 -> ~t_1 H^-)
DECAY   2000005     2.32499665E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.14413152E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.72945736E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.72940403E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     5.47204842E-02    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     8.78346920E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     2.73330222E-04    2     2000006       -24   # BR(~b_2 -> ~t_2 W^-)
     1.37759715E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     2.18198816E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     7.35464015E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.03901938E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.84855211E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     9.69372552E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.42043710E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.63498383E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     2.26377489E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     4.96699791E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     5.96477334E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.91538471E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.48704584E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.35471410E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.03898964E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.87294430E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     9.69362999E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.42058130E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.63495926E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     2.28511278E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     4.96691577E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     6.08830321E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.91522028E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.48690426E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     7.55788520E+01   # ~t_1
#    BR                NDA      ID1      ID2
     5.95930835E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.31509100E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.34481708E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.53019473E-04    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.73555404E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     6.07614896E-04    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.72548768E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.11521787E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.83214147E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.85044982E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     4.42908137E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     5.41748837E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     8.96789264E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.69755774E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     2.75575065E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     3.30074733E-03    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     3.65924866E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     3.27451493E-03    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     4.25314167E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.95989987E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     4.00579193E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     2.24129043E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.72308516E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.70649524E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     1.01065775E-03    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     8.20507278E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     7.08328772E-02    2          37   1000025   # BR(chi^+_2 -> H^+ chi^0_3)
     1.70501896E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     7.25299942E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     1.71449439E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     7.20846373E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     6.50665398E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     4.78348134E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     5.27272783E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     3.64870234E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     2.63265559E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     7.36467175E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.10812556E-04    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
DECAY   1000025     5.73969069E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     5.98658877E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     4.01181605E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.20672199E-04    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
DECAY   1000035     2.50106958E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.54872919E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.54872919E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     7.36287861E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     7.36287861E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     2.61863417E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.28016338E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     3.47980899E-04    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     8.13492828E-03    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     6.74036121E-03    2     1000025        36   # BR(chi^0_4 -> chi^0_3 A^0)
     1.27370907E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.07538856E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     5.65030594E-04    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     5.63155035E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     6.73876577E-02    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     2.50985730E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     2.29234549E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     4.77777331E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     4.77777331E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.32124403E+00   # ~g
#    BR                NDA      ID1      ID2
     4.93328683E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     4.93328683E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.85025563E-04    2     1000022        21   # BR(~g -> chi^0_1 g)
     1.04216750E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.16704391E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     2.14915417E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     5.06341149E-04    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     2.12855890E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     5.03556827E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     2.70145553E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.70145553E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.48733379E-03   # Gamma(h0)
     2.20160137E-03   2        22        22   # BR(h0 -> photon photon)
     1.02616319E-03   2        22        23   # BR(h0 -> photon Z)
     1.58588562E-02   2        23        23   # BR(h0 -> Z Z)
     1.45013827E-01   2       -24        24   # BR(h0 -> W W)
     7.14844907E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.84926147E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.60182361E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.50118374E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.56894590E-07   2        -2         2   # BR(h0 -> Up up)
     3.04427438E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.82359665E-07   2        -1         1   # BR(h0 -> Down down)
     2.46793983E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.58452659E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     8.00683685E+00   # Gamma(HH)
     7.38797918E-08   2        22        22   # BR(HH -> photon photon)
     5.39979647E-08   2        22        23   # BR(HH -> photon Z)
     3.38112880E-05   2        23        23   # BR(HH -> Z Z)
     5.31059979E-05   2       -24        24   # BR(HH -> W W)
     5.53119086E-05   2        21        21   # BR(HH -> gluon gluon)
     9.68746555E-09   2       -11        11   # BR(HH -> Electron electron)
     4.31165930E-04   2       -13        13   # BR(HH -> Muon muon)
     1.24499488E-01   2       -15        15   # BR(HH -> Tau tau)
     5.86496931E-13   2        -2         2   # BR(HH -> Up up)
     1.13741577E-07   2        -4         4   # BR(HH -> Charm charm)
     7.65721501E-03   2        -6         6   # BR(HH -> Top top)
     8.42941386E-07   2        -1         1   # BR(HH -> Down down)
     3.04889412E-04   2        -3         3   # BR(HH -> Strange strange)
     8.33798290E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.34183533E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     7.98877127E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     2.35491937E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     2.85828301E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     7.88322083E+00   # Gamma(A0)
     1.15277358E-07   2        22        22   # BR(A0 -> photon photon)
     2.85042912E-08   2        22        23   # BR(A0 -> photon Z)
     1.24111483E-04   2        21        21   # BR(A0 -> gluon gluon)
     9.66660928E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.30237719E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.24233681E-01   2       -15        15   # BR(A0 -> Tau tau)
     5.46945200E-13   2        -2         2   # BR(A0 -> Up up)
     1.06062563E-07   2        -4         4   # BR(A0 -> Charm charm)
     8.62479426E-03   2        -6         6   # BR(A0 -> Top top)
     8.41134293E-07   2        -1         1   # BR(A0 -> Down down)
     3.04235797E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.32045271E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.63919426E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.49334750E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     7.61115100E-03   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     5.27476069E-05   2        23        25   # BR(A0 -> Z h0)
     4.37004754E-37   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     7.10467860E+00   # Gamma(Hp)
     1.10728900E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.73400620E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.33903578E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.42115626E-07   2        -1         2   # BR(Hp -> Down up)
     1.39901106E-05   2        -3         2   # BR(Hp -> Strange up)
     9.17901281E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.47005423E-08   2        -1         4   # BR(Hp -> Down charm)
     3.03608943E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.28538529E-03   2        -5         4   # BR(Hp -> Bottom charm)
     6.08103990E-07   2        -1         6   # BR(Hp -> Down top)
     1.36685872E-05   2        -3         6   # BR(Hp -> Strange top)
     8.25644443E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.82910089E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     6.02295669E-05   2        24        25   # BR(Hp -> W h0)
     9.65900615E-10   2        24        35   # BR(Hp -> W HH)
     1.01833669E-09   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.90018640E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    5.09203301E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    5.09193319E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00001960E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.94428835E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.96389065E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.90018640E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    5.09203301E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    5.09193319E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999951E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    4.91595679E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999951E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    4.91595679E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26195124E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    7.86025229E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.12790381E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    4.91595679E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999951E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.75055356E-04   # BR(b -> s gamma)
    2    1.58622214E-06   # BR(b -> s mu+ mu-)
    3    3.52268761E-05   # BR(b -> s nu nu)
    4    2.13341365E-15   # BR(Bd -> e+ e-)
    5    9.11365480E-11   # BR(Bd -> mu+ mu-)
    6    1.90514994E-08   # BR(Bd -> tau+ tau-)
    7    7.19236420E-14   # BR(Bs -> e+ e-)
    8    3.07256021E-09   # BR(Bs -> mu+ mu-)
    9    6.50796734E-07   # BR(Bs -> tau+ tau-)
   10    9.23269296E-05   # BR(B_u -> tau nu)
   11    9.53701836E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.43100827E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93853518E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16234595E-03   # epsilon_K
   17    2.28171426E-15   # Delta(M_K)
   18    2.47850169E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28682607E-11   # BR(K^+ -> pi^+ nu nu)
   20    2.47669956E-16   # Delta(g-2)_electron/2
   21    1.05886824E-11   # Delta(g-2)_muon/2
   22    2.99669382E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.58252189E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.44753485E-01   # C7
     0305 4322   00   2    -8.97186641E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.50865806E-01   # C8
     0305 6321   00   2    -9.93632276E-04   # C8'
 03051111 4133   00   0     1.61174038E+00   # C9 e+e-
 03051111 4133   00   2     1.61261462E+00   # C9 e+e-
 03051111 4233   00   2     3.55860834E-05   # C9' e+e-
 03051111 4137   00   0    -4.43443248E+00   # C10 e+e-
 03051111 4137   00   2    -4.43024073E+00   # C10 e+e-
 03051111 4237   00   2    -2.68049340E-04   # C10' e+e-
 03051313 4133   00   0     1.61174038E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61261455E+00   # C9 mu+mu-
 03051313 4233   00   2     3.55833734E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.43443248E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43024080E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.68046682E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50444163E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     5.81571765E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50444163E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     5.81577618E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50444163E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     5.83226874E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85811765E-07   # C7
     0305 4422   00   2     5.34835589E-06   # C7
     0305 4322   00   2    -8.41872486E-07   # C7'
     0305 6421   00   0     3.30473292E-07   # C8
     0305 6421   00   2     7.67937968E-06   # C8
     0305 6321   00   2    -2.36284495E-07   # C8'
 03051111 4133   00   2    -5.52251546E-08   # C9 e+e-
 03051111 4233   00   2     2.19573197E-06   # C9' e+e-
 03051111 4137   00   2     2.59593810E-06   # C10 e+e-
 03051111 4237   00   2    -1.65947367E-05   # C10' e+e-
 03051313 4133   00   2    -5.52257000E-08   # C9 mu+mu-
 03051313 4233   00   2     2.19573173E-06   # C9' mu+mu-
 03051313 4137   00   2     2.59593884E-06   # C10 mu+mu-
 03051313 4237   00   2    -1.65947374E-05   # C10' mu+mu-
 03051212 4137   00   2    -5.45781382E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     3.60049318E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -5.45781357E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     3.60049318E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -5.45774249E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     3.60049317E-06   # C11' nu_3 nu_3
