include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

from MadGraphControl.MadGraphUtilsHelpers import *
JOName=get_physics_short()
jobConfigParts = JOName.split('_')

mGluino = float(jobConfigParts[3])
mLSP = float(jobConfigParts[4].rstrip('.py'))
mCN = float(round((1/2 * (mGluino + mLSP)),0))
mStau = float(round((1/2 * (mLSP + mCN)),0))

#Using the five flavour scheme, should be picked up in the post-include 
flavourScheme = 5

masses['1000021'] = mGluino
masses['1000023'] = mCN
masses['1000024'] = mCN
masses['1000015'] = mStau
masses['1000016'] = mStau
masses['1000022'] = mLSP

#Changed j,p to jb pb
process = """
generate pb pb > go go $ susysq susysq~ @1
add process pb pb > go go jb $ susysq susysq~ @2
add process pb pb > go go jb jb $ susysq susysq~ @3
"""

decays['1000021'] = """DECAY   1000021     1.00000000E+00   # gluino decays
#           BR         NDA      ID1       ID2       ID3
     1.25000000E-01    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.25000000E-01    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.25000000E-01    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.25000000E-01    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     1.25000000E-01    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.25000000E-01    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.25000000E-01    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.25000000E-01    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
"""

decays['1000023'] = """DECAY   1000023     1.00000000E-01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000011       -11   # BR(~chi_20 -> ~e_L-     e+)
     0.00000000E+00    2    -1000011        11   # BR(~chi_20 -> ~e_L+     e-)
     0.00000000E+00    2     1000013       -13   # BR(~chi_20 -> ~mu_L-    mu+)
     0.00000000E+00    2    -1000013        13   # BR(~chi_20 -> ~mu_L+    mu-)
     2.50000000E-01    2     1000015       -15   # BR(~chi_20 -> ~tau_1-   tau+)
     2.50000000E-01    2    -1000015        15   # BR(~chi_20 -> ~tau_1+   tau-)
     0.00000000E+00    2     1000012       -12   # BR(~chi_20 -> ~nu_eL    nu_eb)
     0.00000000E+00    2    -1000012        12   # BR(~chi_20 -> ~nu_eL*   nu_e )
     0.00000000E+00    2     1000014       -14   # BR(~chi_20 -> ~nu_muL   nu_mub)
     0.00000000E+00    2    -1000014        14   # BR(~chi_20 -> ~nu_muL*  nu_mu )
     2.50000000E-01    2     1000016       -16   # BR(~chi_20 -> ~nu_tau1  nu_taub)
     2.50000000E-01    2    -1000016        16   # BR(~chi_20 -> ~nu_tau1* nu_tau )
"""

decays["1000024"] = """DECAY   1000024     1.00000000E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000012       -11   # BR(~chi_1+ -> ~nu_eL  e+  )
     0.00000000E+00    2     1000014       -13   # BR(~chi_1+ -> ~nu_muL  mu+ )
     5.00000000E-01    2     1000016       -15   # BR(~chi_1+ -> ~nu_tau1 tau+)
     0.00000000E+00    2    -1000011        12   # BR(~chi_1+ -> ~e_L+    nu_e)
     0.00000000E+00    2    -1000013        14   # BR(~chi_1+ -> ~mu_L+   nu_mu)
     5.00000000E-01    2    -1000015        16   # BR(~chi_1+ -> ~tau_1+  nu_tau)
"""

decays["1000015"]="""DECAY   1000015     1.00000000E-01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
"""

decays["1000016"]="""DECAY   1000016     1.00000000E-01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
"""

evgenConfig.contact  = [ "thillers@cern.ch" ]
evgenConfig.keywords += ['gluino','simplifiedModel']
evgenConfig.description = 'Simplified model of gluino pair production with 2-step decay via C1/N2 then stau/sneutrino, m_glu = %s GeV, m_C1N2 = %s GeV, m_stau = %s GeV, m_N1 = %s GeV'%(mGluino,mCN,mStau,mLSP)

njets=2
if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{go,1000021}{go,1000021}"]

#Add more events in the compressed region
if mGluino-mLSP <= 200 and "_J85" in JOName:
     evt_multiplier = 50.
elif mGluino-mLSP <= 240 and "_J85" in JOName:
     evt_multiplier = 10.    

filters = []
#Included the AntiKtJets, see if this makes a difference -no it does not
# 1tau 1 jet filter
if '_J85' in JOName:
    filters.append("TruthJetFilter")
    evgenLog.info('Adding truth jet filter, Pt > 85 GeV')
    include("GeneratorFilters/FindJets.py")
    CreateJets(prefiltSeq, 0.4) 
    from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
    filtSeq += TruthJetFilter("TruthJetFilter")
    filtSeq.TruthJetFilter.jet_pt1 = 85000.
    filtSeq.TruthJetFilter.NjetMinPt = 0.
    filtSeq.TruthJetFilter.NjetMaxEta = 2.8
    filtSeq.TruthJetFilter.Njet = 1

if "1tau" in jobConfigParts[-1]:
    filters.append("MultiElecMuTauFilter")
    evgenLog.info('Adding 1 tau filter')
    include("GeneratorFilters/MultiElecMuTauFilter.py")
    MultiElecMuTauFilter = filtSeq.MultiElecMuTauFilter
    MultiElecMuTauFilter.NLeptons = 1
    MultiElecMuTauFilter.MinPt = 1e8
    MultiElecMuTauFilter.MaxEta = 2.8
    MultiElecMuTauFilter.MinVisPtHadTau =  10000
    MultiElecMuTauFilter.IncludeHadTaus = 1

if filters:
   filtSeq.Expression = ' and '.join(filters)
   

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )    





