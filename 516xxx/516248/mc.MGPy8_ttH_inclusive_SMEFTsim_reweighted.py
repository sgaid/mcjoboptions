selected_operators = ['cHbox','cHG','ctHRe','ctGRe','cQj11','cQj18','cQj31','cQj38','cQQ1','cQQ8','ctu1','ctu8','ctd1','ctd8','cQu1','cQu8','ctj1','ctj8','cQt1','cQt8','cQd1','cQd8','cHl311','cll1221','ctGIm','ctHIm']

process_definition = 'set group_subprocesses False\ndefine wdec = u u~ c c~ d d~ s s~ l- l- vl vl~\n' # define inclusive W decay
process_definition+= 'generate p p > t t~ h / z a h w+ w- QCD=2 QED=3, (t > wdec wdec b NP=0), (t~ > wdec wdec b~ NP=0) @0 NPprop=0 SMHLOOP=0 NP=1\n'
process_definition+= 'add process p p > t t~ h j / z a h w+ w- QCD=3 QED=3, (t > wdec wdec b NP=0), (t~ > wdec wdec b~ NP=0) @1 NPprop=0 SMHLOOP=0 NP=1\n'

fixed_scale = 470.0 # ~ 2*m(top)+m(Higgs)

gridpack = True

evgenConfig.description = 'SMEFTsim 3.0 tt+H, inclusive, top model, reweighted, EFT vertices, no propagator correction'
evgenConfig.nEventsPerJob=2000

include("Common_SMEFTsim_topmW_ttH_inclusive_reweighted.py")
