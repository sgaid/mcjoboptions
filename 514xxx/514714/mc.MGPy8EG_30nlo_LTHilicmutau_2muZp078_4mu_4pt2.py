##
## 78.0GeV leptophilic Zprime from pp --> 2mu+Zp --> 4mu
##
zpm=78.0
gzpmul=3.080000e-01
include("./MGCtrl_Py8EG_NNPDF30nlo_Leptophilicmutau_2muZp_4mu_4pt2.py")
