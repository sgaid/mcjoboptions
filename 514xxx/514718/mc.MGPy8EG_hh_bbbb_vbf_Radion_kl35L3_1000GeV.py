import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

safefactor=1.1
nevents=5000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor

mode=0

#---------------------------------------------------------------------------------------------------
# Setting parameters for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters = {}


#---------------------------------------------------------------------------------------------------
# Setting higgs mass to 125 GeV for param_card.dat
# Setting Radion mass and width for param_card.dat
# KL is set to 35 GeV and Lambda to 3 TeV
#---------------------------------------------------------------------------------------------------
parameters['MASS']={'25':'1.250000e+02', 
                    'mh02': '1.000000e+03',
                    'K': '35',
                    'LR': "3000" }  

#widths={300:"0.06",400:"0.22",500:"0.50",600:"0.89",700:"1.33",800:"2.1",1000:"3.9",1200:"6.5",1400:"10",1500:"13",1600:"15",1800:"22",2000:"28",2400:"50",2600:"64",3000:"90",3500:"150",4000:"200",4500:"320",5000:"400",6000:"650"}
#parameters['DECAY']={'wh02': widths[ '2.000000e+02' ] }
parameters['DECAY'] = {"wh02": '2.000000e+02' }

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
## generator cuts
extras = {
    'ptj':"0",
    'ptb':"0",
    'pta':"0",
    'ptl':"0",
    'etaj':"-1",
    'etab':"-1",
    'etaa':"-1",
    'etal':"-1",
    'drjj':"0",
    'drbb':"0",
    'drll':"0",
    'draa':"0",
    'drbj':"0",
    'draj':"0",
    'drjl':"0",
    'drbl':"0",
    'dral':"0",
}

extras.update( {"scale": '1.000000e+03',
                "dsqrt_q2fact1": '1.000000e+03',
                "dsqrt_q2fact2": '1.000000e+03',
                "nevents": int(nevents),
                "cut_decays": "F",
                "mmjj": "150"} )

extras.update( { 'lhe_version':'2.0', 
                'fixed_ren_scale':'F',
                'fixed_fac_scale':'F'} )
                
#---------------------------------------------------------------------------------------------------
# Generating resonant VBF-Only HH process with MadGraph
# Parameters are set above
#---------------------------------------------------------------------------------------------------
process="""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
import model heft_radion
generate p p > h2 j j $$ z w+ w- / a j H QED=3 QCD=0, h2 > H H QED=1 QCD=0 
output -f"""

process_dir = new_process(process)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# https://gitlab.cern.ch/atlas/athena/-/tree/master/Generators/MadGraphControl
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used 
#---------------------------------------------------------------------------------------------------
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# https://gitlab.cern.ch/atlas/athena/-/tree/master/Generators/MadGraphControl
# Build a new param_card.dat from an existing one
# Higgs mass is set to 125 GeV by "higgsMass"
#---------------------------------------------------------------------------------------------------
modify_param_card(process_dir=process_dir,params=parameters)

#---------------------------------------------------------------------------------------------------
# Printing cards   
#---------------------------------------------------------------------------------------------------
print_cards()

#---------------------------------------------------------------------------------------------------
# Generate events                                                               
#---------------------------------------------------------------------------------------------------     
generate(process_dir=process_dir,runArgs=runArgs)

#---------------------------------------------------------------------------------------------------
# Move output files into the appropriate place, with the appropriate name
#---------------------------------------------------------------------------------------------------   
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=2,saveProcDir=True)

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.description = "Bulk RS Radion Signal Point, di-Higgs production, VBF, decaying to bbbb."
evgenConfig.keywords = ["hh", "exotic", "VBFHiggs", "bottom"]
evgenConfig.contact = ['Katharina Behr <katharina.behr@cern.ch>', 'Marcus V. G. Rodrigues <marcus.rodrigues@cern.ch>']

evgenConfig.nEventsPerJob = 10000
#evgenConfig.tune = "MMHT2014"

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands += [ "25:oneChannel = on 1.0 100 5 -5"]   # bb decay

#--------------------------------------------------------------                                                                                   
# Dipole option Pythia8                                                                                                          
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]

#---------------------------------------------------------------------------------------------------
# Filter for bbbb
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5])

filtSeq.Expression = "HbbFilter"

