import MadGraphControl.MadGraphUtils

MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf': 260000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
    'pdf_variations':[260000,90400,13100,25200], # pdfs for which all variations (error sets) will be included as weights
    'alternative_pdfs':[266000,265000], # pdfs for which only the central set will be included as weights
    'scale_variations':[0.5,1.,2.], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
    'alternative_dynamic_scales' : [1,2,3,4],
}
from MadGraphControl.MadGraphUtils import *


evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]

evgenConfig.keywords = ['SM', 'diboson', 'WZ', 'electroweak', '3lepton', '2jet', 'VBS']
evgenConfig.contact = ['anna.sophie.tegetmeier@cern.ch']

gridpack_dir = 'madevent/'
Mode=0
gridpack_mode=True
gridpack_compile=False
nevents = 10000

MADGRAPH_GRIDPACK_LOCATION = gridpack_dir
MADGRAPH_CATCH_ERRORS=False
# ---------------------------------------------------------------------------
# Process type based on runNumber:
# ---------------------------------------------------------------------------
if flavorcharge == "W0Z0_OFM":
    runName = 'W0Z0jj_lvll_OFMinus'
    description = 'MadGraph_W0Z0jj_lvll_OFM_tau_EW6_masslessLeptons'
    mgproc = """
    generate p p > w-{0} z{0} j j QCD=0, w- > e- ve~, z > ta+ ta- @0
    add process p p > w-{0} z{0} j j QCD=0, w- > mu- vm~, z > ta+ ta- @0
    add process p p > w-{0} z{0} j j QCD=0, w- > ta- vt~, z > e+ e- @0
    add process p p > w-{0} z{0} j j QCD=0, w- > ta- vt~, z > mu+ mu- @0
    output -f"""
elif flavorcharge == "W0Z0_OFP":
    runName = 'W0Z0jj_lvll_OFPlus'
    description = 'MadGraph_W0Z0jj_lvll_OFP_tau_EW6_masslessLeptons'
    mgproc = """
    generate p p > w+{0} z{0} j j QCD=0, w+ > e+ ve, z > ta+ ta- @0
    add process p p > w+{0} z{0} j j QCD=0, w+ > mu+ vm, z > ta+ ta- @0
    add process p p > w+{0} z{0} j j QCD=0, w+ > ta+ vt, z > e+ e- @0
    add process p p > w+{0} z{0} j j QCD=0, w+ > ta+ vt, z > mu+ mu- @0
    output -f"""
elif flavorcharge == "W0Z0_SFM":
    runName = 'W0Z0jj_lvll_SFMinus'
    description = 'MadGraph_W0Z0jj_lvll_SFM_tau_EW6_masslessLeptons'
    mgproc = """
    generate p p > w-{0} z{0} j j QCD=0, w- > ta- vt~, z > ta+ ta- @0
    output -f"""
elif flavorcharge == "W0Z0_SFP":
    runName = 'W0Z0jj_lvll_SFPlus'
    description = 'MadGraph_W0Z0jj_lvll_SFP_tau_EW6_masslessLeptons'
    mgproc = """
    generate p p > w+{0} z{0} j j QCD=0, w+ > ta+ vt, z > ta+ ta- @0
    output -f"""
elif flavorcharge == "W0ZT_OFM":
    runName = 'W0ZTjj_lvll_OFMinus'
    description = 'MadGraph_W0ZTjj_lvll_OFM_tau_EW6_masslessLeptons'
    mgproc = """
    generate p p > w-{0} z{T} j j QCD=0, w- > e- ve~, z > ta+ ta- @0
    add process p p > w-{0} z{T} j j QCD=0, w- > mu- vm~, z > ta+ ta- @0
    add process p p > w-{0} z{T} j j QCD=0, w- > ta- vt~, z > e+ e- @0
    add process p p > w-{0} z{T} j j QCD=0, w- > ta- vt~, z > mu+ mu- @0
    output -f"""
elif flavorcharge == "W0ZT_OFP":
    runName = 'W0ZTjj_lvll_OFPlus'
    description = 'MadGraph_W0ZTjj_lvll_OFP_tau_EW6_masslessLeptons'
    mgproc = """
    generate p p > w+{0} z{T} j j QCD=0, w+ > e+ ve, z > ta+ ta- @0
    add process p p > w+{0} z{T} j j QCD=0, w+ > mu+ vm, z > ta+ ta- @0
    add process p p > w+{0} z{T} j j QCD=0, w+ > ta+ vt, z > e+ e- @0
    add process p p > w+{0} z{T} j j QCD=0, w+ > ta+ vt, z > mu+ mu- @0
    output -f"""
elif flavorcharge == "W0ZT_SFM":
    runName = 'W0ZTjj_lvll_SFMinus'
    description = 'MadGraph_W0ZTjj_lvll_SFM_tau_EW6_masslessLeptons'
    mgproc = """
    generate p p > w-{0} z{T} j j QCD=0, w- > ta- vt~, z > ta+ ta- @0
    output -f"""
elif flavorcharge == "W0ZT_SFP":
    runName = 'W0ZTjj_lvll_SFPlus'
    description = 'MadGraph_W0ZTjj_lvll_SFP_tau_EW6_masslessLeptons'
    mgproc = """
    generate p p > w+{0} z{T} j j QCD=0, w+ > ta+ vt, z > ta+ ta- @0
    output -f"""
elif flavorcharge == "WTZ0_OFM":
    runName = 'WTZ0jj_lvll_OFMinus'
    description = 'MadGraph_WTZ0jj_lvll_OFM_tau_EW6_masslessLeptons'
    mgproc = """
    generate p p > w-{T} z{0} j j QCD=0, w- > e- ve~, z > ta+ ta- @0
    add process p p > w-{T} z{0} j j QCD=0, w- > mu- vm~, z > ta+ ta- @0
    add process p p > w-{T} z{0} j j QCD=0, w- > ta- vt~, z > e+ e- @0
    add process p p > w-{T} z{0} j j QCD=0, w- > ta- vt~, z > mu+ mu- @0
    output -f"""
elif flavorcharge == "WTZ0_OFP":
    runName = 'WTZ0jj_lvll_OFPlus'
    description = 'MadGraph_WTZ0jj_lvll_OFP_tau_EW6_masslessLeptons'
    mgproc = """
    generate p p > w+{T} z{0} j j QCD=0, w+ > e+ ve, z > ta+ ta- @0
    add process p p > w+{T} z{0} j j QCD=0, w+ > mu+ vm, z > ta+ ta- @0
    add process p p > w+{T} z{0} j j QCD=0, w+ > ta+ vt, z > e+ e- @0
    add process p p > w+{T} z{0} j j QCD=0, w+ > ta+ vt, z > mu+ mu- @0
    output -f"""
elif flavorcharge == "WTZ0_SFM":
    runName = 'WTZ0jj_lvll_SFMinus'
    description = 'MadGraph_WTZ0jj_lvll_SFM_tau_EW6_masslessLeptons'
    mgproc = """
    generate p p > w-{T} z{0} j j QCD=0, w- > ta- vt~, z > ta+ ta- @0
    output -f"""
elif flavorcharge == "WTZ0_SFP":
    runName = 'WTZ0jj_lvll_SFPlus'
    description = 'MadGraph_WTZ0jj_lvll_SFP_tau_EW6_masslessLeptons'
    mgproc = """
    generate p p > w+{T} z{0} j j QCD=0, w+ > ta+ vt, z > ta+ ta- @0
    output -f"""
elif flavorcharge == "WTZT_OFM":
    runName = 'WTZTjj_lvll_OFMinus'
    description = 'MadGraph_WTZTjj_lvll_OFM_tau_EW6_masslessLeptons'
    mgproc = """
    generate p p > w-{T} z{T} j j QCD=0, w- > e- ve~, z > ta+ ta- @0
    add process p p > w-{T} z{T} j j QCD=0, w- > mu- vm~, z > ta+ ta- @0
    add process p p > w-{T} z{T} j j QCD=0, w- > ta- vt~, z > e+ e- @0
    add process p p > w-{T} z{T} j j QCD=0, w- > ta- vt~, z > mu+ mu- @0
    output -f"""
elif flavorcharge == "WTZT_OFP":
    runName = 'WTZTjj_lvll_OFPlus'
    description = 'MadGraph_WTZT_lvlljj_OFP_tau_EW6_masslessLeptons'
    mgproc = """
    generate p p > w+{T} z{T} j j QCD=0, w+ > e+ ve, z > ta+ ta- @0
    add process p p > w+{T} z{T} j j QCD=0, w+ > mu+ vm, z > ta+ ta- @0
    add process p p > w+{T} z{T} j j QCD=0, w+ > ta+ vt, z > e+ e- @0
    add process p p > w+{T} z{T} j j QCD=0, w+ > ta+ vt, z > mu+ mu- @0
    output -f"""
elif flavorcharge == "WTZT_SFM":
    runName = 'WTZTjj_lvll_SFMinus'
    description = 'MadGraph_WTZTjj_lvll_SFM_tau_EW6_masslessLeptons'
    mgproc = """
    generate p p > w-{T} z{T} j j QCD=0, w- > ta- vt~, z > ta+ ta- @0
    output -f"""
elif flavorcharge == "WTZT_SFP":
    runName = 'WTZTjj_lvll_SFPlus'
    description = 'MadGraph_WTZTjj_lvll_SFP_tau_EW6_masslessLeptons'
    mgproc = """
    generate p p > w+{T} z{T} j j QCD=0, w+ > ta+ vt, z > ta+ ta- @0
    output -f"""
else:
    raise RuntimeError(
        "Flavor-charge combination %i not recognised in these jobOptions. Has to be one of W0Z0_OFM, W0Z0_OFP, W0Z0_SFM, W0Z0_SFP, W0ZT_OFM, W0ZT_OFP, W0ZT_SFM, W0ZT_SFP, WTZ0_OFM, WTZ0_OFP, WTZ0_SFM, WTZ0_SFP, WTZT_OFM, WTZT_OFP, WTZT_SFM, WTZT_SFP!" % flavorcharge)

evgenConfig.description = description
# ---------------------------------------------------------------------------
# write MG5 Proc card
# ---------------------------------------------------------------------------
process = """
import model sm-no_masses
# massless b and massless taus
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
%s
output -f
""" % (mgproc)

# ----------------------------------------------------------------------------
# Random Seed
# ----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs, 'randomSeed'):
    randomSeed = runArgs.randomSeed

# ----------------------------------------------------------------------------
# Beam energy
# ----------------------------------------------------------------------------
beamEnergy=-999
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = int(runArgs.ecmEnergy) / 2.
else:
    raise RunTimeError("No center of mass energy found.")

# ---------------------------------------------------------------------------
# Number of Events
# ---------------------------------------------------------------------------
safefactor = 1.1
if hasattr(runArgs, 'maxEvents') and runArgs.maxEvents > 0:
    nevents = int(int(runArgs.maxEvents) * safefactor)
else:
    nevents = int(nevents * safefactor)

extras = {
    'me_frame': '3,4,5,6',
    'ptl': 4.0,
    'ptj': 15.0,
    'ptb': 15.0,
    'ptheavy': 2.0,
    'mmll': 4.0,
    'drll': 0.2,
    'drjl': 0.2,
    'drjj': 0.4,
    'dral': 0.1,
    'etaj': 5.5,
    'etal': 5.0,
    'maxjetflavor': 5,
    'asrwgtflavor': 5,
    'auto_ptj_mjj': False,
    'cut_decays': True,
    'event_norm': 'average',
}
extras["nevents"] = int(nevents)

process_dir = new_process(process)

modify_run_card(process_dir=process_dir, run_card_backup='run_card_backup.dat',
                settings=extras)
print_cards()

generate(required_accuracy=0.001,process_dir=process_dir,grid_pack=gridpack_mode,gridpack_compile=gridpack_compile, runArgs=runArgs)

outputDS=arrange_output(process_dir=process_dir,lhe_version=3,saveProcDir=True,runArgs=runArgs)
runArgs.inputGeneratorFile=outputDS

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SpaceShower:dipoleRecoil=on"]
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")
