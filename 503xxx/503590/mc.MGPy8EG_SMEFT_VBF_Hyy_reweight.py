from MadGraphControl.MadGraphUtils import *
import subprocess
#from os.path import join as pathjoin 

fcard = open('proc_card_mg5.dat','w')

safefactor=1.1
nevents=runArgs.maxEvents*safefactor
runName='run_01'


# General settings                                                                                                          
nevents = runArgs.maxEvents*safefactor if runArgs.maxEvents>0 else safefactor*evgenConfig.nEventsPerJob

gridpack_mode=False

#==================================================================================

print "Generate VBF events."


if not is_gen_from_gridpack():
    process = """
import model SMEFTsim_A_U35_MwScheme_UFO-massless_Hyy
define p = p b b~ 
generate p p > h j j QCD=0  NP^2==1 NP=1
output -f
"""
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION




#==================================================================================



#==================================================================================

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version':'3.0', 
           'cut_decays':'F', 
           'pdlabel':'lhapdf',
           'lhaid':'90400',
           'ickkw'       : 0,  
           'drll'        : 0.05,
           'drjj'        : 0.05,
           'drbb'        : 0.05,
           'drbj'        : 0.05,
           'ptb'         : 20.0,
           'ptj'         : 20.0,
           'ptl'         : 0.0,
           'etal'        : 10, 
           'ktdurham'    : 30,  
           'use_syst'    : "False",
           'nevents'    :int(nevents) 

}
 
   

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

#==================================================================================



#==================================================================================
#Building param_card setting to 0 c_is that are not of interest
 
frblock= {'cHW'   : 0.3, #28
          'cHB'   : 1.03, #30 
          'cHWB'  : 0.15, #32
          'cHl3'  : 0.02, #46
          'cHl1'   : 0.02, #45
          'cHq3'   : 0.01, #49
          'cHq1'   : 0.3, #48
          'cHu'   : 0.91, #50
          'cHd'   : 0.92, #51

          }
yukawa={'ymb'     : 0.0,
        }
params={}
params['frblock']=frblock
params['yukawa']=yukawa
modify_param_card(process_dir=process_dir,params=params)


generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#==================================================================================


rcard = open('reweight_card.dat','w')

params=["cHW","cHB","cHWB","cHl3","cHl1","cHq3","cHq1","cHu","cHd"]
block=[28,30,32,46,45,49,48,50,51]
vals=[-1.0,-0.5,-0.1,0.1,0.5,1.0]

reweightCommand=""


for par in params:
    for val in vals:
        sign = "p"
        if val < 0.0:
            sign="m"
        valabs = str(abs(val)).replace(".","p")

        launch="launch --rwgt_name="+str(par)+"_"+sign+"_"+str(valabs)+"\n"
        reweightCommand=reweightCommand+launch
        idx=0
        for par2 in params:
            paramstring=""
            if(par2==par):
                paramstring="set frblock "+str(block[idx])+" "+str(val)+"\n"
            else:
                paramstring="set frblock "+str(block[idx])+" 0.0"+"\n"
            reweightCommand=reweightCommand+paramstring
            idx=idx+1

#This is SM                                        
                                                                        
SMprocess="change process p p > h j j QCD=0  NP=0 \n" 
launch="launch --rwgt_name=SM"+"\n"
reweightCommand=reweightCommand+"\n"+SMprocess+launch


rcard.write(reweightCommand)
rcard.close()

subprocess.call('cp reweight_card.dat ' + process_dir+'/Cards/', shell=True)

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#==================================================================================
# Shower 
evgenConfig.description = 'MadGraphSMEFT_VBF'
evgenConfig.keywords    = ['VBF', 'Higgs', 'mH125', 'EFT']
evgenConfig.contact     = ['ana.cueto@cern.ch']
evgenConfig.generators  = ['MadGraph', 'Pythia8', 'EvtGen']



include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 22 22' ]



