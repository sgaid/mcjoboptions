include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()
#get the file name and split it on _ to extract relavent information                                                                                              
jobConfigParts = JOName.split("_")

mstop=float(jobConfigParts[4])
mneutralino=float(jobConfigParts[5].split('.')[0])

masses['6'] = 172.5  ## TODO: overwrite top mass value of 175GeV in MSSM_SLHA2
masses['24'] = 80.3
masses['1000006'] = mstop
masses['1000022'] = mneutralino
if masses['1000022']<0.5: masses['1000022']=0.5

for mass  in masses.keys():
    if not any(mass==x for x in ["1000006","1000022","2000006","1000021"]) and int(mass)>= 1000000:
        del masses[mass]
    if any(mass==x for x in ["35","36","37"]):
        del masses[mass]

process = '''
define p = g u c d s u~ c~ d~ s~ 
define j = g u c d s u~ c~ d~ s~ 
generate p p > t1 t1~  $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @1
add process p p > t1 t1~ j $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @2
add process p p > t1 t1~ j j $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @3
'''

decays['1000006'] = """DECAY   1000006     1.34259598E-01   # stop1 decays
     1.00000000E+00    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
"""

run_settings['maxjetflavor'] = 4


evgenLog.info('Registered generation of stop pair production, stop to t+LSP; grid point '+str(runArgs.jobConfig[0])+' decoded into mass point mstop=' + str(masses['1000006']) + ', mlsp='+str(masses['1000022']))

#--------------------------------------------------------------
#                      Madspin configuration
#--------------------------------------------------------------
 
#if 'MadSpin' in JOName:
evgenLog.info('Running w/ MadSpin option')
madspin_card='madspin_card.dat'

mscard = open(madspin_card,'w')

mscard.write("""#************************************************************   
#*                        MadSpin                           *   
#*                                                          *   
#*  P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer   *   
#*                                                          *   
#*         Part of the MadGraph5_aMC@NLO Framework:         *   
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *   
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *   
#*                                                          *   
#************************************************************   
#Some options (uncomment to apply)  
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event  
#   
set seed %i 
# specify the decay for the final state particles   
decay t1 > n1 t
decay t1~ > n1 t~
#   
#   
# running the actual code       
launch"""%runArgs.randomSeed)

mscard.close()


evgenConfig.contact  = [ "daniele.zanzi@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel','stop']
evgenConfig.description = 'stop direct pair production, st->t+LSP in simplified model'

genSeq.Pythia8.Commands += [ "Merging:Process = guess" ]
genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

