from MadGraphControl.MadGraphUtils import *

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
ma = float(phys_short.split('_')[2].replace("a",""))
mH = float(phys_short.split('_')[3].replace("H",""))
mchi = float(phys_short.split('_')[4].replace("D",""))
tanb=float(phys_short.split('_')[5].replace("tb","").replace("p","."))
sinp=float(phys_short.split('_')[6].replace("st","").replace("p","."))
evgenLog.info('Processing model with masses: (ma, mH) = (%e,%e)' %(ma, mH))

gen_process = """
import model Pseudoscalar_2HDM-bbMET_5FS -modelname
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
generate p p > xd xd~ t w-
add process p p > xd xd~ t~ w+ 
output -f
"""

if "1L0L" in phys_short:
    evgenLog.info('1lepton and MET 60 filter or MET 150 is applied')
    include ( 'GeneratorFilters/LeptonFilter.py' )
    filtSeq.LeptonFilter.Ptcut  = 20000.
    filtSeq.LeptonFilter.Etacut = 2.8

    include('GeneratorFilters/MissingEtFilter.py')
    filtSeq.MissingEtFilter.METCut = 60000.


    filtSeq += MissingEtFilter("MissingEtFilterHard")
    filtSeq.MissingEtFilterHard.METCut = 150000.

    filtSeq.Expression = "(LeptonFilter and MissingEtFilter) or MissingEtFilterHard"


if "2L" in phys_short:
    evgenLog.info('MultiElecMuTauFilter is applied')
    include ( 'GeneratorFilters/MultiElecMuTauFilter.py' )
    filtSeq.MultiElecMuTauFilter.MinPt  = 15000
    filtSeq.MultiElecMuTauFilter.MaxEta = 2.8
    filtSeq.MultiElecMuTauFilter.NLeptons = 2
    filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0
    filtSeq.Expression = "MultiElecMuTauFilter"


if evt_multiplier>0:
    if runArgs.maxEvents>0:
        nevents=runArgs.maxEvents*evt_multiplier
    else:    
        nevents=evgenConfig.nEventsPerJob*evt_multiplier

run_settings = {'lhe_version':'3.0',
          'pdlabel'    : "'lhapdf'",
          'lhaid'      : 260000,
          'cut_decays'      : 'F',
          'maxjetflavor':5, # 5 flavor scheme,
          'use_syst': 'F' # 5 flavor scheme
          }

#run_settings['event_norm']='sum'
run_settings['nevents'] = nevents


# Set up the process
process_dir = new_process(gen_process)
# Set up the run card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_settings)

# Set up the MadSpin card (if needed)
madspin_card_loc=process_dir+'/Cards/madspin_card.dat'
mscard = open(madspin_card_loc,'w')  

if "2L" in phys_short:
    mscard.write("""
    set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
    set seed %i                                                                                                                                                               
    # specify the decay for the final state particles                                                                                                                         
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    decay t > w+ b, w+ > l+ vl
    decay t~ > w- b~, w- > l- vl~ 
    decay w+ > l+ vl 
    decay w- > l- vl~ 
    # running the actual code                                                              
    launch"""%runArgs.randomSeed)      
else:
    mscard.write("""
    set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
    set seed %i                                                                                                                                                               
    # specify the decay for the final state particles                                                                                                                         
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    decay t > w+ b
    decay t~ > w- b~
    decay w+ > all all
    decay w- > all all
    # running the actual code                                                                                                                                                 
    launch"""%runArgs.randomSeed)   

mscard.close()

# this is a dictionary
params = {}
params["DMINPUTS"] = {}
params["DMINPUTS"]["GPXD"] = 1.0 
params["FRBLOCK"]={}
params["FRBLOCK"]["TANBETA"]=tanb
params["FRBLOCK"]["SINBMA"]=1.0
params["HIGGS"]={}
if sinp==0.7: sinp=0.7071
params["HIGGS"]["SINP"]=sinp
params["HIGGS"]["LAM3"]=3.
params["HIGGS"]["LAP1"]=3.
params["HIGGS"]["LAP2"]=3.
params['MASS']={'1000022':mchi,'55':ma,'37':mH,'36':mH,'35':mH}
params['DECAY']={'35':'DECAY  35 Auto','36':'DECAY  36 Auto','37':'DECAY  37 Auto','55':'DECAY  55 Auto'}

modify_param_card(process_dir=process_dir,params=params)

#reweight
if reweight:
  print(params)
  reweight_card_loc=process_dir+'/Cards/reweight_card.dat'
  rwcard = open(reweight_card_loc,'w')

  for rw_name in reweights:
    params_rwt = params.copy()
    for param in rw_name.split('-'):
      param_name, value = param.split('_')
      if param_name == "SINP":
        if value==0.7: value=0.7071
        params_rwt['HIGGS']['SINP'] = value
      elif param_name == "TANB":
        params_rwt['FRBLOCK']['TANBETA'] = value

    param_card_reweight = process_dir+'/Cards/param_card_reweight.dat'
    #shutil.copy(process_dir+'/Cards/param_card_default.dat', param_card_reweight)
    shutil.copy(process_dir+'/Cards/param_card.dat', param_card_reweight)
    param_card_rwt_new=process_dir+'/Cards/param_card_rwt_%s.dat' % rw_name
    modify_param_card(param_card_input=param_card_reweight, process_dir=process_dir,params=params_rwt, output_location=param_card_rwt_new)

    rwcard.write("launch --rwgt_info=%s\n" % rw_name)
    rwcard.write("%s\n" % param_card_rwt_new)
  rwcard.close()


print_cards()

# Generate the events
generate(process_dir=process_dir,runArgs=runArgs)

# Remember to set saveProcDir to FALSE before sending for production!!
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

# Set the evgen metadata
evgenConfig.description = 'DMtW LO, m_med = %s GeV, m_chi = %s GeV'%(ma, mH)
evgenConfig.keywords = ["exotic","BSM","WIMP", "SUSY"]
evgenConfig.contact = ["Claudia Seitz <claudia.seitz@cern.ch>, Priscilla Pani <ppani@cern.ch>, Michaela Queitsch-Maitland <michaela.queitsch-maitland@cern.ch>"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Reset the number of processes for Pythia8
check_reset_proc_number(opts)

genSeq.Pythia8.Commands += ["1000022:all = xd xd~ 2 0 0 %s " %mchi,
                            "1000022:isVisible = false"
                           ]

