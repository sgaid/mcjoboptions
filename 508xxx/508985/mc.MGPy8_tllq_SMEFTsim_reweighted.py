selected_operators = ['ctWRe','ctBRe','cHQ1','cHQ3','cHt','cQj31','cQj38','cQl111','cQl122','cQl133','cQl311','cQl322','cQl333','ctWIm','ctBIm']

process_definition = 'generate p p > t j l+ l- $$ w+ w- QCD=1 QED=4 NP=1, (t > w+ b NP=0,  w+ > l+ vl NP=0) @0 NPprop=0 SMHLOOP=0 NPcG=0 NPcHbox=0 NPcHG=0 NPcHW=0 NPcHB=0 NPctH=0 NPcbW=0 NPceH=0 NPcHl=0 NPcll1=0 NPcHtb=0\n'
process_definition+= 'add process p p > t~ j l+ l- $$ w+ w- QCD=1 QED=4 NP=1, (t~ > w- b~ NP=0, w- > l- vl~ NP=0) @1 NPprop=0 SMHLOOP=0 NPcG=0 NPcHbox=0 NPcHG=0 NPcHW=0 NPcHB=0 NPctH=0 NPcbW=0 NPceH=0 NPcHl=0 NPcll1=0 NPcHtb=0'

fixed_scale = 263.5 # ~ m(top)+m(Z)

gridpack = False

evgenConfig.description = 'SMEFTsim 3.0 t+ll, top model, trilepton, reweighted, EFT vertices, no propagator correction'

include("Common_SMEFTsim_topmW_tllq_reweighted.py")
