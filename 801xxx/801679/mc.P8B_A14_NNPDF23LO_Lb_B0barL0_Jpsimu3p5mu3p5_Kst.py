###########################################################################################
# Job options for Pythia8B_i generation of L_b(Xi_bcd)->B0bar(->J/psi(mumu)Kpi)Lambda0(ppi)
###########################################################################################
evgenConfig.description = "Signal  L_b(Xi_bcd)->B0bar(->J/psi(mumu)Kpi)Lambda0(ppi)"
evgenConfig.keywords = ["bottom","exclusive","Lambda_b0","Jpsi","2muon"]
evgenConfig.contact = [ 'gladilin@mail.cern.ch' ]
evgenConfig.process = "pp->bb->L_b(Xi_bcd)->B0bar(->J/psi(mumu)Kpi)Lambda0(ppi)"
evgenConfig.nEventsPerJob = 200

include("Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py")

genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = off']
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
#
genSeq.Pythia8B.VetoDoubleBEvents = True

#
# Event selection
#
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 10.'] 
genSeq.Pythia8B.QuarkPtCut = 10.0
genSeq.Pythia8B.AntiQuarkPtCut = 0.0
genSeq.Pythia8B.QuarkEtaCut = 3.5
genSeq.Pythia8B.AntiQuarkEtaCut = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

#
# J/psi:
#
genSeq.Pythia8B.Commands += ['443:m0 = 3.096900']  # PDG 2017/2019/2021/2022
genSeq.Pythia8B.Commands += ['443:mWidth = 0.0000926'] # PDG 2021/2022
#
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']

#
# B_d:
#
genSeq.Pythia8B.Commands += ['511:m0 = 5.27966']  # PDG 2022
genSeq.Pythia8B.Commands += ['511:tau0 = 0.4554'] # PDG 2022
#
# B_d decays:
#
genSeq.Pythia8B.Commands += ['511:onMode = 2']
#
# channels 850-869 have onMode = 2 in Pythia8 default
#
#genSeq.Pythia8B.Commands += ['511:850:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:851:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:852:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:853:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:854:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:855:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:856:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:857:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:858:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:859:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:860:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:861:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:862:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:863:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:864:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:865:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:866:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:867:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:868:onMode = 0']
#genSeq.Pythia8B.Commands += ['511:869:onMode = 0']
#
# channels 870-889 have onMode = 3 in Pythia8 default
#
genSeq.Pythia8B.Commands += ['511:870:onMode = 0']
genSeq.Pythia8B.Commands += ['511:871:onMode = 0']
genSeq.Pythia8B.Commands += ['511:872:onMode = 0']
genSeq.Pythia8B.Commands += ['511:873:onMode = 0']
genSeq.Pythia8B.Commands += ['511:874:onMode = 0']
genSeq.Pythia8B.Commands += ['511:875:onMode = 0']
genSeq.Pythia8B.Commands += ['511:876:onMode = 0']
genSeq.Pythia8B.Commands += ['511:877:onMode = 0']
genSeq.Pythia8B.Commands += ['511:878:onMode = 0']
genSeq.Pythia8B.Commands += ['511:879:onMode = 0']
genSeq.Pythia8B.Commands += ['511:880:onMode = 0']
genSeq.Pythia8B.Commands += ['511:881:onMode = 0']
genSeq.Pythia8B.Commands += ['511:882:onMode = 0']
genSeq.Pythia8B.Commands += ['511:883:onMode = 0']
genSeq.Pythia8B.Commands += ['511:884:onMode = 0']
genSeq.Pythia8B.Commands += ['511:885:onMode = 0']
genSeq.Pythia8B.Commands += ['511:886:onMode = 0']
genSeq.Pythia8B.Commands += ['511:887:onMode = 0']
genSeq.Pythia8B.Commands += ['511:888:onMode = 0']
genSeq.Pythia8B.Commands += ['511:889:onMode = 0']
#
genSeq.Pythia8B.Commands += ['511:addChannel = 3 1.0 0  443 313']
#

#
# K*0 -> K+ pi-
#
genSeq.Pythia8B.Commands += ['313:m0 = 0.89555']  # PDG 2022
genSeq.Pythia8B.Commands += ['313:mWidth = 0.0473'] # PDG 2022
#
genSeq.Pythia8B.Commands += ['313:1:onMode = 2']
genSeq.Pythia8B.Commands += ['313:2:onMode = 2']
#

#
# L_b as Xi_bcd
#
# L_b:
#
genSeq.Pythia8B.Commands += ['5122:m0 = 8.123']  # as found by Vladimir Nikolaenko
genSeq.Pythia8B.Commands += ['5122:tau0 = 0.15289'] # as for B_c from PDG 2022
#
# L_b decays:
#
genSeq.Pythia8B.Commands += ['5122:onMode = 3']
#
genSeq.Pythia8B.Commands += ['5122:addChannel = 2 1.0 0 -511 3122']
#
# To allow Sigma_b decays to Xi_bc
#
genSeq.Pythia8B.Commands += ['5112:m0 = 8.31']  # Sigma_b-  -> L_b pi
genSeq.Pythia8B.Commands += ['5114:m0 = 8.33']  # Sigma_b*- -> L_b pi
genSeq.Pythia8B.Commands += ['5212:m0 = 8.30']  # Sigma_b0  -> L_b pi
genSeq.Pythia8B.Commands += ['5214:m0 = 8.31']  # Sigma_b*0 -> L_b pi
genSeq.Pythia8B.Commands += ['5222:m0 = 8.31']  # Sigma_b+  -> L_b pi
genSeq.Pythia8B.Commands += ['5224:m0 = 8.33']  # Sigma_b*+ -> L_b pi
#
genSeq.Pythia8B.SignalPDGCodes = [5122,-511,443,-13,13,-313,-321,211,3122]
genSeq.Pythia8B.SignalPtCuts = [0.,0.,0.,3.5,3.5,0.,0.45,0.45,1.2]
genSeq.Pythia8B.SignalEtaCuts = [100.,100.,100.,2.5,2.5,100.,2.7,2.7,2.7]

genSeq.Pythia8B.NHadronizationLoops = 10
