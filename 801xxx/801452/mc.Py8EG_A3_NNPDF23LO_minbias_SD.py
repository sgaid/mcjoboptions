evgenConfig.description = "Pythia8 single diffractive minimum bias events with the A3 NNPDF23LO tune and EvtGen"
evgenConfig.keywords = ["QCD", "minBias", "SM"]
evgenConfig.contact  = [ "karol.krizka@cern.ch"]
evgenConfig.nEventsPerJob = 10000

include ("Pythia8_i/Pythia8_A3_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += [ "SoftQCD:singleDiffractive = on" ]

