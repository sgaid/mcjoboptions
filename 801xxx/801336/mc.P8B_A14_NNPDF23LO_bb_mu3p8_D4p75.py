##############################################################################
# Job options for Pythia8B_i generation of bb->muD
##############################################################################
evgenConfig.description = "Signal bb->muD"
evgenConfig.keywords = ["bottom","exclusive","muon"]
evgenConfig.contact = [ 'gladilin@mail.cern.ch' ]
evgenConfig.process = "pp->bb->muD"
evgenConfig.nEventsPerJob = 200

#include("Pythia8B_i/Pythia8B_A14_CTEQ6L1_Common.py")
include("Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py")

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
#
genSeq.Pythia8B.Commands += ['ParticleDecays:mixB = on']
genSeq.Pythia8B.Commands += ['ParticleDecays:xBdMix = 0.769']  # PDG 2020/21
genSeq.Pythia8B.Commands += ['ParticleDecays:xBsMix = 26.89']  # PDG 2020/21
#
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
#
genSeq.Pythia8B.VetoDoubleBEvents = False
genSeq.Pythia8B.VetoDoubleCEvents = False

#
# Event selection
#
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.'] 
genSeq.Pythia8B.QuarkPtCut = 8.0
genSeq.Pythia8B.AntiQuarkPtCut = 8.0
genSeq.Pythia8B.QuarkEtaCut = 3.5
genSeq.Pythia8B.AntiQuarkEtaCut = 3.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False

#
# mu-:
#
genSeq.Pythia8B.Commands += ['13:m0 = 0.1056583745']  # PDG 2021
#
# tau-:
#
genSeq.Pythia8B.Commands += ['15:m0 = 1.77686']  # PDG 2021
genSeq.Pythia8B.Commands += ['15:tau0 = 0.08703'] # PDG 2021
#
# pi+:
#
genSeq.Pythia8B.Commands += ['211:m0 = 0.13957039']  # PDG 2021
#
# K+:
#
genSeq.Pythia8B.Commands += ['321:m0 = 0.493677']  # PDG 2021
#
# p:
#
genSeq.Pythia8B.Commands += ['2212:m0 = 0.9382720813']  # PDG 2021
#
# Lambda_b:
#
genSeq.Pythia8B.Commands += ['5122:m0 = 5.61960']  # PDG 2020/21
genSeq.Pythia8B.Commands += ['5122:tau0 = 0.4410'] # PDG 2020/21
#
# B_d:
#
genSeq.Pythia8B.Commands += ['511:m0 = 5.27965']  # PDG 2020/21
genSeq.Pythia8B.Commands += ['511:tau0 = 0.4554'] # PDG 2020/21
#
# B_u:
#
genSeq.Pythia8B.Commands += ['521:m0 = 5.27934']  # PDG 2020/21
genSeq.Pythia8B.Commands += ['521:tau0 = 0.4911'] # PDG 2020/21
#
# B_s:
#
genSeq.Pythia8B.Commands += ['531:m0 = 5.36688']  # PDG 2020/21
genSeq.Pythia8B.Commands += ['531:tau0 = 0.4545'] # PDG 2021
#
# B_c:
#
genSeq.Pythia8B.Commands += ['541:m0 = 6.27447']  # PDG 2021
genSeq.Pythia8B.Commands += ['541:tau0 = 0.1529'] # PDG 2021
#
# J/psi:
#
genSeq.Pythia8B.Commands += ['443:m0 = 3.096900']  # PDG 2017/2019/2021
genSeq.Pythia8B.Commands += ['443:mWidth = 0.0000926'] # PDG 2021
#
# D*+:
#
genSeq.Pythia8B.Commands += ['413:m0 = 2.01026']  # PDG 2021
#
# D+:
#
genSeq.Pythia8B.Commands += ['411:m0 = 1.86966']  # PDG 2021
genSeq.Pythia8B.Commands += ['411:tau0 = 0.3118'] # PDG 2021
#
# D0:
#
genSeq.Pythia8B.Commands += ['421:m0 = 1.86484']  # PDG 2021
genSeq.Pythia8B.Commands += ['421:tau0 = 0.1229'] # PDG 2021
#
# D_s:
#
genSeq.Pythia8B.Commands += ['431:m0 = 1.96835']  # PDG 2021
genSeq.Pythia8B.Commands += ['431:tau0 = 0.1520'] # PDG 2021
#
# Lambda_c:
#
genSeq.Pythia8B.Commands += ['4122:m0 = 2.28646']  # PDG 2021
genSeq.Pythia8B.Commands += ['4122:tau0 = 0.06068'] # PDG 2021

#genSeq.Pythia8B.SignalPDGCodes = [443,-13,13]
#genSeq.Pythia8B.SignalPtCuts = [0.,3.5,3.5]
#genSeq.Pythia8B.SignalEtaCuts = [100.,2.5,2.5]

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [3.8]
genSeq.Pythia8B.TriggerStateEtaCut = 2.7
genSeq.Pythia8B.MinimumCountPerCut = [1]

genSeq.Pythia8B.NHadronizationLoops = 40

from GeneratorFilters.GeneratorFiltersConf import ParentsTracksFilter
ParentsTracksFilter421 = ParentsTracksFilter("ParentsTracksFilter421")
filtSeq += ParentsTracksFilter421
ParentsTracksFilter411 = ParentsTracksFilter("ParentsTracksFilter411")
filtSeq += ParentsTracksFilter411
ParentsTracksFilter413 = ParentsTracksFilter("ParentsTracksFilter413")
filtSeq += ParentsTracksFilter413
ParentsTracksFilter431 = ParentsTracksFilter("ParentsTracksFilter431")
filtSeq += ParentsTracksFilter431
ParentsTracksFilter4122 = ParentsTracksFilter("ParentsTracksFilter4122")
filtSeq += ParentsTracksFilter4122

filtSeq.Expression = "(ParentsTracksFilter421 and ParentsTracksFilter413) or ParentsTracksFilter411 or ParentsTracksFilter431 or ParentsTracksFilter4122" 

#include("GeneratorFilters/ParentsTracksFilter.py")
#include("ParentsTracksFilter.py")                                                                                                                                                             
filtSeq.ParentsTracksFilter421.PDGParent  = [421]
filtSeq.ParentsTracksFilter421.PtMinParent =  3750.
filtSeq.ParentsTracksFilter421.EtaRangeParent = 2.7
filtSeq.ParentsTracksFilter421.PtMinLeptons = 950.
filtSeq.ParentsTracksFilter421.EtaRangeLeptons = 2.7
filtSeq.ParentsTracksFilter421.PtMinHadrons = 950.
filtSeq.ParentsTracksFilter421.EtaRangeHadrons = 2.7
filtSeq.ParentsTracksFilter421.NumMinTracks = 2
filtSeq.ParentsTracksFilter421.NumMaxTracks = 2
filtSeq.ParentsTracksFilter421.NumMinLeptons = 0
filtSeq.ParentsTracksFilter421.NumMaxLeptons = 0
filtSeq.ParentsTracksFilter421.NumMinOthers = 0
filtSeq.ParentsTracksFilter421.NumMaxOthers = 0

filtSeq.ParentsTracksFilter413.PDGParent  = [413]
filtSeq.ParentsTracksFilter413.PtMinParent =  4250.
filtSeq.ParentsTracksFilter413.EtaRangeParent = 2.7
filtSeq.ParentsTracksFilter413.PtMinLeptons = 950.
filtSeq.ParentsTracksFilter413.EtaRangeLeptons = 2.7
filtSeq.ParentsTracksFilter413.PtMinHadrons = 450.
filtSeq.ParentsTracksFilter413.EtaRangeHadrons = 2.7
filtSeq.ParentsTracksFilter413.NumMinTracks = 3
filtSeq.ParentsTracksFilter413.NumMaxTracks = 3
filtSeq.ParentsTracksFilter413.NumMinLeptons = 0
filtSeq.ParentsTracksFilter413.NumMaxLeptons = 0
filtSeq.ParentsTracksFilter413.NumMinOthers = 0
filtSeq.ParentsTracksFilter413.NumMaxOthers = 0

filtSeq.ParentsTracksFilter411.PDGParent  = [411]
filtSeq.ParentsTracksFilter411.PtMinParent =  4750.
filtSeq.ParentsTracksFilter411.EtaRangeParent = 2.7
filtSeq.ParentsTracksFilter411.PtMinLeptons = 950.
filtSeq.ParentsTracksFilter411.EtaRangeLeptons = 2.7
filtSeq.ParentsTracksFilter411.PtMinHadrons = 950.
filtSeq.ParentsTracksFilter411.EtaRangeHadrons = 2.7
filtSeq.ParentsTracksFilter411.NumMinTracks = 3
filtSeq.ParentsTracksFilter411.NumMaxTracks = 3
filtSeq.ParentsTracksFilter411.NumMinLeptons = 0
filtSeq.ParentsTracksFilter411.NumMaxLeptons = 0
filtSeq.ParentsTracksFilter411.NumMinOthers = 0
filtSeq.ParentsTracksFilter411.NumMaxOthers = 0

filtSeq.ParentsTracksFilter431.PDGParent  = [431]
filtSeq.ParentsTracksFilter431.PtMinParent =  1950.
filtSeq.ParentsTracksFilter431.EtaRangeParent = 2.7
filtSeq.ParentsTracksFilter431.PtMinLeptons = 950.
filtSeq.ParentsTracksFilter431.EtaRangeLeptons = 2.7
filtSeq.ParentsTracksFilter431.PtMinHadrons = 950.
filtSeq.ParentsTracksFilter431.EtaRangeHadrons = 2.7
filtSeq.ParentsTracksFilter431.NumMinTracks = 3
filtSeq.ParentsTracksFilter431.NumMaxTracks = 3
filtSeq.ParentsTracksFilter431.NumMinLeptons = 0
filtSeq.ParentsTracksFilter431.NumMaxLeptons = 0
filtSeq.ParentsTracksFilter431.NumMinOthers = 0
filtSeq.ParentsTracksFilter431.NumMaxOthers = 0

filtSeq.ParentsTracksFilter4122.PDGParent  = [4122]
filtSeq.ParentsTracksFilter4122.PtMinParent =  4750.
filtSeq.ParentsTracksFilter4122.EtaRangeParent = 2.7
filtSeq.ParentsTracksFilter4122.PtMinLeptons = 950.
filtSeq.ParentsTracksFilter4122.EtaRangeLeptons = 2.7
filtSeq.ParentsTracksFilter4122.PtMinHadrons = 950.
filtSeq.ParentsTracksFilter4122.EtaRangeHadrons = 2.7
filtSeq.ParentsTracksFilter4122.NumMinTracks = 3
filtSeq.ParentsTracksFilter4122.NumMaxTracks = 3
filtSeq.ParentsTracksFilter4122.NumMinLeptons = 0
filtSeq.ParentsTracksFilter4122.NumMaxLeptons = 0
filtSeq.ParentsTracksFilter4122.NumMinOthers = 0
filtSeq.ParentsTracksFilter4122.NumMaxOthers = 0

