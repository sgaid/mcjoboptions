include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

evgenConfig.description = "Dijet truth jet slice JZ1, with the A14 NNPDF23 LO tune, mu-filtered, using Soft QCD"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["yi.yu@cern.ch"]

genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)    
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)  
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(1,filtSeq) 

include("GeneratorFilters/xAODMuonFilter_Common.py")
filtSeq.xAODMuonFilter.Ptcut = 3000.0
filtSeq.xAODMuonFilter.Etacut = 2.8

evgenConfig.nEventsPerJob = 200

