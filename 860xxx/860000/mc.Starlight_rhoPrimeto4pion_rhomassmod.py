evgenConfig.description = "Starlight gamma + A -> rho'->2(pi+pi-)"
evgenConfig.keywords = ["2photon"]
evgenConfig.contact = ["angerami@cern.ch","peter.steinberg@bnl.gov"]
evgenConfig.nEventsPerJob = 10000

if int(runArgs.ecmEnergy) != 5020:
    evgenLog.error("This JO can currently only be run for a beam energy of 5020 GeV")
    sys.exit(1)

include("Starlight_i/Starlight_Common.py")

genSeq.Starlight.ConfigFileName='rhoPrime.slight.in'
