
evgenConfig.description = "MadGraph+Pythia8 production JO with NNPDF30LN and A15NNPDF23LO for VLQ single T to Zt  while produced via Z"
evgenConfig.keywords = ["BSM", "BSMtop", "exotic"]
evgenConfig.process = "ZTZt"
evgenConfig.contact =  ['avik.roy@cern.ch, fschenck@cern.ch']

evgenConfig.inputFilesPerJob = 1
evgenConfig.nEventsPerJob = 10000


include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
