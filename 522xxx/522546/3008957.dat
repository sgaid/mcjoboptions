# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  22:14
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.31791870E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.04330631E+03  # scale for input parameters
    1    1.84062073E+02  # M_1
    2    2.68630789E+02  # M_2
    3    1.73054446E+03  # M_3
   11   -6.85904026E+03  # A_t
   12    1.61384440E+03  # A_b
   13   -4.03490346E+02  # A_tau
   23    4.69090435E+02  # mu
   25    1.21887039E+00  # tan(beta)
   26    2.46051141E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.72887899E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.69426337E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.52168748E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.04330631E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.04330631E+03  # (SUSY scale)
  1  1     1.08450753E-05   # Y_u(Q)^DRbar
  2  2     5.50929827E-03   # Y_c(Q)^DRbar
  3  3     1.31017221E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.04330631E+03  # (SUSY scale)
  1  1     2.65625856E-05   # Y_d(Q)^DRbar
  2  2     5.04689127E-04   # Y_s(Q)^DRbar
  3  3     2.63417614E-02   # Y_b(Q)^DRbar
Block Ye Q=  3.04330631E+03  # (SUSY scale)
  1  1     4.63536543E-06   # Y_e(Q)^DRbar
  2  2     9.58446557E-04   # Y_mu(Q)^DRbar
  3  3     1.61194944E-02   # Y_tau(Q)^DRbar
Block Au Q=  3.04330631E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -6.85903970E+03   # A_t(Q)^DRbar
Block Ad Q=  3.04330631E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.61384441E+03   # A_b(Q)^DRbar
Block Ae Q=  3.04330631E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -4.03490347E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.04330631E+03  # soft SUSY breaking masses at Q
   1    1.84062073E+02  # M_1
   2    2.68630789E+02  # M_2
   3    1.73054446E+03  # M_3
  21   -2.04469011E+05  # M^2_(H,d)
  22    9.49067449E+04  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.72887899E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.69426337E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.52168748E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     9.72109146E+01  # h0
        35     2.70061247E+02  # H0
        36     2.46051141E+02  # A0
        37     2.40414496E+02  # H+
   1000001     1.01183198E+04  # ~d_L
   2000001     1.00930158E+04  # ~d_R
   1000002     1.01182174E+04  # ~u_L
   2000002     1.00964933E+04  # ~u_R
   1000003     1.01183209E+04  # ~s_L
   2000003     1.00930159E+04  # ~s_R
   1000004     1.01182185E+04  # ~c_L
   2000004     1.00964954E+04  # ~c_R
   1000005     2.69496879E+03  # ~b_1
   2000005     4.57342881E+03  # ~b_2
   1000006     2.66231865E+03  # ~t_1
   2000006     3.47881472E+03  # ~t_2
   1000011     1.00215127E+04  # ~e_L-
   2000011     1.00086401E+04  # ~e_R-
   1000012     1.00210053E+04  # ~nu_eL
   1000013     1.00215127E+04  # ~mu_L-
   2000013     1.00086402E+04  # ~mu_R-
   1000014     1.00210053E+04  # ~nu_muL
   1000015     1.00086656E+04  # ~tau_1-
   2000015     1.00215267E+04  # ~tau_2-
   1000016     1.00210185E+04  # ~nu_tauL
   1000021     2.10915416E+03  # ~g
   1000022     1.77187212E+02  # ~chi_10
   1000023     2.67284497E+02  # ~chi_20
   1000025     4.83342751E+02  # ~chi_30
   1000035     5.17063533E+02  # ~chi_40
   1000024     2.64958589E+02  # ~chi_1+
   1000037     5.12207031E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -7.74012064E-01   # alpha
Block Hmix Q=  3.04330631E+03  # Higgs mixing parameters
   1    4.69090435E+02  # mu
   2    1.21887039E+00  # tan[beta](Q)
   3    2.43254667E+02  # v(Q)
   4    6.05411640E+04  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.81839633E-01   # Re[R_st(1,1)]
   1  2     1.89712772E-01   # Re[R_st(1,2)]
   2  1    -1.89712772E-01   # Re[R_st(2,1)]
   2  2     9.81839633E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99999988E-01   # Re[R_sb(1,1)]
   1  2     1.54061559E-04   # Re[R_sb(1,2)]
   2  1    -1.54061559E-04   # Re[R_sb(2,1)]
   2  2    -9.99999988E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     6.72646529E-03   # Re[R_sta(1,1)]
   1  2     9.99977377E-01   # Re[R_sta(1,2)]
   2  1    -9.99977377E-01   # Re[R_sta(2,1)]
   2  2     6.72646529E-03   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.78010396E-01   # Re[N(1,1)]
   1  2     1.17785447E-01   # Re[N(1,2)]
   1  3    -1.27336755E-01   # Re[N(1,3)]
   1  4     1.15791214E-01   # Re[N(1,4)]
   2  1    -1.67122432E-01   # Re[N(2,1)]
   2  2    -9.36280431E-01   # Re[N(2,2)]
   2  3     2.25379168E-01   # Re[N(2,3)]
   2  4    -2.11313221E-01   # Re[N(2,4)]
   3  1    -6.71570628E-03   # Re[N(3,1)]
   3  2     1.13180423E-02   # Re[N(3,2)]
   3  3     7.05966761E-01   # Re[N(3,3)]
   3  4     7.08122683E-01   # Re[N(3,4)]
   4  1    -1.24581934E-01   # Re[N(4,1)]
   4  2     3.30722610E-01   # Re[N(4,2)]
   4  3     6.59242378E-01   # Re[N(4,3)]
   4  4    -6.63702783E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.36079428E-01   # Re[U(1,1)]
   1  2     3.51788722E-01   # Re[U(1,2)]
   2  1     3.51788722E-01   # Re[U(2,1)]
   2  2     9.36079428E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.43687720E-01   # Re[V(1,1)]
   1  2     3.30837554E-01   # Re[V(1,2)]
   2  1     3.30837554E-01   # Re[V(2,1)]
   2  2     9.43687720E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02534396E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.56595756E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     2.79102413E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     1.54490795E-02    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44666547E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     4.99042120E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.20238075E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.11648218E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     5.33600651E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     7.50741036E-02    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02538042E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.56588884E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     2.79102238E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.54505413E-02    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
DECAY   1000013     1.44666729E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     4.99041696E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.20237736E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.11653426E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     5.33599979E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     7.50740090E-02    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.03608761E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.54570170E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.80201058E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     5.50240320E-04    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.57573456E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.83765582E-04    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     7.18372844E-04    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.44713983E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     4.98950597E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     3.20110804E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.97187258E-04    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.13486253E-02    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.33337004E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     7.51113200E-02    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44657522E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     1.24914526E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.18435028E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     4.78679006E-02    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     5.42316227E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.63988882E-02    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44657704E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.24914368E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.18434753E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     4.78678403E-02    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     5.42315700E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.63999086E-02    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.44709274E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.24870017E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.18357198E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     4.78508453E-02    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     5.42167108E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     6.66874254E-02    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.34245218E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.33602867E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     2.14043463E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     1.18486713E-04    2     1000035         1   # BR(~d_R -> chi^0_4 d)
     9.92331096E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.65548910E+02   # ~d_L
#    BR                NDA      ID1      ID2
     4.38278705E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     4.22249114E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     6.37485323E-03    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     9.00491344E-02    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.26702475E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.44290045E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.34245327E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.33602877E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     2.14046958E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     1.18516735E-04    2     1000035         3   # BR(~s_R -> chi^0_4 s)
     9.92330959E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.65555150E+02   # ~s_L
#    BR                NDA      ID1      ID2
     4.38275688E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     4.22246145E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     6.37483356E-03    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     9.00492593E-02    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.26764191E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.44284066E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.52457381E+02   # ~b_1
#    BR                NDA      ID1      ID2
     6.57790565E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     6.26979969E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.25387020E-04    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     9.08971926E-03    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     1.94491419E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     5.17517734E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     2.09499838E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     2.26447565E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.07573031E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     3.27148511E-04    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.36349147E-04    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.88316835E-04    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.36794881E-04    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     9.88172706E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     7.51442442E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.86824477E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     8.36868795E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     4.63261210E-04    2     1000035         2   # BR(~u_R -> chi^0_4 u)
     9.70016075E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.65545720E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.63411577E-04    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     4.79514242E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     4.87262349E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     9.15182994E-02    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.12059335E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.44283060E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.51454731E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.86820916E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     8.37217279E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     4.66810004E-04    2     1000035         4   # BR(~c_R -> chi^0_4 c)
     9.70000410E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.65551930E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.63505038E-04    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     4.79513965E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     4.87568558E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     9.15176600E-02    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.12059060E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.44277076E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.50945551E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.53675194E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.06846255E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.78054386E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.30184306E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.67885707E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     3.01700560E-04    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     2.00029598E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
     1.33052807E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     6.05943711E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.23306388E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     5.20207793E-03    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     9.42897526E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     8.96035129E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.06054379E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.72370750E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.72600804E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     1.07047018E-01    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     9.52037947E-02    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     5.72233147E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     5.08507982E-02    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     7.85630150E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     5.41090847E-02    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     5.66015586E-03   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99996003E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     4.57351355E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     4.20615897E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     4.02054923E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     3.14501156E-03    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     1.09984766E-03    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     3.31447657E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     3.57863962E-04    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     2.19505450E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.01941066E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     2.08517472E-04    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     5.26218997E-06   # chi^0_2
#    BR                NDA      ID1      ID2
     1.17509732E-04    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     8.55968588E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     9.51846409E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.09849843E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.09947424E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     3.55813012E-01    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     2.46824171E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     2.47639006E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     4.76476890E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.46317301E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     6.09422996E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.92219747E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.92219747E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     1.31911670E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.41712118E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     4.13787601E-02    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
     1.22771325E-04    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     1.46624686E-04    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     1.46624686E-04    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     4.29784783E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     3.47643552E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     3.47643552E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.08804134E-03    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     2.08804134E-03    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     4.06541559E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     3.45201037E-04    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     8.62678812E-03    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     1.10349351E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.80209414E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.34720353E-04    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     2.34720353E-04    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.24002008E-01   # ~g
#    BR                NDA      ID1      ID2
     2.05296116E-04    2     1000022        21   # BR(~g -> chi^0_1 g)
     5.34986752E-04    2     1000023        21   # BR(~g -> chi^0_2 g)
     4.59894936E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
     3.80632071E-03    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
     1.06256196E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     4.28236249E-03    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.16671890E-04    3     1000023         2        -2   # BR(~g -> chi^0_2 u u_bar)
     1.16673717E-04    3     1000023         4        -4   # BR(~g -> chi^0_2 c c_bar)
     4.81675483E-02    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.01624412E-04    3     1000023         1        -1   # BR(~g -> chi^0_2 d d_bar)
     1.01624385E-04    3     1000023         3        -3   # BR(~g -> chi^0_2 s s_bar)
     3.68346083E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.74425527E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.57918878E-01    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     4.76570343E-03    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.17736432E-04    3     1000024         1        -2   # BR(~g -> chi^+_1 d u_bar)
     2.17736432E-04    3    -1000024        -1         2   # BR(~g -> chi^-_1 d_bar u)
     2.17738666E-04    3     1000024         3        -4   # BR(~g -> chi^+_1 s c_bar)
     2.17738666E-04    3    -1000024        -3         4   # BR(~g -> chi^-_1 s_bar c)
     9.15055732E-02    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     9.15055732E-02    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.84565543E-01    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.84565543E-01    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     2.50488331E-03   # Gamma(h0)
     1.52195161E-03   2        22        22   # BR(h0 -> photon photon)
     1.44209952E-05   2        22        23   # BR(h0 -> photon Z)
     4.21748927E-04   2        23        23   # BR(h0 -> Z Z)
     3.83704676E-03   2       -24        24   # BR(h0 -> W W)
     4.21953785E-02   2        21        21   # BR(h0 -> gluon gluon)
     7.09527787E-09   2       -11        11   # BR(h0 -> Electron electron)
     3.15586468E-04   2       -13        13   # BR(h0 -> Muon muon)
     9.09217420E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.21499489E-07   2        -2         2   # BR(h0 -> Up up)
     2.35717681E-02   2        -4         4   # BR(h0 -> Charm charm)
     8.59936749E-07   2        -1         1   # BR(h0 -> Down down)
     3.11021441E-04   2        -3         3   # BR(h0 -> Strange strange)
     8.36888347E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     3.84208860E-01   # Gamma(HH)
     1.23907279E-05   2        22        22   # BR(HH -> photon photon)
     1.34358220E-05   2        22        23   # BR(HH -> photon Z)
     6.80398948E-02   2        23        23   # BR(HH -> Z Z)
     1.56332699E-01   2       -24        24   # BR(HH -> W W)
     7.48914411E-03   2        21        21   # BR(HH -> gluon gluon)
     1.48242772E-10   2       -11        11   # BR(HH -> Electron electron)
     6.59563181E-06   2       -13        13   # BR(HH -> Muon muon)
     1.90375389E-03   2       -15        15   # BR(HH -> Tau tau)
     1.47305290E-09   2        -2         2   # BR(HH -> Up up)
     2.85537062E-04   2        -4         4   # BR(HH -> Charm charm)
     1.51158213E-08   2        -1         1   # BR(HH -> Down down)
     5.46733265E-06   2        -3         3   # BR(HH -> Strange strange)
     1.47331142E-02   2        -5         5   # BR(HH -> Bottom bottom)
     2.04020438E-05   2        23        36   # BR(HH -> Z A0)
     7.51157549E-01   2        25        25   # BR(HH -> h0 h0)
DECAY        36     3.15525474E-02   # Gamma(A0)
     3.93421572E-04   2        22        22   # BR(A0 -> photon photon)
     1.23236897E-04   2        22        23   # BR(A0 -> photon Z)
     1.19493715E-01   2        21        21   # BR(A0 -> gluon gluon)
     2.08267521E-09   2       -11        11   # BR(A0 -> Electron electron)
     9.26600688E-05   2       -13        13   # BR(A0 -> Muon muon)
     2.67489640E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.14392498E-08   2        -2         2   # BR(A0 -> Up up)
     2.21961410E-03   2        -4         4   # BR(A0 -> Charm charm)
     2.15458080E-07   2        -1         1   # BR(A0 -> Down down)
     7.79308375E-05   2        -3         3   # BR(A0 -> Strange strange)
     2.11404090E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     6.39446138E-01   2        23        25   # BR(A0 -> Z h0)
     1.20432712E-31   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.82811065E+00   # Gamma(Hp)
     3.77742698E-11   2       -11        12   # BR(Hp -> Electron nu_e)
     1.61496739E-06   2       -13        14   # BR(Hp -> Muon nu_mu)
     4.56755197E-04   2       -15        16   # BR(Hp -> Tau nu_tau)
     3.71365458E-09   2        -1         2   # BR(Hp -> Down up)
     5.58107218E-08   2        -3         2   # BR(Hp -> Strange up)
     3.59742647E-08   2        -5         2   # BR(Hp -> Bottom up)
     2.43958866E-06   2        -1         4   # BR(Hp -> Down charm)
     5.53357758E-05   2        -3         4   # BR(Hp -> Strange charm)
     5.11806148E-06   2        -5         4   # BR(Hp -> Bottom charm)
     6.21611506E-05   2        -1         6   # BR(Hp -> Down top)
     1.35531594E-03   2        -3         6   # BR(Hp -> Strange top)
     9.86290453E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.17707109E-02   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.21452316E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.27112187E+00    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.48564503E+00        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    8.55602681E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    8.17505619E-01    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    6.73108301E-01        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.21452316E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.27112187E+00    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.48564503E+00        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.92461103E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    7.53889713E-03        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.92461103E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    7.53889713E-03        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.05404113E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    8.79513659E-01        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    8.15161582E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    7.53889713E-03        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.92461103E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    5.31556175E-04   # BR(b -> s gamma)
    2    1.87497550E-06   # BR(b -> s mu+ mu-)
    3    4.05815976E-05   # BR(b -> s nu nu)
    4    3.22381024E-15   # BR(Bd -> e+ e-)
    5    1.37717477E-10   # BR(Bd -> mu+ mu-)
    6    2.88276344E-08   # BR(Bd -> tau+ tau-)
    7    1.08619067E-13   # BR(Bs -> e+ e-)
    8    4.64020050E-09   # BR(Bs -> mu+ mu-)
    9    9.84073553E-07   # BR(Bs -> tau+ tau-)
   10    9.66266985E-05   # BR(B_u -> tau nu)
   11    9.98116802E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    7.28620797E-01   # |Delta(M_Bd)| [ps^-1] 
   13    2.60328750E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.92369099E-03   # epsilon_K
   17    2.28973781E-15   # Delta(M_K)
   18    2.85654595E-11   # BR(K^0 -> pi^0 nu nu)
   19    9.17704896E-11   # BR(K^+ -> pi^+ nu nu)
   20   -3.76385742E-18   # Delta(g-2)_electron/2
   21   -1.60916691E-13   # Delta(g-2)_muon/2
   22   -4.55038993E-11   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -3.09199530E-03   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -3.86285985E-01   # C7
     0305 4322   00   2    -3.62726584E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -2.74768881E-01   # C8
     0305 6321   00   2    -3.37112952E-03   # C8'
 03051111 4133   00   0     1.61739299E+00   # C9 e+e-
 03051111 4133   00   2     1.66116794E+00   # C9 e+e-
 03051111 4233   00   2     1.10019937E-07   # C9' e+e-
 03051111 4137   00   0    -4.44008509E+00   # C10 e+e-
 03051111 4137   00   2    -4.94487807E+00   # C10 e+e-
 03051111 4237   00   2    -2.43525441E-08   # C10' e+e-
 03051313 4133   00   0     1.61739299E+00   # C9 mu+mu-
 03051313 4133   00   2     1.66116794E+00   # C9 mu+mu-
 03051313 4233   00   2     1.10018459E-07   # C9' mu+mu-
 03051313 4137   00   0    -4.44008509E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.94487807E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.43510644E-08   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.61473841E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     4.31851962E-09   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.61473841E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     4.31884055E-09   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.61473841E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     4.40928252E-09   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85822395E-07   # C7
     0305 4422   00   2     2.07565643E-06   # C7
     0305 4322   00   2     1.22912821E-07   # C7'
     0305 6421   00   0     3.30482397E-07   # C8
     0305 6421   00   2     7.29072253E-07   # C8
     0305 6321   00   2     5.96853671E-08   # C8'
 03051111 4133   00   2     1.56101717E-06   # C9 e+e-
 03051111 4233   00   2     2.27101969E-08   # C9' e+e-
 03051111 4137   00   2     2.06634637E-05   # C10 e+e-
 03051111 4237   00   2    -1.56146136E-07   # C10' e+e-
 03051313 4133   00   2     1.56101715E-06   # C9 mu+mu-
 03051313 4233   00   2     2.27101968E-08   # C9' mu+mu-
 03051313 4137   00   2     2.06634637E-05   # C10 mu+mu-
 03051313 4237   00   2    -1.56146136E-07   # C10' mu+mu-
 03051212 4137   00   2    -4.39884790E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     3.38182601E-08   # C11' nu_1 nu_1
 03051414 4137   00   2    -4.39884790E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     3.38182601E-08   # C11' nu_2 nu_2
 03051616 4137   00   2    -4.39884766E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     3.38182601E-08   # C11' nu_3 nu_3
