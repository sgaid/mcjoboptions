# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 12.05.2022,  12:33
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.63019110E+00  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.58197305E+03  # scale for input parameters
    1    1.80466262E+03  # M_1
    2    7.17458294E+02  # M_2
    3    4.70004173E+03  # M_3
   11    1.48461933E+03  # A_t
   12    1.00894304E+03  # A_b
   13   -3.52688498E+02  # A_tau
   23   -1.59814363E+02  # mu
   25    3.43725729E+00  # tan(beta)
   26    3.88979668E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.61815371E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.34783666E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.63488804E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.58197305E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.58197305E+03  # (SUSY scale)
  1  1     8.73199271E-06   # Y_u(Q)^DRbar
  2  2     4.43585230E-03   # Y_c(Q)^DRbar
  3  3     1.05489486E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.58197305E+03  # (SUSY scale)
  1  1     6.03122671E-05   # Y_d(Q)^DRbar
  2  2     1.14593307E-03   # Y_s(Q)^DRbar
  3  3     5.98108698E-02   # Y_b(Q)^DRbar
Block Ye Q=  3.58197305E+03  # (SUSY scale)
  1  1     1.05249316E-05   # Y_e(Q)^DRbar
  2  2     2.17622206E-03   # Y_mu(Q)^DRbar
  3  3     3.66004749E-02   # Y_tau(Q)^DRbar
Block Au Q=  3.58197305E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     1.48461933E+03   # A_t(Q)^DRbar
Block Ad Q=  3.58197305E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.00894306E+03   # A_b(Q)^DRbar
Block Ae Q=  3.58197305E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -3.52688495E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.58197305E+03  # soft SUSY breaking masses at Q
   1    1.80466262E+03  # M_1
   2    7.17458294E+02  # M_2
   3    4.70004173E+03  # M_3
  21    1.39143356E+07  # M^2_(H,d)
  22    1.45267167E+06  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.61815371E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.34783666E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.63488804E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.14815881E+02  # h0
        35     3.88976040E+03  # H0
        36     3.88979668E+03  # A0
        37     3.89347718E+03  # H+
   1000001     1.00885608E+04  # ~d_L
   2000001     1.00642724E+04  # ~d_R
   1000002     1.00882703E+04  # ~u_L
   2000002     1.00669818E+04  # ~u_R
   1000003     1.00885614E+04  # ~s_L
   2000003     1.00642724E+04  # ~s_R
   1000004     1.00882709E+04  # ~c_L
   2000004     1.00669828E+04  # ~c_R
   1000005     2.78019961E+03  # ~b_1
   2000005     3.73618429E+03  # ~b_2
   1000006     3.43018380E+03  # ~t_1
   2000006     3.74047914E+03  # ~t_2
   1000011     1.00210053E+04  # ~e_L-
   2000011     1.00084591E+04  # ~e_R-
   1000012     1.00202830E+04  # ~nu_eL
   1000013     1.00210055E+04  # ~mu_L-
   2000013     1.00084595E+04  # ~mu_R-
   1000014     1.00202832E+04  # ~nu_muL
   1000015     1.00085530E+04  # ~tau_1-
   2000015     1.00210535E+04  # ~tau_2-
   1000016     1.00203309E+04  # ~nu_tauL
   1000021     5.12848869E+03  # ~g
   1000022     1.79161433E+02  # ~chi_10
   1000023     1.87725403E+02  # ~chi_20
   1000025     7.89131930E+02  # ~chi_30
   1000035     1.82120897E+03  # ~chi_40
   1000024     1.84667763E+02  # ~chi_1+
   1000037     7.89126614E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.69154748E-01   # alpha
Block Hmix Q=  3.58197305E+03  # Higgs mixing parameters
   1   -1.59814363E+02  # mu
   2    3.43725729E+00  # tan[beta](Q)
   3    2.43101506E+02  # v(Q)
   4    1.51305182E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -8.92437500E-02   # Re[R_st(1,1)]
   1  2     9.96009816E-01   # Re[R_st(1,2)]
   2  1    -9.96009816E-01   # Re[R_st(2,1)]
   2  2    -8.92437500E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -5.13123506E-04   # Re[R_sb(1,1)]
   1  2     9.99999868E-01   # Re[R_sb(1,2)]
   2  1    -9.99999868E-01   # Re[R_sb(2,1)]
   2  2    -5.13123506E-04   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.23151947E-03   # Re[R_sta(1,1)]
   1  2     9.99999242E-01   # Re[R_sta(1,2)]
   2  1    -9.99999242E-01   # Re[R_sta(2,1)]
   2  2    -1.23151947E-03   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -1.28389312E-02   # Re[N(1,1)]
   1  2     6.25155133E-02   # Re[N(1,2)]
   1  3     7.15083435E-01   # Re[N(1,3)]
   1  4     6.96119712E-01   # Re[N(1,4)]
   2  1    -1.94048968E-02   # Re[N(2,1)]
   2  2     7.34778520E-02   # Re[N(2,2)]
   2  3    -6.98993276E-01   # Re[N(2,3)]
   2  4     7.11078656E-01   # Re[N(2,4)]
   3  1    -4.04530045E-03   # Re[N(3,1)]
   3  2    -9.95333904E-01   # Re[N(3,2)]
   3  3    -6.67995230E-03   # Re[N(3,3)]
   3  4     9.61739694E-02   # Re[N(3,4)]
   4  1     9.99721085E-01   # Re[N(4,1)]
   4  2    -1.79846380E-03   # Re[N(4,2)]
   4  3    -4.41123815E-03   # Re[N(4,3)]
   4  4     2.31313453E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1     9.08264922E-03   # Re[U(1,1)]
   1  2    -9.99958752E-01   # Re[U(1,2)]
   2  1     9.99958752E-01   # Re[U(2,1)]
   2  2     9.08264922E-03   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -1.37031270E-01   # Re[V(1,1)]
   1  2     9.90566722E-01   # Re[V(1,2)]
   2  1     9.90566722E-01   # Re[V(2,1)]
   2  2     1.37031270E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     4.70154593E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.76202061E-04    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     4.02483720E-04    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.99404027E-01    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.42465809E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.58431157E-04    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.23229299E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.04244106E-01    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     8.20162243E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.11497900E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     4.70173460E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.86443772E-04    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     4.12259636E-04    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     9.99363967E-01    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
DECAY   1000013     1.42466755E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.61811421E-04    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.23552045E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     3.04242091E-01    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     8.20156814E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.11493850E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     4.75491987E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     3.04963914E-03    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     3.12643520E-03    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     9.88195529E-01    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     5.60302857E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     1.42733262E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.90986748E-03    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.14699369E-03    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     3.03674734E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     8.18632834E-02    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     6.10353408E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.42468001E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     1.48896937E-03    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.17474576E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.01586302E-01    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     8.31203574E-02    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     1.16182341E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.00011391E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.42468948E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.48895951E-03    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.17473136E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.01584305E-01    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     8.31198072E-02    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     1.16247789E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.00007418E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.42735575E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.48618525E-03    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.17067935E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.01022420E-01    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     8.29649898E-02    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.34660572E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.98889669E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.40667605E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.19226350E-02    2     1000035         1   # BR(~d_R -> chi^0_4 d)
     9.88070257E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.70846682E+02   # ~d_L
#    BR                NDA      ID1      ID2
     3.26447809E-04    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     4.60089139E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     7.60135291E-02    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     2.35408926E-03    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.53665509E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.67167510E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.40668129E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.19226209E-02    2     1000035         3   # BR(~s_R -> chi^0_4 s)
     9.88069082E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.70850960E+02   # ~s_L
#    BR                NDA      ID1      ID2
     3.26680678E-04    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     4.60310501E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     7.60129640E-02    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     2.35407178E-03    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.53664495E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.67161869E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     8.97159390E-01   # ~b_1
#    BR                NDA      ID1      ID2
     1.12211751E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.07314175E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     5.63551717E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.16836212E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     1.28550675E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.58689379E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.75963444E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.15511484E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.40436829E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     6.25607987E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.45466236E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     7.65436447E-03    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     4.56726841E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.60277301E-02    2     1000035         2   # BR(~u_R -> chi^0_4 u)
     9.53944844E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.70833851E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.82001185E-04    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.81138440E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     7.62333678E-02    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     2.26083193E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     2.91936641E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.50791424E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.67131870E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.56734791E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.60269380E-02    2     1000035         4   # BR(~c_R -> chi^0_4 c)
     9.53928399E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.70838099E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.85349061E-04    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.84630825E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     7.62328684E-02    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     2.26081834E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     2.91980498E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.50790311E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.67126217E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.53894346E+02   # ~t_1
#    BR                NDA      ID1      ID2
     2.34944499E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.44675056E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     8.11612389E-03    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.57851990E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.70122174E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.63549672E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.29597286E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.09415053E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     3.23279464E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.15580007E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.41117912E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.13836735E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.17004689E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.81155437E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     7.10801610E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     6.06182605E-09   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.78468129E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     2.92758394E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.26156055E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.25924334E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     7.66930879E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     5.23359040E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.47648838E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.55515882E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.50161589E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.38074401E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.34033656E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.41401762E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     4.84486129E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.95003205E-07   # chi^0_2
#    BR                NDA      ID1      ID2
     3.79182215E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     9.07217686E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     6.75311094E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.16323332E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.16156987E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.31386175E-04    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     2.62508769E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     2.61917818E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     1.37307381E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.55557911E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     1.90603259E-03    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     1.90603259E-03    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     8.23949253E-04    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     8.23949253E-04    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     6.35349970E-04    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     6.35349970E-04    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     6.31585440E-04    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     6.31585440E-04    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
     1.14029690E-04    3     1000024        15       -16   # BR(chi^0_2 -> chi^+_1 tau^- nu_bar_tau)
     1.14029690E-04    3    -1000024       -15        16   # BR(chi^0_2 -> chi^-_1 tau^+ nu_tau)
DECAY   1000025     6.37344695E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.12279574E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.12279574E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     1.17350669E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     9.12446708E-02    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     8.30204197E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     2.78442054E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     5.22795836E-04    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     7.84233835E-04    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     2.03196268E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     2.03196268E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     4.79980501E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.19429585E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.19429585E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     8.90540580E-03    2     1000037       -24   # BR(chi^0_4 -> chi^+_2 W^-)
     8.90540580E-03    2    -1000037        24   # BR(chi^0_4 -> chi^-_2 W^+)
     1.54427946E-01    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     7.41782200E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.11642093E-03    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     6.49386694E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.15730806E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     7.54951208E-03    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.96016391E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     5.92477539E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.24652250E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     6.94713738E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     6.94713738E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
     2.39207387E-04    3     1000037         5        -6   # BR(chi^0_4 -> chi^+_2 b t_bar)
     2.39207387E-04    3    -1000037        -5         6   # BR(chi^0_4 -> chi^-_2 b_bar t)
DECAY   1000021     1.88584473E+02   # ~g
#    BR                NDA      ID1      ID2
     1.25165328E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.25165328E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     8.54329916E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     8.54329916E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     2.00678248E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.00678248E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     8.86295343E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     8.86295343E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     2.60984442E-03   # Gamma(h0)
     2.41740877E-03   2        22        22   # BR(h0 -> photon photon)
     7.58751711E-04   2        22        23   # BR(h0 -> photon Z)
     1.00955930E-02   2        23        23   # BR(h0 -> Z Z)
     1.01105350E-01   2       -24        24   # BR(h0 -> W W)
     8.42697636E-02   2        21        21   # BR(h0 -> gluon gluon)
     6.09489933E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.71105039E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.81514963E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.76025145E-07   2        -2         2   # BR(h0 -> Up up)
     3.41555146E-02   2        -4         4   # BR(h0 -> Charm charm)
     7.16339772E-07   2        -1         1   # BR(h0 -> Down down)
     2.59082102E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.88515036E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     5.69006080E+01   # Gamma(HH)
     1.12637641E-07   2        22        22   # BR(HH -> photon photon)
     3.11086699E-09   2        22        23   # BR(HH -> photon Z)
     2.06743316E-05   2        23        23   # BR(HH -> Z Z)
     4.51198998E-06   2       -24        24   # BR(HH -> W W)
     3.76957385E-05   2        21        21   # BR(HH -> gluon gluon)
     1.24298234E-10   2       -11        11   # BR(HH -> Electron electron)
     5.53479093E-06   2       -13        13   # BR(HH -> Muon muon)
     1.59858083E-03   2       -15        15   # BR(HH -> Tau tau)
     1.16114152E-11   2        -2         2   # BR(HH -> Up up)
     2.25263642E-06   2        -4         4   # BR(HH -> Charm charm)
     1.68608861E-01   2        -6         6   # BR(HH -> Top top)
     8.87416614E-09   2        -1         1   # BR(HH -> Down down)
     3.20981034E-06   2        -3         3   # BR(HH -> Strange strange)
     8.36287730E-03   2        -5         5   # BR(HH -> Bottom bottom)
     1.11043042E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.52050678E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     2.52050678E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     4.50953381E-04   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     2.40608911E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     4.93845349E-04   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.92282959E-01   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     3.60535245E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.03250266E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     6.09774520E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.20911897E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     2.10709153E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     2.16964274E-05   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     2.80411459E-08   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     1.29067349E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     5.60783910E+01   # Gamma(A0)
     5.70494894E-08   2        22        22   # BR(A0 -> photon photon)
     7.08866783E-08   2        22        23   # BR(A0 -> photon Z)
     5.09801232E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.20709399E-10   2       -11        11   # BR(A0 -> Electron electron)
     5.37505379E-06   2       -13        13   # BR(A0 -> Muon muon)
     1.55244620E-03   2       -15        15   # BR(A0 -> Tau tau)
     1.07101358E-11   2        -2         2   # BR(A0 -> Up up)
     2.07775056E-06   2        -4         4   # BR(A0 -> Charm charm)
     1.56707446E-01   2        -6         6   # BR(A0 -> Top top)
     8.61784285E-09   2        -1         1   # BR(A0 -> Down down)
     3.11709972E-06   2        -3         3   # BR(A0 -> Strange strange)
     8.12067212E-03   2        -5         5   # BR(A0 -> Bottom bottom)
     1.20571748E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.55112593E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     2.55112593E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.18904964E-03   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     8.24139482E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.05823205E-04   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     6.84504650E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.35852952E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     3.36244520E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.87830631E-01   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     3.51363883E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     5.89487450E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.52376976E-04   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     6.34201450E-06   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     4.29446639E-05   2        23        25   # BR(A0 -> Z h0)
     5.13697235E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     5.87746268E+01   # Gamma(Hp)
     1.44366821E-10   2       -11        12   # BR(Hp -> Electron nu_e)
     6.17213250E-06   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.74583054E-03   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.95793276E-09   2        -1         2   # BR(Hp -> Down up)
     1.51431147E-07   2        -3         2   # BR(Hp -> Strange up)
     9.74079304E-08   2        -5         2   # BR(Hp -> Bottom up)
     1.15350707E-07   2        -1         4   # BR(Hp -> Down charm)
     5.67340521E-06   2        -3         4   # BR(Hp -> Strange charm)
     1.36444104E-05   2        -5         4   # BR(Hp -> Bottom charm)
     1.26081239E-05   2        -1         6   # BR(Hp -> Down top)
     2.74903686E-04   2        -3         6   # BR(Hp -> Strange top)
     2.02375505E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.37639714E-03   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.55328925E-01   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.05466853E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.43323389E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     2.48283982E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.28455790E-07   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     4.59909403E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.65752380E-04   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     4.11064875E-05   2        24        25   # BR(Hp -> W h0)
     1.64336693E-11   2        24        35   # BR(Hp -> W HH)
     1.56470210E-11   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.06151859E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.19085858E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.18147377E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00794331E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    7.66967396E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    8.46400510E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.06151859E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.19085858E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.18147377E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99805132E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.94868270E-04        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99805132E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.94868270E-04        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.28265742E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    7.16940257E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    2.97521172E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.94868270E-04        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99805132E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.24427327E-04   # BR(b -> s gamma)
    2    1.59011232E-06   # BR(b -> s mu+ mu-)
    3    3.52601944E-05   # BR(b -> s nu nu)
    4    2.54301206E-15   # BR(Bd -> e+ e-)
    5    1.08634589E-10   # BR(Bd -> mu+ mu-)
    6    2.27415982E-08   # BR(Bd -> tau+ tau-)
    7    8.57471066E-14   # BR(Bs -> e+ e-)
    8    3.66311366E-09   # BR(Bs -> mu+ mu-)
    9    7.76978370E-07   # BR(Bs -> tau+ tau-)
   10    9.68036287E-05   # BR(B_u -> tau nu)
   11    9.99944424E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42645552E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93891234E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15999304E-03   # epsilon_K
   17    2.28168578E-15   # Delta(M_K)
   18    2.48041259E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29150324E-11   # BR(K^+ -> pi^+ nu nu)
   20   -5.06194471E-17   # Delta(g-2)_electron/2
   21   -2.16413944E-12   # Delta(g-2)_muon/2
   22   -6.12055394E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -7.59642554E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.93181342E-01   # C7
     0305 4322   00   2    -8.84398172E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.00961195E-01   # C8
     0305 6321   00   2    -1.13702325E-04   # C8'
 03051111 4133   00   0     1.62241936E+00   # C9 e+e-
 03051111 4133   00   2     1.62278528E+00   # C9 e+e-
 03051111 4233   00   2     3.17266928E-06   # C9' e+e-
 03051111 4137   00   0    -4.44511146E+00   # C10 e+e-
 03051111 4137   00   2    -4.44418445E+00   # C10 e+e-
 03051111 4237   00   2    -2.30557278E-05   # C10' e+e-
 03051313 4133   00   0     1.62241936E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62278527E+00   # C9 mu+mu-
 03051313 4233   00   2     3.17266926E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.44511146E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44418445E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.30557278E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50515080E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     4.98989844E-06   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50515080E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     4.98989844E-06   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50515080E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     4.98989893E-06   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85822620E-07   # C7
     0305 4422   00   2    -5.89486320E-07   # C7
     0305 4322   00   2    -3.04880168E-08   # C7'
     0305 6421   00   0     3.30482590E-07   # C8
     0305 6421   00   2     5.31983746E-07   # C8
     0305 6321   00   2     4.71975272E-10   # C8'
 03051111 4133   00   2     4.13307564E-07   # C9 e+e-
 03051111 4233   00   2     6.06303753E-08   # C9' e+e-
 03051111 4137   00   2    -3.58946139E-08   # C10 e+e-
 03051111 4237   00   2    -4.41334895E-07   # C10' e+e-
 03051313 4133   00   2     4.13307536E-07   # C9 mu+mu-
 03051313 4233   00   2     6.06303751E-08   # C9' mu+mu-
 03051313 4137   00   2    -3.58945869E-08   # C10 mu+mu-
 03051313 4237   00   2    -4.41334895E-07   # C10' mu+mu-
 03051212 4137   00   2     3.15953655E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     9.55176291E-08   # C11' nu_1 nu_1
 03051414 4137   00   2     3.15953667E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     9.55176291E-08   # C11' nu_2 nu_2
 03051616 4137   00   2     3.15957176E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     9.55176290E-08   # C11' nu_3 nu_3
