# Job options for event generation with direct slepton (selectron/muon/stau) production for long-lived particles in co annihilation model.
#
# To be used for MC16 in r21.6 as well as MC20 and MC21 in r22.6.
#
# Based on previous version for gravity mediated sleptions:
# https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/516xxx/516640/SUSY_SimplifiedModel_SlepSlep_directLLP.py
# 

### includes
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

# keepOutput=True
GMSB = False
coannihilation = True
### helpers
def StringToFloat(s):
    if "p" in s:
        s = s.replace("p", ".") # Replace p by a decimal point.
    if "ns" in s:
        s = s.replace("ns", "") # Remove the lifetime unit ns.
    return float(s)

### parse job options name
# N.B. 60 chars limit on PhysicsShort name...
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
JOName = get_physics_short()
evgenLog.info("Job Option name: " + str(JOName))

# Get the file name and split it on _ to extract relavent information
jobConfigParts = JOName.split("_")

# Extract job settings/masses etc.
process_type = jobConfigParts[2].strip("LLP")
mslep        = StringToFloat(jobConfigParts[3])
m_neutralino = StringToFloat(jobConfigParts[7] )
if m_neutralino == 0:
    m_neutralino = 0.0000001
lifetime     = StringToFloat(jobConfigParts[5])
masses['1000011'] = mslep
masses['2000011'] = mslep
masses['1000013'] = mslep
masses['2000013'] = mslep
masses['1000015'] = mslep
masses['2000015'] = mslep

# slepton pairs + up to 2 extra partons
sel_process = '''
import model MSSM_SLHA2-full
generate p p > el- el+ $ susystrong @1
add process p p > er- er+ $ susystrong @1
add process p p > el- el+ j $ susystrong @2
add process p p > er- er+ j $ susystrong @2
add process p p > el- el+ j j $ susystrong @3
add process p p > er- er+ j j $ susystrong @3
'''

smu_process = '''
import model MSSM_SLHA2-full
generate p p > mul- mul+ $ susystrong @1
add process p p > mur- mur+ $ susystrong @1
add process p p > mul- mul+ j $ susystrong @2
add process p p > mur- mur+ j $ susystrong @2
add process p p > mul- mul+ j j $ susystrong @3
add process p p > mur- mur+ j j $ susystrong @3
'''

stau_process = '''
import model MSSM_SLHA2-full
generate p p > ta1- ta1+ $ susystrong @1
add process p p > ta2- ta2+ $ susystrong @1
add process p p > ta1- ta1+ j $ susystrong @2
add process p p > ta2- ta2+ j $ susystrong @2
add process p p > ta1- ta1+ j j $ susystrong @3
add process p p > ta2- ta2+ j j $ susystrong @3
'''

if process_type == "SelSel":
    slepton_type = "selectron"
    process = sel_process
if process_type == "SmuSmu":
    slepton_type = "smuon"
    process = smu_process
if process_type == "StauStau":
    slepton_type = "stau"
    process = stau_process

if slepton_type == "stau":
    process_keyword = "stau"
else:
    process_keyword = "slepton"

useguess = True

evgenLog.info('Registered generation of %s pair production via direct decays; grid point decoded into m(%s) = %f GeV with lifetime %f ns' % (slepton_type, slepton_type, mslep, lifetime))

evgenConfig.contact = [ "nyoung@uoregon.edu" ]
evgenConfig.keywords += ['SUSY', process_keyword, 'longLived', 'gravitino', 'simplifiedModel']
evgenConfig.description = 'Direct %s pair production in simplified model with non-prompt decays, m_slepLR = %s GeV, lifetime = %s ns' % (slepton_type, mslep, lifetime)



if lifetime != 0:
    # Gravitino mass set to 10**-7 GeV
    if GMSB:
        if slepton_type == "selectron" or slepton_type == "smuon":
            evgenConfig.specialConfig = 'GMSBSlepton=%s*GeV;GMSBGravitino=%s*GeV;GMSBSleptonTime=%s*ns;preInclude=SimulationJobOptions/preInclude.SleptonsLLP.py' % (mslep, m_neutralino, lifetime)
        if slepton_type == "stau":
            evgenConfig.specialConfig = 'GMSBStau=%s*GeV;GMSBGravitino=%s*GeV;GMSBStauTime=%s*ns;preInclude=SimulationJobOptions/preInclude.SleptonsLLP.py' % (mslep, m_neutralino, lifetime)
    if coannihilation:
        if slepton_type == "selectron" or slepton_type == "smuon":
            evgenConfig.specialConfig = 'coannihilationSlepton=%s*GeV;coannihilationSlepton=%s*GeV;coannihilationSleptonTime=%s*ns;preInclude=SimulationJobOptions/preInclude.SleptonsLLP.py' % (
            mslep, m_neutralino, lifetime)
        if slepton_type == "stau":
            evgenConfig.specialConfig = 'coannihilationStau=%s*GeV;coannihilationNeutralino=%s*GeV;coannihilationStauTime=%s*ns;preInclude=SimulationJobOptions/preInclude.SleptonsLLP.py' % (
            mslep, m_neutralino, lifetime)

# Filter and event multiplier
evt_multiplier = 3

# Configure our decays
decays['1000011'] = """DECAY 1000011 0.00000000E+00 # stable selectronL"""
decays['2000011'] = """DECAY 2000011 0.00000000E+00 # stable selectronR"""

decays['1000013'] = """DECAY 1000013 0.00000000E+00 # stable smuonL"""
decays['2000013'] = """DECAY 2000013 0.00000000E+00 # stable smuonR"""

decays['1000015'] = """DECAY 1000015 0.00000000E+00 # stable stau1"""
decays['2000015'] = """DECAY 2000015 0.00000000E+00 # stable stau2"""

# Configure slepton mixing (No mixing)
param_blocks['selmix'] = {}
param_blocks['selmix'][ '3   3' ] = '1.00000000E+00' # RRl3x3
param_blocks['selmix'][ '3   6' ] = '0.00000000E+00' # RRl3x6
param_blocks['selmix'][ '6   3' ] = '0.00000000E+00' # RRl6x3
param_blocks['selmix'][ '6   6' ] = '1.00000000E+00' # RRl6x6

# post include
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

# Bug fix from Zach Marshall (zach.marshall@cern.ch)
# Only N1 is modified by MG, put it back
decays['1000022'] = """DECAY 1000022 0.00000000E+00 # stable N1"""

import os
os.rename(runArgs.inputGeneratorFile+'.events', runArgs.inputGeneratorFile+'_old.events')
modify_param_card(param_card_input=runArgs.inputGeneratorFile+'_old.events',params={'DECAY':decays},output_location=runArgs.inputGeneratorFile+'.events')
os.unlink(runArgs.inputGeneratorFile+'_old.events.old_to_be_deleted')

# AGENE-1511: new CKKW-L "guess" merging features
if useguess:
    evgenLog.info('Using Merging:Process = guess')
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
else:
    evgenLog.info('Using standard merging syntax: ' + str(genSeq.Pythia8.Commands))

bonus_file = open('pdg_extras.dat', 'w')
if process_type == "SelSel":
    bonus_file.write( '1000011 SelectronL %s (MeV/c) fermion Selectron -1\n' % (str(mslep)) )
    bonus_file.write( '2000011 SelectronR %s (MeV/c) fermion Selectron -1\n' % (str(mslep)) )
    bonus_file.write( '-1000011 Anti-selectronL %s (MeV/c) fermion Selectron 1\n' % (str(mslep)) )
    bonus_file.write( '-2000011 Anti-selectronR %s (MeV/c) fermion Selectron 1\n' % (str(mslep)) )
if process_type == "SmuSmu":
    bonus_file.write( '1000013 SmuonL %s (MeV/c) fermion Smuon -1\n' % (str(mslep)) )
    bonus_file.write( '2000013 SmuonR %s (MeV/c) fermion Smuon -1\n' % (str(mslep)) )
    bonus_file.write( '-1000013 Anti-smuonL %s (MeV/c) fermion Smuon 1\n' % (str(mslep)) )
    bonus_file.write( '-2000013 Anti-smuonR %s (MeV/c) fermion Smuon 1\n' % (str(mslep)) )
if process_type == "StauStau":
    bonus_file.write( '1000015 Stau1 %s (MeV/c) fermion Stau -1\n' % (str(mslep)) )
    bonus_file.write( '2000015 Stau2 %s (MeV/c) fermion Stau -1\n' % (str(mslep)) )
    bonus_file.write( '-1000015 Anti-stau1 %s (MeV/c) fermion Stau 1\n' % (str(mslep)) )
    bonus_file.write( '-2000015 Anti-stau2 %s (MeV/c) fermion Stau 1\n' % (str(mslep)) )
bonus_file.close()

testSeq.TestHepMC.G4ExtraWhiteFile = 'pdg_extras.dat'

import os
os.system("get_files %s" % testSeq.TestHepMC.G4ExtraWhiteFile)
