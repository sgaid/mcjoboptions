# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  18:11
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.76851894E+00  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.58281423E+03  # scale for input parameters
    1    3.04578807E+02  # M_1
    2    4.08710722E+02  # M_2
    3    1.53478606E+03  # M_3
   11   -3.93403510E+03  # A_t
   12   -5.07742629E+02  # A_b
   13    1.99204226E+03  # A_tau
   23   -3.32039084E+02  # mu
   25    2.62535382E+00  # tan(beta)
   26    2.10978492E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.71654031E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.49850179E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.88492065E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.58281423E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.58281423E+03  # (SUSY scale)
  1  1     8.97200533E-06   # Y_u(Q)^DRbar
  2  2     4.55777871E-03   # Y_c(Q)^DRbar
  3  3     1.08389031E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.58281423E+03  # (SUSY scale)
  1  1     4.73323004E-05   # Y_d(Q)^DRbar
  2  2     8.99313708E-04   # Y_s(Q)^DRbar
  3  3     4.69388102E-02   # Y_b(Q)^DRbar
Block Ye Q=  2.58281423E+03  # (SUSY scale)
  1  1     8.25983254E-06   # Y_e(Q)^DRbar
  2  2     1.70787140E-03   # Y_mu(Q)^DRbar
  3  3     2.87235874E-02   # Y_tau(Q)^DRbar
Block Au Q=  2.58281423E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -3.93403837E+03   # A_t(Q)^DRbar
Block Ad Q=  2.58281423E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -5.07742385E+02   # A_b(Q)^DRbar
Block Ae Q=  2.58281423E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.99204146E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.58281423E+03  # soft SUSY breaking masses at Q
   1    3.04578807E+02  # M_1
   2    4.08710722E+02  # M_2
   3    1.53478606E+03  # M_3
  21   -7.89516808E+04  # M^2_(H,d)
  22    7.52237694E+04  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.71654031E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.49850179E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.88492065E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.08705323E+02  # h0
        35     2.23186493E+02  # H0
        36     2.10978492E+02  # A0
        37     2.25860744E+02  # H+
   1000001     1.01193066E+04  # ~d_L
   2000001     1.00939887E+04  # ~d_R
   1000002     1.01190236E+04  # ~u_L
   2000002     1.00975564E+04  # ~u_R
   1000003     1.01193078E+04  # ~s_L
   2000003     1.00939888E+04  # ~s_R
   1000004     1.01190248E+04  # ~c_L
   2000004     1.00975586E+04  # ~c_R
   1000005     2.71493659E+03  # ~b_1
   2000005     2.93177017E+03  # ~b_2
   1000006     2.42396931E+03  # ~t_1
   2000006     2.75206841E+03  # ~t_2
   1000011     1.00215051E+04  # ~e_L-
   2000011     1.00085127E+04  # ~e_R-
   1000012     1.00208175E+04  # ~nu_eL
   1000013     1.00215053E+04  # ~mu_L-
   2000013     1.00085131E+04  # ~mu_R-
   1000014     1.00208178E+04  # ~nu_muL
   1000015     1.00086320E+04  # ~tau_1-
   2000015     1.00215738E+04  # ~tau_2-
   1000016     1.00208811E+04  # ~nu_tauL
   1000021     1.87593621E+03  # ~g
   1000022     3.00653401E+02  # ~chi_10
   1000023     3.39734964E+02  # ~chi_20
   1000025     3.49026091E+02  # ~chi_30
   1000035     4.62770006E+02  # ~chi_40
   1000024     3.37291056E+02  # ~chi_1+
   1000037     4.63066145E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -5.20282812E-01   # alpha
Block Hmix Q=  2.58281423E+03  # Higgs mixing parameters
   1   -3.32039084E+02  # mu
   2    2.62535382E+00  # tan[beta](Q)
   3    2.43435183E+02  # v(Q)
   4    4.45119241E+04  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     3.37774364E-01   # Re[R_st(1,1)]
   1  2     9.41227114E-01   # Re[R_st(1,2)]
   2  1    -9.41227114E-01   # Re[R_st(2,1)]
   2  2     3.37774364E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99999911E-01   # Re[R_sb(1,1)]
   1  2     4.21720317E-04   # Re[R_sb(1,2)]
   2  1    -4.21720317E-04   # Re[R_sb(2,1)]
   2  2    -9.99999911E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.97228877E-02   # Re[R_sta(1,1)]
   1  2     9.99805485E-01   # Re[R_sta(1,2)]
   2  1    -9.99805485E-01   # Re[R_sta(2,1)]
   2  2    -1.97228877E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -8.68781969E-01   # Re[N(1,1)]
   1  2     7.79972094E-02   # Re[N(1,2)]
   1  3     3.88019184E-01   # Re[N(1,3)]
   1  4     2.97616260E-01   # Re[N(1,4)]
   2  1    -4.88091226E-01   # Re[N(2,1)]
   2  2    -2.61810756E-01   # Re[N(2,2)]
   2  3    -5.88356204E-01   # Re[N(2,3)]
   2  4    -5.89117188E-01   # Re[N(2,4)]
   3  1     6.13531233E-02   # Re[N(3,1)]
   3  2    -9.26925626E-02   # Re[N(3,2)]
   3  3     6.98343032E-01   # Re[N(3,3)]
   3  4    -7.07079127E-01   # Re[N(3,4)]
   4  1    -5.67506775E-02   # Re[N(4,1)]
   4  2     9.57486111E-01   # Re[N(4,2)]
   4  3    -1.24880340E-01   # Re[N(4,3)]
   4  4    -2.53780632E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.85045511E-01   # Re[U(1,1)]
   1  2    -9.82729952E-01   # Re[U(1,2)]
   2  1     9.82729952E-01   # Re[U(2,1)]
   2  2    -1.85045511E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -3.63608316E-01   # Re[V(1,1)]
   1  2     9.31551927E-01   # Re[V(1,2)]
   2  1     9.31551927E-01   # Re[V(2,1)]
   2  2     3.63608316E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.01919021E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     7.54879837E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     2.38144739E-01    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     3.76232927E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     3.21309470E-03    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44346614E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     4.54779742E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     8.32533377E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.09437854E-03    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.61573004E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     2.08800791E-02    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     5.87721226E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.01930629E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     7.54864150E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     2.38143235E-01    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     3.76787127E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     3.21320015E-03    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
DECAY   1000013     1.44347197E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     4.54783967E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     8.32543949E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.09633399E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.61572015E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     2.08799952E-02    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     5.87718864E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.05568602E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     7.50127627E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.37551851E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     5.13231175E-03    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     3.27154351E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.69140929E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     2.25257431E-04    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.44475099E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     4.55390147E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     8.35726704E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.71165037E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.61348864E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.06652934E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.87162507E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44346988E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.96406041E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     4.79860283E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     2.96882986E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     8.06142074E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.28063256E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44347573E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.96402438E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     4.79858354E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.96881793E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     8.06177657E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.28061270E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.44512013E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.95388096E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     4.79315364E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.96545860E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     8.16195966E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.27502237E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.48289207E+02   # ~d_R
#    BR                NDA      ID1      ID2
     5.67421772E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.79008178E-03    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.92483266E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.79277652E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.73822815E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.54597066E-03    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     5.42580263E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     4.72273849E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     3.46138143E-03    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     9.74328700E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.47051585E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.48289538E+02   # ~s_R
#    BR                NDA      ID1      ID2
     5.67424782E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.79015572E-03    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.92482837E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.79282102E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.73824240E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.54602682E-03    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     5.42667400E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     4.72271543E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     3.46548016E-03    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     9.74330173E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.47047412E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.68105670E+02   # ~b_1
#    BR                NDA      ID1      ID2
     3.86117102E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.34480373E-03    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.07118970E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     6.27499459E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.18301144E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.78434545E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     3.48100181E-01    2     1000021         5   # BR(~b_1 -> ~g b)
     6.84483426E-02    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
     1.66886766E-02    2     1000006       -37   # BR(~b_1 -> ~t_1 H^-)
DECAY   2000005     8.25605722E+01   # ~b_2
#    BR                NDA      ID1      ID2
     1.48784676E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     5.12037047E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     8.11196588E-04    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.45041608E-03    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     9.77599277E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     7.65471308E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.21952766E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     7.00208771E-03    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.10622727E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.70597536E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.79269489E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.97546784E-04    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     6.14626126E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     3.37865610E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     4.52741122E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.33644722E-02    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     8.75473553E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.47032387E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.65479792E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.21955149E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     7.00390124E-03    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.13340223E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.70586980E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.79273908E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.97966026E-04    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     6.14787590E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     3.40235595E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     4.52741964E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.33645847E-02    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     8.75469319E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.47028209E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.43450472E+02   # ~t_1
#    BR                NDA      ID1      ID2
     7.33718014E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.38421912E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.93925923E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     5.22024977E-03    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     3.44414256E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     8.15673167E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     2.36453483E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.72070318E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.95340460E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.33340664E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.72661583E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.00949708E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     4.55007179E-03    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.54191004E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.15655464E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     4.16036659E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.23901360E-02    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     3.29073719E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     1.22162856E-02    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     2.42477267E-05   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.34324775E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.32481836E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.11441426E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.11441289E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.10310675E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     6.01863293E-01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.57461643E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.40694353E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     4.99028355E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     1.80503529E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     6.40205717E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     9.14632350E-07   # chi^0_2
#    BR                NDA      ID1      ID2
     3.66984982E-02    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.16661136E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.15138872E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.49548502E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.49538856E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.32175006E-01    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.36980011E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.36944718E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     3.27201972E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.99973557E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     2.36453758E-04   # chi^0_3
#    BR                NDA      ID1      ID2
     1.61423822E-02    2     1000022        22   # BR(chi^0_3 -> chi^0_1 photon)
     5.03186061E-04    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     1.17208885E-01    3     1000022         2        -2   # BR(chi^0_3 -> chi^0_1 u u_bar)
     1.16265054E-01    3     1000022         4        -4   # BR(chi^0_3 -> chi^0_1 c c_bar)
     1.50284105E-01    3     1000022         1        -1   # BR(chi^0_3 -> chi^0_1 d d_bar)
     1.50278355E-01    3     1000022         3        -3   # BR(chi^0_3 -> chi^0_1 s s_bar)
     1.39587118E-01    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
     3.39148550E-02    3     1000022        11       -11   # BR(chi^0_3 -> chi^0_1 e^- e^+)
     3.39128004E-02    3     1000022        13       -13   # BR(chi^0_3 -> chi^0_1 mu^- mu^+)
     3.33404089E-02    3     1000022        15       -15   # BR(chi^0_3 -> chi^0_1 tau^- tau^+)
     2.00977406E-01    3     1000022        12       -12   # BR(chi^0_3 -> chi^0_1 nu_e nu_bar_e)
     1.02861912E-04    3     1000023         1        -1   # BR(chi^0_3 -> chi^0_2 d d_bar)
     1.02737025E-04    3     1000023         3        -3   # BR(chi^0_3 -> chi^0_2 s s_bar)
     1.37556319E-04    3     1000023        12       -12   # BR(chi^0_3 -> chi^0_2 nu_e nu_bar_e)
     1.20940355E-03    3     1000024         1        -2   # BR(chi^0_3 -> chi^+_1 d u_bar)
     1.20940355E-03    3    -1000024        -1         2   # BR(chi^0_3 -> chi^-_1 d_bar u)
     1.14224763E-03    3     1000024         3        -4   # BR(chi^0_3 -> chi^+_1 s c_bar)
     1.14224763E-03    3    -1000024        -3         4   # BR(chi^0_3 -> chi^-_1 s_bar c)
     4.03135163E-04    3     1000024        11       -12   # BR(chi^0_3 -> chi^+_1 e^- nu_bar_e)
     4.03135163E-04    3    -1000024       -11        12   # BR(chi^0_3 -> chi^-_1 e^+ nu_e)
     4.02974251E-04    3     1000024        13       -14   # BR(chi^0_3 -> chi^+_1 mu^- nu_bar_mu)
     4.02974251E-04    3    -1000024       -13        14   # BR(chi^0_3 -> chi^-_1 mu^+ nu_mu)
     3.61066395E-04    3     1000024        15       -16   # BR(chi^0_3 -> chi^+_1 tau^- nu_bar_tau)
     3.61066395E-04    3    -1000024       -15        16   # BR(chi^0_3 -> chi^-_1 tau^+ nu_tau)
DECAY   1000035     9.68245571E-01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.61791931E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.61791931E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     6.50857843E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     2.72577717E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     2.65996843E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     5.33644132E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     3.09502616E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     3.45115784E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000021     8.12719140E-02   # ~g
#    BR                NDA      ID1      ID2
     9.71487967E-04    2     1000022        21   # BR(~g -> chi^0_1 g)
     4.29023363E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     5.82435450E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
     5.89832372E-04    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
     4.30392188E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     3.67815471E-03    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.47970473E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.69123822E-03    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.92038063E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     5.93564407E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     4.95082728E-02    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     2.39601151E-02    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     1.85861082E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.85861082E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.51487088E-04    3     1000037         1        -2   # BR(~g -> chi^+_2 d u_bar)
     1.51487088E-04    3    -1000037        -1         2   # BR(~g -> chi^-_2 d_bar u)
     1.51488189E-04    3     1000037         3        -4   # BR(~g -> chi^+_2 s c_bar)
     1.51488189E-04    3    -1000037        -3         4   # BR(~g -> chi^-_2 s_bar c)
     7.64901474E-02    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     7.64901474E-02    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     4.43531052E-03   # Gamma(h0)
     1.15509712E-03   2        22        22   # BR(h0 -> photon photon)
     1.69468981E-04   2        22        23   # BR(h0 -> photon Z)
     2.14831745E-03   2        23        23   # BR(h0 -> Z Z)
     2.28611728E-02   2       -24        24   # BR(h0 -> W W)
     3.37984027E-02   2        21        21   # BR(h0 -> gluon gluon)
     7.20282074E-09   2       -11        11   # BR(h0 -> Electron electron)
     3.20380762E-04   2       -13        13   # BR(h0 -> Muon muon)
     9.23406448E-02   2       -15        15   # BR(h0 -> Tau tau)
     8.07367828E-08   2        -2         2   # BR(h0 -> Up up)
     1.56662116E-02   2        -4         4   # BR(h0 -> Charm charm)
     8.55628989E-07   2        -1         1   # BR(h0 -> Down down)
     3.09463419E-04   2        -3         3   # BR(h0 -> Strange strange)
     8.31229897E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.96450059E-01   # Gamma(HH)
     8.11753043E-06   2        22        22   # BR(HH -> photon photon)
     4.58230709E-05   2        22        23   # BR(HH -> photon Z)
     1.05613119E-01   2        23        23   # BR(HH -> Z Z)
     2.66666733E-01   2       -24        24   # BR(HH -> W W)
     3.10886419E-03   2        21        21   # BR(HH -> gluon gluon)
     1.07404494E-09   2       -11        11   # BR(HH -> Electron electron)
     4.77837768E-05   2       -13        13   # BR(HH -> Muon muon)
     1.37901590E-02   2       -15        15   # BR(HH -> Tau tau)
     9.89213823E-10   2        -2         2   # BR(HH -> Up up)
     1.91783356E-04   2        -4         4   # BR(HH -> Charm charm)
     1.12979519E-07   2        -1         1   # BR(HH -> Down down)
     4.08632945E-05   2        -3         3   # BR(HH -> Strange strange)
     1.09478760E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.36792358E-06   2        23        36   # BR(HH -> Z A0)
     5.01006510E-01   2        25        25   # BR(HH -> h0 h0)
DECAY        36     3.04651560E-02   # Gamma(A0)
     2.37887869E-05   2        22        22   # BR(A0 -> photon photon)
     1.10261619E-06   2        22        23   # BR(A0 -> photon Z)
     1.22822855E-02   2        21        21   # BR(A0 -> gluon gluon)
     7.66792420E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.41137095E-04   2       -13        13   # BR(A0 -> Muon muon)
     9.84732431E-02   2       -15        15   # BR(A0 -> Tau tau)
     2.60385240E-09   2        -2         2   # BR(A0 -> Up up)
     5.04815840E-04   2        -4         4   # BR(A0 -> Charm charm)
     8.13729966E-07   2        -1         1   # BR(A0 -> Down down)
     2.94316342E-04   2        -3         3   # BR(A0 -> Strange strange)
     7.90240180E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     9.78383066E-02   2        23        25   # BR(A0 -> Z h0)
DECAY        37     3.06244481E-01   # Gamma(Hp)
     9.34820329E-10   2       -11        12   # BR(Hp -> Electron nu_e)
     3.99664713E-05   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.13034041E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.61499617E-08   2        -1         2   # BR(Hp -> Down up)
     1.39411755E-06   2        -3         2   # BR(Hp -> Strange up)
     8.90747951E-07   2        -5         2   # BR(Hp -> Bottom up)
     3.13215688E-06   2        -1         4   # BR(Hp -> Down charm)
     1.00371735E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.24830962E-04   2        -5         4   # BR(Hp -> Bottom charm)
     5.84730492E-05   2        -1         6   # BR(Hp -> Down top)
     1.27490147E-03   2        -3         6   # BR(Hp -> Strange top)
     9.28208635E-01   2        -5         6   # BR(Hp -> Bottom top)
     5.88809190E-02   2        24        25   # BR(Hp -> W h0)
     5.80089253E-10   2        24        35   # BR(Hp -> W HH)
     2.99368751E-06   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.95049932E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    5.94198336E+00    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    6.89248268E+00        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    8.62096234E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    2.82989368E-01    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.45085602E-01        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.95049932E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    5.94198336E+00    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    6.89248268E+00        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.75753623E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.42463766E-02        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.75753623E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.42463766E-02        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.02800443E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    3.66574738E-01        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    3.43624659E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.42463766E-02        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.75753623E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    5.39295370E-04   # BR(b -> s gamma)
    2    1.67526284E-06   # BR(b -> s mu+ mu-)
    3    3.64902524E-05   # BR(b -> s nu nu)
    4    2.63458887E-15   # BR(Bd -> e+ e-)
    5    1.12546646E-10   # BR(Bd -> mu+ mu-)
    6    2.35605573E-08   # BR(Bd -> tau+ tau-)
    7    8.88167338E-14   # BR(Bs -> e+ e-)
    8    3.79424803E-09   # BR(Bs -> mu+ mu-)
    9    8.04792943E-07   # BR(Bs -> tau+ tau-)
   10    9.58957014E-05   # BR(B_u -> tau nu)
   11    9.90565882E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.86901434E-01   # |Delta(M_Bd)| [ps^-1] 
   13    2.09684066E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.34557954E-03   # epsilon_K
   17    2.28364129E-15   # Delta(M_K)
   18    2.56617181E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.49524556E-11   # BR(K^+ -> pi^+ nu nu)
   20   -4.37208615E-17   # Delta(g-2)_electron/2
   21   -1.86920352E-12   # Delta(g-2)_muon/2
   22   -5.28850943E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -8.87781783E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -3.92771386E-01   # C7
     0305 4322   00   2    -3.87808161E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -2.79938972E-01   # C8
     0305 6321   00   2    -3.54375166E-03   # C8'
 03051111 4133   00   0     1.61103313E+00   # C9 e+e-
 03051111 4133   00   2     1.62229782E+00   # C9 e+e-
 03051111 4233   00   2    -2.41534930E-06   # C9' e+e-
 03051111 4137   00   0    -4.43372523E+00   # C10 e+e-
 03051111 4137   00   2    -4.55274168E+00   # C10 e+e-
 03051111 4237   00   2     1.98757137E-05   # C10' e+e-
 03051313 4133   00   0     1.61103313E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62229782E+00   # C9 mu+mu-
 03051313 4233   00   2    -2.41538251E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.43372523E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.55274168E+00   # C10 mu+mu-
 03051313 4237   00   2     1.98757470E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.53117909E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -4.31415303E-06   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.53117909E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -4.31414580E-06   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.53117909E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -4.31210866E-06   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85820569E-07   # C7
     0305 4422   00   2    -1.01796614E-06   # C7
     0305 4322   00   2    -2.15634981E-07   # C7'
     0305 6421   00   0     3.30480833E-07   # C8
     0305 6421   00   2     4.43629557E-06   # C8
     0305 6321   00   2    -1.24977107E-08   # C8'
 03051111 4133   00   2     1.56999705E-06   # C9 e+e-
 03051111 4233   00   2     5.08446631E-08   # C9' e+e-
 03051111 4137   00   2     1.45024092E-07   # C10 e+e-
 03051111 4237   00   2    -3.57058146E-07   # C10' e+e-
 03051313 4133   00   2     1.56999703E-06   # C9 mu+mu-
 03051313 4233   00   2     5.08446630E-08   # C9' mu+mu-
 03051313 4137   00   2     1.45024114E-07   # C10 mu+mu-
 03051313 4237   00   2    -3.57058146E-07   # C10' mu+mu-
 03051212 4137   00   2     9.15559329E-09   # C11 nu_1 nu_1
 03051212 4237   00   2     7.74624287E-08   # C11' nu_1 nu_1
 03051414 4137   00   2     9.15559457E-09   # C11 nu_2 nu_2
 03051414 4237   00   2     7.74624287E-08   # C11' nu_2 nu_2
 03051616 4137   00   2     9.15596253E-09   # C11 nu_3 nu_3
 03051616 4237   00   2     7.74624285E-08   # C11' nu_3 nu_3
