# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  20:57
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.11832765E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.71844044E+03  # scale for input parameters
    1   -6.21425108E+01  # M_1
    2    6.56817655E+02  # M_2
    3    1.97485612E+03  # M_3
   11    1.68192400E+03  # A_t
   12   -1.96452115E+03  # A_b
   13    1.34529634E+03  # A_tau
   23    1.67415360E+02  # mu
   25    5.05300481E+01  # tan(beta)
   26    1.92437828E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.77543140E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.89625419E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.10751613E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.71844044E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.71844044E+03  # (SUSY scale)
  1  1     8.38601385E-06   # Y_u(Q)^DRbar
  2  2     4.26009504E-03   # Y_c(Q)^DRbar
  3  3     1.01309783E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.71844044E+03  # (SUSY scale)
  1  1     8.51501730E-04   # Y_d(Q)^DRbar
  2  2     1.61785329E-02   # Y_s(Q)^DRbar
  3  3     8.44422894E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.71844044E+03  # (SUSY scale)
  1  1     1.48593279E-04   # Y_e(Q)^DRbar
  2  2     3.07243772E-02   # Y_mu(Q)^DRbar
  3  3     5.16733481E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.71844044E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     1.68192403E+03   # A_t(Q)^DRbar
Block Ad Q=  3.71844044E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.96452140E+03   # A_b(Q)^DRbar
Block Ae Q=  3.71844044E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.34529633E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.71844044E+03  # soft SUSY breaking masses at Q
   1   -6.21425108E+01  # M_1
   2    6.56817655E+02  # M_2
   3    1.97485612E+03  # M_3
  21    3.64672522E+06  # M^2_(H,d)
  22    2.26929168E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.77543140E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.89625419E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.10751613E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23134982E+02  # h0
        35     1.92336754E+03  # H0
        36     1.92437828E+03  # A0
        37     1.92724677E+03  # H+
   1000001     1.01173895E+04  # ~d_L
   2000001     1.00922648E+04  # ~d_R
   1000002     1.01170212E+04  # ~u_L
   2000002     1.00956365E+04  # ~u_R
   1000003     1.01173952E+04  # ~s_L
   2000003     1.00922752E+04  # ~s_R
   1000004     1.01170269E+04  # ~c_L
   2000004     1.00956373E+04  # ~c_R
   1000005     2.81520175E+03  # ~b_1
   2000005     4.12780068E+03  # ~b_2
   1000006     2.81756874E+03  # ~t_1
   2000006     4.90735120E+03  # ~t_2
   1000011     1.00214040E+04  # ~e_L-
   2000011     1.00087872E+04  # ~e_R-
   1000012     1.00206299E+04  # ~nu_eL
   1000013     1.00214319E+04  # ~mu_L-
   2000013     1.00088411E+04  # ~mu_R-
   1000014     1.00206576E+04  # ~nu_muL
   1000015     1.00242623E+04  # ~tau_1-
   2000015     1.00294564E+04  # ~tau_2-
   1000016     1.00286125E+04  # ~nu_tauL
   1000021     2.38643782E+03  # ~g
   1000022     5.93842653E+01  # ~chi_10
   1000023     1.82048206E+02  # ~chi_20
   1000025     1.94972572E+02  # ~chi_30
   1000035     7.19516360E+02  # ~chi_40
   1000024     1.81607719E+02  # ~chi_1+
   1000037     7.19670293E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.96713591E-02   # alpha
Block Hmix Q=  3.71844044E+03  # Higgs mixing parameters
   1    1.67415360E+02  # mu
   2    5.05300481E+01  # tan[beta](Q)
   3    2.43016067E+02  # v(Q)
   4    3.70323176E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.99925500E-01   # Re[R_st(1,1)]
   1  2     1.22063067E-02   # Re[R_st(1,2)]
   2  1    -1.22063067E-02   # Re[R_st(2,1)]
   2  2    -9.99925500E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99997112E-01   # Re[R_sb(1,1)]
   1  2     2.40334609E-03   # Re[R_sb(1,2)]
   2  1    -2.40334609E-03   # Re[R_sb(2,1)]
   2  2     9.99997112E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     1.19950501E-01   # Re[R_sta(1,1)]
   1  2     9.92779874E-01   # Re[R_sta(1,2)]
   2  1    -9.92779874E-01   # Re[R_sta(2,1)]
   2  2     1.19950501E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.64822673E-01   # Re[N(1,1)]
   1  2    -7.75557039E-03   # Re[N(1,2)]
   1  3    -2.51393907E-01   # Re[N(1,3)]
   1  4    -7.65386497E-02   # Re[N(1,4)]
   2  1    -1.26886058E-01   # Re[N(2,1)]
   2  2    -1.07922568E-01   # Re[N(2,2)]
   2  3     7.01325335E-01   # Re[N(2,3)]
   2  4    -6.93105635E-01   # Re[N(2,4)]
   3  1     2.30151711E-01   # Re[N(3,1)]
   3  2    -6.23540648E-02   # Re[N(3,2)]
   3  3    -6.66251376E-01   # Re[N(3,3)]
   3  4    -7.06577147E-01   # Re[N(3,4)]
   4  1     6.87958056E-03   # Re[N(4,1)]
   4  2    -9.92171629E-01   # Re[N(4,2)]
   4  3    -3.24496744E-02   # Re[N(4,3)]
   4  4     1.20395801E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -4.59662994E-02   # Re[U(1,1)]
   1  2     9.98942991E-01   # Re[U(1,2)]
   2  1     9.98942991E-01   # Re[U(2,1)]
   2  2     4.59662994E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -1.70664843E-01   # Re[V(1,1)]
   1  2     9.85329139E-01   # Re[V(1,2)]
   2  1     9.85329139E-01   # Re[V(2,1)]
   2  2     1.70664843E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02846310E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.30925893E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.60913413E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     5.29358324E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.43565994E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.40926767E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     9.47929074E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.12737730E-03    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.97126395E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.29748973E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.06876771E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.06605712E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.24257148E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.77957137E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     5.41889830E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     3.70003156E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.43754473E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.40653882E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.01103844E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.70658848E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.96739038E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.29579219E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.06082809E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.57377281E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     3.25467106E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.73576428E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.57741472E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.92234957E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.37341921E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     3.95072358E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.96336645E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.37327217E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.38622002E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.28627701E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.16480338E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.01163018E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.41525608E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.43570911E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     7.91714708E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     4.94588266E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     1.05409839E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.01527566E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     1.78840244E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.90381367E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.43759372E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     7.90679000E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     4.93941255E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     1.05271943E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.01133129E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     1.91660212E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.89611814E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.96924017E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     5.77673387E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     3.60875886E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     7.69122063E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.20012717E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.82823467E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.31344381E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.15656252E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.32844516E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.26775101E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     4.16815977E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.92127391E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.45874882E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.41681014E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     3.83140959E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     5.61852235E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.14288493E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     2.22328277E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.04008994E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.41978026E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.15761907E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.33199788E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.62746525E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     4.49232106E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.91982205E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.45931607E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.42064068E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     4.13643069E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     5.89363137E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.14254953E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     2.26502883E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.04002204E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.41922151E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.46618848E+02   # ~b_1
#    BR                NDA      ID1      ID2
     1.95632416E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.33478502E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.20556838E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     7.30967953E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.74800067E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.58484857E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     1.20019700E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     2.63256565E+02   # ~b_2
#    BR                NDA      ID1      ID2
     2.21201711E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.09101915E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     9.88297290E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.39055868E-04    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.20284331E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.77338234E-04    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     5.48561093E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.58994987E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.47644666E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     7.32858260E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.86352278E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     4.94972455E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.62832125E-03    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.69240037E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.45862844E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.69734129E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     8.96216367E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.11742982E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     3.06491508E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.01191070E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.41952135E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.32865583E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.86349721E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     4.97355648E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.63078562E-03    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.69230405E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.45919536E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.69725370E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     8.98229386E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.11709602E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     3.12664469E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.01184477E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.41896258E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.53028117E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.02339117E-03    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     4.93137287E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     5.36647323E-04    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     1.78765262E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.83967320E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     7.53131077E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.62153128E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.38860172E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     4.55024803E-02    2     1000021         4   # BR(~t_1 -> ~g c)
     1.03847243E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
     5.09987550E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     4.44841966E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.40922561E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.08136580E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.13377861E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.83410121E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.18691398E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     5.71296149E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.06000537E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     8.11358142E-03    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     4.04339942E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     8.80643183E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.02576795E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99999732E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     6.25010131E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     1.01822515E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.54738245E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.48656590E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.49282995E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.31789309E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.00181406E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.21815566E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     2.01803936E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     8.61297385E-02   # chi^0_2
#    BR                NDA      ID1      ID2
     9.99920281E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     4.90955837E-02   # chi^0_3
#    BR                NDA      ID1      ID2
     5.65640129E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     4.34241538E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     6.96506597E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.30826046E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.30826046E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     5.41812338E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     5.63936224E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.69480515E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     4.40827384E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.54460453E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.43618181E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.99468091E-04    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     2.47238663E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     1.98672410E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.98672410E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.34149994E-01   # ~g
#    BR                NDA      ID1      ID2
     1.53096607E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     2.17464960E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     3.88725904E-03    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     1.41915451E-02    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.01625195E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     9.39896480E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.04663815E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     8.46833653E-02    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     3.31445017E-02    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     3.20359667E-02    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     1.97935092E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.97935092E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.59913284E-04    3     1000037         1        -2   # BR(~g -> chi^+_2 d u_bar)
     1.59913284E-04    3    -1000037        -1         2   # BR(~g -> chi^-_2 d_bar u)
     1.59913352E-04    3     1000037         3        -4   # BR(~g -> chi^+_2 s c_bar)
     1.59913352E-04    3    -1000037        -3         4   # BR(~g -> chi^-_2 s_bar c)
     6.55099322E-02    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     6.55099322E-02    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.30227944E-03   # Gamma(h0)
     2.59201893E-03   2        22        22   # BR(h0 -> photon photon)
     1.48279136E-03   2        22        23   # BR(h0 -> photon Z)
     2.56293239E-02   2        23        23   # BR(h0 -> Z Z)
     2.19798948E-01   2       -24        24   # BR(h0 -> W W)
     8.16502892E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.00635903E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.22690791E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.42071836E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.41241482E-07   2        -2         2   # BR(h0 -> Up up)
     2.74118747E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.80997112E-07   2        -1         1   # BR(h0 -> Down down)
     2.10132666E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.58377147E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     1.84168731E-02   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     7.61040550E+01   # Gamma(HH)
     1.07973363E-08   2        22        22   # BR(HH -> photon photon)
     5.18647090E-08   2        22        23   # BR(HH -> photon Z)
     4.16925029E-07   2        23        23   # BR(HH -> Z Z)
     2.33784548E-07   2       -24        24   # BR(HH -> W W)
     1.79373479E-05   2        21        21   # BR(HH -> gluon gluon)
     8.67875102E-09   2       -11        11   # BR(HH -> Electron electron)
     3.86364961E-04   2       -13        13   # BR(HH -> Muon muon)
     1.11578929E-01   2       -15        15   # BR(HH -> Tau tau)
     2.34212721E-14   2        -2         2   # BR(HH -> Up up)
     4.54305751E-09   2        -4         4   # BR(HH -> Charm charm)
     3.26207992E-04   2        -6         6   # BR(HH -> Top top)
     6.69953248E-07   2        -1         1   # BR(HH -> Down down)
     2.42324669E-04   2        -3         3   # BR(HH -> Strange strange)
     6.20647399E-01   2        -5         5   # BR(HH -> Bottom bottom)
     6.49700250E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     7.60785015E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     7.60785015E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.53739061E-04   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     3.93707888E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.31040922E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     8.02930825E-03   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     5.41565589E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.77263913E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.23990738E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     3.40994715E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     3.64881255E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     3.82594303E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     7.85908095E-05   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     2.08452309E-06   2        25        25   # BR(HH -> h0 h0)
DECAY        36     7.25901963E+01   # Gamma(A0)
     1.11169871E-07   2        22        22   # BR(A0 -> photon photon)
     1.56494665E-07   2        22        23   # BR(A0 -> photon Z)
     2.71600436E-05   2        21        21   # BR(A0 -> gluon gluon)
     8.52363317E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.79459801E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.09585192E-01   2       -15        15   # BR(A0 -> Tau tau)
     2.23121172E-14   2        -2         2   # BR(A0 -> Up up)
     4.32744262E-09   2        -4         4   # BR(A0 -> Charm charm)
     3.21939982E-04   2        -6         6   # BR(A0 -> Top top)
     6.57935054E-07   2        -1         1   # BR(A0 -> Down down)
     2.37977691E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.09531464E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     7.16759961E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     7.95162159E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     7.95162159E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     4.18803118E-04   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     4.03887038E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.46462398E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     7.74036007E-03   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     5.16464788E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     2.06968368E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.41677245E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     4.42879420E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     3.64511891E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     3.19365208E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     2.13006323E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     5.86413807E-07   2        23        25   # BR(A0 -> Z h0)
     1.59611307E-14   2        23        35   # BR(A0 -> Z HH)
     6.20639915E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     8.61072771E+01   # Gamma(Hp)
     9.69648440E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     4.14554991E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.17259608E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     6.52203451E-07   2        -1         2   # BR(Hp -> Down up)
     1.09515152E-05   2        -3         2   # BR(Hp -> Strange up)
     6.86225939E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.05103233E-08   2        -1         4   # BR(Hp -> Down charm)
     2.35051389E-04   2        -3         4   # BR(Hp -> Strange charm)
     9.60961350E-04   2        -5         4   # BR(Hp -> Bottom charm)
     2.29108919E-08   2        -1         6   # BR(Hp -> Down top)
     8.42440496E-07   2        -3         6   # BR(Hp -> Strange top)
     6.45441650E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.63410510E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     9.18522925E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.68806994E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     6.97340403E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     5.36998243E-05   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     6.04952881E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     6.81709169E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.03368176E-08   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     4.97355950E-07   2        24        25   # BR(Hp -> W h0)
     1.38719066E-11   2        24        35   # BR(Hp -> W HH)
     3.06729144E-12   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.88284983E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.55329748E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.55328576E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00000459E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.87063993E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.91652206E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.88284983E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.55329748E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.55328576E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999986E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.35170089E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999986E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.35170089E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26609651E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.90152808E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.59268941E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.35170089E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999986E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.35048740E-04   # BR(b -> s gamma)
    2    1.58832930E-06   # BR(b -> s mu+ mu-)
    3    3.52437877E-05   # BR(b -> s nu nu)
    4    2.17040202E-15   # BR(Bd -> e+ e-)
    5    9.27170984E-11   # BR(Bd -> mu+ mu-)
    6    1.94090540E-08   # BR(Bd -> tau+ tau-)
    7    7.07548166E-14   # BR(Bs -> e+ e-)
    8    3.02263679E-09   # BR(Bs -> mu+ mu-)
    9    6.40751801E-07   # BR(Bs -> tau+ tau-)
   10    9.30207326E-05   # BR(B_u -> tau nu)
   11    9.60868555E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41880077E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93441548E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15711649E-03   # epsilon_K
   17    2.28166083E-15   # Delta(M_K)
   18    2.47942753E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28914084E-11   # BR(K^+ -> pi^+ nu nu)
   20    4.63891178E-16   # Delta(g-2)_electron/2
   21    1.98330630E-11   # Delta(g-2)_muon/2
   22    5.63047524E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    8.89204992E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.03560858E-01   # C7
     0305 4322   00   2    -2.46374010E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.14374885E-01   # C8
     0305 6321   00   2    -3.60837996E-04   # C8'
 03051111 4133   00   0     1.62499957E+00   # C9 e+e-
 03051111 4133   00   2     1.62531022E+00   # C9 e+e-
 03051111 4233   00   2     6.97411292E-04   # C9' e+e-
 03051111 4137   00   0    -4.44769167E+00   # C10 e+e-
 03051111 4137   00   2    -4.44534534E+00   # C10 e+e-
 03051111 4237   00   2    -5.17189949E-03   # C10' e+e-
 03051313 4133   00   0     1.62499957E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62531007E+00   # C9 mu+mu-
 03051313 4233   00   2     6.97407192E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44769167E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44534549E+00   # C10 mu+mu-
 03051313 4237   00   2    -5.17189688E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50484472E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.11878342E-03   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50484472E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.11878422E-03   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50484473E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.11901123E-03   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85814818E-07   # C7
     0305 4422   00   2     3.20723574E-05   # C7
     0305 4322   00   2     3.79299302E-07   # C7'
     0305 6421   00   0     3.30475907E-07   # C8
     0305 6421   00   2    -2.20725176E-05   # C8
     0305 6321   00   2    -4.77555126E-07   # C8'
 03051111 4133   00   2     1.19200860E-06   # C9 e+e-
 03051111 4233   00   2     1.47954543E-05   # C9' e+e-
 03051111 4137   00   2    -1.14470862E-07   # C10 e+e-
 03051111 4237   00   2    -1.09736687E-04   # C10' e+e-
 03051313 4133   00   2     1.19200046E-06   # C9 mu+mu-
 03051313 4233   00   2     1.47954476E-05   # C9' mu+mu-
 03051313 4137   00   2    -1.14462225E-07   # C10 mu+mu-
 03051313 4237   00   2    -1.09736707E-04   # C10' mu+mu-
 03051212 4137   00   2     6.34574742E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     2.37382028E-05   # C11' nu_1 nu_1
 03051414 4137   00   2     6.34578812E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     2.37382028E-05   # C11' nu_2 nu_2
 03051616 4137   00   2     6.35714143E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     2.37382027E-05   # C11' nu_3 nu_3
